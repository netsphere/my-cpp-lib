
template <typename _Char>
void dump(const _Char* str, int max_len)
{
    for (int i = 0; i < max_len; i++) {
        if (str[i] == 0)
            break;
        if (str[i] == 0xa)
            printf("\n");
        else if (str[i] >= 0x20 && str[i] <= 0x7e)
            putchar(str[i]);
        else {
            if constexpr (sizeof(_Char) == 4)
                printf("%06x ", str[i]);   // U+10FFFF まで
            else
                printf("%04x ", str[i]);
        }
    }
    printf("\n");
}
