﻿// -*- coding:utf-8-with-signature -*-

// base64.cpp

// The Base16, Base32, and Base64 Data Encodings (October 2006)
//     Obsoletes RFC 3548 (July 2003).
// https://tools.ietf.org/html/rfc4648

#include "base64.h"
#include <ctype.h>
#include <assert.h>
#include <string.h>
#include <stdio.h>

#ifndef DWORD
  typedef unsigned int DWORD;
  typedef unsigned char BYTE;
#endif


// @return エラーの場合 -1
//         終端のナル文字を含まない, 文字数.
int base64_encode(char* dest, int* destsize, const BYTE* src, int srcsize)
{
    // 6bit分.
    static const char* tbl = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

    assert(src);
    
    if (!destsize)
        return -1;
    if ( !dest || *destsize < (srcsize + 2) / 3 * 4 + 1) {
        *destsize = (srcsize + 2) / 3 * 4 + 1;
        return -1;
    }
    
    char* p = dest;
    int i = 0;
    for (i = 0; i < srcsize; i += 3) {
        // 3バイトをpackする.
        DWORD b = *(src + i) << 16;
        if (i + 1 < srcsize)
            b |= *(src + i + 1) << 8;
        if (i + 2 < srcsize)
            b |= *(src + i + 2);

        // 4文字作る.
        *p++ = tbl[b >> 18];
        *p++ = tbl[(b >> 12) & 0x3f];
        *p++ = tbl[(b >> 6) & 0x3f];
        *p++ = tbl[b & 0x3f];
    }

    if ( (srcsize % 3) == 1 )
        *(p - 1) = *(p - 2) = '=';
    else if ( (srcsize % 3) == 2 )
        *(p - 1) = '=';

    *p = '\0';

    return p - dest;
}


int base64url_encode(char* dest, int* destsize, const BYTE* src, int srcsize)
{
    // 6bit分.
    static const char* tbl = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_";

    assert(src);

    if (!destsize)
        return -1;

    int needsize = srcsize / 3 * 4 +
                   ( (srcsize % 3) == 1 ? 2 : ((srcsize % 3) == 2 ? 3 : 0) );
    if ( !dest || *destsize < needsize + 1) {
        *destsize = needsize + 1;
        return -1;
    }
    
    char* p = dest;
    int i = 0;
    for (i = 0; i < srcsize; i += 3) {
        // 3バイトをpackする.
        DWORD b = *(src + i) << 16;
        if (i + 1 < srcsize)
            b |= *(src + i + 1) << 8;
        if (i + 2 < srcsize)
            b |= *(src + i + 2);

        // 4文字作る.
        *p++ = tbl[b >> 18];
        *p++ = tbl[(b >> 12) & 0x3f];
        if (p - dest < needsize)
            *p++ = tbl[(b >> 6) & 0x3f];
        if (p - dest < needsize)
            *p++ = tbl[b & 0x3f];
    }

    // padding しない.
    *p = '\0';

    return p - dest;
}


// [ \t]*\r?\n[ \t]* を読みとばす.
static int skip_ws(const char* str, int srcleft)
{
    assert(str);
    if (srcleft <= 0)
        return 0;
    
    const char* p = str;
    
    while ( isblank(*p) )
        p++;
    if (*p == '\r') {
        if (*++p == '\n')
            p++;
    }
    else if (*p == '\n')
        p++;

    while ( isblank(*p) )
        p++;

    return p - str;
}


// エラーチェックを兼ねる.
// TODO: table[128] を作り, char => int を一撃で引けるようにする.
static int one_char(const char** str, int srcleft) 
{
    if (srcleft <= 0)
        return -1;
    
    char c = **str;
    int value = -1;
    
    if (c >= 'A' && c <= 'Z')
        value = c - 'A';
    else if (c >= 'a' && c <= 'z')
        value = c - 'a' + 26;
    else if (c >= '0' && c <= '9')
        value = c - '0' + 52;
    else if (c == '+' || c == '-')
        value = 62;
    else if (c == '/' || c == '_')
        value = 63;

    if (value >= 0 && value <= 63) {
        (*str)++;
        *str += skip_ws(*str, srcleft - 1);
        return value;
    }
    
    return -1;
}


/**
 * base64 or base64url をデコードする.
 * 不正な文字があった場合は, その直前までで止まる.
 * @return     出力 (バイナリ) のバイト数
 *             失敗の場合は -1
 * @param dest    nullptr の場合, *destsize に必要なバイト数を戻す.
 *                大きさが足りなかった場合も同様.
 * @param srcsize 入力文字列の, 終端のナル文字を含まない長さ。
 *                -1 または nullptr の場合は, ナル終端文字列の全部.
 *                nullptr でない場合, 読み込んだバイト数を書き戻す.
 */
int base64_decode(BYTE* dest, int* destsize, const char* src, int* srcsize)
{
    assert(src);

    // src は改行を含んでもよい. => srcsize から計算で destsize は出せない.
    if (!destsize)
        return -1;

    int insize = (!srcsize || *srcsize < 0) ? strlen(src) : *srcsize;
/*
            +--first octet--+-second octet--+--third octet--+
            |7 6 5 4 3 2 1 0|7 6 5 4 3 2 1 0|7 6 5 4 3 2 1 0|
            +-----------+---+-------+-------+---+-----------+
            |5 4 3 2 1 0|5 4 3 2 1 0|5 4 3 2 1 0|5 4 3 2 1 0|
            +--1.index--+--2.index--+--3.index--+--4.index--+
*/
    const char* s = src;
    BYTE* t = dest;
    while (s - src < insize) {
        int c;
        
        // 4文字 => 3バイト
        if ( (c = one_char(&s, insize - (s - src))) < 0 )
            break;
        DWORD b = (c << 18);

        if ( (c = one_char(&s, insize - (s - src))) < 0 ) {
            // ここは切れ目になりえない.
            return -1;
        }
        b |= (c << 12);

        if ( (c = one_char(&s, insize - (s - src))) < 0 ) {
            if (*s == '=') {
                s++;
                if (*s == '=')
                    s++;
            }
            if (dest && t < dest + *destsize)
                *t++ = (b >> 16);  // first octetのみ.
            else
                t++;
            break;
        }
        b |= (c << 6);

        if ( (c = one_char(&s, insize - (s - src))) < 0 ) {
            if (*s == '=')
                s++;
            if (dest && t + 1 < dest + *destsize) {
                *t++ = (b >> 16);
                *t++ = (b >> 8) & 0xff;
            }
            else
                t += 2;
            break;
        }
        b |= c;

        if (dest && t + 2 < dest + *destsize) {
            *t++ = (b >> 16);
            *t++ = (b >> 8) & 0xff;
            *t++ = b & 0xff;
        }
        else
            t += 3;
    }

    if ( !dest || t > dest + *destsize ) {
        if (destsize)
            *destsize = t - dest;
        return -1;
    }

    if (srcsize)
        *srcsize = s - src;
    return t - dest;
}


// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
