// -*- mode:c++ -*-

#ifndef QSLIB_UTF8_IFILTER_H
#define QSLIB_UTF8_IFILTER_H 1

#include <streambuf>
#include <istream>
#include <assert.h>

#ifndef _WIN32
typedef unsigned char BYTE;
#endif

// UTF-8で保存されたストリームから読みとる。
// wchar_t は廃れた! (環境によって大きさが異なり実用ではない)
// istream は派生するようにできていない。メソッドが virtual になっていない。
//   -> streambuf から派生させる
class Utf8InputFilter: public std::basic_streambuf<char16_t>
{
    typedef std::basic_streambuf<char16_t>  super;
    typedef typename super::int_type     int_type;
    typedef typename super::char_type    char_type;
    typedef typename super::traits_type  traits_type;

public:
    Utf8InputFilter(std::istream* is): m_is(is) {
        assert(is);
        m_inbuf[0] = '\0';
    }

    virtual ~Utf8InputFilter() { }

protected:
    // @override
    // バッファに読み込む.
    virtual int_type underflow();

private:
    std::istream* m_is;   // 多バイト版. 所有しない.

    BYTE m_inbuf[4000];
    char_type m_outbuf[sizeof(m_inbuf) * 2]; // 全部がサロゲートペアになる場合.
};


#if 0
class UtfOutputFilter: protected wostream
{
public:
    UtfOutputFilter(ostream* os = NULL): os_(os) { }
    virtual ~UtfOutputFilter() { }
    void attach(ostream* os) { os_ = os; }

private:
    ostream* os_;
};

class KanjiInputFilter: protected istream
{
public:
    KanjiInputFilter(istream* is = NULL): is_(is) { }
    virtual ~KanjiInputFilter() { }

    void attach(istream* is) { is_ = is; }

    KanjiInputFilter& read(char* buf, int count)
    {
        assert(is_);
        is_->read(buf, count);    これは何がしたいの?? 付加価値なさそうだけど
        return *this;
    }

private:
    istream* is_;
};

class KanjiOutputFilter: protected ostream
{
    KanjiOutputFilter(ostream* os = NULL): os_(os) { }
    virtual ~KanjiOutputFilter() { }

    void attach(ostream* os) { os_ = os; }
    KanjiOutputFilter& write(const char* buf, int count)
    {
        assert(os_);
        os_->write(buf, count);
        return *this;
    }

private:
    ostream* os_;
};
#endif // 0


#endif  // !QSLIB_UTF8_IFILTER_H
