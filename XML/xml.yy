// -*- mode:c++ -*-

// bison マニュアル
// 10.1 C++ Parsers
// https://www.gnu.org/software/bison/manual/html_node/C_002b_002b-Parsers.html

// Bison 3.2: C++ position.hh, stack.hh が廃止.
%require "3.7"

// xml.tab.hh を出力.
%defines

// 再入可能 (reentrant)
// C++ parser だと書いてはダメ (エラーになる)
%define api.pure full

// Push parser を生成.
// plain C スケルトンのみ. C++ スケルトンではエラーになる.
%define api.push-pull push

// [C++ only] デフォルト namespace yy class parser
// => namespace yy class XmlParser
/* %define parser_class_name {XmlParser} */

// location tracking
// '%define api.pure full’ と %locations の両方を定義すると,
// yylex() の引数として YYLTYPE* llocp が追加される.
%locations

// [C++ only] When variants are used, symbols must be constructed and destroyed
// properly. %union のときは関係なさそう.
/* %define parse.assert */

// 旧 %debug. --debug コマンドオプション.
%define parse.trace

// %error-verbose is obsoleted.
%define parse.error verbose

// yylex() と yyparse() の両方に追加. %lex-param + %parse-param と同じ.
// entity を展開するため, lexer から document の entity list にアクセス.

// 追加の yyparse() の引数.
%parse-param {EntityMap* general_entities}
%parse-param {Document** doc_ptr}

// parser 側から lexer の param_entities に追加する.
%parse-param {class yy::XmlLexer* scanner}
//%parse-param {std::ostream & err}

%code requires {
// xml.tab.hh の冒頭に置かれるセクション.

//この順番ではダメ. => ここでは宣言のみ, にする.
//#include "xml_lexer.h"
namespace yy {
class XmlLexer;
};
 
#include "node.h"
}

%{
// xml.tab.cc の冒頭に出力されるセクション.

#include "xml_lexer.h"
#include <iostream>
using namespace std;

// %parse-param で指定した追加引数が全部来るバージョンが必要。
// YY_("memory exhausted") などは, こちらが呼び出される.
void yyerror(YYLTYPE* locp, EntityMap* ge, Document** doc_ptr, 
             yy::XmlLexer* scanner, const char* str);
void yyerror(const YYLTYPE& locp, const string& str);

// C++ parser では, %parse-param との組み合わせで, yylex() を呼び出す. 
#undef yylex
#define yylex scanner->yylex
%}

// union YYSTYPE 型
// xml.tab.hh にて, class XmlParser union semantic_type として定義される. 
%union {
    char* sval;
  
    NodeList* node_list;

    Node* node;
  
    Element* element;
    
    DocType* doctype;
    Document* document;

    std::pair<DocType*, NodeList*>* doctype_pis;
  
    AttributeList* attribute_list;

    std::pair<std::string*, std::string*>* str_pair;

    std::string* stdstr;
}

// ストリーム終端は 0 固定. YYEOF が自動的に定義される.
//%token END_OF_FILE  0
%token XML_DECL DOCTYPE DTD_END DECL_END SPC EQ SYSTEM PUBLIC NDATA
%token ENTITY_DECL NOTATION_DECL
  //%token WSP
%token <sval> PI_NAME PI_BODY
%token <sval> DECL_NAME DECL_BODY
%token <sval> STAG  ATTNAME
%token <stdstr> ATTVALUE
%token <sval> ETAG
%token '>'
%token PIC NETC
%token <stdstr> ENTITY_REF PE_REF
%token <stdstr> CHAR_DATA T_COMMENT
  //%token <stdstr> CHAR_REF
%token <stdstr> CDATA

// lexical error の場合, 自動定義される YYUNDEF (yytokentype型) を返せばよい.
// C++ parser スケルトンでは, この方式ではなく, yylex() から syntax_error 例外
// を投げればよい.
 //%token LEX_ERROR

%type<document> document

%type<node_list> content markupdecl_0_N intSubset_0_1
%type<node> sub_element markupdecl_or_declsep EntityDecl NotationDecl
%type<stdstr> body_or_attvalue_0_N

%type<element> element
%type<node> pi Misc

%type<doctype> doctypedecl
%type<str_pair> ExternalID

  // 有意なのは PI のみ. 
%type<node_list> Misc_0_N

%type<doctype_pis> doctypedecl_Misc_0_N_0_1

%type<attribute_list> Attribute_0_N  xmldecl_0_1

  /* 必須. 次の場合のみ, 呼び出される;
   *  - エラーリカバリのとき, 先読みしていたトークン.
   *        YYERROR, syntax_error 例外.
   *        right-hand side symbols は自動解放されない.
   *  - YYABORT or YYACCEPT のとき. right-hand side symbols を除く.
   *  - 例外 (ただし syntax_error以外) のとき. right-hand side symbols を含む
   *  - start シンボル
   */
%destructor { free($$); } <sval>
%destructor { delete $$; } <node_list>
%destructor { delete $$; } <node>
%destructor { delete $$; } <element>
%destructor { delete $$; } <doctype>
%destructor { delete $$; } <document>
%destructor { if ($$) { delete $$->first; delete $$->second; }
        delete $$; } <doctype_pis>
%destructor { delete $$; } <attribute_list>
%destructor { if ($$) { delete $$->first; delete $$->second; }
        delete $$; } <str_pair>
%destructor { delete $$; } <stdstr>

%%

  /*
     [1]  document ::= prolog element Misc*
     [22] prolog   ::= XMLDecl? Misc* (doctypedecl Misc*)?
     [27] Misc     ::= Comment | PI | S
   */
document
    : xmldecl_0_1  Misc_0_N  doctypedecl_Misc_0_N_0_1 element Misc_0_N   {
    Attribute* version = nullptr;
    Attribute* encoding = nullptr;
    Attribute* standalone = nullptr;
    if ($1) {
        version = $1->detach("version");
        encoding = $1->detach("encoding");
        standalone = $1->detach("standalone");
        if ($1->size() > 0) {
            delete version; delete encoding; delete standalone; delete $1;
            delete $2;
            if ($3) {
                delete $3->first; delete $3->second; delete $3;
            }
            delete $4; // element
            delete $5;
            yyerror(@1, "xmldecl has unknown attr"); YYERROR;
        }
        delete $1; $1 = nullptr;
    }
    NodeList* children = $2 ? $2 : new NodeList();
    $2 = nullptr;
    DocType* doctype = nullptr;
    if ($3) {
        doctype = $3->first;
        children->push_back(doctype);
        if ($3->second) {
            NodeList::iterator it;
            while ( (it = $3->second->begin()) != $3->second->end() )
                children->push_back( $3->second->detach(it) );
            delete $3->second;
        }
        delete $3;
    }

    // root要素
    if ( doctype && (doctype->name() != $4->name()) ) {
        delete version; delete encoding; delete standalone;
        delete children;
        delete doctype;
        delete $4;
        delete $5;
        yyerror(@4, "root element unmatch"); YYERROR;
    }
    children->push_back($4);
    
    if ($5) {
        NodeList::iterator it;
        while ( (it = $5->begin()) != $5->end() )
            children->push_back( $5->detach(it) );
        delete $5;
    }
    $$ = new Document(version, encoding, standalone, doctype, $4, children);
    *doc_ptr = $$;
    $$ = nullptr;
  }
  ;

xmldecl_0_1
    : XML_DECL Attribute_0_N PIC       {
    $$ = $2;      
    }
  | %empty {
    $$ = nullptr;
     }
    ;

Misc_0_N
  : Misc_0_N Misc     {
    NodeList* ret = $1;
    if ($2 != nullptr) {
        if (!ret)
            ret = new NodeList();
        ret->push_back($2);
    }
    $$ = ret;
  }
    | %empty   {
    $$ = nullptr;
  }
  ;

Misc
  : T_COMMENT     {
    // Canonical Form (commented) だと, イキ.
    $$ = new Comment($1);
    //delete $1;   // stdstr
  }
    | pi {     
    $$ = $1;
  }
  | SPC {
    $$ = nullptr;
    }
  ;

pi
  : PI_NAME PI_BODY
  {
    $$ = new PI($1, $2);
  }
  ;

doctypedecl_Misc_0_N_0_1
  : doctypedecl Misc_0_N  {
    $$ = new std::pair<DocType*, NodeList*>($1, $2);
  }
  | %empty   { $$ = nullptr; }
  ;

doctypedecl
  : DOCTYPE ATTNAME ExternalID intSubset_0_1 DTD_END    {
    // external ID と internal subset は両立する.
    $$ = new DocType($2, $3->first, $3->second, $4);
    delete $3;
  }
  | DOCTYPE ATTNAME intSubset_0_1 DTD_END    {
    $$ = new DocType($2, nullptr, nullptr, $3);
  }
  ;


    /*
       [75] ExternalID  ::=   	'SYSTEM' S SystemLiteral
                              | 'PUBLIC' S PubidLiteral S SystemLiteral
     */
ExternalID :
  SYSTEM ATTVALUE {
    $$ = new pair<string*, string*>(nullptr, $2);
  }
  | PUBLIC ATTVALUE ATTVALUE {
    $$ = new pair<string*, string*>($2, $3);
  }    
  ;

intSubset_0_1 :
  '[' markupdecl_0_N ']' {
    $$ = $2;
  }
  | %empty { $$ = nullptr; }
  ;

markupdecl_0_N :
  markupdecl_0_N markupdecl_or_declsep     {
    NodeList* list = $1;
    if ($2 != nullptr ) {
        if (!list)
            list = new NodeList();   
        list->push_back($2);
    }
    $$ = list;
  }
  | %empty { $$ = nullptr; }
  ;

markupdecl_or_declsep :
  EntityDecl {
    $$ = $1; // NULL のことがある.
  }
  | NotationDecl { $$ = $1; }
  | DECL_NAME body_or_attvalue_0_N DECL_END {
    string name = $1;
    free($1); $1 = nullptr; // sval
    if (name == "ELEMENT" || name == "ATTLIST") {
        // [45] elementdecl	   ::=   	'<!ELEMENT' S Name S contentspec S? '>'	[VC: Unique Element Type Declaration]
        
        // 捨てる.
        delete $2;
        $$ = nullptr;
    }
    else if (name == "NOTATION") {
        // [82] NotationDecl ::= '<!NOTATION' S Name S (ExternalID | PublicID)
        //                       S? '>'	[VC: Unique Notation Name]
        // [83] PublicID     ::= 'PUBLIC' S PubidLiteral
        $$ = new NotationDecl("", $2); // TODO: impl.
    }
    else {
        delete $2;
        yyerror(@1, "unknown declaration '" + name); YYERROR;
    }
  }    
  | pi { $$ = $1; }
  | T_COMMENT {
    delete $1; // stdstr
    $$ = nullptr;  
  }
  /*| PE_REF {
    $$ = nullptr;  // TODO: 展開
    free($1);
    }  */
  ;

    /*
       [70]   	EntityDecl	   ::=   	GEDecl | PEDecl
       [71]   	GEDecl	   ::=   	'<!ENTITY' S Name S EntityDef S? '>'
       [72]   	PEDecl	   ::=   	'<!ENTITY' S '%' S Name S PEDef S? '>'
     */
EntityDecl :
  ENTITY_DECL ATTNAME ATTVALUE DECL_END {
    // 先に定義したほうが優先. エラーではない. See xmltest/valid/sa/086.xml 
    auto r = general_entities->insert(make_pair(string($2), *$3));
    if (r.second == false) {
        free($2);
        delete $3; // stdstr
        $$ = nullptr; //YYERROR;  // parse() が非0を返す.
    }
    else {
        if ( $3 && $3->length() > Entity::max_size() ) {
            free($2);
            delete $3;
            yyerror(@3, "Entity declaration: too long body"); YYERROR;
        }
        $$ = new Entity($2, $3);
        free($2); // sval
    }
  }
  | ENTITY_DECL ATTNAME ExternalID DECL_END {
    // skip. TODO: impl.
    free($2);
    delete $3->first; delete $3->second; delete $3;
    $$ = nullptr;
  }
  | ENTITY_DECL ATTNAME ExternalID NDATA ATTNAME DECL_END {
    // <!NOTATION n SYSTEM "http://www.w3.org/">
    // <!ENTITY e SYSTEM "http://www.w3.org/" NDATA n>
    free($2); // sval
    delete $3->first; delete $3->second; delete $3;
    free($5); // sval
    $$ = nullptr;
  }
  | ENTITY_DECL '%' ATTNAME ATTVALUE DECL_END {
    string name = $3;
    free($3); // sval
    auto r = scanner->param_entities.insert(make_pair(name, *$4));
    if (r.second == false) {
        delete $4;
        yyerror(@3, "duplicate parameter name '" + name); YYERROR;
    }
    // DOM には追加しない
    delete $4; // stdstr
    $$ = nullptr;
  }
  | ENTITY_DECL '%' ATTNAME ExternalID DECL_END {
    // skip
    free($3);
    delete $4->first; delete $4->second; delete $4;
    $$ = nullptr;
  }
  ;

    /*
       [82]   	NotationDecl	   ::=   	'<!NOTATION' S Name S (ExternalID | PublicID) S? '>'	[VC: Unique Notation Name]
     */
NotationDecl :
  NOTATION_DECL ATTNAME ExternalID DECL_END {
    // skip
    free($2); // sval
    delete $3->first; delete $3->second; delete $3;
    $$ = nullptr;
  }
  | NOTATION_DECL ATTNAME PUBLIC ATTVALUE DECL_END {
    free($2); // sval
    delete $4;
    $$ = nullptr;
  }
  ;


body_or_attvalue_0_N :
  body_or_attvalue_0_N DECL_BODY {
    string* str = $1 ? $1 : new string();
    if (str->size() > 0)
        str->append(string(" ") + $2);
    else
        str->append($2);
    free($2); // sval
    $$ = str;
  }
  | body_or_attvalue_0_N ATTNAME {
    // 手抜き
    assert($2);
    string* str = $1 ? $1 : new string();
    if (str->size() > 0)
        str->append(string(" ") + $2);
    else
        str->append($2);
    free($2); // sval
    $$ = str;
  }
  | body_or_attvalue_0_N ATTVALUE {
    // 手抜き
    assert($2);
    string* str = $1 ? $1 : new string();
    str->append(string("\"") + *$2 + "\"");
    delete $2; // stdstr
    $$ = str;
  }
  | %empty {
    $$ = nullptr;      
  }
  ;

Attribute_0_N
  : Attribute_0_N ATTNAME EQ ATTVALUE     {
    AttributeList* attlist = $1 ? $1 : new AttributeList();
    $1 = nullptr;
    string name = string($2);
    Attribute* attr = new Attribute($2, $4);
    auto r = attlist->insert( make_pair(name, attr) );
    if ( r.second == false ) {
        // 重複
        delete attlist;
        delete attr;  // $2, $4
        yyerror(@2, string("duplicate attr name '") + name); YYERROR;
    }
    $$ = attlist;
  }
  | Attribute_0_N ATTNAME {
    // 属性名の省略.    
    AttributeList* attlist = $1 ? $1 : new AttributeList();
    Attribute* attr = new Attribute(nullptr, new string($2));
    free($2); // sval
    auto r = attlist->insert(make_pair(string(""), attr));
    if ( r.second == false ) {
        delete attlist;
        delete attr;
        yyerror(@2, "no-name attr multiples."); YYERROR;
    }
    $$ = attlist;
  }
  | %empty   { $$ = nullptr; }
  ;

  /* 開始タグと, 空要素タグ 
   * [39] element    ::=  EmptyElemTag
   *                      | STag content ETag	[WFC: Element Type Match]
   */
element
  : STAG Attribute_0_N NETC       {
    $$ = new Element($1, $2, true, nullptr);
  }
  | STAG Attribute_0_N '>' content ETAG     {
    $$ = new Element($1, $2, false, $4);
    if (string($1) != $5) {
        delete $$;
        string etag = $5;
        free($5);
        yyerror(@5, string("unmatch end tag '") + etag); YYERROR;
    }
    free($5);   // sval
  }
  ;

  /* [43] content ::= CharData? ((element | Reference | CDSect | PI | Comment) 
   *                             CharData?)*
   */
content
  : content sub_element   {
    NodeList* nodelist = $1 ? $1 : new NodeList();
    // TODO: CHAR_DATA が続く場合, 前のと繋げる.
    nodelist->push_back($2);
    $$ = nodelist;
  }
  | %empty    { $$ = nullptr; }
  ;

sub_element
  : CHAR_DATA   { $$ = new Text($1); }
  | element    { $$ = $1; }
//  | STag_or_EmptyElemTag { $$ = $1; }
//  | ETAG { $$ = new EndTag($1); }
  | ENTITY_REF { $$ = new EntityRef($1); }
  | CDATA   { $$ = new CDATASect($1); }
  | pi   { $$ = $1; }
  | T_COMMENT   { $$ = new Comment($1); }
  ;




%%

// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
