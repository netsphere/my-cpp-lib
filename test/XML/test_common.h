
#include <string>
#include <sstream>

// @return 新しく生成した stringstream. 呼出し側が delete すること。
//         If error, nullptr.
extern std::stringstream* new_stringstream_from_file(const char* filename,
                                                     std::string* enc);


/*
'stringstream' は 'istringstream' の派生ではない. どうしてこうなった??
<sstream>
  template<typename _CharT, typename _Traits, typename _Alloc>
    class basic_istringstream : public basic_istream<_CharT, _Traits>

  template <typename _CharT, typename _Traits, typename _Alloc>
    class basic_stringstream : public basic_iostream<_CharT, _Traits>

<istream>
  template<typename _CharT, typename _Traits>
    class basic_iostream
    : public basic_istream<_CharT, _Traits>,
      public basic_ostream<_CharT, _Traits>
*/
