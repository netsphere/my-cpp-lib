

#include "../base64.h"
#include <string.h>
#include <assert.h>
#include <stdio.h>


void test_round(const char* orig, const char* expected)
{
    char b64[1000];
    unsigned char trip[1000];

    int bufsiz = sizeof(b64);

    int r = base64_encode(b64, &bufsiz, (unsigned char*) orig, strlen(orig));
    assert( !strcmp(b64, expected) );
    
    bufsiz = sizeof(trip);
    int srcsiz = -1;
    r = base64_decode(trip, &bufsiz, b64, &srcsiz);
    trip[r] = 0;
    if ( strcmp(orig, (char*) trip) ) {
        printf("failed: r = %d, orig = %s, b64 = %s, trip = %s\n", r, orig, b64, trip);
    }
}


void test_base64url(const char* orig, const char* expected)
{
    char b64[1000];
    unsigned char trip[1000];

    int bufsiz = sizeof(b64);

    int r = base64url_encode(b64, &bufsiz, (unsigned char*) orig, strlen(orig));
    assert( !strcmp(b64, expected) );
    
    bufsiz = sizeof(trip);
    int srcsiz = -1;
    r = base64_decode(trip, &bufsiz, b64, &srcsiz);
    trip[r] = 0;
    if ( strcmp(orig, (char*) trip) ) {
        printf("failed: r = %d, orig = %s, b64 = %s, trip = %s\n", r, orig, b64, trip);
    }
}


/*
            +--first octet--+-second octet--+--third octet--+
            |7 6 5 4 3 2 1 0|7 6 5 4 3 2 1 0|7 6 5 4 3 2 1 0|
            +-----------+---+-------+-------+---+-----------+
            |5 4 3 2 1 0|5 4 3 2 1 0|5 4 3 2 1 0|5 4 3 2 1 0|
            +--1.index--+--2.index--+--3.index--+--4.index--+
*/
int main()
{
    test_round("", "");
    // U+0066
    test_round("f", "Zg==");
    test_round("fo", "Zm8=");
    test_round("foo", "Zm9v");
    test_round("foob", "Zm9vYg==");
    test_round("fooba", "Zm9vYmE=");
    test_round("foobar", "Zm9vYmFy");
    test_round("f\x0f\xf0", "Zg/w");
    
    test_base64url("", "");
    test_base64url("f", "Zg");
    test_base64url("fo", "Zm8");
    test_base64url("foo", "Zm9v");
    test_base64url("foob", "Zm9vYg");
    test_base64url("fooba", "Zm9vYmE");
    test_base64url("foobar", "Zm9vYmFy");
    test_base64url("f\x0f\xf0", "Zg_w");  // 63 = '_'
    
    return 0;
}


// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
