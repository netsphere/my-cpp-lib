﻿// -*- coding:utf-8-with-signature; mode:c++ -*-

#ifndef HTTP_COMMON_H
#define HTTP_COMMON_H 1

#ifdef _WIN32
  #define STRICT
  #define WIN32_LEAN_AND_MEAN
  #include <winsock2.h>
  #include <windows.h>
#else
  //typedef int SOCKET;
  //typedef unsigned char BYTE;
#endif
#include "socket_streambuf.h"
#include <string>
#include <map>

#define FRAME_HDR_LENGTH 9
#define CLIENT_CONNECTION_PREFACE "PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n"


// HTTP/2 フレーム単位での読み込み.
class Http2Reader
{
    typedef void (*Func)(const char* buf, int payload_length);
    
    std::streambuf* m_conn;
  
    char* m_framebuf;
    int m_bufsize;

    Func m_func;
  
public:
    Http2Reader() : m_conn(nullptr), m_framebuf(nullptr), m_bufsize(0) { }

    virtual ~Http2Reader()
    {
        if (m_framebuf) {
            free(m_framebuf);
            m_framebuf = nullptr;
            m_bufsize = 0;
        }
    }
    
    void add_listener( Func func );

    void attach(std::streambuf* conn);

    // フレーム (バイナリデータ)
    char* read_frame();

    // 行
    std::string getline();
};


int get_error();

const char* ntoh3( const char* p, int* n );

int send_HEADERS_frame( std::streambuf* conn, int stream_id,
                        const std::map<std::string, std::string>& m );


#define dump_frame_header(send_or_recv, p) dump_frame_header_(__func__, __LINE__, send_or_recv, p)

void dump_frame_header_( const char* func, int lineno,
                         char send_or_recv, const char* p );

int dump_HEADERS_payload( const char* payload, int payload_length );

void recv_streams(Http2Reader& reader);


#endif // !HTTP_COMMON_H


// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
