
// See https://www.javainthebox.net/laboratory/JavaSE6/stax/stax.html

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javax.xml.stream.*;
import javax.xml.stream.events.*;
import java.util.*;
import java.util.stream.StreamSupport;

public class StAXSample3
{
    private static final Map<Integer, String> Constants;
    static {
/* entity reference &ent1; の取扱い:
   XMLInputFactory.IS_REPLACING_ENTITY_REFERENCES = true (既定値)
      自動的に展開する。展開後の CHARACTERS, START_ELEMENTなどを発生.
      XMLデータ中にエンティティ参照があり, それが定義されていないときは 
      javax.xml.stream.XMLStreamException 例外を発生.

      展開した内容が長すぎるときは javax.xml.stream.XMLStreamException 例外
      XML Entity Expansion Attack (XML Bomb, Billion Laughs Attack)

      IS_SUPPORTING_EXTERNAL_ENTITIES = true (既定値)
         外部エンティティ <!ENTITY ent2 SYSTEM "world.txt"> も展開する。
         ファイルが見つからないときは, javax.xml.stream.XMLStreamException
         *重要* セキュリティホールになるので、パースするXMLファイルが無制限に信
                頼できる場合を除き, 有効にしてはならない。
         => XML External Entity Injection (XXE). 
            <!ENTITY xxe SYSTEM "file:///dev/random" >
            <!ENTITY xxe SYSTEM "file:///etc/passwd" >
      IS_SUPPORTING_EXTERNAL_ENTITIES = false
         エンティティ参照は、無視 (削除) される。イベントは発生しない。

   XMLInputFactory.IS_REPLACING_ENTITY_REFERENCES = false
      展開しない。ENTITY_REFERENCE イベントを発生する
      ただしこの場合でも、属性値のなかのエンティティ参照は、展開する (!)
      => 動作が首尾一貫していない

      IS_SUPPORTING_EXTERNAL_ENTITIES = true
         外部エンティティ (のみ), 展開する。マジか!
      IS_SUPPORTING_EXTERNAL_ENTITIES = false
         内部エンティティと同様, ENTITY_REFERENCE イベントを発生する
 */
        Map<Integer, String> map = new LinkedHashMap<>();
        map.put(XMLStreamConstants.ATTRIBUTE, "ATTRIBUTE");
            // Attribute が独立したイベントとして来ることはない.
            // StartElement の一部.
        //map.put(XMLStreamConstants.CDATA, "CDATA");  これは来ない
        // Canonical Form では展開される
        
        map.put(XMLStreamConstants.CHARACTERS, "CHARACTERS");
            // &#10; &#x32; は、独立したイベントになる
            //   => 文字に変換される.
            // CDATA セクションも、中身の文字列だけになる.

        map.put(XMLStreamConstants.COMMENT, "COMMENT");
        map.put(XMLStreamConstants.DTD, "DTD");
            // DTD 全部で一つのイベント
        map.put(XMLStreamConstants.END_DOCUMENT, "END_DOCUMENT");
            // root 要素の終了タグ, コメントそのほかの後ろ, とにかくストリーム
            // の最後に来る.
        map.put(XMLStreamConstants.END_ELEMENT, "END_ELEMENT");
            // 空要素タグは、StartElement + EndElement と二つのイベントになる
            // 意外な挙動。しかし、これで Canonical Form になる.
        map.put(XMLStreamConstants.ENTITY_DECLARATION, "ENTITY_DECLARATION");
            // 独立したイベントとしては来ない. DTD の一部
        map.put(XMLStreamConstants.ENTITY_REFERENCE, "ENTITY_REFERENCE");
            // エンティティ参照がイベントとして来るかは, 設定による。

        map.put(XMLStreamConstants.NAMESPACE, "NAMESPACE");
            // 独立したイベントとしては来ない。StartElement の一部
        map.put(XMLStreamConstants.NOTATION_DECLARATION, "NOTATION_DECLARATION");
            // 独立したイベントとしては来ない. DTDの一部
        map.put(XMLStreamConstants.PROCESSING_INSTRUCTION, "PROCESSING_INSTRUCTION");
        //map.put(XMLStreamConstants.SPACE, "SPACE");   これは来ない
        map.put(XMLStreamConstants.START_DOCUMENT, "START_DOCUMENT");
            // DTDの前に来る.
        map.put(XMLStreamConstants.START_ELEMENT, "START_ELEMENT");
            // ATTLIST で宣言したデフォルト属性値も含まれる。
            // => DTDを解釈し、そのように動いている.
        Constants = Collections.unmodifiableMap(map);
    }


    private static String attrs_tostr(Iterator<Attribute> attrs) {
        Iterable<Attribute> iterable = () -> attrs;
        return StreamSupport.stream(iterable.spliterator(), false)
            .reduce("", (left, attr) -> left + " " +
                                      (attr.getName() + "=" + attr.getValue()),
                    (l,r) -> l + " " + r);
    }

    
    public StAXSample3(String xmlfile)
                throws FileNotFoundException, XMLStreamException
    {
        XMLInputFactory factory = XMLInputFactory.newInstance();
        // 必須. セキュリティホールになる. 必ず false にすること。
        factory.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES,
                            false);
        // こちらは false でも展開されてしまう。true か。
        factory.setProperty(XMLInputFactory.IS_REPLACING_ENTITY_REFERENCES,
                            true);
        BufferedInputStream stream = new BufferedInputStream(
                                             new FileInputStream(xmlfile));
        XMLEventReader reader = factory.createXMLEventReader(stream);

        while (reader.hasNext()) {
            XMLEvent event = reader.nextEvent();
            switch (event.getEventType())
            {
            case XMLStreamConstants.START_ELEMENT:
                StartElement element = (StartElement) event;
                System.out.println("START_ELEMENT " + element.getName() + " " +
                                   attrs_tostr(element.getAttributes()) );
                break;
            case XMLStreamConstants.END_ELEMENT:
                EndElement end_elem = (EndElement) event;
                System.out.println("END_ELEMENT " + end_elem.getName());
                break;
            case XMLStreamConstants.CHARACTERS:
                Characters c = (Characters) event;  // CDATAも同じ.
                System.out.println("CHARACTERS " + c.getData());
                break;
            case XMLStreamConstants.DTD:
                DTD dtd = (DTD) event;
                System.out.println("DTD " + dtd.getDocumentTypeDeclaration());
                break;
            default:
                System.out.println(Constants.get(event.getEventType()) );
                break;
            }
        }

        reader.close();
    }

    public static void main(String[] args)
                     throws FileNotFoundException, XMLStreamException
    {
        new StAXSample3(args[0]);
    }
}
