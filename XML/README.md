﻿# XML DOM parser

Bison 3.0 / Flex の習作. 実用性はありません。

 - Bison 3.0 対応.
 - <s>Bison C++ parser.</s> 再入可能. Push parser (パーサの呼び出しごとに新しいトークンを得る).
 - Flex も C++ モード.
 - 位置 (location) 情報
 - エラーハンドリング
 - C++ での <code>yylex()</code> の呼び出し方
 - [lex] "stack" start conditions の正しい使い方.

XML DTD まで, それなりに構文解析する. ただし, DTD から XML 外部実体 (XML External Entity; XXE) は参照しない。XXE Injection 避け.



## Topic: Push parser

Bison の push parser については、Web上に事例が非常にすくない。その点では, このサンプルが役に立つのではないか.

C++ モードでは使えず, plain C かつ `yacc.c` スケルトンでしか有効にできない。曖昧な文法を扱える `glr.c` スケルトンでもサポートされない。なので、C++ モードのように構文エラー時に例外を投げるようには書けない。

`xml.yy` ファイルで次のように指定。

<pre>
// 再入可能
%define api.pure full

// Push parser
%define api.push-pull push
</pre>

構文解析をおこなう `yypush_parse()` を呼び出す部分を抜き出すと、次のようになる。

```cpp
    yy::XmlLexer scanner(iss, &general_entities);
    iss = nullptr; // 所有権を scanner に渡す
    
    int status;
    YYLTYPE location { .first_line = 1, .first_column = 0, .last_line = 1 };
    yypstate* ps = yypstate_new();
    do {
        YYSTYPE value;
        yytokentype t = scanner.yylex(&value, &location);
        // 正常に parse できたときは 0, エラーの時は 1
        status = yypush_parse(ps,   // yypstate *ps
                        t,          // int pushed_char
                        &value,     // YYSTYPE const *pushed_val
                        &location,  // YYLTYPE *pushed_loc
                        &general_entities, // EntityMap* general_entities,
                        &doc_ptr,   // Document** doc_ptr,
                        &scanner);  // parser 側から lexer.param_entities に追加
    } while (status == YYPUSH_MORE);
    yypstate_delete(ps);
```



## Topic: 指数エンティティ膨張攻撃 (Exponential Entity Expansion; XEE) or Billion laughs attack

XXE と XEE は別物. 
DTD の実体宣言 (Entity Declaration) 内に別の実体への実体参照を記述することができる。例えば, 次のようなファイル:

```xml
<?xml version="1.0"?>
<!DOCTYPE lolz [
 <!ENTITY lol "lol">
 <!ELEMENT lolz (#PCDATA)>
 <!ENTITY lol1 "&lol;&lol;&lol;&lol;&lol;&lol;&lol;&lol;&lol;&lol;">
 <!ENTITY lol2 "&lol1;&lol1;&lol1;&lol1;&lol1;&lol1;&lol1;&lol1;&lol1;&lol1;">
 <!ENTITY lol3 "&lol2;&lol2;&lol2;&lol2;&lol2;&lol2;&lol2;&lol2;&lol2;&lol2;">
 <!ENTITY lol4 "&lol3;&lol3;&lol3;&lol3;&lol3;&lol3;&lol3;&lol3;&lol3;&lol3;">
 <!ENTITY lol5 "&lol4;&lol4;&lol4;&lol4;&lol4;&lol4;&lol4;&lol4;&lol4;&lol4;">
 <!ENTITY lol6 "&lol5;&lol5;&lol5;&lol5;&lol5;&lol5;&lol5;&lol5;&lol5;&lol5;">
 <!ENTITY lol7 "&lol6;&lol6;&lol6;&lol6;&lol6;&lol6;&lol6;&lol6;&lol6;&lol6;">
 <!ENTITY lol8 "&lol7;&lol7;&lol7;&lol7;&lol7;&lol7;&lol7;&lol7;&lol7;&lol7;">
 <!ENTITY lol9 "&lol8;&lol8;&lol8;&lol8;&lol8;&lol8;&lol8;&lol8;&lol8;&lol8;">
]>
<lolz>&lol9;</lolz>
```

GNU Emacs 28.1 (Fedora 36) で開くとメモリを食いつぶす。意外と穴になる。実体参照を展開するときに一定の長さを超えたら打ち切りが必要。




## TODO

 - <s>xmltest/not-wf/sa/006.xml: should reject but succeeded.</s> 済み
 - <s>XEE対策.</s> 2022.9 済み.


