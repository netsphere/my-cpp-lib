// Q's C++ Library
// Copyright (c) 1998-1999 Hisashi HORIKAWA. All rights reserved.

#include <assert.h>
#include <X11/Xlib.h>

#include <gtk/gtk.h>
#include <gdk/gdkx.h>

#include "../window.h"

//////////////////////////////////////////////////////////////////////
// CWindow

CWindow::CWindow(): window(NULL), to_allocate(0)
{
}

CWindow::~CWindow()
{
    // destroy();
        // 1999.05.12 デストラクタはメインループの外で呼ばれることがある。
}

int CWindow::setVisible(bool visible)
{
    assert(window);
    GtkWidget* w = GTK_WIDGET(window);

    if (GTK_WIDGET_VISIBLE(w) == visible)
        return 0;

    if (visible) {
        gtk_widget_show(w);
        if (to_allocate == 1)
            setLocation(CPoint(alloc.x, alloc.y));
        else if (to_allocate == 2)
            setSize(alloc.width, alloc.height);
        else if (to_allocate == 3)
            setBounds(CRect(alloc.x, alloc.y, alloc.width, alloc.height));
    }
    else
        gtk_widget_hide(GTK_WIDGET(window));

    to_allocate = 0;
    return 0;
}

bool CWindow::isVisible() const
{
    return window && GTK_WIDGET_VISIBLE(window);
}

GtkWidget* CWindow::getWidget() const
{
    assert(window);
    return GTK_WIDGET(window);
}

void CWindow::setTitle(const string& title)
{
    if (window)
        gtk_window_set_title(window, title.c_str());
}

void CWindow::add(GtkWidget* child)
{
    if (window)
        gtk_container_add(GTK_CONTAINER(window), child);
}

void CWindow::destroy()
{
    if (window) {
        gtk_widget_destroy(GTK_WIDGET(window));
        window = 0;
    }
}

CPoint CWindow::getLocation() const
    // トップレベルウィンドウに対してしか使ってはならない
    //      XQueryTree()でトップレベルか調べれるが，手を抜いている
{
    assert(window);
    Display* disp = GDK_DISPLAY();
    Window w = GDK_WINDOW_XWINDOW(GTK_WIDGET(window)->window);

    Window root;
    int gx, gy;
    unsigned int width, height, border, depth;
    XGetGeometry(disp, w, &root, &gx, &gy, &width, &height, &border, &depth);

    int wx, wy;
    Window c;
    XTranslateCoordinates(disp, w, root, 0, 0, &wx, &wy, &c);
    if (wx == gx && wy == gy) {
        // ウィンドウマネージャによるデコレーションはない
        return CPoint(wx, wy);
    }
    else {
        return CPoint(wx - gx, wy - gy);
    }
}

void CWindow::setLocation(const CPoint& pt)
{
    if (window) {
        if (isVisible())
            gdk_window_move(GTK_WIDGET(window)->window, pt.x, pt.y);
        else {
            alloc.x = pt.x;
            alloc.y = pt.y;
            to_allocate |= 1;
        }
    }
}

void CWindow::setSize(int width, int height)
{
    if (window) {
        if (isVisible())
            gdk_window_resize(GTK_WIDGET(window)->window, width, height);
        else {
            alloc.width = width;
            alloc.height = height;
            to_allocate |= 2;
        }
    }
}

void CWindow::setBounds(const CRect& r)
{
    if (window) {
        if (isVisible()) {
            gdk_window_move_resize(GTK_WIDGET(window)->window,
                r.x, r.y, r.width, r.height);
        }
        else {
            alloc.x = r.x;
            alloc.y = r.y;
            alloc.width = r.width;
            alloc.height = r.height;
            to_allocate |= 3;
        }
    }
}

//////////////////////////////////////////////////////////////////////
// PopupWindow

PopupWindow::PopupWindow()
{
}

PopupWindow::~PopupWindow()
{
}

int PopupWindow::setVisible(bool visible)
{
    return super::setVisible(visible);
}

