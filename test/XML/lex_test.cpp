
#include "xml_lexer.h"
#include "xml.tab.hh"
#include "test_common.h" // new_stringstream_from_file

#ifdef _MSC_VER
  // mingw は除外.
  #define strncasecmp _strnicmp
#else
  #include <strings.h>
#endif

#ifndef _WIN32
  #include <dirent.h> // scandir()
#endif

using namespace std;
using namespace yy;


struct M { yytokentype t; const char* s; int texttype; };

static const M tname[] = {
    {yytokentype::YYEOF, "END_OF_FILE"},
    {yytokentype::XML_DECL, "XML_DECL", false},

    // DTD
    {yytokentype::DOCTYPE, "DOCTYPE", false},
    {yytokentype::DTD_END, "DTD_END", false},
    {yytokentype::DECL_END, "DECL_END", false},
    {yytokentype::SYSTEM, "SYSTEM", false},
    {yytokentype::PUBLIC, "PUBLIC", false},
    {yytokentype::ENTITY_DECL, "ENTITY_DECL", 0},
    {yytokentype::NOTATION_DECL, "NOTATION_DECL", 0},
    {yytokentype::DECL_NAME, "DECL_NAME", true},  // その他
    {yytokentype::DECL_BODY, "DECL_BODY", true},
    {yytokentype::PE_REF, "PE_REF", 2},

    {yytokentype::SPC, "SPC", false},
    {yytokentype::EQ, "EQ", false},
    {yytokentype::PI_NAME, "PI_NAME", true},
    {yytokentype::PI_BODY, "PI_BODY", true},
    {yytokentype::PIC, "PIC", false},  // "?>"

    {yytokentype::STAG, "STAG", true},
    {yytokentype::ATTNAME, "ATTNAME", true},
    {yytokentype::ATTVALUE, "ATTVALUE", 2},
    {yytokentype::ETAG, "ETAG", true},
    {yytokentype::NETC, "NETC", false}, // "/>"
    {yytokentype::CHAR_DATA, "CHAR_DATA", 2},
    {yytokentype::ENTITY_REF, "ENTITY_REF", 2},
    //{yytokentype::CHAR_REF, "CHAR_REF", true},
    {yytokentype::CDATA, "CDATA", 2},
    {yytokentype::T_COMMENT, "COMMENT", 2},

    //{yytokentype::LEX_ERROR, "LEX_ERROR", false},
    { (yytokentype) -1, nullptr}
};


static const M* lookup( yytokentype t ) {
    const M* p;
    for (p = tname; p->s; p++) {
        if (p->t == t)
            return p;
    }
    return nullptr;
}


static void print_token( const XmlLexer& lex,
                         yytokentype t,         // pushed_char
                         const YYSTYPE& value,  // pushed_val
                         const YYLTYPE& locp )  // pushed_loc
{
    printf("%d:%d: ", locp.first_line, locp.first_column);
    const M* p = lookup(t);
    if (p) {
        printf("token = %s, ", p->s);
        if (p->texttype == 1)
            printf("sval = '%s'\n", value.sval );
        else if (p->texttype == 2)
            printf("stdstr = '%s'\n", value.stdstr->c_str() );
        else
            printf("yytext = '%s'\n", lex.YYText() );
    }
    else {
        // '[' など1文字トークン
        printf("token = %d, yytext = '%s'\n", t, lex.YYText() );
    }
    if ( t < 0 || t == yytokentype::YYUNDEF ) // 字句解析エラー
        abort();
}


// @return 字句解析中にエラーが発生したら -1
int check_lexer(const string& dir, const char* filename )
{
    int result = 0;

    EntityMap general_entities;
    string enc = "";
    stringstream* iss =
            new_stringstream_from_file( (dir + "/" + filename).c_str(), &enc );
    printf("enc = %s\n", enc.c_str());
    if (enc != "UTF-8") {
        printf("UTF-8 only supported, skip.\n");
        delete iss;
        return 0;
    }

    // iss の所有権を渡す。
    XmlLexer lex(iss, &general_entities);
    iss = nullptr; // delete しない.

    // DEBUG
    general_entities.insert(make_pair("ent1", "Hello"));

    yytokentype t;
    YYSTYPE value;
    YYLTYPE location { .first_line = 1, .first_column = 0, .last_line = 1 };
    while (true) {
        try {
            t = lex.yylex(&value, &location);
            if (!t)
                break; // EOF

            print_token(lex, t, value, location);  // DEBUG

            const M* p = lookup(t);
            if (p) {
                if (p->texttype == 1)
                    free(value.sval);
                else if (p->texttype == 2)
                    delete value.stdstr;
            }
        }
        catch (exception& err) {
            result = -1;
            break;
        }
    }

    return result;
}


int check_dir(const string& dir)
{
    struct dirent** namelist = nullptr;
    int num = ::scandir(dir.c_str(), &namelist,
                        [](const struct dirent* ent)->int {
                            if (string(ent->d_name).find(".xml") != string::npos)
                                return true;
                            else
                                return false;
                        }, nullptr);
    if (num < 0) {
        perror("scandir() failed");
        abort();
        //return -1;
    }

    for (int i = 0; i < num; i++) {
        printf("file = %s/%s\n", dir.c_str(), namelist[i]->d_name);
        int r = check_lexer( dir, namelist[i]->d_name );
        if (r < 0) {
            printf("%s/%s: failed.\n", dir.c_str(), namelist[i]->d_name);
            abort();
        }
        free(namelist[i]);
    }
    free(namelist);

    return 0;
}

/**
 * XML parsing test
 * non-validating, external entities ignored.
 *
 * Lexer では, should accept のみを確認する.
 */
int main(int argc, char* argv[])
{
    if (argv[1]) {
        int r = check_lexer(".", argv[1]);
        if ( r < 0 )
            printf("should accept: failed.\n");
        return 0;
    }

    // XML W3C Conformance Test Suite
    //      ext-sa: external entity
    //      not-sa: non-standalone
    //      sa: standalone

    // 1. valid documents: should accept
    check_dir("xmlts20130923/xmltest/valid/ext-sa");
    check_dir("xmlts20130923/xmltest/valid/not-sa");
    check_dir("xmlts20130923/xmltest/valid/sa");

    // 2. invalid documents: lex = should accept
    check_dir("xmlts20130923/xmltest/invalid");
    check_dir("xmlts20130923/xmltest/invalid/not-sa");

    // 3. non-wf documents: 字句解析でエラーと構文解析でエラーの両方がある


    return 0;
}



// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
