﻿// -*- coding:utf-8-with-signature -*-
// Q's C++ Library
// Copyright (c) 2001 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

// エンコーディングを保持する文字列クラス
// ナル文字を含むことができる (UTF-16対策)

iconv() を使ったらポータブルでない。ICU plain C版を使え.

#include "config.h"

#ifndef _WIN32
  #include <iconv.h>
#endif
#include <stdlib.h>
#include "text/EncString.h"
#include <string.h>
using namespace std;


////////////////////////////////////////////////////////////////////////
// EncString

EncString::EncString(): str(NULL), len(-1)
{
}

EncString::~EncString()
{
    free(str);
    str = NULL;
}


EncString::EncString(const EncString& s): str(NULL), len(-1)
{
    if (s.str) {
        str = (unsigned char*) malloc(s.len + 1);
        memcpy(str, s.str, s.len);
        len = s.len;
        str[len] = '\0';
        m_encoding = s.m_encoding;
    }
}

EncString::EncString(const char* s, int len_, const string& enc)
    : str(NULL), len(len_)
{
    if ( !s || len_ < 0 )
        len = -1;
    else {
        str = (unsigned char*) malloc(len + 1);
        memcpy(str, s, len);
        str[len] = '\0';
        m_encoding = enc;
    }
}


EncString& EncString::operator = (const EncString& s)
{
    if (this != &s) {
        free(str);
        if ( !s.str ) {
            str = nullptr;
            len = -1;
        }
        else {
            str = (unsigned char*) malloc(s.len + 1);
            memcpy(str, s.str, s.len);
            len = s.len;
            str[len] = '\0';
            m_encoding = s.m_encoding;
        }
    }
    return *this;
}

#if DEBUG > 1
#include <stdio.h>
static void d_(unsigned char* s, int len) {
    for (int i = 0; i < len; i++)
        printf("%02x ", s[i]);
    printf("\n");
}
#endif


// @return 変換に失敗した場合, -1
int EncString::convert(const string& new_enc)
{
    if ( !str )
        return 0;

    // TODO: ちゃんとBOMを取る
    string ne = strcasecmp(new_enc.c_str(), "UTF-16") ? new_enc : "UTF-16LE";

    iconv_t cd = iconv_open(ne.c_str(), m_encoding.c_str());
    if (cd == (iconv_t) -1)
        return -1;

    size_t bufsiz = len * 6 + 1;
    char* buf = (char*) malloc(bufsiz);

    char* inbuf = (char*) str;
    size_t inbytes = len;
    char* outbuf = buf;
#if DEBUG > 1
        d_(str, len);
#endif

    size_t r = iconv(cd, &inbuf, &inbytes,
                         &outbuf, &bufsiz);
    if (r == (size_t) -1 || inbytes != 0) {
        iconv_close(cd);
        free(buf);
        return -1;
    }
    iconv_close(cd);

    free(str);
    str = (unsigned char*) buf;
    len = outbuf - buf;
    str[len] = '\0';
#if DEBUG > 1
        d_(str, len);
#endif

    m_encoding = new_enc;
    return 0;
}


// 内部文字データを変えずに, エンコーディングだけ変更する.
void EncString::regard(const string& new_enc)
{
    m_encoding = new_enc;
}

const unsigned char* EncString::data() const
{
    return str;
}

int EncString::length() const
{
    return len;
}
