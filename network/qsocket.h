// -*- coding:utf-8; mode:c++ -*-

// Q's C++ Library
// Copyright (c) 1996-2001 HORIKAWA Hisashi. All rights reserved.
//     http://www.nslabs.jp/

#ifndef QLIB_SOCKET__
#define QLIB_SOCKET__

#ifdef _WIN32
  #define STRICT
  #define WIN32_LEAN_AND_MEAN
  #include <winsock2.h>
  #include <windows.h>
  #include <ws2ipdef.h> // Windows Vista以降
  #include <ws2tcpip.h> // struct addrinfo, socklen_t

  #pragma warning(disable: 4290)
  //typedef int ssize_t;
  typedef unsigned int in_addr_t;    // 4バイト固定
  #define sockaddr_in SOCKADDR_IN
  #define sockaddr_in6 SOCKADDR_IN6 
  #define sa_family_t ADDRESS_FAMILY
  //typedef size_t socklen_t;
#else
  #include <netdb.h>  // hostent
  typedef int SOCKET;
#endif  // _WIN32

#include <stdexcept> // std::runtime_error
#include <system_error>
#include <string>


////////////////////////////////////////////////////////////////////////
// SystemCallError

// For backward compatibility. エラーコードのみを保持するエラー class.
class SystemCallError: public std::system_error
{
    int const m_err;

public:
    SystemCallError(int err, const char* msg):
        std::system_error(std::error_code(err, std::system_category()), msg),
        m_err(err)
    { }
    
    virtual ~SystemCallError() noexcept
    { }
    
    int getErrno() const noexcept
    {
        return m_err;
    }
};


////////////////////////////////////////////////////////////////////////
// CSocket

/** ソケットのラッパクラス */
class CSocket
{
    socklen_t addrlen;
  
public:
    SOCKET sock_fd;

    // 先頭に sa_family_t 型の family がある.
    sockaddr_storage addr;
  
    CSocket();
    virtual ~CSocket();

    sa_family_t family() const {
        return ((sockaddr_in*) &addr)->sin_family;
    }
  
    int port() const {
        switch (family())
        {
        case AF_INET:
            return ntohs(((sockaddr_in*) &addr)->sin_port);
        case AF_INET6:
            return ntohs(((sockaddr_in6*) &addr)->sin6_port);
        default:
            return -1;
        }
    }

    /**
     * TCP で listen する. IPv4/IPv6 両対応.
     */
    static CSocket* setupServer(const char* node, int port) noexcept(false);
    
private:
    CSocket(const CSocket& );    // not implement
    CSocket& operator = (const CSocket& ); // not implement
};


////////////////////////////////////////////////////////////////////////
// SocketException

class SocketException: public std::runtime_error
    // java.net.SocketException
{
    int err;
    //std::string msg;

public:
    SocketException(int code, const char* msg);

    virtual ~SocketException() noexcept;
  
    int getCode() const noexcept;
  
    //virtual const char* what() const noexcept;
};


////////////////////////////////////////////////////////////////////////
// InterruptedIOException

// タイムアウトのとき発生
class InterruptedIOException: public std::runtime_error
    // java.io.InterruptedIOException
{
public:
    InterruptedIOException( const char* msg );

    virtual ~InterruptedIOException() noexcept;
};


// @param socktype TCP なら SOCK_STREAM
extern struct addrinfo* ngethostbyname(const char* hostname, int port,
                                       int socktype);

extern ssize_t nrecv(int socket, void* buffer, size_t length, int timeout)
        noexcept(false);


#endif // QLIB_SOCKET__


// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
