﻿// -*- coding:utf-8-with-signature -*-

// $ g++ -std=c++20 -Wall -Wextra ptr_vector-and-ranges_test.cpp

#include <iostream>
//#include <vector>
#include "../ptr_vector.h"
#include <ranges>
#include <algorithm>
using namespace std;

struct Hoge {
    string name;
    Hoge(const string& n): name(n) { }
};

void print(const Hoge* x)
{
    std::cout << x->name << " ";
}

void print_int(int x)
{
    std::cout << x << " ";
}

int main()
{
    // コンテナ
    ptr_vector<Hoge*> v = {new Hoge("a"), new Hoge("b"), new Hoge("c")};
#if __cplusplus >= 202002L
    decltype(v)::iterator first1 = std::ranges::begin(v);
    decltype(v)::iterator last1 = std::ranges::end(v);
#else
    decltype(v)::iterator first1 = std::begin(v);
    decltype(v)::iterator last1 = std::end(v);
#endif

    std::for_each(first1, last1, print);
    std::cout << std::endl;

    // 組み込み配列
    int ar[] = {4, 5, 6};

#if __cplusplus >= 202002L
    int* first2 = std::ranges::begin(ar);
    int* last2 = std::ranges::end(ar);
#else
    int* first2 = std::begin(ar);
    int* last2 = std::end(ar);
#endif

    std::for_each(first2, last2, print_int);

    return 0;
}
