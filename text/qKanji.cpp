﻿// -*- coding:utf-8-with-signature -*-
// codetype.cpp

こっちのほうが実装がよさそう
全角・半角変換表も備える。こっちをイキにするのがよい。

#include <assert.h>
#include <iostream>
#include <vector>
#include <fstream>
using namespace std;

#include "text/qKanji.h"
#include <win/qsDebug.h>


////////////////////////////////////////////////////////////////////////////
// CodeContext

// 全角・半角変換表
uint16_t CodeContext::Queue::norm[] = {
    0xA1, 0x2123, 0xA2, 0x2156, 0xA3, 0x2157, 0xA4, 0x2122, // 半角カナ
    0xA5, 0x2126, 0xA6, 0x2572, 0xA7, 0x2521, 0xA8, 0x2523,
    0xA9, 0x2525, 0xAA, 0x2527, 0xAB, 0x2529, 0xAC, 0x2563,
    0xAD, 0x2565, 0xAE, 0x2567, 0xAF, 0x2543, 0xB0, 0x213c,
    0xB1, 0x2522, 0xB2, 0x2524, 0xB3, 0x2526, 0xB4, 0x2528,
    0xB5, 0x252a, 0xB6, 0x252b, 0xB7, 0x252d, 0xB8, 0x252f,
    0xB9, 0x2531, 0xBA, 0x2533, 0xBB, 0x2535, 0xBC, 0x2537,
    0xBD, 0x2539, 0xBE, 0x253b, 0xBF, 0x253d, 0xC0, 0x253f,
    0xC1, 0x2541, 0xC2, 0x2544, 0xC3, 0x2546, 0xC4, 0x2548,
    0xC5, 0x254a, 0xC6, 0x254b, 0xC7, 0x254c, 0xC8, 0x254d,
    0xC9, 0x254e, 0xCA, 0x254f, 0xCB, 0x2552, 0xCC, 0x2555,
    0xCD, 0x2558, 0xCE, 0x255b, 0xCF, 0x255e, 0xD0, 0x255f,
    0xD1, 0x2560, 0xD2, 0x2561, 0xD3, 0x2562, 0xD4, 0x2564,
    0xD5, 0x2566, 0xD6, 0x2568, 0xD7, 0x2569, 0xD8, 0x256a,
    0xD9, 0x256b, 0xDA, 0x256c, 0xDB, 0x256d, 0xDC, 0x256f,
    0xDD, 0x2573, 0xDE, 0x212b, 0xDF, 0x212c,
    0x2330, 0x30, 0x2331, 0x31, 0x2332, 0x32, 0x2333, 0x33, // 数字
    0x2334, 0x34, 0x2335, 0x35, 0x2336, 0x36, 0x2337, 0x37,
    0x2338, 0x38, 0x2339, 0x39,
    0x2341, 0x41, 0x2342, 0x42, 0x2343, 0x43, 0x2344, 0x44, // 英字
    0x2345, 0x45, 0x2346, 0x46, 0x2347, 0x47, 0x2348, 0x48,
    0x2349, 0x49, 0x234a, 0x4a, 0x234b, 0x4b, 0x234c, 0x4c,
    0x234d, 0x4d, 0x234e, 0x4e, 0x234f, 0x4f, 0x2350, 0x50,
    0x2351, 0x51, 0x2352, 0x52, 0x2353, 0x53, 0x2354, 0x54,
    0x2355, 0x55, 0x2356, 0x56, 0x2357, 0x57, 0x2358, 0x58,
    0x2359, 0x59, 0x235a, 0x5a,
    0x2361, 0x61, 0x2362, 0x62, 0x2363, 0x63, 0x2364, 0x64,
    0x2365, 0x65, 0x2366, 0x66, 0x2367, 0x67, 0x2368, 0x68,
    0x2369, 0x69, 0x236a, 0x6a, 0x236b, 0x6b, 0x236c, 0x6c,
    0x236d, 0x6d, 0x236e, 0x6e, 0x236f, 0x6f, 0x2370, 0x70,
    0x2371, 0x71, 0x2372, 0x72, 0x2373, 0x73, 0x2374, 0x74,
    0x2375, 0x75, 0x2376, 0x76, 0x2377, 0x77, 0x2378, 0x78,
    0x2379, 0x79, 0x237a, 0x7a,
    0, 0
};

CodeContext::Queue::Queue(KanjiFilter* k): kanjiFilter(k), ret_cr(false)
{
}

void CodeContext::Queue::push_char(int c)
{
	if (ret_cr) {
		ret_cr = false;
		if (c == 0x0a)
			return;
	}
	if (c == 0x0d) {	// CR CRLFという順を考えよ。
		push_back(0x0a);
		ret_cr = true;
		return;
	}

    // 半角，全角変換
    if (kanjiFilter->isNormalize()) {
        for (int i = 0; norm[i]; i += 2) {
            if (c == norm[i]) {
                push_back(norm[i + 1]);
                return;
            }
        }
    }
    push_back(c);
}

CodeContext::CodeContext(KanjiFilter* k): error(false), queue(k)
{
}

CodeContext::~CodeContext()
{
}

void CodeContext::clear()
{
    queue.clear();
    error = false;
}

bool CodeContext::isValid() const
{
    return !error;
}

////////////////////////////////////////////////////////////////////////////
// Jis7Context

Jis7Context::Jis7Context(KanjiFilter* k): CodeContext(k), setsize(1), charset(0x42)
{
    buf[0] = 0;
}

Jis7Context::~Jis7Context()
{
}

void Jis7Context::push(int c)
    // JIS7ではG0へ指示するエスケープシーケンスを採用しているから，
    // JIS X 0201半角カナは，ESC 2/8 4/9
{
	assert(c >= 0 && c <= 0xff);

    if (!buf[0]) {
		if (c == 0x0d || c == 0x0a) {
			setsize = 1; charset = 0x42;	// IRV
			queue.push_char(c);
		}
        else if (c == 0x1b) {
            buf[0] = c; buf[1] = 0;
		}
        else {
			if (setsize == 1 && (charset == 0x42 || charset == 0x4a) && c <= 0x7e)
				queue.push_char(c);			// IRV or ラテン
			else if (setsize == 2 && (charset == 0x40 || charset == 0x42)
							&& c >= 0x21 && c <= 0x7e) {
				buf[0] = c; buf[1] = 0;		// 漢字
			}
			else if (setsize == 1 && charset == 0x49 && c >= 0x21 && c <= 0x5f)
				queue.push_char(c | 0x80);	// 半角カナ
			else if (c >= 0x21 && c <= 0x7e) {
				// 知らない文字集合
				if (setsize == 1)
					queue.push_back(0x222e);
				else {
					buf[0] = c; buf[1] = 0;
				}
			}
			else
				error = true;
        }
    }
    else if (!buf[1]) {
        if (buf[0] == 0x1b && (c == 0x28 || c == 0x24))
            buf[1] = c;
        else if (buf[0] >= 0x21 && buf[0] <= 0x7e
                            && c >= 0x21 && c <= 0x7e) {
			if (charset == 0x40 || charset == 0x42)
				queue.push_char((buf[0] << 8) | c);	// 漢字
			else
				queue.push_back(0x222e);	// 知らない文字集合
            buf[0] = 0;
        }
        else
            error = true;
    }
    else {
        if (buf[1] == 0x28) {
			setsize = 1; charset = c;
            buf[0] = 0;
        }
        else if (buf[1] == 0x24) {
			setsize = 2; charset = c;
            buf[0] = 0;
        }
        else
            error = true;
    }
}

int KanjiFilter::jis7_out(char* buf, CodeContext* context)
{
    CodeContext::Queue& queue = context->queue;

    int j = queue.front();
    queue.erase(queue.begin());
    unsigned char k = j >> 8;
    unsigned char t = j & 0xff;
    int count = 0;

    if (!k) {
        if (t >= 0xa1 && t <= 0xdf) {
            // 半角カナ
            if (m_nMbOutMode != 2) {
                *(buf++) = 0x1b;
                *(buf++) = 0x28;
                *(buf++) = 0x49;
                m_nMbOutMode = 2;
                count = 3;
            }
            *buf = t - 0x80;
            return count + 1;
        }
        else {
            // ラテン文字
            if (m_nMbOutMode != 0) {
                *(buf++) = 0x1b;    // kanji-out
                *(buf++) = 0x28;
                *(buf++) = 0x4a;
                m_nMbOutMode  = 0;
                count = 3;
            }
            if (t == 0x0a)
                return count + ret_out(buf);
            else {
                *buf = t;
                return count + 1;
            }
        }
    }
    else {
        if (m_nMbOutMode != 1) {
            *(buf++) = 0x1b;    // kanji-in
            *(buf++) = 0x24;
            *(buf++) = 0x42;
            m_nMbOutMode = 1;
            count = 3;
        }
        *(buf++) = k;
        *(buf++) = t;
        return count + 2;
    }
}

void Jis7Context::clear()
{
	setsize = 1; charset = 0x42;	// IRV
	buf[0] = 0;
    CodeContext::clear();
}

////////////////////////////////////////////////////////////////////////////
// SjisContext

class SjisContext: public CodeContext
    // Shift_JIS (preferred MIME name)
{
    int first;
public:
    SjisContext(KanjiFilter* k);
    virtual ~SjisContext();
    virtual void push(int c);
    virtual void clear();
};

SjisContext::SjisContext(KanjiFilter* k): CodeContext(k), first(0)
{
}

SjisContext::~SjisContext()
{
}

void SjisContext::push(int c)
{
	assert(c >= 0 && c <= 0xff);

    if (!first) {
        if (c == 0x1b)
            error = true;
        else if (c <= 0x7e || c >= 0xa1 && c <= 0xdf)
            queue.push_char(c);
        else if (c >= 0x81 && c <= 0x9f || c >= 0xe0 && c <= 0xef)
                                        // F0〜FC = 新JISだが，EUCとの判定が難しくなる
            first = c;
        else
            error = true;
    }
    else {
        if (c >= 0x40 && c <= 0xfc && c != 0x7f) {
            if (first >= 0xf0 && first <= 0xfc)
                queue.push_back(0x222e);    // 新JISは扱えないので，下駄にする。
            else {
                int k = (first >= 0xe0 ? first - 0xc1 : first - 0x81) * 2 + (c >= 0x9f) + 1;
                int t = ((c >= 0x80 ? c - 0x41 : c - 0x40) % 94) + 1;
                queue.push_char(((k + 0x20) << 8) | (t + 0x20));
            }
            first = 0;
        }
        else
            error = true;
    }
}

int KanjiFilter::sjis_out(char* buf, CodeContext* context)
{
    CodeContext::Queue& queue = context->queue;

    int j = queue.front();
    queue.erase(queue.begin());
    unsigned char k = j >> 8;
    unsigned char t = j & 0xff;

    if (!k) {
        if (t == 0x0a)
            return ret_out(buf);
        else {
            *buf = t;
            return 1;
        }
    }
    else {
        *(buf++) = (k - 33) / 2 + (k <= 94 ? 129 : 193);
        *buf = t + (k % 2 ? (t <= 95 ? 31 : 32) : 126);
        return 2;
    }
}

void SjisContext::clear()
{
    first = 0;
    CodeContext::clear();
}

////////////////////////////////////////////////////////////////////////////
// EucContext

EucContext::EucContext(KanjiFilter* k): CodeContext(k)
{
    buf[0] = 0;
}

EucContext::~EucContext()
{
}

void EucContext::push(int c)
{
	assert(c >= 0 && c <= 0xff);

    if (!buf[0]) {
        if (c == 0x1b)
            error = true;
        else if (c <= 0x7e)
            queue.push_char(c);
        else if (c == 0x8e || c == 0x8f || c >= 0xa1 && c <= 0xfe) {
            buf[0] = c; buf[1] = 0;
        }
        else
            error = true;
    }
    else if (!buf[1]) {
        if (buf[0] == 0x8e && c >= 0xa1 && c <= 0xdf) {
            queue.push_char(c);
            buf[0] = 0;
        }
        else if (buf[0] == 0x8f && c >= 0xa1 && c <= 0xfe)
            buf[1] = c;
        else if (buf[0] >= 0xa1 && buf[0] <= 0xfe
                            && c >= 0xa1 && c <= 0xfe) {
            queue.push_char(((buf[0] & 0x7f) << 8) | (c & 0x7f));
            buf[0] = 0;
        }
        else
            error = true;
    }
    else {
        if (c >= 0xa1 && c <= 0xfe) {
            queue.push_back(0x222e);    // 補助漢字は扱えない
            buf[0] = 0;
        }
        else
            error = true;
    }
}

int KanjiFilter::euc_out(char* buf, CodeContext* context)
{
    CodeContext::Queue& queue = context->queue;

    int j = queue.front();
    queue.erase(queue.begin());
    unsigned char k = j >> 8;
    unsigned char t = j & 0xff;

    if (!k) {
        if (t >= 0xa1 && t <= 0xdf) {
            // 半角カタカナ
            *(buf++) = (char) 0x8e;
            *buf = t;
            return 2;
        }
        else {
            // ASCII
            if (t == 0x0a)
                return ret_out(buf);
            else {
                *buf = t;
                return 1;
            }
        }
    }
    else {
        // 漢字
        *(buf++) = k | 0x80;
        *buf = t | 0x80;
        return 2;
    }
}

void EucContext::clear()
{
    buf[0] = 0;
    CodeContext::clear();
}

////////////////////////////////////////////////////////////////////////////
// KanjiFilter

// 濁音・半濁音変換表
uint16_t KanjiFilter::norm2[] = {
    0x242b, 0x212b, 0x242c, 0x242d, 0x212b, 0x242e,
    0x242f, 0x212b, 0x2430, 0x2431, 0x212b, 0x2432,
    0, 0, 0
};

KanjiFilter::KanjiFilter(): iconv(0), oconv(&KanjiFilter::euc_out),
                            m_ret(CRLF), normalize(false), outbuf_left(0),
                            input_mode(7), m_nMbOutMode(0)
{
    jis7 = new Jis7Context(this);
    sjis = new SjisContext(this);
    euc = new EucContext(this);
}

KanjiFilter::KanjiFilter(istream* in_): InputFilter(in_),
                        iconv(0), oconv(&KanjiFilter::euc_out),
                        m_ret(CRLF), normalize(false), outbuf_left(0),
                        input_mode(7), m_nMbOutMode(0)
{
    jis7 = new Jis7Context(this);
    sjis = new SjisContext(this);
    euc = new EucContext(this);
}

KanjiFilter::~KanjiFilter()
{
    delete jis7;
    delete sjis;
    delete euc;
}

int KanjiFilter::read(char* buf, int size)
    // 入力
    //      size: bufの大きさ（バイト単位）
    // 出力
    //      buf: 変換後バイト列。最大でsize - 1バイト格納する。末尾にナル文字を必ず付加する。
    // 戻り値
    //      bufに格納した，ナル文字を除くバイト数
    // 解説
    //      bufの末端で文字が分断されることがある。
{
    int count = 0;

    while (size > 1) {
        if (outbuf_left) {
            int len = outbuf_left <= size - 1 ? outbuf_left : size - 1;
            memcpy(buf, outbuf, len);
            buf += len;
            count += len;
            size -= len;
            memmove(outbuf, outbuf + len, outbuf_left - len);
            outbuf_left -= len;
        }
        else {
            // 出力バッファが空の時，出力バッファに1文字分溜める。
            // 最終的な出力が文字の区切りと一致しないことがあるので，必ず出力バッファを経由する。
            if (!iconv || iconv->queue.size() < 2) {
                convert();
                convert();
            }
            if (!iconv) {
                // EOFが先に来た。
                if (input_mode & 4)
                    iconv = euc;
                else if (input_mode & 2)
                    iconv = sjis;
                else
                    iconv = jis7;
            }
            if (iconv->queue.size() >= 2 && normalize) {
                // 濁点，半濁点が分離してれば結合する。
                for (int i = 0; norm2[i]; i += 3) {
                    if (iconv->queue[0] == norm2[i] && iconv->queue[1] == norm2[i + 1]) {
                        iconv->queue.front() = norm2[i + 2];
                        iconv->queue.erase(iconv->queue.begin() + 1);
                        break;
                    }
                }
            }
            if (iconv->queue.size()) {
                outbuf_left = (this->*oconv)(outbuf, iconv);
                // iconv->queue.pop_front();    // *_out()でpop_front()してる。
            }
            else
                break;
        }
    }
    *buf = 0;
    return count;
}

int KanjiFilter::getSjisLen(const char* str_)
{
    const unsigned char* str = (unsigned char*) str_;
    if (!str || !*str)
        return 0;
    if ((*str >= 0x81 && *str <= 0x9f || *str >= 0xe0 && *str <= 0xfc)
            && (str[1] >= 0x40 && str[1] <= 0xfc && str[1] != 0x7f))
        return 2;
    return 1;
}

int KanjiFilter::getEucLen(const char* str_)
{
    const unsigned char* str = (unsigned char*) str_;
    if (!str || !*str)
        return 0;
    if ((*str >= 0xa1 && *str <= 0xfe && str[1] >= 0xa1 && str[1] <= 0xfe)
            || (*str == 0x8e && str[1] >= 0xa1 && str[1] <= 0xdf))
        return 2;
    else if (*str == 0x8f && str[1] >= 0xa1 && str[1] <= 0xfe
                && str[2] >= 0xa1 && str[2] <= 0xfe)
        return 3;
    return 1;
}

void KanjiFilter::setInputCode(code_t code)
{
    switch (code)
    {
    case UNKNOWN:   iconv = 0; break;
    case ISO_2022_JP:      iconv = jis7; break;
    case Shift_JIS:      iconv = sjis; break;
    case EUC_JP:     iconv = euc; break;
    default:
        assert(0);
    }
}

void KanjiFilter::setOutputCode(code_t code, ret_t ret, bool normalize_)
{
    switch (code)
    {
    case ISO_2022_JP:      oconv = &KanjiFilter::jis7_out; m_nMbOutMode = 0; break;
    case Shift_JIS:      oconv = &KanjiFilter::sjis_out; break;
    case EUC_JP:     oconv = &KanjiFilter::euc_out; break;
    default:
        assert(0);
    }
    m_ret = ret;
    normalize = normalize_;
}

string KanjiFilter::euc2sjis(const char* euc)
{
    const unsigned char* p = (unsigned char*) euc;
    string sjis;
    while (*p) {
        if (*p >= 0xa1 && *p <= 0xfe) {
            int k = *(p++) - 0xa0;
            int t = *(p++) - 0xa0;
            sjis += (k - 1) / 2 + (k <= 62 ? 0x81 : 0xc1);
            sjis += t + (k % 2 ? (t <= 63 ? 0x3f : 0x40) : 0x9e);
        }
        else if (*p == 0x8e) {
            p++;
            sjis += *(p++);
        }
        else if (*p == 0x8f) {
            p += 3;
            sjis += "〓";
        }
        else
            sjis += *(p++);
    }
    return sjis;
}

int KanjiFilter::ret_out(char* buf) const
{
    char* p = buf;
    switch (m_ret)
    {
    case CR: *p++ = 0x0d; break;
    case LF: *p++ = 0x0a; break;
    case CRLF: *p++ = 0x0d; *p++ = 0x0a; break;
    default: assert(0);
    }
    return p - buf;
}

bool KanjiFilter::isNormalize() const
{
	return normalize;
}

void KanjiFilter::convert()
/*
    入力は，いったん内部コードに変換する
    内部コードは16ビットで，
        上位バイト == 0のとき，下位バイト = JIS X 0201
        上位バイト != 0のとき，JIS X 0208 GL
*/
{
    int c;
    while ((c = in->get()) != EOF) {
        jis7->push(c);
        sjis->push(c);
        euc->push(c);
        if (!jis7->isValid()) {
			// QsTRACE("jis7 fail\n");
            input_mode &= ~1;
            jis7->clear();
        }
        if (!sjis->isValid()) {
			// QsTRACE("sjis fail\n");
            input_mode &= ~2;
            sjis->clear();
        }
        if (!euc->isValid()) {
			// QsTRACE("euc fail\n");
            input_mode &= ~4;
            euc->clear();
        }
        if (!input_mode)
            input_mode = 7; // 真の文字化けとみられる。やり直し。
        else if (input_mode == 1)
            iconv = jis7;
        else if (input_mode == 2)
            iconv = sjis;
        else if (input_mode == 4)
            iconv = euc;

        if (iconv != 0 && iconv->queue.size() > 0)
            break;
    }
}
