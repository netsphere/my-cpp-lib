// Q's C++ Library
// Copyright (c) 1996-2000 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

// RFC 3629 (Nov 2003) UTF-8, a transformation format of ISO 10646
// obsoletes RFC 2279.
//
// Restricted the range of characters to 0000-10FFFF (the UTF-16 accessible
// range).

#ifndef Q_UTF8_H
#define Q_UTF8_H

#include <stdlib.h>
#include <inttypes.h>
#include <wchar.h>  // mbstate_t

#ifdef __cplusplus
extern "C" {
#endif

#define UTF8_CUR_MAX 4

/**
 * UCS-4 code point 一つを UTF-8 バイト列に変換する。
 * wcrtomb() と同じ.
 * @param s 受け取るバッファへのポインタ. 少なくとも UTF8_CUR_MAX バイト必要.
 *          NULL でもよい. 末尾にナル文字は付加されない.
 *
 * @return s が指すバイト列に書き込まれたバイト数. s が NULL の場合, 必要バイト
 *         数.
 */
extern size_t u32c_to_utf8( char* s, char32_t wc, mbstate_t* ps );

/**
 * UTF-16 文字列を UTF-8文字列に変換する。基本的に wcsrtombs() と同じ.
 *
 * src が指す UTF-16 文字列を UTF-8 に変換して, dest が指す領域に格納する。
 * dest は，src がナル文字に達したときはナル終端する。他方, 先に dest がいっぱ
 * いになったときはナル終端しないことに注意.
 * 一杯になったとき, それが len と一致するバイト数とは限らない。ナル文字に達し
 * たときは *src が NULL になる. 一杯になったときは *src は次に変換されるべき
 * UTF-16 文字を指す.
 *
 * U+FEFF ZERO WIDTH NO-BREAK SPACE が *src 冒頭にあった場合, BOM として削除す
 * る。
 *  -> use as an indication of non-breaking is deprecated; see U+2060 instead.
 *
 * @param dest_len destの大きさ (バイト数).
 *                 dest が NULL の場合, 無視される.
 *
 * @return dest に何バイト書き込んだか。終端のナル文字は数えない。
 *         dest が NULL のとき，(ナル文字を除く) 必要なバイト数を返す。
 *         無効なワイド文字があったときは, (size_t) -1を返す。errno に EILSEQ
 *         を設定する.
 *
 * Note. wcstombs() はスレッドセーフとは限らない。非推奨.
 */
extern size_t utf16_to_utf8( char* dest, const char16_t** src, size_t dest_len,
                             mbstate_t* ps );


/**
 * UTF-8 バイト列を UCS-4 code point に変換する。
 * mbrtowc() と同じ挙動.
 *
 * @param pwc NULL でもよい.
 * @return s から始まる UTF-8 バイト列から解析したバイト数 (ナル文字を除く).
 *         UTF-8 バイト列が src_n より長い場合, (size_t) -2. この場合, src_n が
 *         何バイト必要なのかを返してくれる訳ではない (役に立たない).
 *         不正なマルチバイト列の場合, (size_t) -1.
 */
extern size_t utf8_to_u32c(char32_t* pwc, const char* s, size_t src_n,
                           mbstate_t* ps);

/**
 * UTF-8 文字列を UTF-16 文字列に変換する。
 * 入力が UTF-8, 出力が UTF-16 であることを除き, mbsrtowcs() と同じ.
 * 変換は, mbrtowc(pwcs, *src, MB_CUR_MAX, ps) を繰り返し実行したのと同様.
 * 要素 len に達するか, ナル終端まで変換する。(終端のバイト'\0'は変換する.)
 * 先に len に達した場合, 出力がナル終端されないことに注意.
 *
 * U+FEFF ZERO WIDTH NO-BREAK SPACE が *src 冒頭にあった場合, BOM として削除す
 * る。
 *  -> use as an indication of non-breaking is deprecated; see U+2060 instead.
 *
 * @param src  0 終端の文字列
 * @param pwcs 出力先へのポインタ。NULLの場合, 必要な要素数 (0終端を除く) を数
 *             えるのみ。
 * @param dest_len 出力される最大の code unit (surrogate pair なら 2) の数。
 *                 pwcs が NULL の場合, dest_len は無視される.
 *
 * @return 文字が誤り: (size_t) -1. この場合, errno に EILSEQ を設定する.
 *         ナル終端を除く, 保存した code unit (surrogate pair なら 2) の数
 *
 * Note. mbstowcs() はスレッドセーフとは限らない。非推奨.
 */
extern size_t utf8_to_utf16( char16_t* pwcs, const char** src, size_t dest_len,
                             mbstate_t* ps );


/**
 * mbrlen() のUTF-8版.
 * この関数は, mbrtowc(NULL, str, n, ps ? ps : &internal) と同じ挙動.
 *
 * @param n str から始まる, 検査する最大バイト数. 通常は MB_CUR_MAX.
 * @return  pが指すバイト列が1 code pointを表すのに必要なバイト数. (文字列の
 *          長さではない.)
 *          *pが '\0' の場合は 0
 *          不正なバイト列の場合は -1. errno に EILSEQ を設定する.
 *          (size_t) -2 を返すこともある.
 *
 * Note. mblen() はスレッドセーフではない。状態を静的に割り当てられた領域に保存
 *       するため. 非推奨.
 */
extern size_t utf8len( const char* str, size_t n, mbstate_t* ps );

#ifdef __cplusplus
}
#endif

#endif // Q_UTF8_H
