
# 暗号ライブラリ

FIPS 140 シリーズは、暗号モジュールに関するセキュリティ要件を規定する米国の規格です。FIPS 140-2 および FIPS 140-3 が有効。暗号モジュールそのものではなく、それを組み込んだソフトウェア・ハードウェアについて、商用ベンダが認証を取得しています。Red Hat, SUSE, Canonical 社がそれぞれ OpenSSL Cryptographic Module に認証を受ける、といったように。

FIPS 140-2 認証を受けているもので、主なもの:
<table>
  <tr><th>ライブラリ  <th>開発グループ    <th>ライセンス  <th>備考

  <tr><td><i>OpenSSL</i>  <td>OpenSSL project <td>OpenSSL-SSLeay dual-license

  <tr><td><i>BoringSSL</i><td>Googleが主体    <td>OpenSSL-SSLeay dual-license
    <td>OpenSSL の fork.

  <tr><td><i>GnuTLS</i>   <td>Gnu             <td>LGPL
    <td>昔は基盤として libgcrypt を利用していたが、現在は libnettle を使用。

  <tr><td><i>NSS</i>      <td>Mozilla         <td>MPL 2.0
</table>

libgcrypt は GnuPG 由来の汎用暗号モジュール。これもFIPS 140-2 認証ずみ。このほか、Crypto++ ライブラリがあったが、廃れた。



## NSS

<a href="https://developer.mozilla.org/en-US/docs/Mozilla/Projects/NSS/">Network Security Services - Mozilla | MDN</a>

NSS はクロスプラットフォームのライブラリ。Applications built with NSS can support SSL v3, TLS, PKCS #5, PKCS #7, PKCS #11, PKCS #12, S/MIME, X.509 v3 certificates, and other security standards.

ドキュメントが分かりにくい。メンテナンスされていないようにも見える。リンク切れ、古いページなど。Netscape Portable Runtime (NSPR) に依存しているが, NSPR が廃れつつあるか?

<code>PR_Connect()</code> がハンドシェイク。<code>SSL_HANDSHAKE_AS_SERVER</code> が off ならクライアントとして振る舞う。

サンプルは、例えば <a href="https://github.com/dogtagpki/pki/blob/master/base/tps-client/src/httpClient/engine.cpp">pki/engine.cpp at master · dogtagpki/pki</a>




## OpenSSL

ヴァージョン番号は 1.1.1k のように表記される。アルファベット (patch) までがヴァージョン。3つ目の数字がメジャーヴァージョン。CentOS 8 は openssl-1.1.1g, Fedora 34 は openssl-1.1.1k. v1.1.1 を仮定してよい。

<a href="https://wiki.openssl.org/index.php/TLS1.3">TLS1.3 - OpenSSLWiki</a>





## GnuTLS

<a href="https://www.gnutls.org/">GnuTLS</a>

これはよい。

プロトコルの選択は, ソースコード内で指定もできるが, <code>/etc/crypto-policies/back-ends/gnutls.config</code> ファイル (crypto-policies パッケージ) で行う。Fedora 34 では TLS1.1 までが <code>disabled-version</code> になっている。

<p>Debian 11 (bullseye) では <code>/etc/gnutls/config</code> ファイルに次を追加する [20210213版].

<pre>
[overrides]
insecure-hash = SHA1
insecure-hash = MD5
insecure-hash = STREEBOG-256
insecure-hash = STREEBOG-512
insecure-hash = GOSTR341194
tls-disabled-mac = MD5
tls-disabled-mac = STREEBOG-256
tls-disabled-mac = STREEBOG-512
tls-disabled-mac = GOST28147-TC26Z-IMIT
tls-disabled-group = GROUP-GC256B
tls-disabled-group = GROUP-GC512A
insecure-sig = RSA-MD5
insecure-sig = RSA-SHA1
insecure-sig = DSA-SHA1
insecure-sig = ECDSA-SHA1
insecure-sig = DSA-SHA224
insecure-sig = DSA-SHA256
insecure-sig = DSA-SHA384
insecure-sig = DSA-SHA512
insecure-sig = GOSTR341012-512
insecure-sig = GOSTR341012-256
insecure-sig = GOSTR341001
insecure-sig-for-cert = rsa-sha1
insecure-sig-for-cert = dsa-sha1
insecure-sig-for-cert = ecdsa-sha1
tls-disabled-cipher = CAMELLIA-256-GCM
tls-disabled-cipher = CAMELLIA-128-GCM
tls-disabled-cipher = CAMELLIA-256-CBC
tls-disabled-cipher = CAMELLIA-128-CBC
tls-disabled-cipher = 3DES-CBC
tls-disabled-cipher = ARCFOUR-128
tls-disabled-cipher = GOST28147-TC26Z-CFB
tls-disabled-cipher = GOST28147-CPA-CFB
tls-disabled-cipher = GOST28147-CPB-CFB
tls-disabled-cipher = GOST28147-CPC-CFB
tls-disabled-cipher = GOST28147-CPD-CFB
tls-disabled-cipher = GOST28147-TC26Z-CNT
tls-disabled-kx = DHE-DSS
tls-disabled-kx = VKO-GOST-12
disabled-version = SSL3.0
disabled-version = TLS1.0
disabled-version = TLS1.1
disabled-version = DTLS1.0
min-verification-profile = medium
</pre>


