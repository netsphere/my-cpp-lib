
#include "text/utf8.h"
#include <string.h> // memcpy
#include "text/utf8_ifilter.h"
using namespace std;

// @override
// バッファに読み込む.
Utf8InputFilter::int_type Utf8InputFilter::underflow()
{
    // eback() 先頭, gptr() 現在の位置, egptr() 終端. 名前が??
    if ( super::gptr() && super::gptr() < super::egptr() )
        return *super::gptr();   // return buffered.

    // バッファを読み切った.
    int inpos = strlen((const char*) m_inbuf);  // 切れ端がある.
    m_is->read((char*) (m_inbuf + inpos), sizeof(m_inbuf) - inpos - 1);
    m_inbuf[inpos + m_is->gcount()] = '\0';

    if ( m_is->gcount() == 0 ) {  // EOF
        return traits_type::eof();
    }
    // Trap: istream::read() は, EOF に達すると ios_base::eofbit と
    // ios_base::failbit を立てる.
    //   -> fail() で検査してはならない!
    if ( m_is->bad() ) {
        throw runtime_error(string("m_is->read() failed: errno=") +
                            to_string(errno));
    }

    mbstate_t ps = {0};
    const char* src = (const char*) m_inbuf;
    char16_t* p = m_outbuf;
    do {
        // 戻り値は code unit の数.
        size_t cu = utf8_to_utf16(p, &src,
                        (sizeof(m_outbuf) / sizeof(char_type)) - (p - m_outbuf),
                        &ps );
        if ( cu != (size_t) -1 ) {
            p += cu;
            break;
        }
        else {
            p += char_traits<char16_t>::length(p);
            assert(src);
            src++; *p++ = u'〓';
            continue;
        }
    } while (true);

    if (!src)
        m_inbuf[0] = '\0';
    else  // 切れ端がある
        memmove(m_inbuf, src, strlen(src) + 1);

    // TODO: sungetc() できるように, 多少, 先頭を残すこと.
    super::setg(m_outbuf, m_outbuf, p);

    return *super::gptr();
}
