﻿// -*- coding:utf-8-with-signature -*-

// ルートCA証明書が必要.
// Fedora では ca-certificates パッケージに含まれる.

// 参考 [C言語] HTTPSクライアントを作ってみる
//      https://qiita.com/edo_m18/items/41770cba5c166f276a83
// - このページの解説は, 使っている関数が古い.
// - サーバ証明書を検証していないので, 不十分. (セキュリティ欠陥)

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <memory>

#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/x509v3.h>

#include "network/qsocket.h" // ngethostbyname()
#include "network/socket_stream.h"
#include "network/tls_socket_streambuf.h"

#include "URI.h"

using namespace std;


// origin_form   = absolute-path [ "?" query ]
string origin_form(const URI& uri)
{
    return uri.getPath() +
           (uri.getQuery() != "" ? string("?") + uri.getQuery() : "");
}

// HTTP/1.1 の Host: はホスト名ではない。
string host_port(const URI& uri)
{
    return uri.getHost() +
           (uri.getPort() != -1 ? string(":") + to_string(uri.getPort()) : "");
}


void print_webpage(const URI& uri, SocketStream& ss)
{
    char msg[1000];
    sprintf(msg, "GET %s HTTP/1.1\r\nHost: %s\r\n\r\n",
            origin_form(uri).c_str(), host_port(uri).c_str() );
    printf("\nRequest =================================================\n");
    printf(msg);

    ss.write(msg, strlen(msg));

    constexpr int BUF_SIZE = 2000;
    char buf[BUF_SIZE];
    int read_size;
    string header;
    do {
        header = ss.getline();
        printf("%s\n", header.c_str());
    } while (header != "");

    // TODO: Transfer-Encoding:chunked の場合, 単にバイナリデータではない.
    read_size = ss.read(buf, BUF_SIZE);
    write(1, buf, read_size);
}


// RFC 7230 Hypertext Transfer Protocol (HTTP/1.1): Message Syntax and Routing
// 6. Connection Management
// @return If error, -1.
int http_connection(const string& host, int port, SSL_CTX* ctx )
{
    SocketStream ss;
    TlsSocketStreamBuf tssb(ctx);
    ss.ios::rdbuf(&tssb);

    // アドレスの解決
    printf("host = %s, port = %d\n", host.c_str(), port );
    ss.open(host.c_str(), port);

    SSL* ssl = tssb.ssl();

    // ハンドシェイク
    if ( !SSL_connect(ssl) ) { // この中で, 検証そのものは行なわれている.
                      // 証明書の検証に失敗しても, 接続は確立される!! マジか
        fprintf(stderr, "SSL_connect() failed.\n");
        ERR_print_errors_fp(stderr);
        return -1;
    }
    // 1. サーバ側の証明書が存在することを確認. 必須.
    // SSL_get_verify_result() は, SSL_get_peer_certificate() との併用時のみ
    // 意味を持つ。
    X509* x509 = SSL_get_peer_certificate(ssl);
    if ( !x509 ) {
        fprintf(stderr, "SSL_get_peer_certificate(): no certificate.\n");
        ERR_print_errors_fp(stderr);
        return -1;
    }
    X509_print_fp(stdout, x509);

    // [BUGS]
    // If no peer certificate was presented, the returned result code is
    // X509_V_OK!
    // 2. 証明書を検証する. Confirm the server's certificate chains back to a
    //    trusted root.
    // 3. ホスト名検証はここの中で行われる。明示的に有効になっていれば!
    int r = SSL_get_verify_result(ssl);
    if (r != X509_V_OK) {
        fprintf(stderr, "verification failed.\n");
        return -1;
    }
    printf("Connected to %s\n", host.c_str());

    X509_free(x509); // これを忘れると memory leak する。しかもどこか分かりにく
                     // い.
/*
==4209== 7,863 (384 direct, 7,479 indirect) bytes in 1 blocks are definitely lost in loss record 101 of 101
==4209==    at 0x484386F: malloc (vg_replace_malloc.c:393)
==4209==    by 0x4ABB4DC: CRYPTO_zalloc (mem.c:197)
==4209==    by 0x49D4F13: asn1_item_embed_new (tasn_new.c:136)
==4209==    by 0x49D50F5: ASN1_item_new_ex (tasn_new.c:41)
==4209==    by 0x4B43BD0: X509_new_ex (x_x509.c:165)
==4209==    by 0x48CED3E: UnknownInlinedFun (statem_clnt.c:1800)
==4209==    by 0x48CED3E: ossl_statem_client_process_message (statem_clnt.c:1033)
==4209==    by 0x48CA951: UnknownInlinedFun (statem.c:647)
==4209==    by 0x48CA951: state_machine (statem.c:442)
==4209==    by 0x4062ED: http_connection(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const&, int, ssl_ctx_st*) (https_test.cpp:90)
 */

    print_webpage(URI(string("https://") + host + "/"), ss);
    print_webpage(URI(string("https://") + host + "/hoge"), ss); // error page

    // TlsSocketStreamBuf のなかでセッションを閉じる処理が走る

    return 0; // OK
}


//unique_ptr<URI> pUri1(URI::parse("https://store.steampowered.com/", nullptr));
// SNI の例
unique_ptr<URI> pUri2(URI::parse("https://outlook.uservoice.com/", nullptr));

int main()
{
    SSL_load_error_strings(); // 人間用のエラーを出力.
    SSL_library_init();

    // SSL_CTX は, 使いまわせる.
    // Web上の解説で SSLv23_client_method() を使っているものがあるが,
    // deprecated.
    // TLS_client_method() は, SSLv3, TLSv1, TLSv1.1 and TLSv1.2.
    SSL_CTX* ctx = SSL_CTX_new( TLS_client_method() );
    if (!ctx) {
        ERR_print_errors_fp(stderr);
        return EXIT_FAILURE;
    }

    // SSLv3, TLS 1.0 は脆弱なため, 無効化.
    // SSL_set_options() で特定のバージョンを無効化するのは、推奨されない.
    // TLS 1.1 も無効化が決定.
    //   - Microsoft Edge v84 (2020年7月)
    //   - Google Chrome v83 (2020年5月)
    SSL_CTX_set_min_proto_version(ctx, TLS1_2_VERSION);

    // ルートCA証明書を読み込む. これがないと, SSL_get_verify_result() が失敗.
    // SSL_connect() より前でなければならない。
    // Fedora: 'ca-certificates' package.
    int r = SSL_CTX_load_verify_locations(ctx,
                                          "/etc/pki/tls/certs/ca-bundle.crt",
                                          nullptr);
    if ( !r ) {
        fprintf(stderr, "CA certificates missing\n");
        ERR_print_errors_fp(stderr);
        return EXIT_FAILURE;
    }

//http_connection(pUri1->getHost(), pUri1->getPort(), ctx);
    http_connection(pUri2->getHost(), pUri2->getPort(), ctx);

    SSL_CTX_free(ctx);
    ERR_free_strings();

    return EXIT_SUCCESS;
}

// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
