// Q's C++ Library
// Copyright (c) 1998-2000 Hisashi HORIKAWA. All rights reserved.

#include <cassert>
#include <misc.h>
#include <strings.h>
#include "InputMethod.h"

///////////////////////////////////////////////////////////////////////
// InputMethod

InputMethod::InputMethod(): disp(NULL), im(0)
{
}

InputMethod::~InputMethod()
{
    close();
}

void InputMethod::onImInstantiate(
    Display* disp, XPointer client_data, XPointer call_data)
{
    TRACE("onImInstantiate()\n");

    InputMethod* this_ = (InputMethod*) client_data;

    XUnregisterIMInstantiateCallback(disp, NULL, NULL, NULL,
                            onImInstantiate, (XPointer) this_);
        // 解除しておかないと，多重に呼ばれる

    Listeners::const_iterator i;
    for (i = this_->listeners.begin(); i != this_->listeners.end(); i++)
        (*i)->inputMethodInstantiated();
}

void InputMethod::onImServerDestroy(
    XIM im_, InputMethod* this_, XPointer /*call_data*/)
{
    TRACE("onImServerDestroy()\n");

    if (this_->im == im_) {
        this_->im = 0;

        this_->to_register = true;
            // listenerでopen()してると，open()内でregister済み
        Listeners::const_iterator i;
        for (i = this_->listeners.begin();
                                   i != this_->listeners.end(); i++)
            (*i)->inputMethodDestroyed();

        if (!this_->im && this_->to_register) {
            XRegisterIMInstantiateCallback(
                this_->disp, NULL, NULL, NULL,
                onImInstantiate, (XPointer) this_);
        }
    }
}

void InputMethod::close()
{
    if (im) {
        XCloseIM(im);
        im = 0;
    }
}

XIMStyle InputMethod::selectInputStyle(const XIMStyle* styles) const
    // IMサーバーにサポートする入力スタイルを問い合わせ，折衝する。
{
    assert(styles);
    if (!im)
        return 0;

    XIMStyles* im_styles = NULL;
    const char* mes = XGetIMValues(im, XNQueryInputStyle, &im_styles,
                                   NULL);
    if (mes) {
        error("XGetIMValues() failed: '%s'\n", mes);
        return 0;
    }
#if DEBUG > 1
    for (int iii = 0; iii < im_styles->count_styles; iii++)
        printStyle(im_styles->supported_styles[iii]);
#endif
    const XIMStyle* p;
    for (p = styles; *p; p++) {
        for (int i = 0; i < im_styles->count_styles; i++) {
            if (*p == im_styles->supported_styles[i]) {
                XFree(im_styles);
                return *p;
            }
        }
    }
    XFree(im_styles);
    return 0;
}

extern "C" {
    // XFree86 bug: X11/Xlib.hで宣言し忘れている。
    extern char* XSetIMValues(XIM im, ...);
}

bool InputMethod::open(Display* d)
    // IMを開く = IMサーバーと接続する
{
    assert(d);
    disp = d;
    im = XOpenIM(disp, NULL, NULL, NULL);
        // XmbLookupString()等の関数が返す文字列はこの時点のロ
        // ケール（のエンコーディング）でエンコードされる。
    if (!im) {
        error("warning: cannot open IM\n");
        // IMサーバーが有効になった時点で呼び出してもらう
        XRegisterIMInstantiateCallback(disp, NULL, NULL, NULL,
                                    onImInstantiate, (XPointer) this);
        to_register = false;
            // onImServerDestroy()と多重に登録しないように
        return false;
    }
    TRACE("im locale = '%s'\n", XLocaleOfIM(im));

#if 0 // TEST
    changeLocale("C");
        // XOpenIM()後ロケールを変更してもXmbLookupString()等のエン
        // コードは変わらない
#endif

    // IMサーバーが落ちたときに呼び出してもらう
    XIMCallback callback =
        { (XPointer) this, (XIMProc) onImServerDestroy };
    const char* mes = XSetIMValues(im, XNDestroyCallback,
                                   &callback, NULL);
    if (mes)
        error("XSetIMValues failed: '%s'\n", mes);

    return true;
}

bool InputMethod::is_open() const
{
    return im != 0;
}

XIM InputMethod::getXIM() const
{
    return im;
}

void InputMethod::attachInputMethodListener(InputMethodListener* l)
{
    listeners.push_back(l);
}

////////////////////////////////////////////////////////////////////////
// XIM VJE-Delta拡張

bool is_pfcs(const char* s)
    // portable filename character set
{
    return *s >= 'a' && *s <= 'z' || *s >= 'A' && *s <= 'Z'
        || *s >= '0' && *s <= '9' || *s == '.' || *s == '_' || *s == '-';
}

string getIMServerName()
{
    const char* modif = XSetLocaleModifiers(NULL);
        // 1999.10.04 環境変数XMODIFIERSも考慮されることを確認
    if (!modif || !*modif) 
        return "";
    
    const char* p = strstr(modif, "@im=");
    	// X11/lcWrap.c: _XlcDefaultMapModifiers()
    if (p) {
        string r;
        p += 4;
        while (is_pfcs(p))
            r += *p++;
        return r;
    }
    return "";
}

Window getIMExtWindow(Display* disp)
{
    string ims = getIMServerName();
    if (ims == "")
        return 0;
    
    Window r_root, r_parent;
    Window* children = NULL;
    unsigned int count;

    Window root = XRootWindow(disp, 0);
    if (!XQueryTree(disp, root, &r_root, &r_parent, &children, &count))
        return 0;

    Window ret = 0;
    char* name = NULL;
    for (unsigned int i = 0; i < count; i++) {
        XFetchName(disp, children[i], &name);
        if (name != NULL) {
            TRACE("wnd name = '%s'\n", name);
            if (!strcasecmp(ims.c_str(), name)) {
                // VJE-Deltaは，IMS名 = 'vje', ウィンドウ名 = 'VJE'
                // TODO: XIM拡張をサポートしてないIMサーバーでどうなる？
                ret = children[i];
                XFree(name);
                break;
            }
            XFree(name);
        }
    }
    XFree(children);

  return ret;
}

enum IMExtType {
    IMEXT_FUNCTION = 5,         // モード変更など
    IMEXT_REGISTER_WORD = 6     // 単語登録
};

void setIMExtValue(Display* disp, Window focus_window, IMExtCmd cmdid)
{
    Window imext_window = getIMExtWindow(disp);
        // IMサーバーが落ちてるかもしれないので，毎回確認する
    if (!imext_window)
      return;

    XEvent event;
    memset(&event, 0, sizeof(event));
    event.xclient.type = ClientMessage;
    event.xclient.format = 8;
    event.xclient.window = imext_window;
    event.xclient.message_type = XInternAtom(disp, "_XIM_PROTOCOL", False);
    event.xclient.data.l[0] = focus_window;
    event.xclient.data.l[1] = IMEXT_FUNCTION;
    event.xclient.data.l[2] = cmdid;
        // バイトオーダーの考慮がないように見えるが
    XSendEvent(disp, imext_window, False, NoEventMask, &event);
    XFlush(disp);
}

