﻿// -*- mode:c++; coding:utf-8-with-signature; -*-

#ifndef QSDEBUG_H__
#define QSDEBUG_H__ 1

#ifdef _WIN32
//#include <WinNT.h>  これはダメ
//#include <winbase.h> これもダメ
  #include <windows.h>
  #include <tchar.h>
#else
  typedef char TCHAR;
  typedef const TCHAR* LPCTSTR;
#endif


#ifndef NDEBUG
// debug環境

#ifdef _WIN32
#ifndef __func__
  #define __func__ __FUNCTION__
#endif
#endif // _WIN32


/**
 * @param flag 非0のとき, メッセージ出力.
 */
extern void q_trace( unsigned int flag, const char* filename, int lineno,
                     LPCTSTR format, ...);

// __VA_ARGS__ は, 引数が一つ以上、という重要な制約がある. => 1つ加える.
#define QsTRACE(flag, ...)   _QsTRACE2(flag, __VA_ARGS__, "")
#define _QsTRACE2(flag, format, ...)  q_trace(flag, __FILE__, __LINE__, format, __VA_ARGS__ )


/**
 * QsTRACE(), QsASSERT() をファイルへ出力する.
 * @param filename nullptr の場合, ファイル出力をしない.
 */
extern void QsTRACE_SET_OUTPUT_FILE( LPCTSTR filename,
                                     bool separateThread = false);


// assert() の代わり.
extern void q_assert_fail(const char* message, const char* filename, int lineno,
                          const char* funcname );
#define QsASSERT(check)  (static_cast<bool>(check) ? (void) 0 :  \
                                    q_assert_fail(#check, __FILE__, __LINE__, __func__))


#ifndef VERIFY
// VERIFY()マクロは, リリースビルドでも, 式を評価します.
// <afx.h>
// C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Tools\MSVC\14.13.26128\atlmfc\include
  #define VERIFY     QsASSERT
#endif



#else
// release環境 ///////////////////////////////////////////////////////////

#define QsTRACE(flag, format, ...)  ((void) 0)

inline void QsTRACE_SET_OUTPUT_FILE_( LPCTSTR,
                                      bool separateThread = false) { }
#define QsTRACE_SET_OUTPUT_FILE   true ? (void) 0 : ::QsTRACE_SET_OUTPUT_FILE_


#define QsASSERT(check)    ((void) 0)

#ifndef VERIFY
// VERIFY()マクロは, リリースビルドでも, 式を評価します.
  #define VERIFY(check)   (static_cast<void>(check))
#endif


#endif // !NDEBUG

#endif // !QSDEBUG_H__


// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
