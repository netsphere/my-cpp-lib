﻿// -*- coding:utf-8-with-signature; mode:c++ -*-

/*
以下はいずれも, 内部 UTF-16:
 - ICU の <code>UnicodeString</code> クラス,
 - Qt の <code>QChar</code> クラス, <code>QString</code> クラス
 - Windows の WCHAR
 - Java の文字列

絵文字など Unicode にしか収録されていない文字が大量にある。
現代では、こちらの方法でなければ、まともな処理はできない。

昔の ISO/IEC 2022, "Code Extension Technique" ベースで作る。
エンコード (シフトJIS や EUC-JP) ではなく, 符号化文字集合 Coded Character Set
で文字を区別する。
*/

#ifndef ENC_STRING_H__
#define ENC_STRING_H__ 1

#include <string>
#include <stdint.h>
#ifndef _WIN32
typedef unsigned char BYTE;
#endif

// 登録簿 https://itscj.ipsj.or.jp/english/vbcqpr00000004qn-att/ISO-IR.pdf
//        ISO International Register of Coded Character Sets To Be Used With
//        Escape Sequences.
enum CCSType
{
    CS_94CHAR_GRAPHIC = 1,
    CS_94CHAR_2ND_GRAPHIC = 2,
    CS_96CHAR_GRAPHIC = 3,
    CS_MULTIBYTE_GRAPHIC = 4,
    CS_C0_CONTROL = 5,
    CS_C1_CONTROL = 6,
};

struct CCS
{
    unsigned reg_no;
    const char* name;
    CCSType type;
    unsigned superset;
};

/*
内部コードは, 次のようにする:
|  1   |   2   3   4  |
|reg_no|  code point  |

Unicode の場合は特別に reg_no = 0 とする。U+10FFFF (21bit) なので OK.
U+0000..U+007F ISO/IEC 646 IRV (国際基準版), = US ASCII と一致
               reg#2 1983年版は異なる。reg#6 US-ASCII のほう。
U+0020..U+007E, U+00A0..U+00FF  ISO/IEC 8859-1 (reg#100) と一致
U+0080..U+009F  ISO/IEC 6429 (= JIS X 0211 符号化文字集合用制御機能) と一致
                reg#77 1983年版は異なる。結構いいかげんだな...
*/

// code-point 列にはナル文字を含むことができない。
class EncString
{
    // code-point 列
    uint32_t* str;   // char32_t は UCS-4 固定

    // code-point 単位. 終端ナル文字は含まない.
    int len;

    //std::string m_encoding;

public:
    EncString();
    virtual ~EncString();

    EncString(const EncString& );

    // ISO 2022 テキスト
    // @param len バイト単位. 終端のナル文字を含まず.
    // @param enc "シフトJIS" など。
    EncString(const char* str, int len, const std::string& enc);

    EncString& operator = (const EncString& );

    // 最低限, これだけ定義が必要.
    // TODO: C++20 より宇宙船演算子 <=> が導入された。
    //       See https://onihusube.hatenablog.com/entry/2019/01/13/180100
    bool operator < (const EncString& x) const;
    bool operator == (const EncString& x) const;
    EncString& operator += (const EncString& x);

    int convert(const std::string& new_enc);
    void regard(const std::string& new_enc);

    //std::string encoding() const {
    //    return str ? m_encoding : "";
    //}

    const uint32_t* data() const;

    // code-point 単位
    int length() const;
};

#endif // !ENC_STRING_H__
