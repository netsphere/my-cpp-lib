// -*- mode:c++ -*-

// Q's C++ Library
// Copyright (c) 1996-1998 Hisashi Horikawa. All rights reserved.

// The Base16, Base32, and Base64 Data Encodings (October 2006)
// https://tools.ietf.org/html/rfc4648

// Base64 って結構カオス？ - DELPHIER＠はてな
// http://d.hatena.ne.jp/izariuo440/20110127/1296138201
//     62番目, 63番目, paddingの有無, 改行の有無など、バリエーションが多い.

/**
 * Base64でエンコードし, 末尾にナル文字を付加する.
 *     #62 = '+', #63 = '/'
 *     padding = '='
 * @param destsize  destの大きさ. 必要な文字数 + 1 バイトが必要.
 *                  0を入れて渡すと, 必要な大きさを格納する.
 * @param src     バイナリ列.
 * @param srcsize バイナリの長さ.  
 */
extern int base64_encode(char* dest, int* destsize, const unsigned char* src,
                         int srcsize);

/**
 * "base64url" でエンコードする.
 *     #62 = '-' (minus), #63 = '_' (underline)
 *     padding は付加しない.
 */
extern int base64url_encode(char* dest, int* destsize, const unsigned char* src,
                            int srcsize);

/**
 * Base64 または base64url をデコードする.
 * @param src 文字列.
 *            [ \t]*\r?\n[ \t]* を途中に含んでよい.
 * @param srcsize 終端のナル文字を含まない, 長さ.
 *                -1 の場合, src をナル終端の文字列として取り扱う.
 */
extern int base64_decode(unsigned char* dest, int* destsize, const char* src,
                         int* srcsize );

