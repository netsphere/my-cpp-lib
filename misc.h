﻿// -*- mode:c++; coding:utf-8-with-signature -*-

// Q's C++ Library
// Copyright (c) 1998-1999 Hisashi HORIKAWA. All rights reserved.

// 細々した関数群

#ifndef QSNICOLA_MISC
#define QSNICOLA_MISC

#include <vector>
//#include <list>
//#include <set>
#include <string>
#include <cctype>
#include <map>          // for CFont
#include <string.h>


////////////////////////////////////////////////////////////////////////

template <typename C__>
C__* skip_space(C__* p)
{
    while (*p && strchr(" \t\r\n", *p))
        p++;
    return p;
}

extern bool createFolder(const char* path);
extern int char_length(const char* b, const char* e);
extern int byte_length(const char* s, int char_len);
extern void error(const char* format, ...);
extern std::string getHomeDir();
extern bool isYesString(const char* s);



////////////////////////////////////////////////////////////////////////
// SizeList

template <typename Ty>
class SizeList: public std::vector<Ty>
{
public:
    typedef std::vector<Ty> super;
    typedef typename super::const_iterator const_iterator;

    Ty sum(int first, int last) const
        // [first, last)の合計
    {
        Ty r = 0;
        const_iterator i;
        for (i = super::begin() + first;
             i != super::end() && i != super::begin() + last; i++) {
            r += *i;
        }
        return r;
    }
    void replace(int pos, int n, const Ty& val)
        // posからn要素を一つのvalで置き換える
    {
        erase(super::begin() + pos, super::begin() + pos + n - 1);
        super::operator [](pos) = val;
    }
};


////////////////////////////////////////////////////////////////////////
// CDimension

class CDimension
{
public:
    int width, height;
    CDimension(): width(0), height(0) { }
};


#endif  // !QSNICOLA_MISC
