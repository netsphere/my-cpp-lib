﻿// -*- coding:utf-8-with-signature -*-

#include "x-misc.h"
#include <assert.h>
//#include <error.h> // GNU拡張; error()

#include "../qsdebug.h"
#include "../misc.h"


//////////////////////////////////////////////////////////////////////
// X11用

int getColor(Display* disp, const char* name)
{
    assert(disp != 0 && name != 0);
    Colormap cmap;
    XColor c0, c1;
    cmap = DefaultColormap(disp, 0);
    XAllocNamedColor(disp, cmap, name, &c1, &c0);
    return c1.pixel;
}

XFontSet createFont(
    Display* disp,
    const char* name,
    const char* fallback_font)
{
    XFontSet fs = 0;
    char** miss = NULL;
    int miss_count;
    char* def_string;

    fs = XCreateFontSet(disp, name, &miss, &miss_count, &def_string);
    if (!fs) {
        error("error: cannot create fontset: '%s'\n", name);
        XFreeStringList(miss);
        miss = NULL;
        fs = XCreateFontSet(disp, fallback_font, &miss, &miss_count, &def_string);
        if (!fs) {
            error("error: cannot create fontset with fallback-font-name.\n");
            XFreeStringList(miss);
            return 0;
        }
    }

    if (miss_count > 0) {
        error("warning: cannot load font: ");
        for (int i = 0; i < miss_count; i++)
            error("%s ", miss[i]);
        error("\n");
        XFreeStringList(miss);

        QsTRACE(1, "fs locale = '%s'\n", XLocaleOfFontSet(fs));
        QsTRACE(1, "def_string = '%s'\n", def_string);

        if (def_string[0] == '\0') {
            miss = NULL;
            fs = XCreateFontSet(disp, fallback_font, &miss, &miss_count, &def_string);
            XFreeStringList(miss);
        }
    }

    return fs;
}

bool isToplevelWindow(Display* disp, Window w)
{
    assert(disp);
    assert(w);
    
    Window root = 0;
    Window parent = 0;
    Window* children = NULL;
    unsigned int count;
    XQueryTree(disp, w, &root, &parent, &children, &count);
    XFree(children);
    return root == parent;
}

Window getToplevelWindow(Display* disp, Window w)
{
    assert(disp);
    assert(w);

    Window root, parent;
    Window* children;
    unsigned int count;
    while (true) {
        root = parent = 0;
        children = NULL;
        XQueryTree(disp, w, &root, &parent, &children, &count);
        XFree(children);
        if (root == parent)
            break;
        w = parent;
    }
    return w;
}


///////////////////////////////////////////////////////////////////////
// CFont

CFont::FontMap CFont::fm;

CFont::CFont(): disp(0)
{
};

CFont::~CFont()
{
    destroy();
}

bool CFont::create(Display* d, const char* name)
{
    assert(d);
    assert(name && name[0]);

    if (cur_name != "")
        destroy();
    
    disp = d;
    ref* p = fm[name];
    if (p) {
        p->count++;
        cur_name = name;
        return true;
    }
    
    p = new ref();
    p->font = createFont(disp, name);
    if (!p->font) {
        delete p;
        return false;
    }
    p->count = 1;
    fm[name] = p;
    cur_name = name;
    return true;
}

void CFont::destroy()
{
    if (!disp || cur_name == "")
        return;

    ref* p = fm[cur_name];
    if (p) {
        p->count--;
        if (p->count <= 0) {
            XFreeFontSet(disp, p->font);
            fm.erase(cur_name);
            delete p;
        }
    }
    cur_name = "";
}

XFontSet CFont::xfont() const
{
    if (cur_name == "")
        return 0;
    ref* p = fm[cur_name];
    return p ? p->font : 0;
}
