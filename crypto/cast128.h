// Q's C++ Library
// Copyright (c) 1996-1998 Hisashi Horikawa. All rights reserved.

// The CAST-128 Encryption Algorithm (RFC 2144)

#include <qTypes.h>

struct cast_subkey
{
private:
    uint32_t Km[16];
    uint32_t Kr[16];
    int keylen;

    friend void cast_compute_subkey(cast_subkey* s, const BYTE* key, int keylen);
    friend void cast_encrypt_subkey(BYTE cipher[8], const BYTE plain[8], const cast_subkey* s);
    friend void cast_decrypt_subkey(BYTE plain[8], const BYTE cipher[8], const cast_subkey* s);
};

extern bool cast_encrypt(BYTE cipher[8], const BYTE plain[8], const BYTE* key, int keylen);
            // 鍵の長さは，5 (40bit)から16 (128bit)

extern int cast_ecb_encrypt(BYTE* out, const BYTE* in, int len, const BYTE* key, int keylen);
extern int cast_ecb_decrypt(BYTE* out, const BYTE* in, int len, const BYTE* key, int keylen);
