
#include "network/qsocket.h"
#include <memory>

using namespace std;

int main()
{
    unique_ptr<CSocket> s(CSocket::setupServer(nullptr, 0));
    if ( !s ) {
        printf("setup failed.\n");
        return 1;
    }

    if (s->family() == AF_INET)
        printf("IPv4, port = %d\n", s->port());
    else
        printf("IPv6, port = %d\n", s->port());
    
    return 0;
}
