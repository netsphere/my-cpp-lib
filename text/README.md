
# 文字コード関係

## Unicode ベース

外部とのやりとり: UTF-8 が原則。まれに UTF-16 (XMLファイルなど).

内部コードは UTF-16 にする。Windows API, Unicode標準ライブラリ ICU, Qt のいずれも, UTF-16 になっている。これらとの交換に便利。クロスプラットフォームでは <i>wxWidgets</i> がビルドオプションで UTF-8, UTF-16, UCS-4 を切り替えられるが、ユーザ (開発者) が三つとも考慮しなければならなくなっており、無駄が多い。



### 厳密な UTF-8 - UTF-16変換

仕様: RFC 3629 (Nov 2003) <i>UTF-8, a transformation format of ISO 10646</i>. Obsoletes RFC 2279. 

Plain C のインタフェイスでUTF-8 - UTF-16 変換する関数. ほかのクラスなどの基盤となる。
引数や挙動は, 以下の対応する関数に合わせた. 

|関数               |対応する関数   |
|-------------------|---------------|
|`u32c_to_utf8()`     |`wcrtomb()`      |
|`utf16_to_utf8()`    |`wcsrtombs()`    |
|`utf8_to_u32c()`     |`mbrtowc()`      |
|`utf8_to_utf16()`    |`mbsrtowcs()`    |
|`utf8len()`          |`mbrlen()`       |

入力の冒頭の BOM は削除する。U+FEFF as a ZWNBSP character is deprecated in Unicode 3.2. とりあえず冒頭以外の U+FEFF は削除しない.

UTF-8 は, 受け入れられないバイト列の範囲が所々あり、きちんとエラーとして弾く必要がある。対応した。

100M バイトの UTF-8 ファイルを UTF-16 に変換して別ファイルに保存するのに, プロファイリング有効にして, <s>0.09 秒</s> [2022-09] ダメ押しで0.03秒まで詰めた. 爆速とはいかないが、まぁまぁの速度と思う。一度 UCS-4 にしている部分を直接 UTF-16 にすればもう少し稼げるはず。

<b>同種のライブラリ:</b> <a href="https://github.com/nemtrif/utfcpp/">nemtrif/utfcpp: UTF-8 with C++ in a Portable Way</a>

高速な変換器 <a href="https://github.com/rdentato/u8c/">rdentato/u8c: Fast validating utf-8 encoder/decoder for C</a>. 元の論文 <a href="http://bjoern.hoehrmann.de/utf-8/decoder/dfa/">Flexible and Economical UTF-8 Decoder</a> 実装比較もある。

ICU の `u_strFromUTF8()` がまぁまぁ優秀:

common/unicode/ustring.h
common/ustrtrns.cpp
   `u_strFromUTF8()` -> `u_strFromUTF8WithSub()` に丸投げ.
       -> 単に手で書き下している。 // modified copy of `U8_NEXT()`
          コードの重複を気にせず、場合分けを展開している。
common/utf_impl.cpp
   `utf8_nextCharSafeBody()`
       -> 場合分けのカタマリ。
1 code point のなかでは、まったくループしない。サロゲートペアにする場合分けも, code point に変換してからではなく、もっと手前にできる。




## 文字集合 Character Set ベース

シフトJIS, EUC-JP などの相互変換は、文字集合ベースのほうが簡単。

Ruby は CSI (Character Set Independent) を謳っているが、文字集合 Coded Character Set (CCS) と文字コード (CES) を誤解して混同し、実装も中途半端。Ruby 以外のプロジェクトを参照せよ。
CSI は、文字コードではなく、文字集合を区別しながら格納する。

IANA Character Sets -- これは文字コード
https://www.iana.org/assignments/character-sets/character-sets.xhtml

|文字コード (CES)     |IANA名        |別名     |文字集合 (CCS)     |
|---------------------|--------------|---------|-------------------|
|シフトJIS            |Windows-31J   |MS932    |JIS X0201:1997, JIS X0208:1997 + NEC special + IBM extensions. |
|EUC-JP               |EUC-JP        |eucJP-ms |US-ASCII, JIS X0208-1990, Half Width Katakana, JIS X0212-1990 |
|ISO-2022-JP          |ISO-2022-JP   |         |下記参照           |
|ISO-2022-JP-2        |ISO-2022-JP-2 |         |下記参照           |
|UTF-8 (BOMあり/なし) |UTF-8         |         |Unicode            |
|UTF-16               |UTF-16, UTF-16LE, UTF-16BE | |Unicode       |


ISO-2022-JP  [RFC1468] (Jun 1993)

       Esc Seq    Character Set                  ISOREG
       ---------  ---------------------------    ------
       ESC ( B    ASCII                             6
       ESC ( J    JIS X 0201-1976 ("Roman" set)    14
       ESC $ @    JIS X 0208-1978  いわゆる旧JIS   42
       ESC $ B    JIS X 0208-1983  いわゆる新JIS   87

新JIS は, 83JIS, 90JIS, 97JIS まで, ほぼほぼ同じ.


ISO-2022-JP-2 [RFC1554] (Dec 1993)

                              94 character sets
      reg#  character set      ESC sequence                designated to
      ------------------------------------------------------------------
      6     ASCII              ESC 2/8 4/2      ESC ( B    G0
      42    JIS X 0208-1978    ESC 2/4 4/0      ESC $ @    G0
      87    JIS X 0208-1983    ESC 2/4 4/2      ESC $ B    G0
      14    JIS X 0201-Roman   ESC 2/8 4/10     ESC ( J    G0
      58    GB2312-1980        ESC 2/4 4/1      ESC $ A    G0
      149   KSC5601-1987       ESC 2/4 2/8 4/3  ESC $ ( C  G0
      159   JIS X 0212-1990    ESC 2/4 2/8 4/4  ESC $ ( D  G0

                              96 character sets
      reg#  character set      ESC sequence                designated to
      ------------------------------------------------------------------
      100   ISO8859-1          ESC 2/14 4/1     ESC . A    G2
      126   ISO8859-7(Greek)   ESC 2/14 4/6     ESC . F    G2

