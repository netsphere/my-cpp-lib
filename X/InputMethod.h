// Q's C++ Library
// Copyright (c) 1998-2000 Hisashi HORIKAWA. All rights reserved.

#ifndef QSLIB_XIM__
#define QSLIB_XIM__

#include <vector>
#include <X11/Xlib.h>

using namespace std;

////////////////////////////////////////////////////////////////////////
// InputMethodListener

class InputMethodListener
{
public:
    virtual void inputMethodInstantiated() = 0;
    virtual void inputMethodDestroyed() = 0;
    virtual ~InputMethodListener() { }
};

////////////////////////////////////////////////////////////////////////
// InputMethod

class InputMethod
{
    Display* disp;
    XIM im;
    typedef vector<InputMethodListener*> Listeners;
    Listeners listeners;
    bool to_register;
    
public:
    InputMethod();
    virtual ~InputMethod();

    bool open(Display* disp);
    void close();
    XIMStyle selectInputStyle(const XIMStyle* styles) const;
    XIM getXIM() const;
    void attachInputMethodListener(InputMethodListener* l);
    void detachInputMethodListener(InputMethodListener* l);
    bool is_open() const;

private:
    static void onImInstantiate(Display* disp, XPointer client_data, XPointer call_data);
    static void onImServerDestroy(XIM im_, InputMethod* this_, XPointer /*call_data*/);
};

////////////////////////////////////////////////////////////////////////
// XIM VJE-Delta拡張

enum IMExtCmd {
    IMEXT_NIHONGO = 1,    // ??

    IMEXT_ON = 2,         // 日本語モードにする。ユーザーによるモード変更は禁止しない
    IMEXT_OFF = 3,        // 半角英数モード（IMサーバーを素通りするモード）にする。
                        //      ユーザーによるモード変更は禁止しない

    IMEXT_DISABLE = 4,    // IMサーバーを一時的に無効化。他のモードへの遷移はできない。
    IMEXT_ENABLE = 5,     // IMEXT_DISABLEする前の状態に戻す。
                    // nicolatterでは，IMEXT_DISABLE以外が発行された場合は自動的にenableにする。

    IMEXT_ROMA = 6,       // ローマ字入力に変更  nicolatterは実装しない
    IMEXT_KANA = 7,       // かな入力に変更      nicolatterは実装しない

    IMEXT_HIRAGANA = 8,       // ひらがな固定。ユーザーによるモード変更を禁止する。
    IMEXT_KATAKANA = 9,       // カタカナ固定
    IMEXT_FULLWIDTH_ALNUM = 10    // 全角英数固定
                        // 半角英数固定はIMEXT_DISABLEを使う
};

extern void setIMExtValue(Display* disp, Window focus_window, IMExtCmd cmdid);

#endif
