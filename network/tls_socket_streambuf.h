﻿// -*- coding:utf-8-with-signature -*-

#include "socket_streambuf.h"

#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/x509v3.h>

// OpenSSL を利用
// SocketStream::rdbuf() に与える.
class TlsSocketStreamBuf: public SocketStreamBuf
{
    // 参照.
    SSL_CTX* m_ctx;

    // 所有. connected() メソッド内で設定する.
    SSL* m_ssl;

    typedef SocketStreamBuf super;
  
public:
    TlsSocketStreamBuf(SSL_CTX* ctx): m_ctx(ctx), m_ssl(nullptr) {
        assert(ctx);
    }

    virtual ~TlsSocketStreamBuf() {
        close();
    }


    /**
     * SocketStream::open() から呼び出される.
     * TLS の事前設定を行う。
     * HTTP/2 ではハンドシェイクの前に ALPN 設定が必要のため、ここで
     * SSL_connect() (ハンドシェイク) しない。呼び出し側が行うこと。
     */
    virtual void connected( SOCKET sock, const char* host ) {
        assert( sock != -1 );
        assert( host && *host );
        assert( m_ssl == nullptr ); // TODO: 例外を投げる?
        
        super::connected(sock, host);

        m_ssl = SSL_new(m_ctx);
        if ( !m_ssl ) {
            ERR_print_errors_fp(stderr);
            return; // TODO: 例外投げる
        }
        SSL_set_fd(m_ssl, sock);

        // ホスト名の検証を行う. デフォルトでは有効になっていない。必須.
        SSL_set_hostflags(m_ssl, X509_CHECK_FLAG_NO_PARTIAL_WILDCARDS);
        if ( !SSL_set1_host(m_ssl, host) ) {
            ERR_print_errors_fp(stderr);
            return ; // TODO: 例外投げる
        }

        // For SNI
        // これで, Subject: C=US, ST=CA, L=San Francisco, O=Cloudflare, Inc., CN=uservoice.com
        // 呼び出さないときは Subject: CN=ssl764794.cloudflaressl.com
        // => X509v3 Subject Alternative Name (SAN) でカバーされているので,
        //    ホスト名検証を有効にしても, 接続はできる.
        // 現代では必須.
        if ( !SSL_set_tlsext_host_name(m_ssl, host) ) {
            ERR_print_errors_fp(stderr);
            return ; // TODO: 例外投げる
        }
    }

    SSL* ssl() const {
      return m_ssl;
    }


    // @return If failed, -1.
    virtual int close() {
        if (m_ssl) {
            // SSL/TLS接続をシャットダウンする。
            SSL_shutdown(m_ssl);
            SSL_free(m_ssl);
            m_ssl = nullptr;
        }
        return super::close();
    }
  
protected:
    virtual ssize_t sysread(void* buffer, size_t length)
    {
        assert(m_ssl);

        while (true) {
            int r = SSL_read(m_ssl, buffer, length);
            if (r > 0)
                return r;

            int err = SSL_get_error(m_ssl, r);
            switch (err)
            {
            case SSL_ERROR_WANT_READ:
                continue;
            default:
                throw err; // TODO: SSL_ERROR_SYSCALL の場合, errno.
            }
        }
    }


    // Writes characters from the array pointed to by buffer into the
    // controlled output sequence.
    virtual std::streamsize xsputn( const char_type* buffer,
                                    std::streamsize length )
    {
        if (!is_open())
            return 0;
        
        assert( m_ssl );

        while (true) {
            int r = SSL_write(m_ssl, buffer, length);
            if (r > 0)
                return r;
        
            int err = SSL_get_error(m_ssl, r);
            switch (err)
            {
            case SSL_ERROR_WANT_WRITE:
                continue;
            default:
                throw err; // TODO: SSL_ERROR_SYSCALL の場合, errno.
            }
        }
    }
};


// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
