
- Unicode 対応.
  xmltest/valid/sa/049.xml: should accept but failed.
  xmltest/valid/sa/050.xml: should accept but failed.
  xmltest/valid/sa/051.xml: should accept but failed.
 Flex では対応できない。手書きに書き直さないといけない.

- namespace

- node::parent


<s>実体参照 entity reference を展開する際に, 再帰した場合にエラーにしなければならない。</s> 済み. See <code>XmlLexer::expand_ref()</code>
  xmltest/not-wf/sa/071.xml
