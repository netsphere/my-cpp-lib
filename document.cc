// Q's C++ Library
// Copyright (c) 1998-1999 Hisashi HORIKAWA. All rights reserved.

#include <cstdio>
#include <cassert>
#include <algorithm>

#include "document.h"
#include "dialog.h"

using namespace std;

//////////////////////////////////////////////////////////////////////
// Document

Document::Document(): modified(false)
{
}

Document::~Document()
{
}

bool Document::isModified() const
{
    return modified;
}

void Document::setModified(bool m)
{
    modified = m;
}

void Document::updateTitle() const
{
    Conts::const_iterator i;
    for (i = conts.begin(); i != conts.end(); i++)
        (*i)->updateTitle(getTitle(*i));
}

string Document::getTitle(Controller* cont) const
{
    string title;
    if (filename != "") {
        string home = getHomeDir();
        if (!strncmp(filename.c_str(), home.c_str(), home.length()))
            title = "~" + string(filename, home.length());
        else
            title = filename;
    }
    else
        title = "(noname)";

    if (cont && conts.size() > 1) {
        Conts::const_iterator i = find(conts.begin(), conts.end(), cont);
        if (i != conts.end()) {
            char buf[10];
            sprintf(buf, "%d", (i - conts.begin()) + 1);
            title += string(":") + buf;
        }
    }
    if (modified)
        title += " *";
    return title;
}

void Document::attachController(Controller* cont)
{
    cont->document = this;
    if (find(conts.begin(), conts.end(), cont) == conts.end())
        conts.push_back(cont);
}

void Document::detachController(Controller* cont)
{
    Conts::iterator i = find(conts.begin(), conts.end(), cont);
    if (i != conts.end())
        conts.erase(i);
    cont->document = NULL;
}

int Document::countControllers() const
{
    return conts.size();
}

//////////////////////////////////////////////////////////////////////
// Controller

Controller::Controller(): frame(NULL), document(NULL)
{
}

Controller::~Controller()
{
}

void Controller::attachWindow(GtkWindow* f)
{
    frame = f;
}

Document* Controller::getDocument() const
{
    return document;
}

bool Controller::saveAs()
{
    assert(document);

    FileDialog dlg(frame, "名前を付けて保存", document->filename.c_str());
    if (dlg.setVisible(true) == FileDialog::IDOK) {
        document->filename = dlg.getFile();
        return document->save();
    }
    else
        return false;
}

bool Controller::save()
{
    assert(document);
    if (!document->isModified())
        return true;

    if (document->filename == "")
        return saveAs();
    else
        return document->save();
}

bool Controller::close()
    // ドキュメントが変更されていれば，保存するか尋ね，保存する。
    // 戻り値
    //      true: ドキュメントは閉じられた。終了処理を続行して良い
    //      false: エラーが発生したか，ユーザーによってキャンセルされた。
{
    assert(document);
    if (!document->isModified() || document->countControllers() != 1)
        return true;

    SaveConfirm dlg(frame);
    int r = dlg.setVisible(true);
    switch (r)
    {
    case SaveConfirm::IDSAVE:
        return save();
    case SaveConfirm::IDSAVEAS:
        return saveAs();
    case SaveConfirm::IDDISCARD:
        return true;
    default:
        return false;
    }
}

