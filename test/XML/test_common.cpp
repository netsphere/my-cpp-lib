
#include <sstream>
#include <assert.h>
#ifndef _WIN32
  #include <sys/stat.h>
  #include <fcntl.h>
  #include <sys/mman.h> // mmap()
  #include <unistd.h> // close()
#endif

using namespace std;


// @return 新しく生成した stringstream. 呼出し側が delete すること。
//         If error, nullptr.
stringstream* new_stringstream_from_file(const char* filename, string* enc)
{
    assert(filename);
    assert(enc);

    int fd = open(filename, O_RDONLY);
    if (fd < 0)
        return nullptr;
    
    struct stat sb;
    if ( fstat(fd, &sb) < 0 ) {
        close(fd);
        return nullptr;
    }
    if (sb.st_size == 0) {
        close(fd);
        return new stringstream("");
    }

#ifndef _WIN32
    unsigned char* const addr = (unsigned char*) mmap(nullptr, sb.st_size, PROT_READ,
                                    MAP_PRIVATE,
                                    fd, 0);
    if (addr == MAP_FAILED) {
        close(fd);
        return nullptr;
    }
#else
    #error TODO: impl.
#endif

    // Detect encoding
    int skip = 0;
    if (*addr == 0xef && *(addr + 1) == 0xbb && *(addr + 1) == 0xbf) {
      // UTF-8
      *enc = "UTF-8";
      skip = 3;
    }
    else if (*addr == 0xff && *(addr + 1) == 0xfe) { // With BOM
      // UTF-16 little-endian
      *enc = "UTF-16LE";
      skip = 2;
    }
    else if (*addr == 0x3c && *(addr + 1) == 0x00) {
      // UTF-16 little-endian
      *enc = "UTF-16LE";
    }
    else if (*addr == 0xfe && *(addr + 1) == 0xff) { // With BOM
      // UTF-16 big-endian
      *enc = "UTF-16BE";
      skip = 2;
    }
    else if (*addr == 0x00 && *(addr + 1) == 0x3c) {
      // UTF-16 big-endian
      *enc = "UTF-16BE";
    }
    else
        *enc = "UTF-8";

    stringstream* ret = new stringstream(string((char*) (addr + skip), sb.st_size - skip));

#ifndef _WIN32
    munmap(addr, sb.st_size);
#else
    #error TODO: impl.
#endif

    return ret;
}


// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
