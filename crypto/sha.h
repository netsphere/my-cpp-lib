#ifndef SHA_H
#define SHA_H

/* NIST Secure Hash Algorithm */
/* heavily modified by Uwe Hollerbach <uh@alumni.caltech edu> */
/* from Peter C. Gutmann's implementation as found in */
/* Applied Cryptography by Bruce Schneier */

/* This code is in the public domain */

#include <stdint.h>
#include <string>

#define SHA_BLOCKSIZE       64
#define SHA_DIGESTSIZE      20

//////////////////////////////////////////////////////////////////////////
// SHA1Hash

class SHA1Hash
{
    uint32_t digest[5];     /* message digest */
    uint32_t count_lo, count_hi;    /* 64-bit bit count */
    uint8_t data[SHA_BLOCKSIZE];    /* SHA data buffer */
    int local;          /* unprocessed amount in data */
public:
    SHA1Hash();
    virtual ~SHA1Hash();
    void init();
    void update(const uint8_t* input, size_t count);
    void final(uint8_t* digest);    // digest[20]
    std::string version() const;
    static std::string dig2str(const uint8_t* digest);

private:
    void transform();
};

#endif /* SHA_H */
