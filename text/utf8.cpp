﻿// -*- coding:utf-8-with-signature -*-
// Q's C++ Library
// Copyright (c) 1996-2001 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

// RFC 3629 (Nov 2003) UTF-8, a transformation format of ISO 10646
// obsoletes RFC 2279.
//
// Restricted the range of characters to 0000-10FFFF (the UTF-16 accessible
// range).

#ifndef _WIN32
typedef unsigned char BYTE;
#else
  #include <windows.h>
#endif

#include "utf8.h"
#include <stdlib.h>
#include <assert.h>
#include <errno.h>
#include <inttypes.h>
#include <string>
#include <string.h> // strnlen()
using namespace std;


// @param dest NULL でもよい.
size_t u32c_to_utf8( char* dest, char32_t wc, mbstate_t* ps )
{
    // wc == 0 の場合も同じでよい.
    if ( wc <= 0x7f ) {    // 1バイト
        if (dest)
            *dest = (char) wc;
        return 1;
    }
    if ( wc <= 0x7ff) {    // 2バイト
        if (dest) {
            *dest++ = (wc >> 6) | 0xc0;
            *dest++ = (wc & 0x3f) | 0x80;
        }
        return 2;
    }
    if ( wc <= 0xffff) {   // 3バイト
        // UCS にサロゲートペア領域あれば不正
        if ( wc >= 0xD800 && wc <= 0xDFFF) {
            errno = EILSEQ;
            return (size_t) -1;
        }
        if (dest) {
            *dest++ = (wc >> 12) | 0xe0;
            *dest++ = ((wc >> 6) & 0x3f) | 0x80;
            *dest++ = (wc & 0x3f) | 0x80;
        }
        return 3;
    }
    if ( wc <= 0x10ffff) {  // 4バイト. UTF-16の範囲に限定.
        if (dest) {
            *dest++ = (wc >> 18) | 0xf0;
            *dest++ = ((wc >> 12) & 0x3f) | 0x80;
            *dest++ = ((wc >>  6) & 0x3f) | 0x80;
            *dest++ = (wc & 0x3f) | 0x80;
        }
        return 4;
    }

    errno = EILSEQ;
    return (size_t) -1;
}


// @param dest NULL でもよい. この場合, dest_len は無視される.
size_t utf16_to_utf8( char* dest, const char16_t** src_ptr, size_t dest_len,
                      mbstate_t* ps )
{
    assert(src_ptr);

    const char16_t*& src = *src_ptr;  // 以下のコードを簡便にする.
    size_t count = 0;
    if (!dest) {
        string s;
        dest_len = s.max_size() - 1;
    }

    // 冒頭のみ BOM 確認.
    if (*src == 0xfeff)
        src++;  // count は増やさない.

    while (count < dest_len) {
        if (!*src) {
            if (dest)
                dest[count] = '\0';
            src = NULL;  // 完了.
            break;
        }

        char32_t wc; size_t wclen;
        if (*src >= 0xd800 && *src <= 0xdbff) {
            // サロゲートペア
            if (*(src + 1) < 0xdc00 || *(src + 1) > 0xdfff) {
                src++;
                errno = EILSEQ;
                return (size_t) -1;
            }
            wc = 0x10000 + (*src - 0xd800) * 0x400 + (*(src + 1) - 0xdc00);
            wclen = 2;
        }
        else {
            wc = *src;
            wclen = 1;
        }

        // 変換不能があるので, 毎回調べる
        size_t n = u32c_to_utf8(NULL, wc, ps);
        if (n == (size_t) -1)
            return -1;
        if (count + n > dest_len)
            return count;

        count += dest ? u32c_to_utf8(dest + count, wc, ps) : n;
        src += wclen;
    }

    return count;
}


/**
 * 妥当なバイト列は, 微妙に範囲がある. U+10FFFF より上とサロゲートペアを先に弾
 * く.
 * @src バイト列. 途中で切れていない (中途半端なバイト列がない) と仮定する.
 * @return true OK
 *
   Char. number range  |        UTF-8 octet sequence
      (hexadecimal)    |              (binary)
   --------------------+---------------------------------------------
   0000 0000-0000 007F | 0xxxxxxx
   0000 0080-0000 07FF | 110xxxxx 10xxxxxx     除くC0, C1
   0000 0800-0000 FFFF | 1110xxxx 10xxxxxx 10xxxxxx
   0001 0000-0010 FFFF | 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx
     U+10FFFF => F4 8F BF BF
サロゲートペアの code unit を誤ってUTF-8 にすると
  D800..DBFF  => ED A0 80 ..  => この関数で弾く.
  DC00..DFFF  => ED B0 80 ..
 */
static inline bool conv(const BYTE* src, int length, char32_t* ch)
{
    BYTE b;
    const BYTE* p = src;

    switch (length) {
    case 1:
        // Lead byte. '\0' でもよい
        *ch = *p;
        return true;

    case 2:
        *ch = *p++ & 0x1F;
        if ( (b = *p) < 0x80 || b > 0xBF ) return false;
        *ch = (*ch << 6) | (b & 0x3F);
        return true;

    case 3:
        *ch = *p++ & 0x0F;
        switch (*src) {
        case 0xE0: // UTF8-3      = %xE0 %xA0-BF UTF8-tail /
            if ( (b = *p++) < 0xA0 || b > 0xBF ) return false;
            break;
        case 0xED: //               %xED %x80-9F UTF8-tail
            if ( (b = *p++) < 0x80 || b > 0x9F ) return false;   // サロゲートペアを誤って符号化
            break;
        default:
            if ( (b = *p++) < 0x80 || b > 0xBF ) return false;
            break;
        }
        *ch = (*ch << 6) | (b & 0x3F);
        if ( (b = *p) < 0x80 || b > 0xBF ) return false;
        *ch = (*ch << 6) | (b & 0x3F);
        return true;

    case 4:
        *ch = *p++ & 0x07;
        switch (*src) {
        case 0xF0:
            if ( (b = *p++) < 0x90 || b > 0xBF ) return false;
            break;
        case 0xF4:
            if ( (b = *p++) < 0x80 || b > 0x8F ) return false;   // サロゲートペアを誤って符号化
            break;
        default:
            if ( (b = *p++) < 0x80 || b > 0xBF ) return false;
            break;
        }
        *ch = (*ch << 6) | (b & 0x3F);
        if ( (b = *p++) < 0x80 || b > 0xBF ) return false;
        *ch = (*ch << 6) | (b & 0x3F);
        if ( (b = *p) < 0x80 || b > 0xBF ) return false;
        *ch = (*ch << 6) | (b & 0x3F);
        return true;

    default:
        return false;
    }

    return false;  // ここには来ない
}


static const signed char trailingBytesForUTF8[256] = {
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, // 0x 1x
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, // 2x 3x
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    -2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2, // 8x
    -2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2, // 9x
    -2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2, // Ax
    -2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2, // Bx
    -2,-2,1,1,1,1,1,1,1,1,1,1,1,1,1,1, 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, // C0 は2バイト
    2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2, 3,3,3,3,3,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2
};


// @param pwc NULL でもよい.
// @param src 途中で切れているバイト列はないと仮定する.
// @return 不正なバイト列のとき (size_t) -1
static inline size_t utf8_to_u32c__(char32_t* pwc, const BYTE* src)
{
    int extra_bytes = trailingBytesForUTF8[*src];

    if (*src == '\0') {
        if (pwc) *pwc = 0;
        return 0;
    }

    // 全部展開する
    char32_t ch = 0;
    bool legal = conv(src, extra_bytes + 1, &ch);
    if ( !legal ) {
        errno = EILSEQ; return (size_t) -1;
    }
    if (pwc) *pwc = ch;

    return extra_bytes + 1;
}


// UTF-8 バイト列が途中で切れていないか確認
// @param maxlen 検査する最大バイト.
static const BYTE* check_src_tail(const BYTE* beg, size_t maxlen)
{
    const BYTE* end = beg + strnlen((const char*) beg, maxlen);
    const BYTE* cur = end;

    while ( beg < cur && ((*--cur) & 0xC0) == 0x80 ) ;
    // cur は lead byte を指す
    if ( trailingBytesForUTF8[*cur] >= 0 &&
                    cur + trailingBytesForUTF8[*cur] + 1 > end )
        return cur;
    else
        return end;  // 不正なバイトは、切れている訳ではない.
}


size_t utf8_to_u32c(char32_t* pwc, const char* src, size_t src_n, mbstate_t* ps)
{
    assert(src);

    const BYTE* end = check_src_tail((const BYTE*) src, src_n);
    if (src == (const char*) end)
        return (size_t) -2;

    return utf8_to_u32c__(pwc, (const BYTE*) src);
}


// @param pwcs NULL でもよい. この場合, dest_len は無視される.
size_t utf8_to_utf16( char16_t* pwcs, const char** src_ptr, size_t dest_len,
                      mbstate_t* ps )
{
    assert(src_ptr);

    size_t count = 0;
    const BYTE*& src = *(const BYTE**) src_ptr;  // 以下のコードを簡便にするため.
    char16_t* p = pwcs;

    if ( !pwcs ) {
        u16string s;
        dest_len = s.max_size() - 1;
    }

    // 途中でバイト列が切れていないか確認
    const BYTE* end = check_src_tail(src, dest_len * 4 + 3);

    // 冒頭のみ BOM 確認.
    if (src[0] == 0xEF && src[1] == 0xBB && src[2] == 0xBF)
        src++;  // count は増やさない.

    while (count < dest_len) {
        if (!*src) {
            if (pwcs) *p = 0;
            src = NULL;  // 完了.
            break;
        }
        if ( src == end ) { // 中途半端に src が終わった
            if (pwcs) *p = 0;
            break;
        }

        char32_t wc = 0;
        size_t n = utf8_to_u32c__(&wc, src);
        if (n == (size_t) -1) {
            if (pwcs) *p = 0;  // こうしないとどこまで書き込んだか不明.
            return -1;
        }

        // サロゲートペアにするとバッファに入らないかも.
        if (wc > 0xffff) {
            if (count + 2 > dest_len)
                break;
            if (pwcs) {
                *p++ = (wc - 0x10000) / 0x400 + 0xd800;
                *p++ = ((wc - 0x10000) % 0x400) + 0xdc00;
            }
            count += 2;
        }
        else {
            if (pwcs)
                *p++ = wc;
            count++;
        }
        src += n;
    }

    return count;
}


// mbrtowc(NULL, str, src_n, ps ? ps : &internal) と同じ.
size_t utf8len( const char* str, size_t src_n, mbstate_t* ps )
{
    assert(str);
    return utf8_to_u32c(NULL, str, src_n, ps);
}



// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
