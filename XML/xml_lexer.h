﻿// -*- mode:c++; coding:utf-8-with-signature -*-

// Q's C++ Library
// Copyright (c) 1996-1998 Hisashi Horikawa. All rights reserved.

#ifndef QSLIB_XML_LEXER_H
#define QSLIB_XML_LEXER_H

#ifndef __FLEX_LEXER_H
  #define yyFlexLexer XmlImplFlexLexer
  #include <FlexLexer.h>
  #undef yyFlexLexer
#endif

#include <string>
#include "xml.tab.hh"
#include "node.h"
#include <sstream>
#include <map>
#include <vector>
#include <set>


////////////////////////////////////////////////////////////////////////////
// XmlLexer

// LexerError() のなかから行番号、列番号が取得できない。マクロを書き換える。
#define YY_FATAL_ERROR(msg)  fprintf(stderr, "%d:%d: %s\n", YY_CURRENT_BUFFER->yy_bs_lineno, YY_CURRENT_BUFFER->yy_bs_column, msg)

namespace yy {

class XmlParser;

class XmlLexer: public XmlImplFlexLexer
{
    // 型 //////////////////////////////////////////////
public:
    typedef yytokentype tokentype;
    typedef XmlImplFlexLexer super;

    // Bison 側で自動的に宣言される. yylex() の引数として渡してもらう.
    //YYLTYPE location;

    static constexpr unsigned int EXPAND_CHAR_REF = 1;
    static constexpr unsigned int EXPAND_ENTITY_REF = 2;
    //static constexpr unsigned int EXPAND_PE_REF = 4;
    static constexpr unsigned int EXPAND_ATTR_NORM = 8;

    // 属性 ////////////////////////////////////////////
public:
    // マクロ. DOM には格納しない.
    EntityMap param_entities;

private:
    std::string buffer;
    int sub_state;
    EntityMap* _general_entities;

    // yyin が istream インスタンスのため, 別に持つ.
    std::stringstream* _inbuf;
    int _pos;

    typedef std::set<std::string, ci_less> EntityStack;
    EntityStack _entity_stack;

    // メソッド ////////////////////////////////////////
public:
    // 'strstream' has been deprecated since C++98.
    // @param arg_yyin XmlLexer クラスが所有し, 解放する。呼び出し側で解放して
    //                 はならない.
    XmlLexer( std::stringstream* arg_yyin, EntityMap* general_entities ):
        super(arg_yyin, nullptr),
        _general_entities(general_entities), _inbuf(arg_yyin), _pos(0)
    {
        assert(arg_yyin);
        assert(general_entities);

        _general_entities->insert(std::make_pair("lt",   "&#60;"));
        _general_entities->insert(std::make_pair("gt",   "&#62;"));
        _general_entities->insert(std::make_pair("amp",  "&#38;"));
        _general_entities->insert(std::make_pair("apos", "&#39;"));
        _general_entities->insert(std::make_pair("quot", "&#34;"));
    }

    virtual ~XmlLexer() {
        delete _inbuf;
    }

    virtual tokentype yylex( YYSTYPE* yylval,
                             YYLTYPE* yylloc );

protected:
    // @override
    // バッファへの読み込み. アクション呼び出しとは異なるタイミング.
    //virtual int LexerInput( char* buf, int max_size );

    // @override
    //virtual void LexerError( const char* msg );

    // character reference (&#178;) and general entity (&lt;)
    std::string* expand_ref(const char* text, int len,
                            unsigned int targets );

    void replace_buf(int orig_len, const std::string& str);

private:
    XmlLexer(const XmlLexer& );                 // not implement
    XmlLexer& operator = (const XmlLexer& );    // not implement
};

}; // namespace yy


#ifndef _WIN32
// _strlwr() は非推奨.
// @param num 入力の最大バイト数.
inline int _strlwr_s(char* str, size_t num)
{
    char* p = str;
    for (p = str; *p && p < str + num; p++)
        *p = tolower(*p);

    return 0;
}
#endif

#endif // !QSLIB_XML_LEXER_H

// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
