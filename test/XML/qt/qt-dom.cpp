
// Qt5

// 次のように起動しないとデバッグメッセージが出力されない.
// a) QT_LOGGING_RULES="*=true" build-qt-dom-print-Desktop-Debug/qt-dom-print
// b) ~/.config/QtProject/qtlogging.ini ファイル:
//      [Rules]
//      *.debug=true
// c) グローバルな /usr/share/qt5/qtlogging.ini ファイル

#include <QCoreApplication>
#include <QFile>
#include <QtXml/QtXml>

int main(int argc, char* argv[])
{
    // 非GUIでイベントループを生成.
    QCoreApplication a(argc, argv);

    QFile file(argv[1]);
    file.open(QFile::ReadOnly | QFile::Text);

    QDomDocument dom;
    dom.setContent(&file);

    QString s = dom.toString();
    qDebug() << s;  // 末尾の改行は不要
    
    return 0;  // a.exec();
}
