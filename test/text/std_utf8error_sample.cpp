
// UTF-8 として不正なバイト列の場合の挙動

// How to build:
//   $ gcc -g -Wall -Wextra utf8error_test.cpp -lstdc++

// 切り詰めるのはセキュリティ上問題. 0 や "?" など特別な意味がありそうな文字へ
// の置換も不味い.
// よって, 次のいずれかであること.
//   - 例外を投げる
//   - 意味がなさそうな文字, 例えば "〓" などへの置換.

// 結果:
// codecvt_utf8<> は、デフォルトでは, 単に全体が失敗する。badbit が立ち,
// gcount() = 0
// basic_ios::exceptions(bits) メソッドで、例外を発生させることもできる。
// どこでエラーになったかは分からない。これはダメだ...

// ★設計
// UTF-8ファイルから単に読み込む、という場合は、これで十分。ただし、それにして
// は大げさすぎる
// 他方, ソケットから読み込んだものを UTF-16 に変換していく、のように異なるリ
// ソースを使いたい場合, streambuf のほうにそのリソースからの読み込みと変換の
// 両方を実装しなければならず、根本的にデザインが間違っている。


#include <fstream>
#include <assert.h>
#include <codecvt>
using namespace std;

typedef basic_ifstream<char32_t > u32ifstream;  // UCS-4
typedef basic_ofstream<char32_t > u32ofstream;  // UCS-4

void prepare_file()
{
    // Broken UTF-8 data
    // From https://www.cl.cam.ac.uk/~mgk25/ucs/examples/
}

constexpr int MYBUFSIZ = 1000000;
char32_t buf[MYBUFSIZ];    // wchar_t と char32_t は互換性なし. Good!


void test_utf8(const string& filename)
{
    u32ifstream ifs;
    assert( sizeof(decltype(ifs)::char_type) == 4 );
    // ios_base::imbue() .. なのに変換の実装は filebuf?
    ifs.imbue(std::locale(std::locale(), new codecvt_utf8<char32_t>()));

    ifs.open(filename, ios::binary);
    ifs.exceptions(ios::failbit | ios::badbit);

    printf("state = %x\n", ifs.rdstate());
    ifs.read(buf, MYBUFSIZ);
    printf("gcount = %ld\n", ifs.gcount());
    buf[ifs.gcount()] = 0;
    printf("state = %x\n", ifs.rdstate());  // badbit が立つ

    // 単に書き出す
    u32ofstream ofs(filename + ".out");
    ofs.imbue(std::locale(std::locale(), new codecvt_utf8<char32_t>()));
    ofs.write(buf, u32string(buf).length());
    ofs.close();
}

int main(int argc, char* argv[])
{
    test_utf8(argv[1]);
}
