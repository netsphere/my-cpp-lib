// Q's C++ Library
// Copyright (c) 1998-1999 Hisashi HORIKAWA. All rights reserved.

#include <config.h>

#include <clocale>
#include <string>
#include <cstdio>
#include <cassert>
#include <gtk/gtk.h>
#include <gdk/gdkx.h>

#include "misc-gtk.h"
#include "misc.h"

using namespace std;

//////////////////////////////////////////////////////////////////////

bool q_rc_parse(const char* /*filename*/)
    // 次の順でリソースファイルを検索し，取り込む。
    //      1. $HOME/$LANG/filename
{
    const char* locale = setlocale(LC_ALL, NULL);   // 問い合わせるだけ
    if (!strcmp(locale, "C")) {
        g_error("ロケールが設定されていません\n");
        exit(1);
    }

    string file = getHomeDir() + "/" + ".gtkrc";
    gtk_rc_parse(file.c_str());
    return true;
}

void q_gtk_init(int* pargc, char*** pargv)
{
    gtk_set_locale();
    gtk_init(pargc, pargv);
    q_rc_parse("gtkrc");
}

XFontSet GDK_FONT_XFONTSET(GdkFont* font)
{
    assert(font && font->type == GDK_FONT_FONTSET);
    GdkFontPrivate* priv = (GdkFontPrivate*) font;
    return (XFontSet) priv->xfont;
}

GtkWidget* createDropDownListBox()
{
    GtkCombo* combo = GTK_COMBO(gtk_combo_new());
    gtk_entry_set_editable(GTK_ENTRY(GTK_COMBO(combo)->entry), FALSE);
    gtk_combo_set_use_arrows_always(GTK_COMBO(combo), TRUE);

    // gtk_widget_set_usize(combo->list, 10, 150);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(combo->popup),
                            GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);
    return GTK_WIDGET(combo);
}

GtkWidget* getWindowWidget(GtkWidget* widget)
{
    while (true) {
        GtkWidget* parent;
        if (GTK_IS_MENU(widget))
            parent = gtk_menu_get_attach_widget(GTK_MENU(widget));
        else
            parent = widget->parent;
        if (!parent)
            break;
        widget = parent;
    }
    return widget;
}

/*
bool GtkTextLoad(GtkText* text, const string& filename)
{
    FILE* fp = fopen(filename.c_str(), "r");
    if (!fp) {
        error("File not found: '%s'\n", filename.c_str());
        return false;
    }

    gtk_text_freeze(text);
    gtk_editable_delete_text(GTK_EDITABLE(text), 0, -1);

    char buf[1000];
    while (fgets(buf, sizeof(buf), fp) != NULL) {
        gtk_text_insert(text, NULL,
                        &GTK_WIDGET(text)->style->black, NULL,
                        buf, strlen(buf));
        // TODO: 色，フォントなどを読めるようにする
    }

    fclose(fp);
    gtk_text_thaw(text);
    return true;
}

bool GtkTextSave(GtkText* text, const string& filename)
{
    FILE* fp = fopen(filename.c_str(), "w");
    if (!fp) {
        error("File not found: '%s'\n", filename.c_str());
        return false;
    }

    gtk_text_freeze(text);

    char* const buf = gtk_editable_get_chars(GTK_EDITABLE(text), 0, -1);
    if (buf) {
        fwrite(buf, 1, strlen(buf), fp);
        g_free(buf);
    }
    gtk_text_thaw(text);
    
    if (fclose(fp)) {
        error("save error: '%s'\n", filename.c_str());
        return false;
    }
    return true;
}
*/

int getSelectedIndex(GtkOptionMenu* option_menu)
{
    GtkWidget* menu = gtk_option_menu_get_menu(option_menu);
    return g_list_index(GTK_MENU_SHELL(menu)->children,
                    option_menu->menu_item);
}

////////////////////////////////////////////////////////////////////////
// Date

Date::Date()
{
    date = g_date_new();
}

Date::Date(int y_, int m_, int d_)
{
    date = g_date_new_dmy(d_, (GDateMonth) m_, y_);
}

Date::Date(int j)
{
    date = g_date_new_julian(j);
}

Date::Date(const Date& a)
{
    date = g_date_new_dmy(a.getDay(), (GDateMonth) a.getMonth(), a.getYear());
}

Date::~Date()
{
    if (date)
        g_date_free(date);
}

int Date::getYear() const
{
    return (int) g_date_year(date);
}

int Date::getMonth() const
{
    return (int) g_date_month(date);
}

int Date::getDay() const
{
    return (int) g_date_day(date);
}

int Date::getJulian() const
{
    return g_date_julian(date);
}

Date Date::addYears(int span) const
{
    Date r(*this);
    if (span > 0)
        g_date_add_years(r.date, span);
    else if (span < 0)
        g_date_subtract_years(r.date, -span);
    return r;
}

Date Date::addMonths(int span) const
{
    Date r(*this);
    if (span > 0)
        g_date_add_months(r.date, span);
    else if (span < 0)
        g_date_subtract_months(r.date, -span);
    return r;
}

Date Date::addDays(int span) const
{
    Date r(*this);
    if (span > 0)
        g_date_add_days(r.date, span);
    else if (span < 0)
        g_date_subtract_days(r.date, -span);
    return r;
}

Date& Date::operator = (const Date& a)
{
    if (this != &a)
        g_date_set_dmy(date, a.getDay(), (GDateMonth) a.getMonth(), a.getYear());
    return *this;
}

