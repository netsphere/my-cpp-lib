﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:8; -*-

#ifndef PTR_VECTOR_H__
#define PTR_VECTOR_H__ 1

#include <vector>
#include <type_traits>
#ifndef NDEBUG
  #include <stdio.h>
#endif
#if __cplusplus >= 201103L
  #include <initializer_list>
#endif

/**
 * 使い方:
 *     ptr_vector<Foo*> var;
 *     var.push_back(new Foo(...));
 *
 * pop_front() はない.
 * 派生にするのは誤り.
 */
template <typename T>
class ptr_vector
{
    static_assert(std::is_pointer<T>::value,
                  "template parameter T must be pointer type");

#if __cplusplus >= 201703L
    typedef std::vector<T, std::pmr::polymorphic_allocator<T> > Container;
#else
    typedef std::vector<T > Container;
#endif
    Container container;

public:
    typedef typename Container::iterator        iterator;
    typedef typename Container::const_iterator  const_iterator;
    typedef typename Container::size_type       size_type;
    typedef typename Container::reference       reference;
    typedef typename Container::const_reference const_reference;
    typedef typename Container::allocator_type  allocator_type;

    // コンストラクタ
#if __cplusplus >= 201103L
    ptr_vector() = default;
#else
    ptr_vector() { }
#endif

    explicit ptr_vector(const allocator_type& alloc) : container(alloc) { }

#if __cplusplus >= 201103L
    ptr_vector(std::initializer_list<T> __l,
               const allocator_type& __a = allocator_type() ): container(__a) {
        for (const T& v: __l)
            push_back(v);
    }
#endif

    virtual ~ptr_vector() { clear(); }

    iterator begin() noexcept { return container.begin(); }
    const_iterator begin() const noexcept { return container.begin(); }

    iterator end() noexcept { return container.end(); }
    const_iterator end() const noexcept { return container.end(); }

#if __cplusplus >= 201103L
    const_iterator cbegin() const noexcept {
        return container.cbegin();
    }

    const_iterator cend() const noexcept {
        return container.cend();
    }
#endif

    size_type size() const noexcept {
        return container.size();
    }

    reference at(size_type n) {
        return container.at(n); // throw out_of_range()
    }

    const_reference at(size_type n) const {
        return container.at(n); // throw out_of_range()
    }

    reference operator [] (size_type n) {
        return container.at(n);
    }

    const_reference operator [] (size_type n) const {
        return container.at(n);
    }

    reference front() {
        assert( size() > 0 );
        return *container.begin();
    }

    const_reference front() const {
        assert( size() > 0 );
        return *container.cbegin();
    }

    reference back() {
        assert( size() > 0 );
        return *(container.end() - 1);
    }

    const_reference back() const {
        assert( size() > 0 );
        return *(container.cend() - 1);
    }

    void push_back(const T& v) {
#ifndef NDEBUG
        if ( v == nullptr )
            printf("warning: add NULL.\n"); // DEBUG
#endif
        container.push_back(v);
    }

/*
    void push_front(const T& v) {
        if ( !v )
            printf("warning: add NULL.\n"); // DEBUG
        super::push_front(v);
    }
 */


    iterator
#if __cplusplus >= 201103L
    erase(const_iterator it)
#else
    erase(iterator it )
#endif
    {
        delete (*it);
        return container.erase(it);
    }

    // コンテナからは削除する
    T detach(iterator it) {
        T r = *it;
        container.erase(it);
        return r;
    }

    iterator
#if __cplusplus >= 201103L
    erase(const_iterator __first, const_iterator __last)
#else
    erase(iterator __first, iterator __last)
#endif
    {
        const_iterator i;
        for (i = __first; i != __last; i++)
            delete *i;
        return container.erase(__first, __last);
    }

    void clear() {
        iterator it;
        for (it = begin(); it != end(); it++)
            delete *it;
        container.clear();
    }
};


#endif // !PTR_VECTOR_H__

// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
