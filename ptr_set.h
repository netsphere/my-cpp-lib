
#ifndef PTR_SET_H
#define PTR_SET_H

// 限定公開継承 (protected inheritance) は, 継承といいながら, "派生クラス has-a
// 基底クラス" 関係になる。基底クラスのポインタに代入することもできない。
// とはいえ,
// TODO: 意図が分かりにくいので, メンバとして持つように修正すること。

/**
 * `std::set` は, 実装が複雑で, 使いどころが少ない. 次のすべてに該当する場合は,
 * `set` が使える;
 *  - NオーダとlogNオーダとの差が顕著になるくらいに要素数が大きいと思われるとき
 *  - 挿入回数が検索回数と同程度もしくはそれ以上である、すなわち挿入に要する時
 *    間を無視できないとき
 *  - 要素の挿入順がランダム（昇順でない）なとき
 *  - 挿入/検索が交互に行われ、挿入フェーズと検索フェーズとに分離できないとき
 * それ以外の (ほとんど多くの) 場合は, sorted vector テクニックがよい.
 * See https://codezine.jp/article/detail/6186
 *
 * 実装例としては `boost::container::flat_set` などがある.
 * See https://www.boost.org/doc/libs/1_80_0/doc/html/container/non_standard_containers.html
 */


////////////////////////////////////////////////////////////////////////
// ptr_set

#include <set>
#include <functional> // std::less

template <typename _Tp>
struct ptr_less //: public std::binary_function<_Tp, _Tp, bool>
{
    constexpr bool operator()(const _Tp& __x, const _Tp& __y) const {
      static_assert(std::is_pointer<_Tp>::value, "must a pointer");
      return *__x < *__y;
    }
};


// `std::set` は単なる集合ではなく, 順序付き集合.
// @param _Key  Hoge* などのポインタ型.
// @param _COMP 比較関数. Default is `ptr_less<_Key>`
template <typename _Key, typename _COMP = ptr_less<_Key> >
class ptr_set :
#if __cplusplus >= 201703L
    protected std::set<_Key, _COMP, std::pmr::polymorphic_allocator<_Key> >
#else
    protected std::set<_Key, _COMP>
#endif
{
    static_assert(std::is_pointer<_Key>::value, "must a pointer");
#if __cplusplus >= 201703L
    typedef std::set<_Key, _COMP, std::pmr::polymorphic_allocator<_Key> > super;
#else
    typedef std::set<_Key, _COMP > super;
#endif

public:
    typedef typename super::iterator iterator;
    typedef typename super::const_iterator const_iterator;
    typedef typename super::size_type size_type;
    typedef typename super::key_type key_type;
    typedef typename super::value_type value_type;
    typedef typename super::allocator_type  allocator_type;

#if __cplusplus >= 201103L
    ptr_set() = default;
#else
    ptr_set() { }
#endif

    explicit ptr_set( const _COMP& __comp,
                      const allocator_type& __a = allocator_type() ) :
        super(__comp, __a) { }

    virtual ~ptr_set() { clear(); }

    virtual void clear() {
        iterator i;
        for (i = super::begin(); i != super::end(); i++)
            delete *i;
        super::clear();
    }

    iterator begin() const noexcept {
        return super::begin();
    }

    iterator end() const noexcept {
        return super::end();
    }

    std::pair<iterator, bool> insert(const value_type& __x) {
        return super::insert(__x);
    }

    virtual size_type erase(const key_type& x) {
        _Key ptr = x;
        size_type retval = super::erase(x);
        delete ptr;
        return retval;
    }

#if __cplusplus >= 201103L
    virtual iterator erase(const_iterator i) {
        _Key ptr = *i;
        iterator r = super::erase(i);
        delete ptr;
        return r;
    }
#else
    virtual void erase(const_iterator i) {
        _Key ptr = *i;
        super::erase(i);
        delete ptr;
    }
#endif
};

#endif // !PTR_SET_H
