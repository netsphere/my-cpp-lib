
#ifndef QSLIB_INPUT_FILTER_H
#define QSLIB_INPUT_FILTER_H

#include <streambuf>

//////////////////////////////////////////////////////////////////////
// InputFilter

/**
 * 標準 C++ は, wistream は wstreambuf と組み合わせることになっている。
 * 多バイト文字のリソースを読み込みながらワイド文字で取り出すのが, wifstream
 * しかできない.
 *
 * <s>wistream メソッドを override しまくり, 他方, istream から読み込んで変換す
 * る.</s>
 *   => ダメ. istream のメソッドは virtual になっていないので, 意味ない.
 * wstreambuf から派生して, istream から読み込むしかなさそう.
 */
template <typename _CharT, typename _Traits = char_traits<_CharT> >
class CodingConvertFilterT: public basic_streambuf<_CharT, _Traits>
{
protected:
    // バイトストリーム. 所有しない.
    std::istream* m_in; 

public:
    CodingConvertFilterT(): m_in(nullptr) { }
  
    CodingConvertFilterT(std::istream* in): m_in(in) { }

    virtual ~CodingConvertFilterT() { }

    // バッファに読み込む.
    virtual int_type underflow() {
        assert( m_in );
        
        ... impl.
        read(char* buf, int size) = 0;
    }

    // @param in 切り替える入力ストリーム。所有しない。呼出し側が解放すること.
    void attach(std::istream* in) {
        m_in = in;
    }
};

// wchar_t は廃れた. 環境依存の大きさでポータブルでない. 使用不可.
typedef CodingConvertFilterT<char16_t> CodingConvertFilter; // UTF-16 

#endif
