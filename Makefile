
# mingw: `gcc` だとリンクエラー
CC = g++

CFLAGS = -Wall -g -I../libs -DDEBUG=1
LDLIBS = -lstdc++

# ダミー
target = 

all: $(target)




ximtest: ximtest.o ../libs/misc.o ../libs/debug.o
	$(CC) -o $@ $^ -L$(XLIBDIR) -lX11 

.c.o: 
	$(CC) -c $< -o $@ $(CFLAGS)
.cc.o:
	$(CC) -c $< -o $@ $(CFLAGS)

clean:
	rm -f $(target) a.out *.o core *~ *.bak *.BAK
	rm -f `find . -name '*.o'`
	cd test; make clean
