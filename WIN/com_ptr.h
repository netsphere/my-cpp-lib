﻿// -*- coding:utf-8-with-signature -*-

#ifndef COM_PTR_H__
#define COM_PTR_H__

#include <type_traits>
#include <assert.h>

#include "safe_release.h"

template<typename _T>
class _RemoveIUnknown : public _T
{
private:
    // 使おうとすると, アクセス違反.
    virtual ULONG STDMETHODCALLTYPE AddRef();
    virtual ULONG STDMETHODCALLTYPE Release();
};


/**
 * C++ COMスマートポインタは, ATL CComPtr と WRL ComPtr の二つある。
 * WRL のほうが新しいが、デキがよくない。
 * また、いずれも, 余計な operator = (T*) があって、バグを作り込みやすい.
 * => Attach(T*) を使わなければならない.
 *
 * ATL CComPtr をベースに作った.
 * 全面的に, 次に基づいた:
 *     Windows と C++: COM のスマート ポインター再考
 *     https://msdn.microsoft.com/ja-jp/magazine/dn904668.aspx
 *
 * ATL CComPtr Class
 *     https://docs.microsoft.com/en-us/cpp/atl/reference/ccomptr-class
 * WRL ComPtr class
 *     https://docs.microsoft.com/en-us/cpp/windows/comptr-class
 */
template <typename _T>
class CComPtr
{
public:
    CComPtr() : m_ptr(nullptr) {
        static_assert(std::is_base_of<IUnknown, _T>::value,
                      "_T must be derived of IUnknown");
        //IUnknown* p = m_ptr; // TEST
    }

    CComPtr( const CComPtr<_T>& lp ) noexcept : m_ptr(lp.m_ptr) {
        if (m_ptr)
            m_ptr->AddRef();
    }

    // ムーブセマンティクス
    CComPtr( CComPtr<_T>&& other ) noexcept : m_ptr(other.m_ptr) {
        other.m_ptr = nullptr;
    }


    // アップキャスト (基底クラスへの代入) を許可する.
    // QueryInterface() ではない.
    //     CComPtr<IHen> hen;
    //     CComPtr<IUnknown> unknown = hen;
    template <typename _Derived>
    CComPtr( const CComPtr<_Derived>& other ) noexcept : m_ptr(other.m_ptr) {
        if (m_ptr)
            m_ptr->AddRef();
    }


    ~CComPtr() noexcept {
        internal_release();
    }


    // 既存のポインタから作る. AddRef() しない.
    void Attach( _T* lp ) noexcept {
        internal_release();
        m_ptr = lp;
    }


    void Swap( CComPtr<_T>& other ) noexcept {
        _T* temp = m_ptr;
        m_ptr = other.m_ptr;
        other.m_ptr = temp;
    }


    // コピー
    CComPtr<_T>& operator =(const CComPtr<_T>& lp) noexcept {
        if (m_ptr != lp.m_ptr)
            CComPtr(lp).Swap(*this); // 先に加算する
        return *this;
    }

    // 基底クラスへの代入.
    template <typename _Derived>
    CComPtr<_T>& operator =(const CComPtr<_Derived>& other ) noexcept {
        if (m_ptr != other.m_ptr)
            CComPtr(other).Swap(*this);
        return *this;
    }


    // ムーブ
    CComPtr<_T>& operator =( CComPtr<_T>&& other ) noexcept {
        if (m_ptr != other.m_ptr) {
            internal_release();
            m_ptr = other.m_ptr;
            other.m_ptr = nullptr;
        }
        return *this;
    }


    // ATL では operator _T*()
    _T* Get() const noexcept {
        return m_ptr;
    }


    _T* Detach() noexcept {
        _T* temp = m_ptr;
        m_ptr = nullptr;
        return temp;
    }


    // WRL ComPtr::GetAddressOf Method
    _T** operator &() {
        // QueryInterface 以外で operator &()が使われることはない。
        assert(!m_ptr);  
        return &m_ptr;
    }

    _RemoveIUnknown<_T>* operator ->() const {
        assert(m_ptr);
        return static_cast<_RemoveIUnknown<_T>*>(m_ptr);
    }

    explicit operator bool() const noexcept {
        return m_ptr != nullptr;
    }
  
    bool operator !() const noexcept {
        return !m_ptr;
    }

    // ATL では QueryInterface() メソッド.
    template <typename _U>
    HRESULT As( _Out_ CComPtr<_U>* new_ref) const {
        assert(m_ptr);
        if ( !new_ref )
            return E_INVALIDARG;
        new_ref->m_ptr = nullptr;
        return m_ptr->QueryInterface(__uuidof(_U), &new_ref->m_ptr);
    }

private:
    _T* m_ptr;

    void internal_release() noexcept {
        safe_release(&m_ptr);
    }
};

template <typename _T>
void swap( CComPtr<_T>& left, CComPtr<_T>& right ) noexcept {
    left.Swap(right);
}

#endif // !COM_PTR_H__
