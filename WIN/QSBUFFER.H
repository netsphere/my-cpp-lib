// Q's C++ Library
// Copyright (c) 1996-1998 Hisashi Horikawa. All rights reserved.

#ifndef QS_BUFFER
#define QS_BUFFER

////////////////////////////////////////////////////////////////////////////
// CBuffer

template <class T>
class CBuffer
    // CBufferクラスは軽い可変サイズ配列を提供する。
{
    DWORD nSize;        // 現在の要素数
    DWORD nMaxSize;     // 最大要素数

public:
    T* p;

    CBuffer(DWORD m = 1000000 / sizeof(T)): nMaxSize(m), nSize(0)
        // 入力
        // m = 要素数。指定がないときは，1MB確保する
    {
        p = (T*) ::VirtualAlloc(NULL, nMaxSize * sizeof(T),
                                MEM_RESERVE | MEM_TOP_DOWN, PAGE_READWRITE);
    }

    virtual ~CBuffer()
    {
        if (p != NULL)
            ::VirtualFree(p, 0, MEM_RELEASE);
    }

    T* Alloc(DWORD nNeedNum)
        // 要素数nNeedNumだけのメモリを確保する。確保できなかったときはNULLを返す。
    {
        if (p == NULL || nNeedNum > nMaxSize)
            return NULL;

        if (nNeedNum > nSize) {
            if (!VirtualAlloc(p, nNeedNum * sizeof(T), MEM_COMMIT, PAGE_READWRITE))
                return NULL;
        }
//      else {
//          VirtuelAlloc(p + rlen, nSize - rlen, MEM_DECOMMIT);
//          開放はページ単位で行われる（開放されすぎる）ので，このコードではうまく行かない
//      }

        nSize = nNeedNum;
        return p;
    }

    void Replace(UINT index, UINT oldlen, const char* string)
        // 機能
        //      indexからoldlenバイトをstringで置き換える。
        // 入力
        //      index: 0から始まるインデックス
        //      string: NULL終端。長さはlenでなくてもよい。
    {
        ASSERT(index + oldlen <= nSize);

        int newlen = strlen(string);
        UINT needsize = index + newlen;
        int oldbufsize = nSize;
        if (needsize > nSize)
            Alloc(needsize);

        memmove(p + index + newlen, p + index + oldlen, oldbufsize - index - oldlen); // まず元のデータを移動する
        memcpy(p + index, string, newlen);  // strcpy()はNul文字を書き込んでしまう
    }

private:
    CBuffer(const CBuffer&);                // not impl
    CBuffer& operator = (const CBuffer&);   // not impl
};

////////////////////////////////////////////////////////////////////////////

#endif
