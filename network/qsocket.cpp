﻿// -*- coding:utf-8-with-signature -*-

// Q's C++ Library
// Copyright (c) 1996-2001 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

#include "../config.h"
#include "qsocket.h"

#ifdef _WIN32
  #define STRICT
  #define WIN32_LEAN_AND_MEAN
  #include "../win/sync.h"
  #include <ws2tcpip.h> // getaddrinfo()
#else
  #include <arpa/inet.h>  // inet_addr()
  #include <sys/time.h>   // timeval
  #include <netinet/in.h> // in_addr_t
  #include <sys/socket.h>
  #include <unistd.h>
  typedef int SOCKET;
  #define INVALID_SOCKET -1
  #define closesocket close
#endif  // _WIN32
#include <assert.h>
#include <errno.h>
#include <string.h> // memset()

using namespace std;


//////////////////////////////////////////////////////////////////////////

#ifdef _WIN32
static Synchronize cs;
#endif

/**
 * ホスト名を名前解決し, socket(), connect() に渡せる IPアドレスなどを得る.
 * gethostbyname() is obsolete.
 *
 * @param hostname ホスト名. nullptr のときは, loopbackアドレス.
 * @param port     ポート番号. 1以上.
 * @param socktype TCP なら SOCK_STREAM
 * @return エラーの場合, NULL. TODO: エラーが発生したとき例外を投げるか?
 *
 * 呼び出し側で freeaddrinfo() すること.
 */
struct addrinfo* ngethostbyname(const char* hostname, int port, int socktype)
{
    //assert(name && *name);
    if (port < 0)
        return nullptr;
    
#ifdef _WIN32
    // マルチスレッドで排他しないと，RASダイアログがスレッドの本数だけ出現する。
    cs.lock();
#endif
    
    struct addrinfo hints;
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC; // IPv4/IPv6両対応
    hints.ai_socktype = socktype;
#ifndef _WIN32
    hints.ai_flags = AI_NUMERICSERV;
#endif    
    char service[11];
    sprintf(service, "%d", port);

    struct addrinfo* retval = nullptr;
    int err = getaddrinfo(hostname, service, &hints, &retval);

#ifdef _WIN32
    cs.unlock();
#endif
    
    if (err != 0) {
        fprintf(stderr, "getaddrinfo() failed: %s\n", gai_strerror(err));
        return nullptr;
    }

    return retval;
}


/**
 * timeout付き recv()
 * @param timeout  ミリ秒単位。ブロックするときは INFINITE (-1)
 * @throws InterruptedIOException タイムアウトしたとき.
 */
ssize_t nrecv(SOCKET socket, void* buffer, size_t length, int timeout)
        noexcept(false)
{
    assert(buffer);

    if (timeout > 0) {
        fd_set ready;
        timeval timeout_;

        FD_ZERO(&ready);
        FD_SET(socket, &ready);
        memset(&timeout_, 0, sizeof(timeout_));
        timeout_.tv_sec = timeout / 1000;   // ms -> sec.
        timeout_.tv_usec = (timeout % 1000) * 1000;
        if (timeout_.tv_sec == 0 && timeout_.tv_usec == 0)
            timeout_.tv_usec = 1;
        
        // UNIX: signal で割り込まれる場合がある。
        int r;
        do {
            // Linux は timeout を更新するが, FreeBSD は更新しない。
            // POSIX IEEE Std 1003.1-2017 では、may modify となっており、どちら
            // の挙動もありうる。ヒドい.
            // => FreeBSD の場合は, 待ち時間が長くなってしまう。
            r = select(0, &ready, NULL, NULL, &timeout_);
        } while (r == -1 && errno == EINTR);
        if (r == -1)
            throw SocketException(errno, "select() failed");

        if (!FD_ISSET(socket, &ready))
            throw InterruptedIOException("timeout"); // タイムアウト
    }

    ssize_t r;
    do {
      r = ::recv(socket, (char*) buffer, length, 0);
    } while (r == -1 && errno == EINTR);
    if (r == -1)
        throw SocketException(errno, "recv() failed");
    
    return r;
}


////////////////////////////////////////////////////////////////////////
// SocketException

SocketException::SocketException(int code, const char* msg):
                        runtime_error(msg), err(code) // , msg(msg_)
{
}

SocketException::~SocketException() noexcept
{
}

int SocketException::getCode() const noexcept
{
    return err;
}

/*
const char* SocketException::what() const noexcept
{
    return msg.c_str();
}
*/

////////////////////////////////////////////////////////////////////////
// InterruptedIOException

InterruptedIOException::InterruptedIOException(const char* msg):
                                runtime_error(msg)
{
}

InterruptedIOException::~InterruptedIOException() noexcept
{
}


////////////////////////////////////////////////////////////////////////
// CSocket

CSocket::CSocket(): sock_fd(INVALID_SOCKET)
{
}

CSocket::~CSocket()
{
    // 2000.09.16 closeの処理とか入れないように。おもしろいことになるから。
}


#define BACKLOG 5

/**
 * TCP で listen する. IPv6対応
 *
 * @param node bind() するホスト名. NULL の場合, INADDR_ANY, IN6ADDR_ANY_INIT.
 * @param port ポート番号. 0 の場合, ephemeral port のなかで, 空いているポート
 *             を割り当てる.
 *
 * @return 正常な場合, 新しい CSocket インスタンス. 
 *         エラーの場合, NULL か, 例外 SystemCallError
 */
CSocket* CSocket::setupServer(const char* node, int port) noexcept(false)
{
    if ( port < 0 )
        return nullptr;
    
    struct addrinfo hints;
    struct addrinfo* res = NULL;
 
    memset(&hints, 0, sizeof(hints));
    if (!node)
        hints.ai_family = AF_INET6;    // AF_INET6は、IPv4/IPv6両方を受け付ける。
    hints.ai_socktype = SOCK_STREAM;

    // AI_PASSIVE をセットせずに, node = NULL の場合は, loopbackアドレス
    // (INADDR_LOOPBACK or IN6ADDR_LOOPBACK_INIT).
    // AI_PASSIVE をセットして, node = NULLのときは, INADDR_ANY, IN6ADDR_ANY_INIT.
#ifdef _WIN32
    hints.ai_flags = AI_PASSIVE;
#else
    hints.ai_flags = AI_PASSIVE | AI_NUMERICSERV;
#endif
  
    // (node, service) の両方を nullptr にすると getaddrinfo() が失敗するが,
    // このようにすれば, ephemeral port から取れる.
    char service[11];
    sprintf(service, "%d", port);
    int err = getaddrinfo(node, service, &hints, &res);
    if (err != 0) {
        fprintf(stderr, "getaddrinfo() failed: %s\n", gai_strerror(err));
        return nullptr;
    }

    //struct addrinfo* ai = res;
    SOCKET sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    if ( sockfd == INVALID_SOCKET ) {
        int e = errno;
        //perror("socket open failed");
        freeaddrinfo(res);
        throw SystemCallError(e, "socket open failed");
    }

    int reuse = 1;
    if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (char*) &reuse,
                   sizeof(reuse)) < 0) {
        closesocket(sockfd);
        freeaddrinfo(res);
        return nullptr;
    }

    // port = 0 の場合, ephemeral port から選ばれる.
    if (bind(sockfd, res->ai_addr, res->ai_addrlen) < 0) {
        int e = errno;
        //perror("bind() failed");
        closesocket(sockfd);
        freeaddrinfo(res);
        throw SystemCallError(e, "bind() failed");
    }

    // もういらん.
    freeaddrinfo(res);
    res = nullptr;
  
    if (listen(sockfd, BACKLOG) < 0) { // 5 is magic
        int e = errno;
        //perror("listen() failed");
        closesocket(sockfd);
        throw SystemCallError(e, "listen() failed");
    }

    // 情報を保存.
    CSocket* result = new CSocket();
    result->sock_fd = sockfd;

    // 自分のポート番号を得るために, いつでも取得しなおす.
    result->addrlen = sizeof(result->addr);
    memset(&result->addr, 0, result->addrlen);
    if (getsockname(sockfd, (sockaddr*) &result->addr, &result->addrlen) < 0) {
        int e = errno;
        //perror("getsockname() failed");
        closesocket(sockfd);
        delete result;
        throw SystemCallError(e, "getsockname() failed");
    }
    
    return result;
}


// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
