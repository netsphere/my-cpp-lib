﻿// -*- coding:utf-8-with-signature; mode:c++ -*-

#ifndef DOM_NODE_H
#define DOM_NODE_H

//////
// TODO: namespace に入れる.

#include <string>
#include <vector>
#include <map>
#include <assert.h>
#include "../ptr_vector.h"
#include "../ptr_map.h"
#include <string.h> // strcmp()
#include <stdexcept>

#ifndef _WIN32
  #include <strings.h> // strcasecmp()
#endif

enum NodeType {
    // DOM では 要素 (element). StAX では Start-tag or empty-element.
    ELEMENT = 1,
    ATTRIBUTE = 2,
    TEXT = 3,
    CDATA_SECTION = 4,
    ENTITY_REFERENCE = 5,
    ENTITY = 6,
    PROCESSING_INSTRUCTION = 7,
    COMMENT = 8,
    DOCUMENT = 9,
    DOCUMENT_TYPE = 10,
    NOTATION = 12,

    // DOM では使わない。StAX では end-tag.
    END_TAG
};

typedef ptr_vector<class Node*> NodeList;

// XML は case-sensitive だが, DOM では, "in a case-insensitive manner" に触れ
// ている.
// => 正規化はしないが, 重複はエラーにする.
// See <functional>
// std::binary_function class は C++11 で非推奨, C++17 で削除.
struct ci_less /*: public std::binary_function<std::string, std::string, bool> */
{
    bool operator()(const std::string& __x, const std::string& __y) const {
        return strcasecmp(__x.c_str(), __y.c_str()) < 0;
    }
};
typedef ptr_map<std::string, class Attribute*, ci_less> AttributeList;

typedef std::map<std::string, std::string, ci_less> EntityMap;


////////////////////////////////////////////////////////////////////////
// Node

// このクラスをインスタンス化させない.
class Node
{
protected:
    Node( int node_type ): _node_type(node_type) { }

public:
    virtual ~Node() { }

    virtual std::string name() const = 0;
    //virtual std::string value() const = 0;

    // 自分自身のXML表現.
    virtual std::string str() const = 0;

private:
    int _node_type;
};


// DOM では要素. StAX では start-tag or empty-element.
class Element: public Node
{
public:
    // @param name 必須.
    Element( char* name, AttributeList* attr_list, bool emptytag,
             NodeList* children ):
      Node(ELEMENT),
      _name(name), _attributes(attr_list), _emptytag(emptytag),
      _children(children)
    {
        if (!_name)
            throw "no name";
        if (!attr_list)
            _attributes = new AttributeList();

        if (emptytag) {
            if (children && children->size() > 0)
                throw "must empty";
        }
        if (!children)
            _children = new NodeList();
    }

    virtual ~Element() {
        free(_name);
        delete _attributes;
        delete _children;
    }

    // @return 見つからなかったとき nullptr
    Attribute* get_attribute(const std::string& name) const;

    // DOM readonly attribute tagName
    // nodeName: same as Element.tagName.
    virtual std::string name() const { return _name; }

    // DOM readonly attribute NamedNodeMap attributes;
    const AttributeList& attributes() const {
        return *_attributes;
    }

    //std::pair<std::string, std::string> ns_split() const;

    virtual std::string str() const;

    // DOM readonly attribute childNodes
    virtual NodeList& children() const {
        return *_children;
    }

private:
    char* _name;
    AttributeList* _attributes;
    bool _emptytag;

    NodeList* _children;
};


// StAX only
class EndTag: public Node
{
public:
    EndTag( char* name ):
        Node(END_TAG), _name(name)
    {
        if (!_name)
            throw "no name";
    }

    virtual ~EndTag() {
        free(_name);
    }

    virtual std::string name() const { return _name; }

    virtual std::string str() const {
        return std::string("</") + _name + ">";
    }

private:
    char* _name;
};


// 属性もノード.
class Attribute: public Node
{
public:
    // @param name 属性名の省略の場合, nullptr.
    Attribute(char* name, std::string* value):
      Node(ATTRIBUTE), _name(name), _value(value) {
        assert( !name || strcmp(name, "") != 0 );
        assert(value);
    }

    virtual ~Attribute() {
        free(_name);
        delete _value;
    }

    virtual std::string str() const;

    virtual std::string name() const { return _name; }

    std::string value() const { return *_value; }

private:
    char* _name;
    std::string* _value;
};


// direct known サブクラス: Text, Comment
class CharData: public Node
{
protected:
    CharData(NodeType node_type, std::string* str):
        Node(node_type), _data(str) { }

public:
    virtual ~CharData() {
        delete _data;
    }

    // DOM attribute DOMString data;
    std::string data() const { return *_data; }

    //std::string str() const;

protected:
    std::string* _data;
};


// サブクラス: CDATASect
class Text: public CharData
{
    typedef CharData super;
public:
    Text(std::string* str, NodeType type = TEXT): CharData(type, str) {
        assert(str);
    }

    virtual ~Text() { }

    virtual std::string name() const {
        return "#text";
    }

    // 自分自身のXML表現.
    virtual std::string str() const {
        return *super::_data;
    }
};


class CDATASect : public Text
{
    typedef Text super;
public:
    CDATASect(std::string* sect): super(sect, CDATA_SECTION) { }

    virtual ~CDATASect() { }

    // XML表現.
    virtual std::string str() const;

    virtual std::string name() const {
        return "#cdata-section";
    }
};


class EntityRef: public Node
{
public:
    EntityRef(std::string* str): Node(ENTITY_REFERENCE), _str(str) {
        assert(str);
    }

    virtual ~EntityRef() {
        delete _str;
    }

    virtual std::string name() const {
        return *_str;
    }

    // 自分自身のXML表現.
    virtual std::string str() const;

private:
    // "&", ";" の間のみ.
    std::string* _str;
};


/* predefined entities
    <!ENTITY lt     "&#38;#60;">
    <!ENTITY gt     "&#62;">
    <!ENTITY amp    "&#38;#38;">
    <!ENTITY apos   "&#39;">
    <!ENTITY quot   "&#34;">
 */
// entity 宣言の内容. 実体宣言を読み込む順序を問わないので, ここでは展開しない。
class Entity: public Node
{
public:
    static size_t max_size() {
        return 255;
    }

    // @exception Entity std::length_error
    //     宣言のときに長さが超えていると, 展開した時に確実に超える.
    //     先に例外を出す.
    Entity(const std::string& name, std::string* body):
                                Node(ENTITY), _name(name), _body(body) {
        assert(body);
        if ( body->length() > max_size() )
            throw std::length_error("Entity declaration: too long body");
    }

    virtual ~Entity() {
        //free(_name);
        delete _body;
    }

    virtual std::string name() const {
        return _name;
    }

    // entity declaration のXML表現.
    virtual std::string str() const;

private:
    std::string _name;
    std::string* _body;
};


// ProcessingInstruction
class PI : public Node
{
public:
    PI( char* target, char* data ):
      Node(PROCESSING_INSTRUCTION), _target(target), _data(data) { }

    virtual ~PI() {
        free(_target);
        free(_data);
    }

    // same as ProcessingInstruction.target.
    virtual std::string name() const {
        return _target;
    }

    virtual std::string str() const;

    const char* data() const;

private:
    char* _target;
    char* _data;
};


class Comment: public CharData
{
public:
    Comment(std::string* str): CharData(COMMENT, str) { }

    virtual std::string name() const {
        return "#comment";
    }

    // 自分自身のXML表現.
    virtual std::string str() const;
};


class DocType;

class Document: public Node
{
public:
    // @param children doctypedecl, root を含む子ノード.
    Document( Attribute* version, Attribute* encoding, Attribute* standalone,
              DocType* doctypedecl, Element* root,
              NodeList* children ) :
      Node(DOCUMENT),
      _doctype(doctypedecl), _root(root), _children(children)
    {
        if (version) {
            _version = version->value();
            delete version;
        }
        if (encoding) {
            _encoding = encoding->value();
            delete encoding;
        }
        if (standalone) {
            _standalone = standalone->value();
            delete standalone;
        }

        if (!children)
            _children = new NodeList();
    }

    virtual ~Document()
    {
        //free(_version);
        //free(_encoding);
        //free(_standalone);
        _doctype = nullptr;
        _root = nullptr;
        delete _children;
    }

    // DOM attribute documentElement
    Element* root() const { return _root; }

    virtual std::string str() const;

    virtual std::string name() const {
        return "#document";
    }

    // DOM readonly attribute childNodes
    virtual NodeList& children() const {
        return *_children;
    }

private:
    // XMLDecl
    std::string _version;  // DOM attribute xmlVersion
    std::string _encoding;
    std::string _standalone;

    // 注意; 解放しない.
    DocType* _doctype;   // readonly attribute DocumentType doctype
    // 注意; 解放しない.
    Element * _root;

    NodeList* _children;
};


/*
    <!DOCTYPE ex SYSTEM "ex.dtd" [
      <!ENTITY foo "foo">
      <!ENTITY bar "bar">
      <!ENTITY bar "bar2">
      <!ENTITY % baz "baz">
    ]>
    <ex />
*/
class DocType : public Node
{
public:
    DocType(char* name, std::string* public_id, std::string* system_uri,
            NodeList* decls ):
      Node(DOCUMENT_TYPE),
      _name(name), _public_id(public_id), _system_uri(system_uri),
      _decls(decls)
    { }

    virtual ~DocType() {
        free(_name);
        delete _public_id;
        delete _system_uri;
        delete _decls;
    }

    virtual std::string str() const;

    virtual std::string name() const {
        return _name;
    }

private:
    char* _name;
    std::string* _public_id;  // <!DOCTYPE xx PUBLIC の場合のみ
    std::string* _system_uri;  // SYSTEM or PUBLIC の両方で.

    NodeList* _decls;
};


// in DTD
class NotationDecl: public Node
{
public:
    NotationDecl(const std::string& name, std::string* body):
        Node(NOTATION), _name(name), _body(body) { }

    virtual ~NotationDecl() {
        //free(_name);
        delete _body;
    }

    virtual std::string name() const {
        return _name;
    }

    // 自分自身のXML表現.
    virtual std::string str() const;

private:
    std::string _name;
    std::string* _body;
};




#endif // !DOM_NODE_H

// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
