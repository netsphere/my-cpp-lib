
// - wide 版 `ifstream` に narrow 版 `streambuf` を渡せるのか?
// - std::codecvt_utf8_utf16<> の正しい使い方

// How to build:
//   $ gcc -g -Wall -Wextra wifstream_test.cpp -lstdc++

/**
 * ★速度が遅い?
 * 例えばこの記事で, codecvt_utf8<>() が 25M bytes で 3分間となっていて遅い.
 *     https://qiita.com/AsladaGSX/items/af50066dbbfd59991af9
 * gcc 12.2 では, 50M bytes のファイルで 0.1秒未満に収まるので、速度は問題ない.
 *
 * ★インタフェイスの設計が根本的に壊れている.
 *     - streambuf の幅を istream の幅に合わせなければならない.
 *     - streambuf のほうで transform するのか?
 *       => basic_filebuf::underflow() 内から _M_codecvt->in() 呼び出し
 *          basic_filebuf::overflow()
 *              -> basic_filebuf::_M_convert_to_external() -> _M_codecvt->out()
 *     - filebuf 限定になっている!
 * stream クラス、いらんやん!
 * wistream は使い物にならない。istream を利用する Reader / Writer クラスを作る
 * べき.
 */

// UCS-4 (4バイト) の場合に定義
#undef UTF8_UCS4_TEST //1

// char版 ifstream, wchar_t 版 wifstream しかない
#include <iosfwd>

// basic_string<> は char 版, char16_t (UTF-16) 版, char32_t (UCS-4) 版がある
// 逆に, ほかのクラスでは char16_t 版, char32_t 版はないようだ.

#include <fstream>
#include <codecvt>
#include <assert.h>
#include "dump.h"
using namespace std;

typedef basic_ifstream<char16_t > u16ifstream;  // UTF-16
typedef basic_ifstream<char32_t > u32ifstream;  // UCS-4

typedef basic_istream<char16_t> u16istream;
typedef basic_streambuf<char16_t > u16streambuf;

// char 版 streambuf から派生させると, 異なる幅の istream に渡すところがコンパ
// イル時エラー.
//  => istream の幅に合わせる必要. マジか〜
class MyStreamBuf: public u16streambuf
{
};

typedef basic_ios<char16_t> u16ios;

constexpr int MYBUFSIZ = 1024; //* 64;
#ifdef UTF8_UCS4_TEST
char32_t buf[MYBUFSIZ];    // wchar_t と char32_t は互換性なし. Good!
#else
char16_t buf[MYBUFSIZ];    // 規格で UTF-16 と決まっている.
#endif


void test(const string& filename, u16streambuf* sb)
{
#ifdef UTF8_UCS4_TEST
    u32ifstream ifs;
#else
    u16ifstream ifs;
#endif
    // wchar_t版: rdbuf() は std::wios クラスのメソッドになる. (型名が変わる)
    //ifs.wios::rdbuf(sb);

    // 型名を適宜合わせる
    //ifs.basic_ios<char16_t>::rdbuf(sb);  // これは通る.
    //ifs.u16ios::rdbuf(sb);   // これでもOK

    //u16istream ifs(sb);  // 幅が合っていれば, これは通る.

    // 幅に合わせてコンバータを変える。何それ
#ifdef UTF8_UCS4_TEST
    assert( sizeof(decltype(ifs)::char_type) == 4 );
    // ios_base::imbue() .. なのに変換の実装は filebuf?
    ifs.imbue(std::locale(std::locale(), new codecvt_utf8<char32_t>()));
#else
    assert( sizeof(decltype(ifs)::char_type) == 2 );
    // codecvt_utf8_utf16<> でなければならない.
    ifs.imbue( std::locale(std::locale(), new codecvt_utf8_utf16<char16_t>()) );
#endif

    ifs.open(filename);
    FILE* fp = fopen("std_output.utf16", "wb");
    while (ifs.read(buf, MYBUFSIZ).gcount() > 0) {
        fwrite(buf, sizeof(decltype(ifs)::char_type), ifs.gcount(), fp);
    }
    fclose(fp);
    dump<decltype(ifs)::char_type>(buf, MYBUFSIZ);
}


int main(int argc, char* argv[])
{
    MyStreamBuf* buf = new MyStreamBuf();
    printf("file = %s\n", argv[1]);
    test(argv[1], buf);
    delete buf;

    return 0;
}
