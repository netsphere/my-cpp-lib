
#include "../qsdebug.h"

int main()
{
    // QsTRACE(1);   これはエラー.
    QsTRACE(1, "foo\n");
    QsTRACE(2, "bar %s\n", "baz");
    QsTRACE(0, "no output");  // 出力されない.
    
    return 0;
}

