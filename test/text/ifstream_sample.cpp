
// `ifstream` に filebuf 以外を渡したらどうなるのかテスト
//   $ gcc -g -Wall -Wextra ifstream_test.cpp ../../qsdebug.cpp -lstdc++

#include <filesystem>
#include <fstream>  // ifstream
using namespace std;
#include "../../qsdebug.h"
#include <fcntl.h>  // open()
#include <unistd.h> // read(), close()

// 最小の streambuf 実装.
// filebuf ではなく streambuf から派生させる
class MyStreamBuf: public streambuf
{
    string m_filename;
    int m_fd;
    char m_buf[1];  // バッファリングしない.

public:
    MyStreamBuf(): m_fd(-1) { }
    ~MyStreamBuf() {
        close();
    }

    // @return 成功したら true
    bool open(const string& filename, ios_base::openmode mode) {
        QsTRACE(true, "called.\n");
        if ( is_open() )
            return false;

        m_fd = ::open(filename.c_str(), O_RDONLY); // 手抜き. TODO: 直す
        return m_fd != -1;
    }

    bool is_open() const throw() {
        return m_fd != -1;
    }

    // @return 成功したら true
    bool close() {
        if (m_fd != -1) {
            int r = ::close(m_fd); // 成功は0
            m_fd = -1;
            return r == 0;
        }
        return true;
    }

protected:
    // @override
    // 文字を読み込み, バッファに格納する.
    // @return 読み込んだ1文字. EOFだった場合, traits_type::eof()
    virtual int_type underflow() {
        // eback() 先頭, gptr() 現在の位置, egptr() 終端. 名前が??
        if ( gptr() && gptr() < egptr() )
            return *gptr();  // return buffered.

        if ( !is_open() )
            return traits_type::eof();

        // バッファを読みきった.
        ssize_t r = ::read(m_fd, m_buf, 1);
        if (r == 0) // EOF
            return traits_type::eof();
        else if (r < 0) {
            throw runtime_error(string("::read() error: errno=") +
                                to_string(errno) );
        }
        // set input pointers: new_eback, new_gptr, new_egptr
        setg(m_buf, m_buf, m_buf + 1);

        return *gptr();
    }
};


// ★設計が壊れている.
// streambuf 派生クラスと istream 派生クラスが密結合になっていて、相互に交換で
// きない。
//
// ifstream::open(s, mode) は, 単に filebuf::open(s, mode | in) を呼び出し, 失
// 敗したら ios_base::failbit を立てる.
//   => ifstream::open() を呼び出さなくても差し支えない! filebuf::open() で開け
//      ばよい.
//      rdbuf(sb) で streambuf を差し替えることができるが, filebuf でなくても通
//      る. dynamic_cast して確認していない.
//
// ios_base::imbue() でロケールを切り替え、入出力を transform するようになって
// いる。codecvt_utf8 (ただしC++17で非推奨.) など, バイト列の変換もする想定.
//   => と思いきや, ロケールは streambuf のほうで持つ! アホか!!


void test(MyStreamBuf* sb)
{
    // これはエラー: no matching function for call to ‘std::basic_ifstream<char>::basic_ifstream(std::streambuf*)’
    //ifstream ifs(static_cast<streambuf*>(sb) );

    ifstream ifs;
    //ifs.rdbuf(sb);  // これはエラー
    ifs.ios::rdbuf(sb);   // これは通る! 型名が必要.

    // コンパイルエラーにならないうえに,
    // streambuf が filebuf かどうか, `dynamic_cast` して確認していない
    //ifs.open("ifstream_test.cpp");

    // 単にこれでよい.
    sb->open("ifstream_test.cpp", ios_base::in);

    char s[1000];
    for (int i = 0; i < 10; i++) {
        ifs.getline(s, 1000 - 1); // 戻り値は *this.
        printf("%s\n", s);
    }
    ifs.close();
}

int main()
{
    MyStreamBuf* buf = new MyStreamBuf();
    test(buf);
    delete buf;

    return 0;
}


// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
