
#include "text/utf8_ifilter.h"
#include <fstream>
#include "dump.h"
using namespace std;

typedef basic_istream<char16_t> u16istream; // UTF-16

constexpr int MYBUFSIZ = 1024; //* 64;
char16_t buf[MYBUFSIZ];    // 規格で UTF-16 と決まっている.

int main(int argc, char* argv[])
{
    ifstream utf8stream;
    utf8stream.open(argv[1]);
    Utf8InputFilter filter(&utf8stream); // 所有しない
    u16istream s(&filter);

    FILE* fp = fopen("utf8_output.utf16", "wb");
    while (s.read(buf, MYBUFSIZ).gcount() > 0) {
        fwrite(buf, sizeof(decltype(s)::char_type), s.gcount(), fp);
    }
    fclose(fp);
    dump<char16_t>(buf, MYBUFSIZ);

    return 0;
}
