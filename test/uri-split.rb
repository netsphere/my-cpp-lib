
# URI のテスト

require 'uri'
p URI::VERSION

p URI::RFC3986_Parser.new.split "http://foo.com/posts?id=30&limit=5#time=1305298413"
# 正規化で末尾の "#" は削除しない. nil と "" で区別される。OK
p URI::RFC3986_Parser.new.split "http://www.w3.org/1999/02/22-rdf-syntax-ns#"
