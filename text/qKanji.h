﻿// -*- coding:utf-8-with-signature -*-

// Q's C++ Library
// Copyright (c) 1996-1998 Hisashi Horikawa. All rights reserved.

こちらのほうが新しい。こちらをベースに作る

#ifndef QSLIB_KANJI
#define QSLIB_KANJI

#include <qTypes.h>
#include <vector>
#include <deque>
#include <IFilter.h>
using namespace std;

////////////////////////////////////////////////////////////////////////////
// CodeContext

class CodeContext
{
protected:
    bool error;
public:
    class Queue: public vector<uint16_t>
    {
        bool ret_cr;
        static uint16_t norm[];     // 半角／全角正規化
        class KanjiFilter* kanjiFilter;
    public:
        Queue(KanjiFilter* k);
        void push_char(int c);
    };
    Queue queue;   // 内部コードに変換して格納

    CodeContext(KanjiFilter* k);
    virtual ~CodeContext();
    virtual void push(int c) = 0;
    virtual void clear();
    bool isValid() const;
};

////////////////////////////////////////////////////////////////////////////
// Jis7Context

class Jis7Context: public CodeContext
    // ISO-2022-JP (preferred MIME name)
{
    int charset;    // 終端バイト
    int setsize;    // 1バイト集合，2バイト集合
    char buf[2];
public:
    Jis7Context(KanjiFilter* k);
    virtual ~Jis7Context();
    virtual void push(int c);
    virtual void clear();
};

////////////////////////////////////////////////////////////////////////////
// EucContext

class EucContext: public CodeContext
    // EUC-JP (preferred MIME name)
{
    int buf[2];
public:
    EucContext(KanjiFilter* k);
    virtual ~EucContext();
    virtual void push(int c);
    virtual void clear();
};

////////////////////////////////////////////////////////////////////////////
// KanjiFilter

class KanjiFilter: public InputFilter
{
    // 定数 //////////////////////////////////////
public:
    enum code_t
        // 符号化表現
    {
        UNKNOWN = 0,
        ISO_2022_JP,
            // この符号化表現は"iso-2022-jp"と呼ばれているが，ISO/IEC 2022に適合するものではない。
            // 次のエスケープシーケンスで文字集合を切り替える。
            //      符号                    文字集合
            //      1B 28 42    ESC ( B     ISO/IEC 646 IRV
            //      1B 28 4A    ESC ( J     JIS X 0201ラテン文字集合
            //      1B 24 40    ESC $ @     JIS X 0208-1978
            //      1B 24 42    ESC $ B     JIS X 0208-1997
            // 出力符号にRFC1468が指定され，normalizeがOFFの時，次のエスケープシーケンスも使う。
            //      1B 28 49    ESC ( I     JIS X 0201片仮名文字集合
        Shift_JIS,
            // シフト符号化表現
        EUC_JP
            // 次の指示と呼び出しを仮定した8単位符号化表現。ただしISO/IEC 2022に適合するものではない。
            //      指示
            //          G0 = JIS X 0201ラテン文字集合,  G1 = JIS X 0208漢字集合，
            //          G2 = JIS X 0201片仮名文字集合,  G3 = 外字
            //      呼び出し
            //          G0集合を列02-07，G1集合を列10-15へ
            // SS2 (0x8e)でG2内の1図形文字を，SS3 (0x8f)でG3内の1図形文字を列10-15（！）へ呼び出す。
    };

    enum ret_t { CR, LF, CRLF };

    // 属性 //////////////////////////////////////
private:
    bool normalize;
    class CodeContext *jis7, *sjis, *euc;
    class CodeContext* iconv;
    int (KanjiFilter::*oconv)(char* buf, class CodeContext* context);

    ret_t m_ret;
    static uint16_t norm2[];    // 濁音・半濁音正規化
    char outbuf[20];
    int outbuf_left;
    int m_nMbOutMode;
    int input_mode;

    // メソッド //////////////////////////////////
public:
    KanjiFilter();
    KanjiFilter(istream* in_);
    virtual ~KanjiFilter();

    virtual int read(char* buf, int size);
    void setInputCode(code_t incode);
    void setOutputCode(code_t outcode, ret_t ret, bool normalize);
    bool isNormalize() const;

    static string euc2sjis(const char* euc);
    static int getSjisLen(const char* str);
    static int getEucLen(const char* str);
private:
    void convert();
    int jis7_out(char* buf, CodeContext* context);
    int sjis_out(char* buf, CodeContext* context);
    int euc_out(char* buf, CodeContext* context);
    int ret_out(char* buf) const;
};

#endif
