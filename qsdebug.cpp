﻿// -*- coding:utf-8-with-signature; -*-

#ifndef NDEBUG
// デバッグ環境のみ

#include "qsdebug.h"
#include <string>
#include <stdarg.h> // va_start()
#ifdef _WIN32
  #include "win/sync.h"
#endif

using namespace std;


//////////////////////////////////////////////////////////////////////
// TRACE

static bool trace_use_file = false;
static string trace_filename;

// スレッド別ファイルに出力する.
static bool trace_use_thread = false;

#ifdef _WIN32
static Synchronize cs;
#endif


// @param flag 0のとき, 何も出力しない
void q_trace( unsigned int flag, const char* filename, int lineno,
              LPCTSTR format, ...)
{
    if (!flag)
        return;

#ifdef _WIN32
    cs.lock();
#endif

    va_list args;
    va_start(args, format);

    char buffer[2000];
    vsnprintf(buffer, sizeof(buffer) - 1, format, args);
    va_end(args);

    if ( !trace_use_file ) {
#ifdef _WIN32
        ::OutputDebugString(buffer);
#else
        // g_logv(G_LOG_DOMAIN, G_LOG_LEVEL_MESSAGE, format, args);
        // g_log()は末尾に改行が入る
        fprintf(stderr, "%s", buffer);
#endif
    }
    else {
        // ファイルに出力
#ifdef _WIN32
        string fn;
        char x[20];
        if (trace_use_thread) {
            sprintf(x, "_%08x.log", GetCurrentThreadId());
            fn = trace_filename + x;
        }
        else
            fn = trace_filename;

        FILE* fp = fopen(fn.c_str(), "a");
#else
        FILE* fp = fopen(trace_filename.c_str(), "a");
#endif
        if (fp) {
            fprintf(fp, "%s", buffer);
            fclose(fp);
        }
    }

#ifdef _WIN32
    cs.unlock();
#endif
}


/**
 * ファイルに出力するようにする
 */
void QsTRACE_SET_OUTPUT_FILE( LPCTSTR filename, bool separateThread )
{
    if ( !filename ) {
        trace_use_file = false;
        trace_filename = "";
        return;
    }

    trace_filename = filename;
    trace_use_thread = separateThread;
    trace_use_file = true;
}


void q_assert_fail( const char* message, const char* filename, int lineno,
                    const char* funcname )
{
    abort();
}


#endif // !NDEBUG


// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
