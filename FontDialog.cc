// Q's C++ Library
// Copyright (c) 1998-2001 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

// フォント選択クラス

#include <string>
#include <vector>
#include <cstdio>
#include <cassert>
#include <strings.h>
#include <X11/Xlib.h>
#include <gtk/gtk.h>
#include <gdk/gdkx.h>

#include <misc.h>

#include "FontDialog.h"

using namespace std;

////////////////////////////////////////////////////////////////////////

int getResoX()
{
    static int reso_x = 0;
    if (!reso_x) {
        Display* disp = GDK_DISPLAY();
        reso_x = int(double(DisplayWidth(disp, 0))
                     / double(DisplayWidthMM(disp, 0)) * 25.4 + 0.5);
                        // 25.4mm = 1インチ
        TRACE("reso_x = %d\n", reso_x);
    }
    return reso_x;
}

int getResoY()
{
    static int reso_y = 0;
    if (!reso_y) {
        Display* disp = GDK_DISPLAY();
        reso_y = int(double(DisplayHeight(disp, 0))
                     / double(DisplayHeightMM(disp, 0)) * 25.4 + 0.5);
                        // 25.4mm = 1インチ
        TRACE("reso_y = %d\n", reso_y);
    }
    return reso_y;
}

enum FontField {
    // "X Logical Font Description Conventions"
    LFD_FOUNDRY = 1,        // face
    LFD_FAMILY_NAME = 2,    // face
    LFD_WEIGHT_NAME = 3,    // style
    LFD_SLANT = 4,          // style
    LFD_SETWIDTH_NAME = 5,  // 'normal'固定
    LFD_ADD_STYLE_NAME = 6, // face. unicodeフォントはここでCJKを区別する
    LFD_PIXEL_SIZE = 7,     // size
    LFD_POINT_SIZE = 8,     // size
    LFD_RESOLUTION_X = 9,   // 計算で算出
    LFD_RESOLUTION_Y = 10,  // 計算で算出
    LFD_SPACING = 11,       // face
    LFD_AVERAGE_WIDTH = 12, // 
    LFD_CHARSET = 13        // charset
            // 文字集合は'iso8859-1'と'iso8859-8'のように
            // CHARSET_ENCODINGフィールドによっても異なる
};

string getFontField(const char* lfd_name, int field)
{
    if (!lfd_name || !lfd_name[0])
        return "";
    
    int size = field == LFD_CHARSET ? 2 : 1;

    int pos = field;
    while (*lfd_name && pos > 0) {
        if (*lfd_name++ == '-')
            pos--;
    }

    string r;
    while (*lfd_name) {
        if (*lfd_name == '-') {
            if (--size <= 0)
                break;
        }
        r += *lfd_name++;
    }

    return r;
}

bool isScalableFont(const char* lfd_name)
{
    if (!lfd_name || *lfd_name != '-')
        return false;

    int field = 0;
    for (const char* p = lfd_name; *p; p++) {
        if (*p == '-') {
            field++;
            if ((field == 7 || field == 8 || field == 12)
                        && (*(p + 1) != '0' || *(p + 2) != '-'))
                return false;
        }
    }
    if (field != 14)
        return false;
    else
        return true;
}

///////////////////////////////////////////////////////////////////////
// FontDialog

FontDialog::FontDialog(GtkWindow* parent):
                    super(parent, true), ptSize(100), isChanged(false)
{
    create();
}

FontDialog::~FontDialog()
{
}

struct stri_cmp {
    bool operator () (const string& x, const string& y) const {
        return strcasecmp(x.c_str(), y.c_str()) < 0;
}};

typedef set<string, stri_cmp> NoCaseStrList;
NoCaseStrList faceList[10]; // TODO: 文字集合の数と合わせる

void FontDialog::getFaces()
{
    // 現ロケールでの文字集合を取得する
    Display* disp = GDK_DISPLAY();
    XOM om = XOpenOM(disp, NULL, NULL, NULL);
    assert(om);
    TRACE("om locale = '%s'\n", XLocaleOfOM(om));

    XOMCharSetList csList;
    const char* mes = XGetOMValues(om, XNRequiredCharSet, &csList, NULL);
    if (mes) {
        error("XGetOMValues() failed: %s\n", mes);
        assert(0);
    }
    for (int i = 0; i < csList.charset_count; i++) {
        Charset cs;
        cs.name = csList.charset_list[i];
        charsets.push_back(cs);
        faceList[i].clear();
    }
    XCloseOM(om);

    // フォント一覧を取得
    int count = 0;
    char** xfonts = XListFonts(disp,
                               "-*-*-*-*-normal-*-*-*-*-*-*-*-*-*",
                               SHRT_MAX, &count);
    assert(count <= SHRT_MAX);

    for (int i = 0; i < count; i++) {
        for (const unsigned char* p = (unsigned char*) xfonts[i];
                 *p; p++) {
            // ASTEC-Xでフォント名がシフトJISになっているのは除外
            if (*p < 0x20 || *p >= 0x7f)
                goto l1;
        }

        for (unsigned j = 0; j < charsets.size(); j++) {
            if (!strcasecmp(getFontField(xfonts[i],
                                         LFD_CHARSET).c_str(),
                            charsets[j].name.c_str())) {
                faceList[j].insert(getFontField(xfonts[i], LFD_FOUNDRY)
                     + "-" + getFontField(xfonts[i], LFD_FAMILY_NAME)
                     + "-" + getFontField(xfonts[i], LFD_ADD_STYLE_NAME)
                     + "-" + getFontField(xfonts[i], LFD_SPACING));
            }
        }
    l1: ;
    }
    XFree(xfonts);
}

gint on_focus_in_event(GtkWidget*widget, GdkEventFocus* event, void* )
{
    gdk_im_end();
    return FALSE;
}

GtkWidget* FontDialog::create_sub()
    // FontChooserに移行する部分
{
    GtkWidget* dlg_vbox = gtk_vbox_new(FALSE, 5);

    // 設定
    GtkWidget* table = gtk_table_new(2, 2, FALSE);
    gtk_widget_show(table);
    gtk_box_pack_start(GTK_BOX(dlg_vbox), table, FALSE, FALSE, 0);

    // ピクセル／ポイントの選択
    GtkWidget* label = gtk_label_new("大きさの単位：");
    gtk_widget_show(label);
    gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
    gtk_table_attach(GTK_TABLE(table), label, 0, 1, 0, 1,
                     GtkAttachOptions(GTK_FILL),
                     GtkAttachOptions(0), 0, 0);
    
    GtkWidget* hbox = gtk_hbox_new(FALSE, 0);
    gtk_widget_show(hbox);
    gtk_table_attach(GTK_TABLE(table), hbox, 1, 2, 0, 1,
                     GtkAttachOptions(GTK_EXPAND | GTK_FILL),
                     GtkAttachOptions(0), 0, 0);

    static const char* size_label[] = { "ポイント", "ピクセル" };
    GSList* size_group = NULL;
    for (int i = 0; i < 2; i++) {
        size_metrics[i] = gtk_radio_button_new_with_label(
            size_group, size_label[i]);
        size_group = gtk_radio_button_group(
            GTK_RADIO_BUTTON(size_metrics[i]));
        gtk_widget_show(size_metrics[i]);
        gtk_box_pack_start(GTK_BOX(hbox), size_metrics[i],
                           FALSE, FALSE, 0);
    }

    // スケーラブルフォント
    label = gtk_label_new("種類：");
    gtk_widget_show(label);
    gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
    gtk_table_attach(GTK_TABLE(table), label, 0, 1, 1, 2,
                     GtkAttachOptions(GTK_FILL),
                     GtkAttachOptions(0), 0, 0);
    
    scalable_only = gtk_check_button_new_with_label("スケーラブルフォントのみ");
    gtk_widget_show(scalable_only);
    gtk_table_attach(GTK_TABLE(table), scalable_only, 1, 2, 1, 2,
                     GtkAttachOptions(GTK_EXPAND | GTK_FILL),
                     GtkAttachOptions(0), 0, 0);

    // フォント選択
    GtkWidget* frame = gtk_frame_new("フォント");
    gtk_widget_show(frame);
    gtk_box_pack_start(GTK_BOX(dlg_vbox), frame, FALSE, FALSE, 0);

    table = gtk_table_new(4, charsets.size() + 1, FALSE);
    gtk_widget_show(table);
    gtk_container_add(GTK_CONTAINER(frame), table);
    gtk_container_set_border_width(GTK_CONTAINER(table), 5);
    gtk_table_set_row_spacings(GTK_TABLE(table), 5);
    gtk_table_set_col_spacings(GTK_TABLE(table), 5);

    // フォント選択のラベル
    static const char* label_text[] = {
        "文字集合", "書体", "スタイル", "大きさ" };

    for (int i = 0; i < 4; i++) {
        GtkWidget* label = gtk_label_new(
                             (string(label_text[i]) + "：").c_str());
        gtk_widget_show(label);
        gtk_misc_set_alignment(GTK_MISC(label), 0, 0.5);
        gtk_table_attach(GTK_TABLE(table), label,
                        i, i + 1, 0, 1,
                        GtkAttachOptions(GTK_FILL),
                        GtkAttachOptions(0), 0, 0);
    }

    getFaces();
    
    // 各文字集合の書体，スタイルをwidgetに設定する
    for (unsigned i = 0; i < charsets.size(); i++) {
        // 文字集合名
        GtkWidget* label = gtk_label_new(charsets[i].name.c_str());
        gtk_widget_show(label);
        gtk_misc_set_alignment(GTK_MISC(label), 0, 0.5);
        gtk_table_attach(GTK_TABLE(table), label,
                         0, 1, i + 1, i + 2,
                         GtkAttachOptions(GTK_FILL),
                         GtkAttachOptions(0), 0, 0);

        // 書体名
        charsets[i].face_sel = gtk_combo_new();
        gtk_widget_show(charsets[i].face_sel);
        gtk_table_attach(GTK_TABLE(table), charsets[i].face_sel,
                         1, 2, i + 1, i + 2,
                         GtkAttachOptions(GTK_EXPAND | GTK_FILL),
                         GtkAttachOptions(0), 0, 0);
        gtk_widget_set_usize(charsets[i].face_sel, 200, 0);
        gtk_editable_set_editable(
            GTK_EDITABLE(GTK_COMBO(charsets[i].face_sel)->entry),
            FALSE);

        NoCaseStrList::const_iterator it;
        GList* glist = NULL;
        for (it = faceList[i].begin(); it != faceList[i].end(); it++)
            glist = g_list_append(glist, (void*) it->c_str());

        gtk_combo_set_popdown_strings(GTK_COMBO(charsets[i].face_sel),
                                      glist);

        // スタイル名は書体の選択時に動的に生成する
        // ここではwidgetだけ生成する
        charsets[i].style_sel = gtk_combo_new();
        gtk_widget_show(charsets[i].style_sel);
        gtk_table_attach(GTK_TABLE(table), charsets[i].style_sel,
                         2, 3, i + 1, i + 2,
                         GtkAttachOptions(GTK_EXPAND | GTK_FILL),
                         GtkAttachOptions(0), 0, 0);
        gtk_widget_set_usize(charsets[i].style_sel, 100, 0);
        gtk_editable_set_editable(
            GTK_EDITABLE(GTK_COMBO(charsets[i].style_sel)->entry),
            FALSE);

        // シグナル
        gtk_signal_connect(
                GTK_OBJECT(GTK_COMBO(charsets[i].face_sel)->entry),
                "changed",
                GTK_SIGNAL_FUNC(onFaceSelected), this);
        gtk_signal_connect(
                GTK_OBJECT(GTK_COMBO(charsets[i].style_sel)->entry),
                "changed",
                GTK_SIGNAL_FUNC(onStyleSelected), this);
        gtk_signal_connect_after(
                GTK_OBJECT(GTK_COMBO(charsets[i].face_sel)->entry),
                "focus_in_event",
                GTK_SIGNAL_FUNC(on_focus_in_event), NULL);
        gtk_signal_connect_after(
                GTK_OBJECT(GTK_COMBO(charsets[i].style_sel)->entry),
                "focus_in_event",
                GTK_SIGNAL_FUNC(on_focus_in_event), NULL);
    }
 
    // 大きさ
    GtkWidget* vbox = gtk_vbox_new(FALSE, 0);
    gtk_widget_show(vbox);
    gtk_table_attach(GTK_TABLE(table), vbox,
                     3, 4, 1, charsets.size() + 1,
                     GtkAttachOptions(GTK_FILL),
                     GtkAttachOptions(GTK_EXPAND | GTK_FILL), 0, 0);
    gtk_widget_set_usize(vbox, 50, 0);
    
    size_entry = gtk_entry_new();
    gtk_widget_show(size_entry);
    gtk_box_pack_start(GTK_BOX(vbox), size_entry, FALSE, FALSE, 0);
    char buf[100];
    sprintf(buf, "%d", ptSize / 10);
    gtk_entry_set_text(GTK_ENTRY(size_entry), buf);
    
    GtkWidget* sw = gtk_scrolled_window_new(NULL, NULL);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(sw),
                                   GTK_POLICY_NEVER,
                                   GTK_POLICY_ALWAYS);
    gtk_widget_show(sw);
    gtk_box_pack_start(GTK_BOX(vbox), sw, TRUE, TRUE, 0);
    
    size_list = gtk_clist_new(1);
    gtk_widget_show(size_list);
    gtk_container_add(GTK_CONTAINER(sw), size_list);

    // プレビュー
    frame = gtk_frame_new("プレビュー");
    gtk_widget_show(frame);
    gtk_box_pack_start(GTK_BOX(dlg_vbox), frame, FALSE, FALSE, 0);
    
    GtkWidget* hbox1 = gtk_hbox_new(FALSE, 0);
    gtk_widget_show(hbox1);
    gtk_container_add(GTK_CONTAINER(frame), hbox1);
    gtk_container_set_border_width(GTK_CONTAINER(hbox1), 5);

    preview_entry = gtk_entry_new();
    gtk_widget_show(preview_entry);
    gtk_box_pack_start(GTK_BOX(hbox1), preview_entry, TRUE, TRUE, 0);
    gtk_entry_set_text(GTK_ENTRY(preview_entry),
               "abc ijk ABC ijk 0123 あいう アイウ 漢字");

    gtk_signal_connect(GTK_OBJECT(size_entry), "focus_out_event",
                       GTK_SIGNAL_FUNC(onSizeEntryChanged), this);
    gtk_signal_connect(GTK_OBJECT(size_list), "select_row",
                       GTK_SIGNAL_FUNC(onSizeListChanged), this);
    gtk_signal_connect(GTK_OBJECT(scalable_only), "toggled",
                       GTK_SIGNAL_FUNC(onScalableOnlyChanged), this);

    gtk_idle_add(onIdle, this);

    return dlg_vbox;
}

void FontDialog::create()
{
    window = GTK_WINDOW(gtk_dialog_new());

    GtkWidget* pane = create_sub();
    gtk_widget_show(pane);
    gtk_container_set_border_width(GTK_CONTAINER(pane), 10);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(window)->vbox), pane,
                       TRUE, TRUE, 0);
    
    // OKボタンなど
    GtkWidget* hbox2 = gtk_hbox_new(FALSE, 0);
    gtk_widget_show(hbox2);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(window)->action_area),
               hbox2, TRUE, TRUE, 0);

    GtkWidget* ok_button = gtk_button_new_with_label("OK");
    gtk_widget_show(ok_button);
    gtk_box_pack_start(GTK_BOX(hbox2), ok_button, FALSE, FALSE, 10);
    gtk_widget_set_usize(ok_button, 100, 0);

    GtkWidget* apply_button = gtk_button_new_with_label("更新");
    gtk_widget_show(apply_button);
    gtk_box_pack_start(GTK_BOX(hbox2), apply_button, FALSE, FALSE, 10);
    gtk_widget_set_usize(apply_button, 100, 0);

    GtkWidget* cancel_button = gtk_button_new_with_label("キャンセル");
    gtk_widget_show(cancel_button);
    gtk_box_pack_start(GTK_BOX(hbox2), cancel_button, FALSE, FALSE, 10);
    gtk_widget_set_usize(cancel_button, 100, 0);

    // シグナル
    gtk_signal_connect(GTK_OBJECT(ok_button), "clicked",
                       GTK_SIGNAL_FUNC(onOK), this);
    gtk_signal_connect(GTK_OBJECT(cancel_button), "clicked",
                       GTK_SIGNAL_FUNC(onCancel), this);
    gtk_signal_connect(GTK_OBJECT(apply_button), "clicked",
                       GTK_SIGNAL_FUNC(onApplyNow), this);
}

void FontDialog::onScalableOnlyChanged(GtkToggleButton* b,
                                       FontDialog* this_)
{
    TRACE("onScalableOnlyChanged()\n");
}

void FontDialog::onOK(GtkButton* , FontDialog* this_)
{
    this_->exit(IDOK);
}

void FontDialog::onCancel(GtkButton* , FontDialog* this_)
{
    this_->exit(IDCANCEL);
}

void FontDialog::onApplyNow(GtkButton* , FontDialog* this_)
{
    // TODO: implement
}

void FontDialog::onSizeListChanged(GtkCList* clist,
                                   gint row, gint column,
                                   GdkEvent* event)
{
    // TODO: implement
}

gint FontDialog::onSizeEntryChanged(GtkWidget* widget,
                             GdkEventFocus* event, FontDialog* this_)
{
    const char* s = gtk_entry_get_text(GTK_ENTRY(widget));
    char* p;
    double pt = strtod(s, &p);
    if (this_->ptSize != pt * 10) {
        this_->ptSize = int(pt * 10);
        this_->setChanged(true);
    }
    return TRUE; // 標準処理をキャンセル
}

void FontDialog::updateStyleSel(Charset* cs)
{
    assert(cs);
    
    char buf[1000];
    sprintf(buf, "-%s-%s-*-*-normal-%s-*-*-*-*-%s-*-%s",
            cs->cur_face[0].c_str(),
            cs->cur_face[1].c_str(),
            cs->cur_face[2].c_str(),
            cs->cur_face[3].c_str(),
            cs->name.c_str());
    TRACE("updateStyleSel(): '%s'\n", buf);

    Display* disp = GDK_DISPLAY();
    int count = 0;
    char** xfonts = XListFonts(disp, buf, SHRT_MAX, &count);
    assert(count <= SHRT_MAX);

    NoCaseStrList ss;
    for (int i = 0; i < count; i++) {
        string s;
        for (const unsigned char* p = (unsigned char*) xfonts[i];
                   *p; p++) {
            if (*p < 0x20 || *p >= 0x7f)
                goto l1;
        }
        ss.insert(getFontField(xfonts[i], LFD_WEIGHT_NAME)
                       + "-" + getFontField(xfonts[i], LFD_SLANT));
    l1: ;
    }
    XFree(xfonts);

    NoCaseStrList::const_iterator it;
    GList* glist = NULL;
    for (it = ss.begin(); it != ss.end(); it++)
        glist = g_list_append(glist, (void*) it->c_str());

    gtk_combo_set_popdown_strings(GTK_COMBO(cs->style_sel), glist);
}

void FontDialog::updateSizeSel()
{
    char buf[1000];
    bool isPoint = gtk_toggle_button_get_active(
                                GTK_TOGGLE_BUTTON(size_metrics[0]));
    if (isPoint) {
        sprintf(buf, "-%s-%s-%s-%s-normal-%s-*-*-%d-%d-%s-*-%s",
            charsets[0].cur_face[0].c_str(),
            charsets[0].cur_face[1].c_str(),
            charsets[0].cur_style[0].c_str(),
            charsets[0].cur_style[1].c_str(),
            charsets[0].cur_face[2].c_str(),
                getResoX(), getResoY(),
            charsets[0].cur_face[3].c_str(),
            charsets[0].name.c_str());
    }
    else {
        sprintf(buf, "-%s-%s-%s-%s-normal-%s-*-*-*-*-%s-*-%s",
            charsets[0].cur_face[0].c_str(),
            charsets[0].cur_face[1].c_str(),
            charsets[0].cur_style[0].c_str(),
            charsets[0].cur_style[1].c_str(),
            charsets[0].cur_face[2].c_str(),
            charsets[0].cur_face[3].c_str(),
            charsets[0].name.c_str());
    }
    
    Display* disp = GDK_DISPLAY();
    int count = 0;
    char** xfonts = XListFonts(disp, buf, SHRT_MAX, &count);
    assert(count <= SHRT_MAX);

    set<string> ss;
    for (int i = 0; i < count; i++) {
        for (const unsigned char* p = (unsigned char*) xfonts[i];
             *p; p++) {
            if (*p < 0x20 || *p >= 0x7f)
                goto l1;
        }
        if (isPoint) {
            int pt = atoi(getFontField(xfonts[i], LFD_POINT_SIZE).c_str());
            sprintf(buf, "%f", double(pt) / 10);
            ss.insert(buf);
        }
        else {
            ss.insert(getFontField(xfonts[i], LFD_PIXEL_SIZE));
        }
    l1: ;
    }
    XFree(xfonts);

    gtk_clist_clear(GTK_CLIST(size_list));
    set<string>::const_iterator ssi;
    for (ssi = ss.begin(); ssi != ss.end(); ssi++) {
        const char* row[1];
        row[0] = ssi->c_str();
        gtk_clist_append(GTK_CLIST(size_list), (char**) row);
    }
}

void FontDialog::onFaceSelected(GtkEditable* editable,
                                      FontDialog* this_)
{
    TRACE("onFaceSelected()\n");

    // どの文字集合？
    Charsets::iterator cs = this_->charsets.end();
    Charsets::iterator i;
    for (i = this_->charsets.begin(); i != this_->charsets.end(); i++) {
        if (GTK_COMBO(i->face_sel)->entry == GTK_WIDGET(editable)) {
            cs = i;
            break;
        }
    }
    assert(cs != this_->charsets.end());

    const char* str = gtk_entry_get_text(GTK_ENTRY(editable));
    for (int i = 0; i < 4; i++) 
        cs->cur_face[i] = getFontField(str, i);

    this_->updateStyleSel(&*cs);
    this_->setChanged(true);
}

void FontDialog::onStyleSelected(GtkEditable* editable,
                                 FontDialog* this_)
{
    // どの文字集合？
    Charsets::iterator cs = this_->charsets.end();
    Charsets::iterator i;
    for (i = this_->charsets.begin(); i != this_->charsets.end(); i++) {
        if (GTK_COMBO(i->style_sel)->entry == GTK_WIDGET(editable)) {
            cs = i;
            break;
        }
    }
    assert(cs != this_->charsets.end());

    const char* str = gtk_entry_get_text(GTK_ENTRY(editable));
    for (int i = 0; i < 2; i++)
        cs->cur_style[i] = getFontField(str, i);

    this_->updateSizeSel();
    this_->setChanged(true);
}

void FontDialog::setChanged(bool c)
{
    if (isChanged == c)
        return;
    isChanged = c;
    if (isChanged)
        gtk_idle_add(onIdle, this);
}

gint FontDialog::onIdle(void* closure)
{
    TRACE("onIdle()\n");
    FontDialog* this_ = (FontDialog*) closure;

    if (this_->isChanged) {
        this_->updatePreview();
        this_->isChanged = FALSE;
    }
    // return TRUE;     // TRUEを返すと，アイドルの間，再び呼ばれる
    
    // gtk_idle_add(onIdle, this_);
    return FALSE;   // FALSEを返すと，それ以上呼ばれない
}

void FontDialog::updatePreview()
{
    char buf[1000];
    char* p = buf;
    Charsets::const_iterator i;
    for (i = charsets.begin(); i != charsets.end(); i++) {
        if (gtk_toggle_button_get_active(
                GTK_TOGGLE_BUTTON(size_metrics[0]))) {
            p += sprintf(p, "-%s-%s-%s-%s-normal-%s-*-%d-%d-%d-%s-*-%s,",
                     i->cur_face[0].c_str(),
                     i->cur_face[1].c_str(),
                     i->cur_style[0].c_str(),
                     i->cur_style[1].c_str(),
                     i->cur_face[2].c_str(),
                     ptSize, getResoX(), getResoY(),
                     i->cur_face[3].c_str(),
                     i->name.c_str());
        }
        else {
            p += sprintf(p, "-%s-%s-%s-%s-normal-%s-%d-*-*-*-%s-*-%s,",
                         i->cur_face[0].c_str(),
                         i->cur_face[1].c_str(),
                         i->cur_style[0].c_str(),
                         i->cur_style[1].c_str(),
                         i->cur_face[2].c_str(),
                         ptSize / 10,
                         i->cur_face[3].c_str(),
                         i->name.c_str());
        }
    }
    p[-1] = '\0';
    cur_font = buf;
    // TRACE("font = '%s'\n", buf);

    GdkFont* font = gdk_fontset_load(buf);
    if (font) {
        GtkStyle* style = gtk_style_copy(
                             gtk_widget_get_style(preview_entry));
        style->font = font;
        gtk_widget_set_style(preview_entry, style);
    }
}

string FontDialog::getFontName() const
{
    return cur_font;
}
