﻿// -*- mode:c++; coding:utf-8-with-signature -*-

#ifndef X_MISC_H__
#define X_MISC_H__

#include <X11/Xlib.h>
#include <string>
#include <map>


////////////////////////////////////////////////////////////////////////
// X11

extern int getColor(Display* disp, const char* name);

extern bool isToplevelWindow(Display* disp, Window w);

extern Window getToplevelWindow(Display* disp, Window w);

extern XFontSet createFont(
    Display* disp,
    const char* name,
    const char* default_font = "-alias-fixed-medium-r-normal--16-*-*-*-*-*-*");


////////////////////////////////////////////////////////////////////////
// CPoint

class CPoint: public XPoint
{
public:
  CPoint() {
    x = y = 0;
  }
  virtual ~CPoint() { }
  
  CPoint(int x_, int y_) {
    x = x_; y = y_;
  }
  
  CPoint(const XPoint& xp) {
    x = xp.x; y = xp.y;
  }

  CPoint operator + (const XPoint& a) const {
    return CPoint(x + a.x, y + a.y);
  }
  
  CPoint operator - (const XPoint& a) const {
    return CPoint(x - a.x, y - a.y);
  }
  
  CPoint& operator = (const XPoint& a) {
    x = a.x; y = a.y;
    return *this;
  }    
};


////////////////////////////////////////////////////////////////////////
// CRect

class CRect: public XRectangle
{
public:
  CRect() {
    x = y = width = height = 0;
  }
  
  virtual ~CRect() { }

  CRect(int x_, int y_, int width_, int height_) {
    x = x_; y = y_; width = width_; height = height_;
  }
    
  CRect(const XRectangle& a) {
    x = a.x; y = a.y; width = a.width; height = a.height;
  }


  CRect& operator = (const XRectangle& a) {
    x = a.x; y = a.y; width = a.width; height = a.height;
    return *this;
  }
};


///////////////////////////////////////////////////////////////////////
// CFont

class CFont
{
    struct ref {
        XFontSet font;
        int count;
        ref(): font(0), count(0) { }
    };
    typedef std::map<std::string, ref*> FontMap;

    Display* disp;
    std::string cur_name;
    static FontMap fm;
    
public:
    CFont();
    virtual ~CFont();
    bool create(Display* disp, const char* name);
    void destroy();
    XFontSet xfont() const;
    
private:
    CFont(const CFont& );               // not implement
    CFont& operator = (const CFont& );  // not implement
};

#endif
