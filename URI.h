﻿// -*- coding:utf-8-with-signature; mode:c++ -*-
// Q's C++ Library
// Copyright (c) 1996-1999 Hisashi HORIKAWA. All rights reserved.

// 意外と難しい URI の世界...

/* ● TODO:
 - URIの正規化というのは可能か????
   -> 定義がある
 - operator == は正規化後で比較すること
 - IPv6ホストへの対応

すみ:
 - <s>&lt;http://www.w3.org/1999/02/22-rdf-syntax-ns#&gt; のように '#' で終わるURI.</s>
   -> 正規化で末尾の "#" は削除しない、となっている。マジか...
 */

#ifndef QSLIB_URI
#define QSLIB_URI

#include <string>

//////////////////////////////////////////////////////////////////////
// URI

/**
 * URIクラス.
 *
 * 次の, "URI Reference" を出発点とする.
 * RFC 3986 Uniform Resource Identifier (URI): Generic Syntax (January 2005)
 *
 * Examples
 *     ftp://ftp.is.co.za/rfc/rfc1808.txt
 *     http://www.ietf.org/rfc/rfc2396.txt
 *     ldap://[2001:db8::7]/c=GB?objectClass?one
 *     mailto:John.Doe@example.com
 *     \    / \                  /
 *      scheme        path        -> John 以下が authority ではないことに注意.
 *     news:comp.infosystems.www.servers.unix
 *     tel:+1-816-555-1212
 *     telnet://192.0.2.16:80/
 *     urn:oasis:names:specification:docbook:dtd:xml:4.1.2
 *
 * 'mailto' スキーマは特殊. 次も可 RFC 6068 The 'mailto' URI Scheme (Oct 2010)
 *     <mailto:addr1@an.example,addr2@an.example> -- 複数可!
 *               -- 一つ前の RFC 2368 (Jul 1998) でも複数書けた。要対応
 *     is equivalent to <mailto:?to=addr1@an.example,addr2@an.example>
 *                     -- path部がない!
 *     or <mailto:addr1@an.example?to=addr2@an.example>
 * query部を持てる:
 *     <mailto:infobot@example.com?body=send%20current-issue> -- query 部がクォートされていない.
 *     <mailto:joe@example.com?cc=bob@example.com&body=hello> -- 複数ﾊﾟﾗﾒｰﾀ. "&"
 * 複雑なメイルアドレスはエスケープする
 *     email addr "not@me"@example.org -> <mailto:%22not%40me%22@example.org>
 */
class URI
{
protected:
    std::string scheme;

    // 割ったほうがいい (正規化しやすくなる)。パース時に割る.
    std::string m_userinfo;
    std::string m_host;

    // URI では整数のみ. 未定義の場合 -1
    int m_port;
    //std::string authority;

    std::string path, query, fragment;

public:
    URI();
    URI(const URI& a);

    // URI文字列をパースする.
    URI(const std::string& a);

    explicit URI(const char* a);

    virtual ~URI();

    URI& operator = (const URI& a);

    // Javaだと getProtocol()
    // 小文字に正規化.
    std::string getScheme() const noexcept;

    ////////////////////
    // authority = [ userinfo "@" ] host [ ":" port ]

    std::string getAuthority() const noexcept;
    std::string getUserInfo() const noexcept;

    // @return `host` 部分. 正規化しない.
    std::string getHost() const noexcept;

    // @return `port` 部を返す. `port` 部がないとき, デフォルトポート番号を返
    //         す. デフォルトもないとき -1
    int getPort() const noexcept;

    std::string getPath() const noexcept;
    std::string getQuery() const noexcept;

    // @return "#" で始まる文字列を返す。空のfragment component は正規化で削除
    //         しないため。See RFC 3986 6.2.3.
    std::string getFragment() const noexcept;

    void setFragment(const char* str);

    // 相対URIの解決
    virtual URI resolve(const URI& ref) const;

    // Self について, baseを基点とした相対URIに変換
    // URIを返さないことに注意
    virtual std::string getRelative(const URI& base) const;

    virtual bool isDescendantOf(const URI& ) const;

    static std::string escape(const char* uri);
    static std::string unescape(const char* escaped_uri);

    // VFAT FSに保存できるようにURI文字をエスケープする。
    // "://" -> "$\\"
    std::string toLocal() const;

    std::string toString() const;

    static int compare(const URI& x, const URI& y);
    bool operator == (const URI& a) const;
    bool operator != (const URI& a) const;
    bool operator < (const URI& a) const;

    /**
     * URI 文字列を parse する。
     * @return 新しい URI インスタンス. エラーの時は NULL. 不要になったら、呼出
     *         し側が delete すること。
     * @param urllen 有効なバイト数.
     */
    static URI* parse(const char* str, int* urllen);
};

#endif // !QSLIB_URI


// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
