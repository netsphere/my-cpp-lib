
#include "file_name.h"
#include <string.h> // strchr()
#include <list>
#include <vector>

using namespace std;

#ifdef _WIN32
  #define STRICT
  #define WIN32_LEAN_AND_MEAN
  //#include <winnls.h>
  #include <windows.h>
#else
inline const char* CharNext(const char* p)
{
    return p + mblen(p, MB_CUR_MAX);
}
inline char* CharNext(char* p)
{
    return p + mblen(p, MB_CUR_MAX);
}
#define stricmp strcasecmp
#endif // _WIN32


//////////////////////////////////////////////////////////////////////
// FileName

FileName::FileName()
{
}

FileName::FileName(const FileName& a): host(a.host), dir(a.dir), file(a.file)
{
}

FileName::FileName(const char* a)
{
    parse(a);
}

FileName::FileName(const string& a)
{
    parse(a.c_str());
}

FileName::~FileName()
{
}

FileName& FileName::operator = (const FileName& a)
{
    if (this != &a) {
        host = a.host;
        dir = a.dir;
        file = a.file;
    }
    return *this;
}

int FileName::parse(const char* str)
    // 戻り値
    //      ファイル名として有効なバイト数
{
    const char* p = str;
    host = dir = file = "";

    // ホスト名
    if ((p[0] == '/' || p[0] == '\\') && (p[1] == '/' || p[1] == '\\')) {
        p += 2;
        while (*p && *p != '/' && *p != '\\') {
#ifdef _WIN32
            if (::IsDBCSLeadByte(*p))
                host += *p++;
            host += *p++;
#else
            for (int ct = mblen(p, MB_CUR_MAX); ct > 0; ct--)
                host += *p++;
#endif
        }
    }

#ifdef _WIN32
    // 1999.09.08 URI("file:///c:/hoge").getPath() = "/c:/hoge"
    // これを救う
    if (*p == '/' && isalpha(p[1]) && p[2] == ':')
        p++;
#endif

    // ディレクトリ
    const char* top = p;
    const char* sep = 0;
    while (*p) {
        if (*p == '/' || *p == '\\')
            sep = p;
        p = ::CharNext(p);
    }

    if (sep) {
        dir.assign(top, sep - top + 1);
        file = sep + 1;
    }
    else {
        dir = "";
        file = top;
    }
    if (file == ".." || file == ".") {
        dir += file + "/";
        file = "";
    }

    for (unsigned i = 0; i < dir.length(); i++) {
        if (dir[i] == '\\')
            dir[i] = '/';
    }

    return strlen(str);
        // TODO: 不適切な文字のチェック
}

string FileName::getHost() const
{
    return host;
}

string FileName::getPath() const
{
    return dir;
}

string FileName::getFile() const
{
    return file;
}

string FileName::getBody() const
{
    const char* top = file.c_str();
    const char* p = top;
    const char* sep = 0;
    while (*p) {
        if (*p == '.')
            sep = p;
        p = ::CharNext(p);
    }

    if (!sep || sep == p)
        return file;
    else
        return string(file, 0, sep - top);
}

string FileName::getExt(bool normalize) const
    // 戻り値
    //      '.'を含まない
{
    const char* p = file.c_str();
    const char* sep = 0;
    while (*p) {
        if (*p == '.')
            sep = p;
        p = ::CharNext(p);
    }
    if (!sep || sep == p)
        return "";
    else {
        if (!normalize)
            return sep + 1;
        else {
            string r;
            for (const char* q = sep + 1; *q; q++)
                r += tolower(*q);
            return r;
        }
    }
}

string FileName::toString() const
{
    string r;
    if (host != "")
        r = string("//") + host;
    return r + dir + file;
}

FileName FileName::resolve(const FileName& ref) const
    // 絶対パスを返す
{
    if (ref.host != "")
        return ref;
    if (ref.host == "" && ref.dir == "" && ref.file == "")
        return *this;

    string retval;
    if (host != "")
        retval = "//" + host;

    if (ref.dir[0] == '/')
        return FileName(retval + ref.dir + ref.file);

    // 相対パス
    vector<int> depth;
    for (unsigned i = 0; i < dir.length(); i++) {
        if (dir[i] == '/')
            depth.push_back(retval.length() + i);
    }

    retval += dir;

    const char* p = ref.dir.c_str();
    while (*p) {
        if (*p == '.' && p[1] == '.' && (p[2] == '/' || p[2] == 0)) {
            if (depth.size() > 1) {
                depth.pop_back();
                retval.erase(retval.begin() + depth.back() + 1,
                                retval.end());
            }
            else {
                // ルートでの"../"は無視したいが，RFC準拠から付ける
                retval += "../";
                // depth.push_back(retval.length() - 1);
            }
            p += 2 + (p[2] != 0);
        }
        else if (*p == '.' && (p[1] == '/' || p[1] == '.'))
            p += 1 + (p[1] != 0);
        else {
            while (*p) {
#ifdef _WIN32
                if (::IsDBCSLeadByte(*p))
                    retval += *p++;
                retval += *p++;
#else
                for (int ct = mblen(p, MB_CUR_MAX); ct > 0; ct--)
                    retval += *p++;
#endif
                if (p[-1] == '/') {
                    depth.push_back(retval.length() - 1);
                    break;
                }
            }
        }
    }
    return FileName(retval + ref.file);
}

string FileName::getRelative(const FileName& base) const
{
    if (host != base.host)
        return toString();
    if (dir[0] != '/'
#ifdef _WIN32
        && !(isalpha(dir[0]) && dir[1] == ':' && dir[2] == '/')
#endif
            ) {
        return toString();
    }

    if (base.dir == dir && base.file == file)
        return "";

    int i;
    list<int> depth;
    for (i = 0; i < base.dir.length(); i++) {
        if (base.dir[i] == '/')
            depth.push_back(i);
    }

    const char* b = base.dir.c_str();
    const char* r = dir.c_str();
    for (i = 0; *r && *b; i++) {
        if (base.dir[i] != dir[i])
            break;
        if (dir[i] == '/') {
            if (!depth.size())
                break;
            depth.pop_front();
        }
        b++;
        r++;
    }

    string retval;
    for (i = 0; i < depth.size(); i++)
        retval += "../";
    retval += r + file;
    if (file == "" && retval == "" && base.file != "")
        retval = "./";

    return retval;
}

bool FileName::operator == (const FileName& a) const
{
    return host == a.host && dir == a.dir && file == a.file;
}

string FileName::getPath2() const
{
    if (file != "")
        return dir;
    string p;
    p.assign(dir, 0, dir.length() - 1);
    return FileName(p).getPath();
}

string FileName::getFile2() const
{
    if (file != "")
        return file;
    string p;
    p.assign(dir, 0, dir.length() - 1);
    return FileName(p).getFile();
}

string FileName::escapeFile(const char* p)
{
    static const char* unuse = "\"%*/:<>?\\|";
        // VFATで使用不可な文字，エスケープ用'%'

    string retval;
    char buf[4];

    while (*p) {
#ifdef _WIN32
        if (::IsDBCSLeadByte(*p)) {
            retval.append(p, 2);
            p += 2;
            continue;
        }
#endif
        if (((unsigned char) *p) < 0x20 || *p == 0x7f || strchr(unuse, *p) != NULL) {
                sprintf(buf, "%%%02x", *p);
                retval += buf;
                p++;
                continue;
        }
        else
            retval += *(p++);
    }
    return retval;
}
