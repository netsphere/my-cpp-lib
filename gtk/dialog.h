// Q's C++ Library
// Copyright (c) 1998-1999 Hisashi HORIKAWA. All rights reserved.

// ダイアログボックスいろいろ

#ifndef QLIB_DIALOG
#define QLIB_DIALOG

#include <string>
#include <gtk/gtk.h>
#include "../window.h"

using namespace std;

////////////////////////////////////////////////////////////////////////
// Dialog

class Dialog: public CWindow
    // java.awt.Dialog
{
public:
    enum { IDCANCEL = 0, IDOK  = 1 };

private:
    GtkWindow* parent;
    bool modal;
    int exitcode;

public:
    virtual ~Dialog();
    void setModal(bool f);
    virtual int setVisible(bool );
protected:
    Dialog(GtkWindow* parent, bool modal);
    void exit(int code);
private:
    Dialog(const Dialog& );                 // not implement
    Dialog& operator = (const Dialog& );    // not implement
};

////////////////////////////////////////////////////////////////////////
// MessageBox

class MessageBox: public Dialog
{
public:
    enum { IDYES = 1, IDNO = 0 };
    enum MessageType { MB_OK = 1, MB_YES_NO = 2 };
private:
    char msg[1000];
    MessageType type;
    GtkWidget* ok_button;
    GtkWidget* cancel_button;

public:
    MessageBox(GtkWindow* parent, const char* format, ...);
    virtual ~MessageBox();
    virtual int setVisible(bool );
    void setType(MessageType type);

private:
    static void onClicked(GtkButton*, MessageBox*);
    MessageBox(const MessageBox& );                 // not implement
    MessageBox& operator = (const MessageBox& );    // not implement
    void create();
};

//////////////////////////////////////////////////////////////////////
// FileDialog

class FileDialog: public Dialog
    // java.awt.FileDialog
{
    string title;
    string filename;
    string prefile;

public:
    FileDialog(GtkWindow* parent, const char* title, const char* prefile);
    virtual ~FileDialog();
    virtual string getFile() const;
    virtual int setVisible(bool );
private:
    static void onClicked(GtkWidget*, FileDialog*);
    FileDialog(const FileDialog&);              // not implement
    FileDialog& operator = (const FileDialog&); // not implement
};

//////////////////////////////////////////////////////////////////////
// SaveConfirm

class SaveConfirm: public Dialog
{
    static const char* labels[];
    GtkWidget* button[4];

public:
    enum { IDSAVE = 1, IDSAVEAS = 2, IDDISCARD = 3, IDCANCEL = 0 };

    SaveConfirm(GtkWindow* parent);
    virtual ~SaveConfirm();
    virtual int setVisible(bool );

private:
    static void onClicked(GtkWidget*, SaveConfirm*);
    void create();
};

#endif
