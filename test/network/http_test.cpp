
#include "network/http.h"
#include <memory>

using namespace std;

int main()
{
    URI* uri = URI::parse("http://www.nslabs.jp", nullptr) ;

    HttpConnection conn("test-app") ;
    HttpHeader* hh = new HttpHeader();
    conn.download(*uri, nullptr, hh, "/tmp/foo");

    delete hh;
    delete uri;
    return 0;
}
