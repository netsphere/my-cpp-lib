﻿// -*- coding:utf-8-with-signature -*-

// Sample
// https://gnutls.org/manual/html_node/Client-example-with-X_002e509-certificate-support.html

// gnutls-devel パッケージのインストールが必要。

// ルートCA証明書が必要.
// Fedora では ca-certificates パッケージに含まれる.

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <memory>
#include <assert.h>

#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>

//#include <openssl/ssl.h>
//#include <openssl/err.h>
#include <gnutls/gnutls.h>
#include <gnutls/x509.h>

#include "network/qsocket.h" // ngethostbyname()
#include "URI.h"

using namespace std;


// origin_form   = absolute-path [ "?" query ]
string origin_form(const URI& uri)
{
    return uri.getPath() +
           (uri.getQuery() != "" ? string("?") + uri.getQuery() : "");
}

// HTTP/1.1 の Host: はホスト名ではない。
string host_port(const URI& uri)
{
    return uri.getHost() +
           (uri.getPort() != -1 ? string(":") + to_string(uri.getPort()) : "");
}


void print_webpage(const URI& uri, gnutls_session_t session)
{
    char msg[1000];
    sprintf(msg, "GET %s HTTP/1.1\r\nHost: %s\r\n\r\n",
            origin_form(uri).c_str(), host_port(uri).c_str() );
    printf("\nRequest =================================================\n");
    printf(msg);

    // Send data
    gnutls_record_send(session, msg, strlen(msg));

    constexpr int BUF_SIZE = 2000;
    char buf[BUF_SIZE];
    int ret;

    // TODO: Transfer-Encoding:chunked の場合, 単にバイナリデータではない.
    do {
        // non-blocking がデフォルト. EINTR はシグナルに割り込まれた.
        ret = gnutls_record_recv(session, buf, BUF_SIZE);
    } while (ret < 0 && (ret == GNUTLS_E_AGAIN || ret == GNUTLS_E_INTERRUPTED));
    if (ret < 0)
        fprintf(stderr, "*** Error: %s\n", gnutls_strerror(ret) );

    write(1, buf, ret);
}


// @return If error, -1.
int http_connection(const string& host, int port,
                    gnutls_certificate_credentials_t xcred)
{
    // アドレスの解決
    printf("host = %s, port = %d\n", host.c_str(), port);
    addrinfo* res = ngethostbyname(host.c_str(), port, SOCK_STREAM);
    if (!res) {
        fprintf(stderr, "Fail to resolve IP address - %s\n",
                host.c_str());
        return -1;
    }

    int mysocket = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    if (mysocket < 0) {
        fprintf(stderr, "Fail to create a socket.\n");
        freeaddrinfo(res);
        return -1;
    }

    if (connect(mysocket, res->ai_addr, res->ai_addrlen) < 0) {
        fprintf(stderr, "connect() error.\n");
        freeaddrinfo(res);
        return -1;
    }

    // もういらない.
    freeaddrinfo(res);
    res = nullptr;

    // Initialize TLS session
    gnutls_session_t session;

    gnutls_init(&session, GNUTLS_CLIENT);

    // SNI 対応.
    // これをコメントアウトすると, subject が異なる証明書が来る.
    // subject `CN=ssl764794.cloudflaressl.com', issuer `CN=COMODO ECC Domain Validation Secure Server CA 2,O=COMODO CA Limited,L=Salford,ST=Greater Manchester,C=GB', serial 0x38b5187596f957a918ee432337f034f0, EC/ECDSA key 256 bits, signed using ECDSA-SHA256, activated `2021-04-07 00:00:00 UTC', expires `2021-10-14 23:59:59 UTC', pin-sha256="tYRVligHIJAz8ic5gEM+8v1julIZIFAw5z7DQQ9P7EY="
    gnutls_server_name_set(session, GNUTLS_NAME_DNS,
                           host.c_str(), host.length() );

    // It is recommended to use the default priorities
    // TLS 1.2以上にするには, "NORMAL:-VERS-TLS1.1:-VERS-TLS1.0" とする.
    // Web 上の例では "NONE:+VERS-TLS1.2:+VERS-TLS1.3" のように加えていくものが
    // 多いが、新しい規格に対応できない。
    // default 値は設定ファイルから組み立てられる。
    // Fedora の場合, /etc/crypto-policies/back-ends/gnutls.config ファイルに
    // insecure, disabled の指定がある。TLS 1.1 までが無効化されている。
    // 次のコマンドで確認できる:
    //    $ gnutls-cli --priority @SYSTEM -l
    // => VERS-TLS1.3, VERS-TLS1.2, VERS-DTLS1.2 が有効.
    gnutls_set_default_priority(session);

    // 2. Put the x509 credentials to the current session
    // 外すと Handshake failed: A TLS fatal alert has been received.   
    gnutls_credentials_set(session, GNUTLS_CRD_CERTIFICATE, xcred);

    // 3. 証明書のホスト名検証. 必須
    gnutls_session_set_verify_cert(session, host.c_str(), 0);

    // ソケットとの関連付け.
    gnutls_transport_set_int(session, mysocket);

    const gnutls_datum_t* cert_list;
    unsigned int cert_list_size = 0;
    gnutls_datum_t out;
    gnutls_x509_crt_t cert;

    // ハンドシェイク準備
    gnutls_handshake_set_timeout(session,
                                     GNUTLS_DEFAULT_HANDSHAKE_TIMEOUT);
    // Perform the TLS handshake
    int ret;
    char* desc;
    do {
        ret = gnutls_handshake(session);
    } while (ret < 0 && gnutls_error_is_fatal(ret) == 0);
    if (ret < 0) {
        // Error
        if (ret == GNUTLS_E_CERTIFICATE_VERIFICATION_ERROR) {
            // Check certificate verification status
            gnutls_certificate_type_t type;
            unsigned int status;
            gnutls_datum_t out;  // これはポインタではない.
            
            type = gnutls_certificate_type_get(session);
            status = gnutls_session_get_verify_cert_status(session);
            gnutls_certificate_verification_status_print(status,
                              type, &out, 0);
            // 例えば The certificate is NOT trusted. The certificate issuer is
            //        unknown.
            fprintf(stderr, "cert verify output: %s\n", out.data);
            gnutls_free(out.data); // 解放するのは data のほう.
        }
        fprintf(stderr, "*** Handshake failed: %s\n", gnutls_strerror(ret));
        goto end;
    }

    // サーバ証明書を表示してみる
    cert_list = gnutls_certificate_get_peers(session, &cert_list_size);
    assert(cert_list_size > 0);

    gnutls_x509_crt_init(&cert);
    // 関数名が import だが, ネイティブにコンバート、の意味。
    gnutls_x509_crt_import(cert, &cert_list[0], GNUTLS_X509_FMT_DER);
    // 例えば
    // subject `CN=uservoice.com,O=Cloudflare\, Inc.,L=San Francisco,ST=CA,C=US', issuer `CN=Cloudflare Inc ECC CA-3,O=Cloudflare\, Inc.,C=US', serial 0x091fbb9a0d2c5f8dfbd01dd37686827e, EC/ECDSA key 256 bits, signed using ECDSA-SHA256, activated `2020-06-26 00:00:00 UTC', expires `2021-06-26 12:00:00 UTC', pin-sha256="JSKETD9tg8XW5YRMkT3TeWB3ywWhq1bH88HWPcJgpYE="
    gnutls_x509_crt_print(cert, GNUTLS_CRT_PRINT_ONELINE, &out);
    printf("%s\n", out.data);
    gnutls_free(out.data);
    gnutls_x509_crt_deinit(cert);

    // 短文 (TLS1.3)-(ECDHE-X25519)-(ECDSA-SECP256R1-SHA256)-(AES-256-GCM)
    desc = gnutls_session_get_desc(session);
    printf("- Session info: %s\n", desc);
    gnutls_free(desc);

    printf("Connected to %s\n", host.c_str() );

    print_webpage(URI(string("https://") + host + "/"), session );
    print_webpage(URI(string("https://") + host + "/hoge"), session ); // error page
end:
    // shutdown
    gnutls_bye(session, GNUTLS_SHUT_RDWR);

    // ソケットを閉じる
    close(mysocket);
    gnutls_deinit(session);
    
    return 0; // OK
}


// SNI の例
unique_ptr<URI> pUri2(URI::parse("https://outlook.uservoice.com/", nullptr));

int main()
{
    gnutls_certificate_credentials_t xcred;

    // GnuTLS 3.6.4 は退行により脆弱性がある。v3.6.14以降にすること。
    if (gnutls_check_version("3.6.14") == NULL) {
        fprintf(stderr, "GnuTLS 3.6.14 or later is required for this example\n");
        return EXIT_FAILURE;
    }

    gnutls_global_init();

    // X509 stuff
    gnutls_certificate_allocate_credentials(&xcred);

    // Sets the system trusted CAs for Internet PKI
    int ret = gnutls_certificate_set_x509_system_trust(xcred);
    if (ret < 0) {
        fprintf(stderr, "Failed to load the system's default trusted CAs.\n");
        return EXIT_FAILURE;
    }

    http_connection(pUri2->getHost(), pUri2->getPort(), xcred);

    gnutls_certificate_free_credentials(xcred);
    gnutls_global_deinit();
    
    return EXIT_SUCCESS;
}

// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
