
// gcc -Wall ptr_list_test.cpp -lstdc++

#include "../ptr_set.h"
#include <iostream>
#include <typeinfo>
using namespace std;

struct Base {
  virtual ~Base() { }
  virtual void foo() = 0;
};

// 比較可能でなければならない
bool operator < (const Base& x, const Base& y) noexcept {
  return 0;
}

struct T: public Base { 
  virtual void foo() { cout << "T" << endl; }
};

struct U: public Base { 
  virtual void foo() { cout << "U" << endl; }
};


int main()
{
  ptr_set<Base*> lst;
  lst.insert(new T);
  lst.insert(new U);

  for (auto e: lst) {
    T* p = dynamic_cast<T*>(e);
    cout << typeid(e).name() << " " << typeid(p).name() << endl; //=> P1S P1T 変数の型が表示
    e->foo(); // RTTI で正しいメソッドが呼び出される
  }
}

    
