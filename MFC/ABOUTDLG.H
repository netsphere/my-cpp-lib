// Q's C++ Library
// Copyright (c) 1996-1998 Hisashi Horikawa. All rights reserved.

class CAboutDlg : public CDialog
{
public:
    CAboutDlg();

    int ChkLicenseAgree(const char* pVerStr);

private:
    BOOL m_bFirstRun;

    //{{AFX_DATA(CAboutDlg)
    enum { IDD = IDD_ABOUTBOX };
    //}}AFX_DATA

    //{{AFX_VIRTUAL(CAboutDlg)
protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV のサポート
    //}}AFX_VIRTUAL

private:

    //{{AFX_MSG(CAboutDlg)
    virtual BOOL OnInitDialog();
    afx_msg void OnLicenseChk();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
};
