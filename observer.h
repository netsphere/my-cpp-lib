// -*- coding:utf-8; mode:c++ -*-

// Q's C++ Library
// Copyright (c) 1996-2001 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

// observer-subjectパターン

#ifndef QS_OBSERVER
#define QS_OBSERVER

#include <vector>
#include <cassert>
#include <algorithm>

////////////////////////////////////////////////////////////////////////
// Observer

class Observable;
class Observer
    // interface java.util.Observer
{
    friend class Observable;

    // Observableから呼ばれる。
    virtual void update(const Observable* o, void* arg) = 0;
};


////////////////////////////////////////////////////////////////////////
// Observable

class Observable
    // class java.util.Observable
{
    typedef std::vector<Observer*> Observers;
    Observers _os;
    bool changed;

public:
    virtual ~Observable();

    virtual void attachObserver(Observer* o);       // 登録
    virtual void detachObserver(const Observer* o); // 削除
    virtual void detachObservers();           // 全て削除
    virtual size_t countObservers() const;    // 接続されているobserverの個数
    virtual bool isChanged() const;
    virtual void setChanged(bool c);

protected:
    Observable();
    virtual void notifyObservers(void* arg = NULL);  // 通知
};

inline Observable::Observable(): changed(false)
{
}

inline Observable::~Observable()
{
}

inline void Observable::attachObserver(Observer* o)
{
    assert(o);
    if (std::find(_os.begin(), _os.end(), o) == _os.end())
        _os.push_back(o);
}

inline void Observable::detachObserver(const Observer* o)
{
    assert(o);
    Observers::iterator i = std::find(_os.begin(), _os.end(), o);
    if (i != _os.end())
        _os.erase(i);
}

inline void Observable::notifyObservers(void* arg)
{
    if (isChanged()) {
        Observers::const_iterator i = _os.begin();
        while (i != _os.end())
            (*i++)->update(this, arg);
        setChanged(false);
    }
}

inline size_t Observable::countObservers() const
{
    return _os.size();
}

inline bool Observable::isChanged() const
{
    return changed;
}

inline void Observable::setChanged(bool c)
{
    changed = c;
}

inline void Observable::detachObservers()
{
    _os.clear();
}

#endif
