﻿// Q's C++ Library
// Copyright (c) 1996-1998 Hisashi Horikawa. All rights reserved.

////////////////////////////////////////////////////////////////////////////
// CAppWithHtmlHelp

// [ヘルプ]メニューで, HTMLファイルを開くようにする。
class CAppWithHelp: public CWinApp
{
public:
    CAppWithHelp();
    virtual ~CAppWithHelp();

    // Command Handlers ////////////////////////////////
protected:
    // @override
    afx_msg void OnHelp();

    //{{AFX_VIRTUAL(CAppWithHelp)
protected:
    // @override
    virtual void WinHelp(DWORD_PTR dwData, UINT nCmd = HELP_CONTEXT);
    //}}AFX_VIRTUAL

private:

    //{{AFX_MSG(CAppWithHelp)
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
};
