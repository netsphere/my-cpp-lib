﻿// -*- coding:utf-8-with-signature -*-

// Copyright (c) 1996-2001 HORIKAWA Hisashi. All rights reserved.

// June 2014, 永らく RFC 2616 (June 1999) が標準だったが, 次に置き換わった;
// - RFC 7230 Hypertext Transfer Protocol (HTTP/1.1): Message Syntax and Routing
// - RFC 7231 Hypertext Transfer Protocol (HTTP/1.1): Semantics and Content
// - RFC 7232 Hypertext Transfer Protocol (HTTP/1.1): Conditional Requests
// - RFC 7233 Hypertext Transfer Protocol (HTTP/1.1): Range Requests
// - RFC 7235 Hypertext Transfer Protocol (HTTP/1.1): Authentication
// and RFC 2818, 7538, 7615, 7694
// -> 括りなおし (先祖返り)
//    RFC 9110 HTTP Semantics (June 2022)

// RFC 7234 Hypertext Transfer Protocol (HTTP/1.1): Caching
// -> 更新
//    RFC 9111 HTTP Caching (June 2022)

// RFC 9112 HTTP/1.1 (June 2022) obsoletes RFC 7230. バージョン特有のみ切り出し


#include "http.h"
#include "../qsdebug.h"

#include <fstream>
#include <cstdio> // sprintf()
#include <cctype> // tolower()
using namespace std;

#ifdef _DEBUG
  #undef THIS_FILE
  static char THIS_FILE[]=__FILE__;
  #define new DEBUG_NEW
#endif

#ifdef _MSC_VER
//not #if defined(_WIN32) || defined(_WIN64) because we have strncasecmp in mingw
  #define strncasecmp _strnicmp
  #define strcasecmp _stricmp
#else
  #include <strings.h>
#endif


////////////////////////////////////////////////////////////////////////
// HttpHeader

// See RFC 7231
const HttpHeader::StatusMap HttpHeader::statusMsgs[] = {
    { 100, "Continue" },
    { 101, "Switching Protocols" },

/* [RFC 7540 Hypertext Transfer Protocol Version 2 (HTTP/2)]
   client to server:
     GET / HTTP/1.1
     Host: server.example.com
     Connection: Upgrade, HTTP2-Settings
     Upgrade: h2c
     HTTP2-Settings: <base64url encoding of HTTP/2 SETTINGS payload>

   response:  ※非対応の場合は, 単に 200 レスポンスを返す.
     HTTP/1.1 101 Switching Protocols
     Connection: Upgrade
     Upgrade: h2c

     [ HTTP/2 connection ...
 */

    { 200, "OK" },
    { 201, "Created" },
    { 202, "Accepted" },
    { 203, "Non-Authoritative Information" },
    { 204, "No Content" },
    { 205, "Reset Content" },
    { 206, "Partial Content" }, // RFC 7233 HTTP/1.1 Range Requests

    { 300, "Multiple Choices" },
    { 301, "Moved Permanently" },
    { 302, "Found" },
    { 303, "See Other" },
    { 304, "Not Modified" },    // RFC 7232 HTTP/1.1 Conditional Requests
    { 305, "Use Proxy" },
    { 307, "Temporary Redirect" },

    { 400, "Bad Request" },
    { 401, "Unauthorized" },    // RFC 7235 HTTP/1.1 Authentication
    { 402, "Payment Required" },
    { 403, "Forbidden" },
    { 404, "Not Found" },
    { 405, "Method Not Allowed" },
    { 406, "Not Acceptable" },
    { 407, "Proxy Authentication Required" }, // RFC 7235 HTTP/1.1 Authentication
    { 408, "Request Time-out" },
    { 409, "Conflict" },
    { 410, "Gone" },
    { 411, "Length Required" },
    { 412, "Precondition Failed" }, // RFC 7232 HTTP/1.1 Conditional Requests
    { 413, "Request Entity Too Large" },
    { 414, "Request-URI Too Large" },
    { 415, "Unsupported Media Type" },
    { 416, "Requested range not satisfiable" }, // RFC 7233 HTTP/1.1 Range Requests
    { 417, "Expectation Failed" },
    { 426, "Upgrade Required" }, // new!

    { 500, "Internal Server Error" },
    { 501, "Not Implemented" },
    { 502, "Bad Gateway" },
    { 503, "Service Unavailable" },
    { 504, "Gateway Time-out" },
    { 505, "HTTP Version not supported" },
    { 0, NULL }
};

HttpHeader::HttpHeader(const HttpHeader& a): code(a.code),
                                        version(a.version), fields(a.fields)
{
}

const char* HttpHeader::getReason() const
{
    const StatusMap* p;
    for (p = statusMsgs; p->msg; p++) {
        if (code == p->code)
            return p->msg;
    }
    return "(Unknown code)";
}

void HttpHeader::clear()
{
    version = "";
    code = -1;
    fields.clear();
}

const HttpHeader::Field* HttpHeader::findField(const char* n) const
{
    Field f(n, "");
    Fields::const_iterator it;
    return (it = fields.find(f)) != fields.end() ? &*it : NULL;
}


////////////////////////////////////////////////////////////////////////
// HttpHeader::Field

HttpHeader::Field::Field()
{
}


HttpHeader::Field::Field(const string& name, const string& value)
     : super(name, value)
{
}


// ':' で区切る. name:value
// name は小文字にする.
HttpHeader::Field::Field(const char* s)
{
    assert(s);
    while (*s && *s != ' ' && *s != '\t' && *s != ':')
        first += tolower(*s++);
    while (*s == ' ' || *s == '\t')
        s++;
    if (*s != ':')
        return;
    s++;
    while (*s == ' ' || *s == '\t')
        s++;
    while (*s && *s != '\r' && *s != '\n')
        second += *s++;
}


////////////////////////////////////////////////////////////////////////
// HttpConnection

// @param app_name User-Agent
HttpConnection::HttpConnection(const char* app_name):
    cur_port(-1), m_app_name(app_name),
    to_stop(false), timeout(30 * 1000)
{
    assert(app_name);
}

HttpConnection::~HttpConnection()
{
}

/*
SocketStream& HttpConnection::getStream()
{
    cur_host = "";
    cur_port = -1;
    return ss;
}
*/

void HttpConnection::getCurrentHost(string& host, int& port) const
{
    host = cur_host;
    port = cur_port;
}

string HttpConnection::createRequest(const char* method,
                                     const URI& uri,
                                     const char* referer)
{
    string r;
    char buf[1000];

    static const string http_ver = "HTTP/1.1";
    int port = uri.getPort() != -1 ? uri.getPort() : 80;

    if (proxy_ != "") {
        // プロキシを使うとき，request-URIは絶対URI
        sprintf(buf, "%s %s %s\r\n",
                method,
                URI::escape(uri.toString().c_str()).c_str(),
                http_ver.c_str());
        r += buf;
    }
    else {
        string object;
        if (uri.getPath() == "")
            object = "/";
        else
            object = URI::escape(uri.getPath().c_str());

        if (uri.getQuery() != "")
            object += string("?") + uri.getQuery();

        sprintf(buf, "%s %s %s\r\n",
                method,
                object.c_str(),
                http_ver.c_str());
        r += buf;
        if (port == 80) {
            // Netscape-Enterprise対策？
            sprintf(buf, "Host: %s\r\n", uri.getHost().c_str());
        }
        else
            sprintf(buf, "Host: %s:%d\r\n", uri.getHost().c_str(), port);
        r += buf;
    }

    sprintf(buf, "Accept: %s\r\nUser-Agent: %s\r\n",
            "*/*", m_app_name.c_str());
    r += buf;
    if (referer && referer[0]) {
        sprintf(buf, "Referer: %s\r\n", referer);
        r += buf;
    }

    r += string("\r\n");
    return r;
}

ssize_t HttpConnection::read_sub(void* buf, size_t len)
{
    ss.setSoTimeout(1 * 1000); // 常に1秒
    elapsed = 0;
    ssize_t r = 0;
label1:
    if (to_stop)
        return 0;
    try {
        r += ss.read(((char*) buf) + r, len - r);
        elapsed = 0;
    }
    catch (InterruptedIOException& /*e*/) {
        r += ss.gcount();
        elapsed += 1000;
        if (elapsed > timeout)
            throw;
        else {
            HttpListenerArg notifyArg(HttpListenerArg::HTTP_WAIT_BODY,
                                      elapsed);
            setChanged(true);
            notifyObservers(&notifyArg);
            goto label1;
        }
    }
    return r;
}

string HttpConnection::getline_sub()
{
    ss.setSoTimeout(1 * 1000);
    elapsed = 0;
    string r;
label1:
    if (to_stop)
        return "";
    try {
        r += ss.getline();
        elapsed = 0;
    }
    catch (InterruptedIOException& /*e*/) {
        elapsed += 1000;
        if (elapsed > timeout)
            throw;
        else {
            HttpListenerArg notifyArg(HttpListenerArg::HTTP_WAIT_BODY,
                                      elapsed);
            setChanged(true);
            notifyObservers(&notifyArg);
            goto label1;
        }
    }
    return r;
}

string str_tolower(const string& s)
{
    string r;
    const char* p = s.c_str();
    while (*p)
        r += tolower(*p++);
    return r;
}


/**
 * ダウンロードする.
 *
 * @param localfile  nullptr のとき, ヘッダのみ取得.
 *
 * @return スキームがhttp以外等：false
 *         成功：true
 * 例外
 *       ソケットがタイムアウトしたときなど
 */
bool HttpConnection::download(const URI& uri, const char* referer,
                              HttpHeader* hh, const char* localfile)
{
    to_stop = false;

    ofstream ofs;
    int port = -1;
    if (uri.getScheme() == "http" )
        port = uri.getPort() != -1 ? uri.getPort() : 80;
    else if ( uri.getScheme() == "https" )
        port = uri.getPort() != -1 ? uri.getPort() : 443;
    else
        return false;  // TODO: error message

    bool to_close = false;

    try {
        // ソケットを開く
        if (proxy_ != "") {
            if (!ss.is_open()) {
                int p = proxy_.find(':');
                if (p == string::npos)
                    return false;

                HttpListenerArg arg(HttpListenerArg::HTTP_RESOLVE_NAME, 0);
                setChanged(true);
                notifyObservers(&arg);

                ss.open(string(proxy_, 0, p).c_str(),
                                    atoi(proxy_.c_str() + p + 1));
                if (!ss.is_open())
                    return false;
            }
        }
        else {
            if (!ss.is_open() || cur_host != uri.getHost()
                            || cur_port != port) {
                ss.close();

                HttpListenerArg arg(HttpListenerArg::HTTP_RESOLVE_NAME, 0);
                setChanged(true);
                notifyObservers(&arg);

                ss.open(uri.getHost().c_str(), port);
                if (!ss.is_open())
                    return false;
                cur_host = uri.getHost();
                cur_port = port;
            }
        }

        // 要求を送信
        HttpListenerArg notifyArg(HttpListenerArg::HTTP_SEND_REQUEST, 0);
        setChanged(true);
        notifyObservers(&notifyArg);

        string req = createRequest(
            (localfile && localfile[0]) ? "GET" : "HEAD",
            uri, referer);
        ss.write(req.c_str(), req.length());

        // ヘッダを読む
        notifyArg = HttpListenerArg(HttpListenerArg::HTTP_WAIT_BODY, 0);
        setChanged(true);
        notifyObservers(&notifyArg);

        assert(hh);
        hh->clear();
        string line = getline_sub();
        if (to_stop)
            return false;
        QsTRACE(1, "%s\n", line.c_str());
        hh->version.assign(line, 0, 8);
        QsTRACE(1, "http version = %s\n", hh->version.c_str());
        if (hh->version != "HTTP/1.1")
            to_close = true;

        hh->code = atoi(line.substr(9, 3).c_str());
//      hh->reason = buf + 13;

        bool chunked = false;
        int cont_len = -1;
        while ((line = getline_sub()) != "") {
            if (to_stop)
                return false;
            HttpHeader::Field f(line.c_str());
            printf("name = '%s', value = '%s'\n", f.first.c_str(),
                   f.second.c_str()); // DEBUG
            if (f.second != "")
                hh->insertField(f);

            if (f.first == "transfer-encoding" && !strcasecmp(f.second.c_str(), "chunked"))
                chunked = true;
            else if (f.first == "content-length")
                cont_len = atoi(f.second.c_str());
            else if (f.first == "connection" && !strcasecmp(f.second.c_str(), "close"))
                to_close = true;
        }

        // 本体を読む
        unsigned char buf[1000];
        if (localfile) {
            ofs.open(localfile, ios::out | ios::trunc | ios::binary);
            if (!ofs.is_open()) {
                ss.close();
                cur_host = "";
                cur_port = -1;
                return false;
            }

            if (!chunked) {
                int count;
                if (cont_len < 0) {
                    // content-length指定なし。EOFまで読む。
                    while ((count = read_sub(buf, sizeof(buf))) > 0) {
                        if (to_stop)
                            return false;

                        notifyArg = HttpListenerArg(
                            HttpListenerArg::HTTP_READ_BODY, 0);
                        setChanged(true);
                        notifyObservers(&notifyArg);

                        ofs.write((char*) buf, count);
                    }
                }
                else {
                    // Content-Lengthで指定された長さを読む。
                    // 次のレスポンスが後ろに続いていることに注意。
                    while (cont_len > 0 &&
                           (count = read_sub(buf, min<int>(sizeof(buf),
                                                         cont_len))) > 0) {
                        if (to_stop)
                            return false;

                        notifyArg = HttpListenerArg(
                            HttpListenerArg::HTTP_READ_BODY, 0);
                        setChanged(true);
                        notifyObservers(&notifyArg);

                        ofs.write((char*) buf, count);
                        cont_len -= count;
                    }
                }
            }
            else {
                // chunked
                line = getline_sub();
                if (to_stop)
                    return false;
                sscanf(line.c_str(), "%x", &cont_len);
                int count;
                while (cont_len > 0) {
                    do {
                        if (to_stop)
                            return false;
                        count = read_sub(buf, min<int>(sizeof(buf), cont_len));
                        cont_len -= count;
                        ofs.write((char*) buf, count);
                    } while (cont_len > 0);
                    line = getline_sub();

                    line = getline_sub();
                    sscanf(line.c_str(), "%x", &cont_len);
                }
                while (getline_sub() != "")
                    ;
                ofs.close();
            }
        }
    }
    catch (SocketException& e) {
        QsTRACE(1, "例外: %d\n", e.getCode());
        ss.close();
        ofs.close();
        cur_host = "";
        cur_port = -1;
        throw;
    }

    if (to_close)
        ss.close();
    // HTTP/1.0の場合，サーバーが接続を切断してくるので，ソケットを再利用できない。
    return true;
}

void HttpConnection::setTimeout(int ms)
{
    timeout = ms;
    if (timeout < 2 * 1000)
        timeout = 2 * 1000;
    else if (timeout > 120 * 1000)
        timeout = 120 * 1000;
}

void HttpConnection::setProxy(const char* proxy)
{
    proxy_ = proxy;
}

string HttpConnection::getProxy() const
{
    return proxy_;
}

void HttpConnection::stop()
{
    to_stop = true;
}
