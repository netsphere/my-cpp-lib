﻿// -*- coding:utf-8-with-signature; mode:c++ -*-

#ifndef FILE_NAME_H__
#define FILE_NAME_H__

#include <string>

//////////////////////////////////////////////////////////////////////
// FileName

class FileName
    // java.io.File
    //      filename = [ '//' host ] path file
    //      dir = ... '/'
    //      file = body [ '.' ext ]
    //      '.'で始まるファイル名は，全体をbodyとして扱う。
{
    std::string host, dir, file;
public:
    FileName();
    FileName(const FileName& a);
    FileName(const std::string& a);
    explicit FileName(const char* a);
    // FileName(const string& h, const string& p, const string& f);
  
    virtual ~FileName();

    FileName& operator = (const FileName& a);

    std::string getHost() const;
    std::string getPath() const; // 必ず'/'で終わる。ドライブ名を含む
        // URI::getPath()はファイル名を含むが，こちらは含まないことに注意
    std::string getFile() const;

    std::string getPath2() const;
    std::string getFile2() const;
        // "/hogehoge/"のとき
        //      getPath() = "/hogehoge/", getFile() = ""
        //      getPath2() = "/", getFile2() = "hogehoge"   末尾に'/'は付かない

    std::string getBody() const;
    std::string getExt(bool normalize) const;
        // normalize = true -> 拡張子を小文字で返す。

    FileName resolve(const FileName& ref) const;
    std::string getRelative(const FileName& base) const;
        // *thisとbaseが同じファイルの時""を返す
    bool isDescendantOf(const FileName& ) const;

    std::string toString() const;

    static std::string escapeFile(const char* str);
        // 入力文字列中の'/', ':'等をエスケープして，ファイル名として使えるようにする。
    static std::string escapePath(const char* str);
        // 入力文字中の':'等をエスケープする。'/'はエスケープせず，
        // この関数の戻り値を使って深い階層のファイルを作成できる。

    bool operator == (const FileName& a) const;
    bool operator < (const FileName& a) const;

private:
    int parse(const char* str);
};

#ifdef _WIN32
extern off_t qGetFileSize(const char* filename);
extern bool isExistFile(const char* filename);
extern bool isDirectory(const char* filename);
#endif


#endif // !FILE_NAME_H__
