// Q's C++ Library
// Copyright (c) 1998-1999 Hisashi HORIKAWA. All rights reserved.

#include <cstdarg>
#include <cstdio>
#include <cassert>
#include "dialog.h"
#include "../misc.h"

using namespace std;

//////////////////////////////////////////////////////////////////////
// Dialog

Dialog::Dialog(GtkWindow* parent_, bool modal_):
                            parent(parent_), modal(modal_)
{
}

Dialog::~Dialog()
{
}

void Dialog::setModal(bool f)
{
    modal = f;
}

int Dialog::setVisible(bool visible)
{
    assert(window);
    if (GTK_WIDGET_VISIBLE(window) == visible)
        return 0;

    if (visible) {
        if (parent && !window->transient_parent)
            gtk_window_set_transient_for(window, parent);

        if (modal) {
            gtk_window_set_modal(window, TRUE);

            gtk_signal_connect(GTK_OBJECT(window), "destroy",
                                GTK_SIGNAL_FUNC(gtk_main_quit), 0);

            gtk_widget_show(GTK_WIDGET(window));

            exitcode = IDCANCEL;
            gtk_main();
            return exitcode;
        }
        else {
            gtk_window_set_modal(window, FALSE);
            gtk_widget_show(GTK_WIDGET(window));
            return 1;
        }
    }
    else {
        gtk_widget_hide(GTK_WIDGET(window));
        return 1;
    }
}

void Dialog::exit(int code)
{
    exitcode = code;
    if (modal)
        destroy();
    else {
        assert(0);
    }
}

//////////////////////////////////////////////////////////////////////
// MessageBox

MessageBox::MessageBox(GtkWindow* parent, const char* format, ...):
                        Dialog(parent, true), type(MB_OK),
                        ok_button(0), cancel_button(0)
{
    va_list arg;
    va_start(arg, format);
    vsnprintf(msg, sizeof(msg), format, arg);
    va_end(arg);
}

MessageBox::~MessageBox()
{
}

void MessageBox::onClicked(GtkButton* button, MessageBox* this_)
    // GtkButton::clicked
{
    if (button == GTK_BUTTON(this_->ok_button))
        this_->exit(IDOK);
    else if (button == GTK_BUTTON(this_->cancel_button))
        this_->exit(IDCANCEL);
    else
        assert(0);
}

void MessageBox::create()
{
    window = GTK_WINDOW(gtk_dialog_new());
    gtk_window_set_position(window, GTK_WIN_POS_MOUSE);

    GtkWidget* label = gtk_label_new(msg);
    gtk_widget_show(label);
    gtk_misc_set_padding(GTK_MISC(label), 10, 10);
    // gtk_misc_set_alignment(GTK_MISC(label), 0, 0.5);

    GtkDialog* me = GTK_DIALOG(window);
    // gtk_box_pack_start(GTK_BOX(me->vbox), label, TRUE, TRUE, 0);
    gtk_box_pack_start_defaults(GTK_BOX(me->vbox), label);

    if (type == MB_OK) {
        ok_button = gtk_button_new_with_label("了解");
        gtk_widget_show(ok_button);
        gtk_misc_set_padding(GTK_MISC(GTK_BIN(ok_button)->child), 10, 0);
        gtk_box_pack_start(GTK_BOX(me->action_area), ok_button, TRUE, FALSE, 0);
        gtk_signal_connect(GTK_OBJECT(ok_button), "clicked",
                   GTK_SIGNAL_FUNC(onClicked), this);
    }
    else if (type == MB_YES_NO) {
        ok_button = gtk_button_new_with_label("はい");
        gtk_widget_show(ok_button);
        gtk_box_pack_start(GTK_BOX(me->action_area), ok_button, TRUE, TRUE, 0);
        gtk_signal_connect(GTK_OBJECT(ok_button), "clicked",
                                    GTK_SIGNAL_FUNC(onClicked), this);
        cancel_button = gtk_button_new_with_label("いいえ");
        gtk_widget_show(cancel_button);
        gtk_box_pack_start(GTK_BOX(me->action_area), cancel_button, TRUE, TRUE, 0);
        gtk_signal_connect(GTK_OBJECT(cancel_button), "clicked",
                                    GTK_SIGNAL_FUNC(onClicked), this);
    }
}

int MessageBox::setVisible(bool visible)
{
    if (visible) {
        if (!window)
            create();
    }
    return Dialog::setVisible(visible);
}

void MessageBox::setType(MessageType type_)
{
    type = type_;
}

//////////////////////////////////////////////////////////////////////
// FileDialog

FileDialog::FileDialog(GtkWindow* parent, const char* title_, const char* prefile_):
                    Dialog(parent, true), title(title_), prefile(prefile_)
{
}

FileDialog::~FileDialog()
{
}

void FileDialog::onClicked(GtkWidget* button, FileDialog* this_)
{
    GtkFileSelection* me = GTK_FILE_SELECTION(this_->window);

    if (button == me->ok_button) {
        this_->filename = gtk_file_selection_get_filename(me);
        this_->exit(IDOK);
    }
    else if (button == me->cancel_button) {
        this_->filename = "";
        this_->exit(IDCANCEL);
    }
}

string FileDialog::getFile() const
{
    return filename;
}

int FileDialog::setVisible(bool visible)
{
    if (visible) {
        if (!window) {
            window = GTK_WINDOW(gtk_file_selection_new(title.c_str()));
            gtk_window_set_position(window, GTK_WIN_POS_MOUSE);

            GtkFileSelection* me = GTK_FILE_SELECTION(window);
            gtk_signal_connect(GTK_OBJECT(me->ok_button),
                        "clicked", GTK_SIGNAL_FUNC(onClicked), this);
            gtk_signal_connect(GTK_OBJECT(me->cancel_button),
                        "clicked", GTK_SIGNAL_FUNC(onClicked), this);

            if (prefile != "")
                gtk_file_selection_set_filename(me, prefile.c_str());
        }
        filename = "";
    }
    return Dialog::setVisible(visible);
}

//////////////////////////////////////////////////////////////////////
// SaveConfirm

void SaveConfirm::onClicked(GtkWidget* btn, SaveConfirm* this_)
{
    for (int i = 0; i < 4; i++) {
        if (btn == this_->button[i]) {
            this_->exit(i + 1);
            return;
        }
    }
    assert(0);
}

SaveConfirm::SaveConfirm(GtkWindow* parent): Dialog(parent, true)
{
}

SaveConfirm::~SaveConfirm()
{
}

const char* SaveConfirm::labels[] = {
    "上書き保存",
    "別の名前で保存...",
    "破棄",
    "キャンセル",
    0
};

void SaveConfirm::create()
{
    window = GTK_WINDOW(gtk_dialog_new());
    gtk_window_set_position(window, GTK_WIN_POS_MOUSE);

    GtkWidget* msg = gtk_label_new("文書は変更されています。保存しますか？");
    gtk_misc_set_padding(GTK_MISC(msg), 10, 10);

    GtkDialog* me = GTK_DIALOG(window);
    gtk_box_pack_start(GTK_BOX(me->vbox), msg, TRUE, TRUE, 0);
    gtk_widget_show(msg);

    for (int i = 0; labels[i]; i++) {
        button[i] = gtk_button_new_with_label(labels[i]);
        gtk_box_pack_start(GTK_BOX(me->action_area), button[i], TRUE, TRUE, 0);
        gtk_signal_connect(GTK_OBJECT(button[i]), "clicked",
                                        GTK_SIGNAL_FUNC(onClicked), this);
        gtk_widget_show(button[i]);
    }
}

int SaveConfirm::setVisible(bool visible)
{
    if (visible) {
        if (!window)
            create();
    }
    return Dialog::setVisible(visible);
}

