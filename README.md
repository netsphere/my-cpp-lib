
# C++ライブラリ。整理中。


## ポインタコンテナ

`ptr_list.h`
`ptr_map.h`
`ptr_set.h`
`ptr_vector.h`




## `text/` 文字コードの検出と変換

漢字に限定した文字コード検出.



## XML関係

See <code>XML/</code>




## URI

RFC 3986 (Jan 2005) <i>Uniform Resource Identifier (URI): Generic Syntax</i>. Obsoletes: RFC 2732, RFC 2396, RFC 1808.

これもほかのクラスの基礎。意外と複雑な URI.

```
      URI         = scheme ":" hier-part [ "?" query ] [ "#" fragment ]

      hier-part   = "//" authority path-abempty
                  / path-absolute
                  / path-rootless
                  / path-empty

      authority   = [ userinfo "@" ] host [ ":" port ]
```

 - パースしきってから unescape しなければならない。query 部と fragment 部は, scheme に基づいて解析するまで勝手に復号できない。`<mailto:infobot@example.com?body=send%20current-issue>` -- `body=` がクォートされていない。
 - path 部を hierarchical と仮定できない. `<mailto:addr1@an.example,addr2@an.example>` と書いてもよい。Hierarchical path は, path 部が "/" で始まるか `file:` など決まった scheme. そうでないものは opaque path.
 - hier-part がない URI すら妥当. `<mailto:?to=addr1@an.example,addr2@an.example>`
 - fragment が "#" で終わるのも妥当. `<http://www.w3.org/1999/02/22-rdf-syntax-ns#>`, `<http://www.w3.org/2000/01/rdf-schema#>`

TODO: RFC 6068 <i>The `mailto` URI Scheme</i> (Oct 2010) は別クラスにしたほうがよい。Ruby でも別クラス `URI::MailTo` になっている。


<b>同種のライブラリ:</b>
 - <a href="https://github.com/ben-zen/uri-library/">ben-zen/uri-library: A Modern C++ URI library.</a>
 - <a href="https://github.com/cpp-netlib/uri/">cpp-netlib/uri: cpp-netlib URI</a>


