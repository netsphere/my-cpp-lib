
#include "xml_lexer.h"
#include "xml.tab.hh"
#include <string>
#include <set>
using namespace std;
using namespace yy;

// syntax_error 例外ハンドラのなかで呼び出される。ここから例外を再送出せず、
// 表示させたりログを記録する.
void yyerror(const YYLTYPE& locp, const string& str)
{
    fprintf(stderr, "%d:%d: %s\n", locp.first_line, locp.first_column,
            str.c_str());
}

// %parse-param で指定した追加引数が全部来るバージョンが必要。
// YYBACKUP() 内から呼び出される.
void yyerror( YYLTYPE* locp, EntityMap* ge, Document** doc_ptr,
              yy::XmlLexer* scanner, const char* str )
{
    assert(locp);
    assert(str);
    yyerror(*locp, str);
}


/*
// YY_FATAL_ERROR マクロから LexerError(msg) が呼び出される. 基底クラスの定義だ
// といきなり exit() してしまう。
//   => 自分で再定義が必要.
// @override
void XmlLexer::LexerError( const char* msg )
{
    // syntax_error 例外は, パーサ側で catch される.
    fprintf(stderr, "%d:%d: %s\n", -1, -1, msg);
}
*/


// character reference (&#178;) and general entity (&lt;)
// @return エラーの場合 nullptr
string* XmlLexer::expand_ref( const char* yytext, int yyleng,
                              unsigned int targets )
{
    assert(_general_entities);

    string* ret = new string();

    const char* p;
    for (p = yytext; *p && (p - yytext) < yyleng; ) {
        if ( (targets & EXPAND_CHAR_REF) != 0 &&
                *p == '&' && *(p + 1) == '#') {
            p += 2;
            if ( (*p == 'x' || *p == 'X') && isxdigit(*(p + 1)) ) {
                // 16進
                int c = 0, n;
                n = sscanf(p + 1, "%x", &c);
                // unicode は 21bit (UTF-8 4バイト) まで.
                if (n == 1 && c >= 0 && c < 0x10FFFF) {
                    ret->append(1, c);
                    p++;
                    while ( isxdigit(*p) ) p++;
                    if (*p == ';') p++;
                    continue;
                }
            }
            else if (isdigit(*p)) {
                // 10進
                int c = 0, n;
                n = sscanf(p, "%d", &c);
                if (n == 1 && c >= 0 && c < 0x10FFFF) {
                    ret->append(1, c);
                    while ( isdigit(*p) )  p++;
                    if (*p == ';') p++;
                    continue;
                }
            }
            p--;
            ret->append(1, '&');
            continue;
        }

        if ( (targets & EXPAND_ENTITY_REF) != 0 &&
             *p == '&' && isalpha(*(p + 1)) ) {
            p++;
            const char* namst = p;
            while ( isalnum(*p) ) p++;

            string name(namst, p - namst);
            auto iter = _general_entities->find(name);
            if (iter != _general_entities->end()) {
                if ( _entity_stack.find(name) != _entity_stack.end() ) {
                    delete ret;
                    return nullptr; // violation no-recursion
                }

                _entity_stack.insert(name);
                // 頭から読み直すと, 展開しすぎになる. entity-refのみ再展開.
                string* ss = expand_ref(iter->second.c_str(),
                                        iter->second.length(),
                                        EXPAND_ENTITY_REF);
                _entity_stack.erase(name);
                if (!ss) {
                    delete ret;
                    return nullptr;
                }

                // XEE or Billion laughs attack 対策
                if ( ss->length() > Entity::max_size() ) {
                    delete ss;
                    delete ret;
                    return nullptr;
                }

                if ( (targets & EXPAND_CHAR_REF) != 0 ) {
                    // stag attvalue だけ, char-ref も展開.
                    string* tt = expand_ref(ss->c_str(), ss->length(),
                                            EXPAND_CHAR_REF);
                    assert(tt);
                    ret->append(*tt);
                    delete tt;
                }
                else {
                    ret->append(*ss);
                }
                delete ss;

                if (*p == ';') p++;
                continue;
            }
            ret->append(string("&") + string(namst, p - namst));
            continue;
        }

        if ( (targets & EXPAND_ATTR_NORM) != 0 && isspace(*p) ) {
            if (*p == '\r' && *(p + 1) == '\n')
                p += 2;
            else
                p++;
            ret->append(1, ' ');
            continue;
        }

        ret->append(1, *p++);
    }

    return ret;
}


void XmlLexer::replace_buf(int orig_len, const string& expanded)
{
/*
    // 失敗: yy_input_file の内容を変更しても, 入力に反映されない.
    stringbuf* strbuf = static_cast<stringbuf*>(buf);
    int pos = strbuf->pubseekoff(0, ios_base::cur); // -1. これもおかしい.
    string new_text = expanded + strbuf->str().substr(pos + len);
    printf("pos = %d, new_text = %s\n", pos, new_text.c_str() );
    strbuf->str(new_text);
    strbuf->pubseekpos(0);
    return;
*/

    // yyin を更新すればよい.
    // yyin は istream インスタンスなので, メソッドを呼び出してはならない.
    // (placeholder ばかり)
    stringbuf* strbuf = static_cast<stringbuf*>(yyin.rdbuf());
    //int pos = strbuf->pubseekoff(0, ios_base::cur); // -1. あかん.
    string new_text = expanded + strbuf->str().substr(_pos);
    //printf("new_text = %s\n", new_text.c_str() ); // DEBUG

    // yyrestart(), switch_streams() ではコピーされない。
    // => ヒープに置かないと, 内容が不正になる。マジか.
    stringstream* ss = new stringstream( new_text );

    // `yyrestart()' does not reset the start condition to INITIAL.
    yyrestart(*ss);
    //switch_streams(ss, nullptr);

    delete _inbuf;
    _inbuf = ss;
    _pos = 0;

/* switch_streams():
       YY_CURRENT_BUFFER を削除し,
       new_in でバッファを作り, それに切り替え.
   yyrestart():
       YY_CURRENT_BUFFER を input_file で初期化.
   結果は同じ.
 */
}


// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
