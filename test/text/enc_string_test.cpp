
#include "../EncString.h"
#include <stdio.h>
#include <string.h>
#include <assert.h>

void d_(const unsigned char* s, int len) {
    for (int i = 0; i < len; i++)
        printf("%02x ", s[i]);
    printf("\n");
}


int main()
{
    // U+1F600 GRINNING FACE
    const char* utf8 = "ABC日本語♥☎\U0001f600";
    EncString s(utf8, strlen(utf8), "UTF-8");
    
    s.convert("UTF-16");
    d_(s.data(), s.length());

    s.convert("UTF-8");
    d_(s.data(), s.length());

    assert( s.convert("ASCII") < 0 );
    printf("enc = %s\n", s.encoding().c_str());

    s.convert("UTF-32");
    d_(s.data(), s.length());

    return 0;
}
