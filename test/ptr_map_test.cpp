
// gcc -Wall ptr_list_test.cpp -lstdc++

#include "../ptr_map.h"
#include <iostream>
#include <typeinfo>
using namespace std;

struct S {
  virtual ~S() { }
  virtual void foo() = 0; };

struct T: public S {
  virtual void foo() { cout << "T" << endl; } };

struct U: public S {
  virtual void foo() { cout << "U" << endl; } };


int main()
{
  ptr_map<string, S*> lst;
  lst.insert(pair("a", new T));
  lst.insert(pair("b", new U));

  for (auto e: lst) {
    T* p = dynamic_cast<T*>(e.second);
    cout << typeid(e.second).name() << " "
         << typeid(p).name() << endl; //=> P1S P1T 変数の型が表示
    e.second->foo(); // RTTI で正しいメソッドが呼び出される
  }

/*
  // operator []
  S* s = lst["a"];
  printf("[a] = %p, !p = %d\n", s, !s );
  s = lst["x"]; // ここで要素が生成される!
  printf("[x] = %p, !p = %d\n", s, !s );
  lst.insert(make_pair("x", new U));      // これはリークする
  s = lst["x"];
  printf("[x] = %p, !p = %d\n", s, !s );
*/

  return 0;
}
