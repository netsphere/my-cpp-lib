// Q's C++ Library
// Copyright (c) 1998-2001 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

#ifndef QSLIB_DOC_H
#define QSLIB_DOC_H

#include <string>
#include <vector>
#include <gtk/gtk.h>
using namespace std;

// #include "observer.h"
#include "window.h"

//////////////////////////////////////////////////////////////////////
// Controller

class Controller
{
    friend class Document;    // Document::attachController()

    GtkWindow* frame;
protected:
    class Document* document;
public:
    virtual ~Controller();
    virtual void updateTitle(const string& ) = 0;
    void attachWindow(GtkWindow* f);
        // ダイアログボックスを表示するときの親となるウィンドウを指定

    virtual Document* getDocument() const;

    virtual bool save();
    virtual bool saveAs();
    virtual bool close();

protected:
    Controller();
private:
    Controller(const Controller& );                 // not implement
    Controller& operator = (const Controller& );    // not implement
};

//////////////////////////////////////////////////////////////////////
// Document

class Document // : public Observable
    // MFC.CDocument
    // プログラムの主要なドキュメントを格納する。
    // このクラスは，ファイルにドキュメントを保存し，
    // 1ドキュメント1ウィンドウのプログラム向け。
{
    typedef vector<Controller*> Conts;
        // iteratorの引き算のできるコンテナ

public:
    string filename;
private:
    Conts conts;
    bool modified;
        // observerはdoc->viewの連絡に使うので，ドキュメント未保存
        // フラグは，別に用意する。

public:
    Document();
    virtual ~Document();

    void attachController(Controller* cont);
    void detachController(Controller* cont);
    int countControllers() const;

    virtual bool save() = 0;

    bool isModified() const;
    void setModified(bool m);
    string getTitle(Controller* ) const;
    void updateTitle() const;
private:
    Document(const Document& );               // not implement
    Document& operator = (const Document& );  // not implement
};

#endif  // QSLIB_DOC_H
