// Q's C++ Library
// Copyright (c) 1996-1998 Hisashi Horikawa. All rights reserved.

// 線形合同法による乱数

// http://www001.upp.so-net.ne.jp/isaku/rand.html
// メルセンヌツイスタ (MT) などを使用すべき.

#include "qTypes.h"

//////////////////////////////////////////////////////////////////////
// rnd64

template <typename Ty>
class basic_rnd 
{
  typedef Ty value_type;
  value_type seed;

public:
  basic_rnd(value_type seed = 1) {
    init(seed);
  }

  ~basic_rnd() { }

  value_type next() {
    seed = seed * 1566083941UL + 1;
    return seed;
  }

private:
  void init(value_type seed_) {
    seed = seed_;
    next(); next(); next(); // warm up
  }

  basic_rnd(const basic_rnd& ); // not impl.
  basic_rnd& operator = (const basic_rnd& ); // not impl.
};
 

//////////////////////////////////////////////////////////////////////

typedef basic_rnd<uint64_t> rnd64;
typedef basic_rnd<uint32_t> rnd32;

