﻿// -*- coding:utf-8-with-signature -*-

// See HTTP2最速実装をmain()関数だけで簡単に説明する(非SSL編)
//     https://qiita.com/0xfffffff7/items/0960527dca6de364daeb
// => 非SSLの場合は, 当面の間, HTTP/1.1 から upgrade したほうがいい.

// Implementations
// https://github.com/http2/http2-spec/wiki/Implementations
//   "h2" HTTP/2 over TLS
//   "h2c" HTTP/2 over TCP (平文)    .. RFC 9113 (June 2022) で廃止.
//   h2-14  ドラフト版.

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string>

#include "network/qsocket.h"
#include "URI.h"
#include "network/http2_common.h"

#ifdef _WIN32
  #define STRICT
  #define WIN32_LEAN_AND_MEAN
  #include <winsock2.h>
  #include <windows.h>
  #pragma warning(disable:4996)
  #define SHUT_RDWR SD_BOTH
#else
  #include <sys/types.h>
  #include <sys/socket.h>  // send(), recv()
  #include <netinet/in.h>
  #include <netinet/tcp.h>
  #include <netdb.h>
  #include <fcntl.h>
  #include <signal.h>
  #include <unistd.h>
#endif
#include <string.h>
#include <assert.h>
#include <map>
#include <memory> // unique_ptr

using namespace std;


#if 0 // RFC 9113 (June 2022) で HTTP/1.1 からの upgrade は廃止された (obsolete)
/**
 * @return HTTP status code.
 *         正しくない行の場合は -1
 */
static int get_response(const string& line)
{
    if (line.find("HTTP/", 0) != 0 )
        return -1;

    // HTTP/1.1 101 Switching Protocols
    return stoi(line.substr(9));
}
#endif


#define SETTINGS_ACK_FLAG 1u

const char settingframeAck[FRAME_HDR_LENGTH] = {
        0x00, 0x00, 0x00,
        0x04, // SETTINGS
        SETTINGS_ACK_FLAG,
        0x00, 0x00, 0x00, 0x00 };


int main(int argc, char* argv[])
{
    //------------------------------------------------------------
    // 接続先ホスト名.
    // HTTP2に対応したホストを指定します.
    //------------------------------------------------------------
    unique_ptr<URI> uri( URI::parse("http://nghttp2.org/", nullptr) );
    // Not HTTP/2
    //unique_ptr<URI> uri( URI::parse("http://info.cern.ch/", nullptr) );

    //------------------------------------------------------------
    // TCPの準備.
    //------------------------------------------------------------
#ifdef _WIN32
    WSADATA wsaData;
    if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) 
        return 1;
#endif

    int error = 0;

    // ホスト名を解決
    addrinfo* res = ngethostbyname(uri->getHost().c_str(), uri->getPort(),
                                   SOCK_STREAM);
    if ( !res ) {
        fprintf(stderr, "host not found: %s\n", uri->getHost().c_str());
        return 1;
    }

    SOCKET _socket = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    if (_socket < 0) {
        fprintf(stderr, "Fail to create a socket.\n");
        freeaddrinfo(res);
        return 1;
    }
    
    // 必須. See https://http2.github.io/faq/
    // man 7 TCP
    int flag = 1;
    int r = setsockopt(_socket, IPPROTO_TCP, TCP_NODELAY, (char*) &flag, sizeof(flag));
    if ( r < 0 ) {
        error = get_error();
        //::shutdown(_socket, SHUT_RDWR);
        //close_socket(_socket);
        return 1;
    }
        
    if (connect(_socket, res->ai_addr, res->ai_addrlen) < 0) {
        fprintf(stderr, "connect() error.\n");
        freeaddrinfo(res);
        return 1;
    }

    // もういらない.
    freeaddrinfo(res);
    res = nullptr;

    SocketStreamBuf conn;
    conn.connected(_socket, uri->getHost().c_str() );
    Http2Reader reader;
    reader.attach(&conn);

#if 0 // RFC 9113 (June 2022) で HTTP/1.1 からの upgrade は廃止された (obsolete)
    
    // (1) HTTP/1.1 で接続して upgrade するか, (2) HTTP/2 で接続して、切断され
    // た場合は HTTP/1.1 で接続しなおすか、のいずれか.
    
    // HTTP/1.1 サーバの可能性が高い場合は, HTTP/1.1 から upgrade する.
    // HTTP2-Settings ヘッダは, ペイロードだけをbase64url するので、すべてデ
    // フォルト値なら空でよい.
    string upgrade = string("GET ") + uri->getPath() + " HTTP/1.1\r\n" +
                 "Host: " + uri->getHost() + "\r\n" +
                 "Connection: Upgrade, HTTP2-Settings\r\n" +
                 "Upgrade: h2c\r\n" +
                 "HTTP2-Settings: " + "" + "\r\n\r\n";
    r = conn.sputn(upgrade.c_str(), upgrade.length() );
    if ( r < 0 ) {
        error = get_error();
        conn.close();
        return 1;
    }

    // response が HTTP/1.1 101 Switching Protocols かどうか
    string line = reader.getline();
    int status_code = get_response(line);
    printf("status_code = %d: %s\n", status_code, line.c_str()); // DEBUG
    
    // HTTP/1.1 ヘッダを読みとばす
    while ( (line = reader.getline()) != "" )
        printf("%s\n", line.c_str());

    if (status_code != 101) {
        fprintf(stderr, "Not HTTP/2\n");
        return 1;
    }
#endif

    //------------------------------------------------------------
    // HTTP/2 通信のフロー
    //
    // まず最初に SETTINGS フレームを必ず交換します.
    // SETTINGS フレームを受信したら、設定を適用したことを伝えるために必ずACKを送ります.
    //
    // Client -> Server  SettingsFrame
    // Client <- Server  SettingsFrame
    // Client -> Server  ACK
    // Client <- Server  ACK
    //
    // Client -> Server  HEADERS_FRAME (GETなど)
    // Client <- Server  HEADERS_FRAME (ステータスコードなど)
    // Client <- Server  DATA_FRAME (Body)
    // 
    // Client -> Server  GOAWAY_FRAME (送信終了)
    //------------------------------------------------------------

    // upgrade の場合でも, 送信必要.
    string pri = CLIENT_CONNECTION_PREFACE;
    r = conn.sputn( pri.c_str(), pri.length() );
    if ( r < 0 ) {
        error = get_error();
        conn.close();
        return 1;
    }

    //------------------------------------------------------------
    // client connection preface = Settingsフレーム (0x4) の送信.
    // 全てデフォルト値を採用するためpayloadは空です。
    // SettingsフレームのストリームIDは0です.(コネクション全体に適用されるため)

    // upgrade 前に送信しているが、再度送信が必須。
    // => これを送らないと, エラーになる.
    const char settingframe[FRAME_HDR_LENGTH] = {
        0x00, 0x00, 0x00,
        0x04, // SETTINGS
        0x00, 0x00, 0x00, 0x00, 0x00 };
    dump_frame_header('S', settingframe);
    r = conn.sputn( settingframe, FRAME_HDR_LENGTH );
    if ( r < 0 ) {
        error = get_error();
        conn.close();
        return 1;
    }
    
    //------------------------------------------------------------
    // server connection preface = Settingsフレームの受信.
    //------------------------------------------------------------
    const char* frame = reader.read_frame();
    if (!frame) {
        fprintf(stderr, "server close.\n");
        conn.close();
        return 1;
    }
    dump_frame_header('R', frame);
    assert(*(frame + 3) == 0x4); // SETTINGS

    // スキップ (手抜き)
    //int payload_length = 0;
    //ntoh3(frame, &payload_length);
    //bufptr += 9 + payload_length; readleft -= (9 + payload_length);
    
    //------------------------------------------------------------
    // ACKの送信.
    // ACKはSettingsフレームを受け取った側が送る必要がある.
    // ACKはSettingsフレームのフラグに0x01を立ててpayloadを空にしたもの.
    //
    dump_frame_header('S', settingframeAck);
    r = conn.sputn( settingframeAck, FRAME_HDR_LENGTH );
    if (r < 0 ) {
        error = get_error();
        conn.close();
        return 1;
    }

    // 二つ目のファイルを要求 -- これがないと, クライアントからの送信が正しいか確認できない
    map<string, string> m;
    m.insert(make_pair(":method", "GET"));
    m.insert(make_pair(":path", "/httpbin/") );
    m.insert(make_pair(":scheme", "http"));
    m.insert(make_pair(":authority", uri->getHost()));
    r = send_HEADERS_frame(&conn, 3, m);
    if ( r < 0 ) {
        int error = get_error();
        conn.close();
        return 1;
    }

    // ストリームを受信する.
    recv_streams(reader);

    recv_streams(reader);

    //------------------------------------------------------------
    // GOAWAYの送信.
    // これ以上データを送受信しない場合は GOAWAYフレーム (0x7) を送信します.
    // ストリームIDは「0x00」(コネクション全体に適用するため)
    //------------------------------------------------------------
    static const char goawayframe[17] = {
        0x00, 0x00, 0x00,
        0x07, // GOAWAY
        0x00, 0x00, 0x00, 0x00, 0x00,
        0x00,0x00,0x00,0x01,0x00,0x00,0x00,0x00 };
    dump_frame_header('S', goawayframe);
    r = conn.sputn( goawayframe, 17 );
    if ( r < 0 ) {
        error = get_error();
        conn.close();
        return 1;
    }

    //------------------------------------------------------------
    // 後始末.
    //------------------------------------------------------------
    conn.close();

    return 0;
}


// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
