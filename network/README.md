# ネットワーク関係

ヘッダ     クラス          内容
--------   -------------   ------------------------
qsocket.h  CSocket         ソケットのラッパ
http.h     HttpConnection  ソケットを再利用する HTTP クライアント.



HTTP/2 sample
 - http://www.nslabs.jp/http2-client-implementation-sample-non-tls.rhtml
 - http://www.nslabs.jp/http2-client-implementation-sample-tls-version.rhtml
 

SocketStreamBuf
 - [streambuf を拡張し, ソケット対応 [C++]](https://www.nslabs.jp/socket-streambuf.rhtml)
