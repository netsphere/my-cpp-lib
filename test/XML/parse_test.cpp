
#include "xml_lexer.h"
#include "xml.tab.hh"
#include "test_common.h" // new_stringstream_from_file

#ifndef _WIN32
  #include <dirent.h> // scandir()
#endif

using namespace std;


// @return parse に失敗したら, 非0
int check_parser( const string& dir, const char* filename )
{
    string enc = "";
    stringstream* iss =
            new_stringstream_from_file((dir + "/" + filename).c_str(), &enc);
    if (!iss) {
        fprintf(stderr, "not found: %s/%s\n", dir.c_str(), filename);
        abort();
    }

    EntityMap general_entities;
    Document* doc_ptr = nullptr;

    yy::XmlLexer scanner(iss, &general_entities);
    iss = nullptr; // 所有権を scanner に渡す

    int status;
    YYLTYPE location { .first_line = 1, .first_column = 0, .last_line = 1, .last_column = 0 };
    yypstate* ps = yypstate_new();
    do {
        YYSTYPE value;
        yytokentype t = scanner.yylex(&value, &location);
        // 正常に parse できたときは 0, エラーの時は 1
        status = yypush_parse(ps,   // yypstate *ps
                        t,          // int pushed_char
                        &value,     // YYSTYPE const *pushed_val
                        &location,  // YYLTYPE *pushed_loc
                        &general_entities, // EntityMap* general_entities,
                        &doc_ptr,   // Document** doc_ptr,
                        &scanner);  // parser 側から lexer.param_entities に追加
    } while (status == YYPUSH_MORE);
    yypstate_delete(ps);

    printf("yypush_parse() finish status = %d\n", status);
    if (doc_ptr)
        printf("%s\n", doc_ptr->str().c_str());

    delete doc_ptr;

    return status;
}


int check_dir( const string& dir, bool should_accept )
{
    struct dirent** namelist = nullptr;
    int num = ::scandir(dir.c_str(), &namelist,
                        [](const struct dirent* ent)->int {
                            if (string(ent->d_name).find(".xml") != string::npos)
                                return true;
                            else
                                return false;
                        }, nullptr);
    if (num < 0) {
        perror("scandir()");
        return -1;
    }

    for (int i = 0; i < num; i++) {
        int r = check_parser( dir, namelist[i]->d_name );
        if (should_accept && r != 0) {
            printf("%s/%s: should accept but failed.\n\n",
                   dir.c_str(), namelist[i]->d_name);
        }
        else if (!should_accept && r == 0) {
            printf("%s/%s: should reject but succeeded.\n\n",
                   dir.c_str(), namelist[i]->d_name);
        }
        free(namelist[i]);
    }
    free(namelist);

    return 0;
}


/**
 * XML parsing test
 *
 * TODO: parse できるだけでなく, out との一致まで確認する.
 */
int main(int argc, char* argv[])
{
    //yydebug = 1;

    if (argv[1]) {
        check_parser(".", argv[1]);
        return 0;
    }

    //check_parser("../xmlts20130923/xmltest/valid/sa", "001.xml", true);
    //check_parser("../xmlts20130923/xmltest/not-wf/sa", "012.xml", false);
    //return 0;

    // XML W3C Conformance Test Suite
    //      ext-sa: external entity
    //      not-sa: non-standalone
    //      sa: standalone

    ///////////////////
    // 1. valid documents: should accept

    // <!ENTITY % e SYSTEM "026.ent">
    // %e;
    check_dir("xmlts20130923/xmltest/valid/not-sa", true);
    check_dir("xmlts20130923/xmltest/valid/sa", true);
    check_dir("xmlts20130923/xmltest/valid/ext-sa", true);

    ///////////////////
    // 2. invalid documents: parse = should reject

    check_dir("xmlts20130923/xmltest/invalid", false);
    check_dir("xmlts20130923/xmltest/invalid/not-sa", false);

    ///////////////////
    // 3. non-wf documents: should reject

    check_dir("xmlts20130923/xmltest/not-wf/sa", false);
    check_dir("xmlts20130923/xmltest/not-wf/ext-sa", false);
    check_dir("xmlts20130923/xmltest/not-wf/not-sa", false);

    return 0;
}

// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
