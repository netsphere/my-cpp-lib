/* -*- mode:c++ -*- */

/* %option unicode 
   RE/flex を試したが, unicode を使おうとすると, scanner が大爆発. */

%option c++

/* C++では必須. yyFlexLexer が $(prefix)FlexLexer になる。
   出力ファイルが lex.$(prefix).cc になる。
   この $(prefix)FlexLexer を派生させて自分の lexer クラスを作る. */
%option prefix="XmlImpl"

/* interactive 不要. read() で読み込み. 
   コメントアウトすると, YY_INTERACTIVE が定義され, get() で読み込む. */
%option batch

%option 8bit

/* マッチしなかったときのデフォルト動作 (ECHO: 標準出力への出力) を抑制. */
%option nodefault

/* ファイル一つで終了 */
%option noyywrap

/* YY_NO_UNPUT が定義され, yyunput() がコメントアウトされる. */   
%option nounput

/* <unistd.h> は不要. YY_NO_UNISTD_H が定義される. */
%option nounistd
   
/* To use start condition stacks.
   BEGIN に代えて, yy_push_state() / yy_pop_state() を使う */
%option stack

%option warn

/* %option yylineno */
   
   
%{
#include "xml_lexer.h"
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <ostream>

#include "node.h"
#include "xml.tab.hh"
using namespace std;

void yyerror(const YYLTYPE& locp, const string& str);

static const char* skip_spc(const char* str)
{
    const char* p = str;
    for (p = str; isspace(*p); p++)
        ;
    return p;
}


// <FlexLexer.h> class yyFlexLexer で yylex() が宣言されており, 使わなくても
// 定義が必要.
int XmlImplFlexLexer::yylex()
{
    abort(); // 呼び出されてはいけない.
}

/**
 * bison の .y ファイル内, %param 命令で, scanner に渡す引数を指定できる.
 * flex 側では, YY_DECL 定義で, 引数, 戻り値の型を宣言する.
 */
#define YY_DECL yytokentype yy::XmlLexer::yylex(  \
                        YYSTYPE* yylval, YYLTYPE* yylloc)

// EOF に達したときのトークン. 値 0 でなければならない。
//   -> 自動定義される YYEOF でよい.
#define yyterminate() return yytokentype::YYEOF; //END_OF_FILE

%}


  /* 名前は SGML による */
etago   "</"
mdo     "<!"
com     "--"
msc     "]]"
cro     "&#"
pio     "<?"

  /* white space. isspace() と合わせる. */
wsp		[ \f\v\t\r\n]+

  /* TODO: Unicode 対応。Flex では対応できない. 手書きで書く必要がある. */
Digit           [0-9]
  /* [4] NameStartChar::=  ":" | [A-Z] | "_" | [a-z] | [#xC0-#xD6] | 
                           [#xD8-#xF6] | [#xF8-#x2FF] 以下略 
         ":" だけでもよいことに注意! 
         下の定義は、手抜きで, UTF-8 [RFC3629] のコード範囲. */
NameStartChar   [\:A-Za-z_\xc2-\xf4\x80-\xbf]
NameChar        ({NameStartChar}|{Digit}|[.-])
NCName		({NameStartChar}{NameChar}*)
QName           (({NCName}\:)?{NCName})

EntityRef       ("&"{NCName}";")
CharRef         ({cro}[0-9]+";"|{cro}"x"[0-9a-fA-F]+";")

  /* XML では, SGML と違い, タグのなかでのコメントを考えなくてよい. */  
commento	({mdo}{com})

cdatao      "<![CDATA["
endcdata   "]]>"


  /**********************************
   * start condition states 
   */

/* 開始タグ or 空要素タグの内側. */
%xstate S_STAG

/* ポイントの一つ. element の内側. 空白も送出 */
%xstate S_CONTENT

%xstate S_ETAG

/* CDATA section の内側 */
%xstate S_CDSECT

/* PI の内側は何でもあり. */
%xstate S_PI

%xstate S_DOCTYPE

%xstate S_MD

%xstate S_COMMENT

%{
  // bison も flex も, 自動で position を更新しない.
  // flex 側で, 位置情報を適切に更新する必要がある.
  // 行番号だけで十分な場合は, %option yylineno を指定し, yylineno が使える.
  // yylloc->last_column はトークンの最後の文字を指す.
#define YY_USER_ACTION \
    _pos += yyleng; \
    yylloc->first_line = yylloc->last_line; \
    yylloc->first_column = yylloc->last_column + 1; \
    for (const char* pp = yytext; *pp; pp++) { \
      if (*pp == '\n') {                        \
        yylloc->last_line++; yylloc->last_column = 1; \
      } \
      else                     \
        yylloc->last_column++; \
    }
  
%}


/***********************************************************************
 * Regular Expressions Part
 */

%%

    /**********
     * prolog 
     * [1]  document ::=   	prolog element Misc*
     * [22] prolog   ::=   	XMLDecl? Misc* (doctypedecl Misc*)?
     */

<INITIAL>{pio}[Xx][Mm][Ll]{wsp}    {
        // XMLDecl があるのは prolog のみ.  
        yy_push_state(S_STAG);  // 開始タグと同様の構文
        return tokentype::XML_DECL;
              }

<INITIAL,S_CONTENT,S_DOCTYPE>{pio}{NCName}  {
        yylval->sval = strdup(yytext + 2);
        buffer.clear();
        yy_push_state(S_PI);
        return tokentype::PI_NAME;
    }
 
<INITIAL>{mdo}{NCName}    {
    if (string(yytext) == "<!DOCTYPE") {
        yy_push_state(S_DOCTYPE); sub_state = 0; // 構文が違う.
        return tokentype::DOCTYPE;
    }
    else {
      yyerror(*yylloc, string("unknown declaration '") + (yytext + 2));
      return tokentype::YYUNDEF; //LEX_ERROR;
    }
  }

<INITIAL,S_CONTENT,S_DOCTYPE>{commento}    {
        yy_push_state(S_COMMENT);
        buffer.clear();
    }

<INITIAL>{wsp}  {
        // XMLDecl の前には空白も置置けない。パーサで確認必要.
        return tokentype::SPC;     
    }

<INITIAL,S_CONTENT>"<"{QName} {
        // XMLDecl, doctypedecl なしに要素、がありうる
        // XML は, case-sensitive. See XML 1.0: 1.2 Terminology 
        yylval->sval = strdup(yytext + 1);
        yy_push_state(S_STAG);
        return tokentype::STAG;
    }


    /**********
     * 開始タグ, 空要素タグ内
     */

<S_STAG>{
\"[^\"]*\"|\'[^']*\'  {
    // XML 1.0 では属性値に"<" を含めることはできない (エラー) が, 受け入れる.
    // param entity は無視し, reference は展開.
    yylval->stdstr = expand_ref(yytext + 1, strlen(yytext + 1) - 1,
                (EXPAND_ENTITY_REF | EXPAND_CHAR_REF | EXPAND_ATTR_NORM) );
    if (!yylval->stdstr) {
      yyerror(*yylloc, string("entity-ref error in attvalue '") + yytext);
      return tokentype::YYUNDEF; //LEX_ERROR;
    }
    return tokentype::ATTVALUE;
  }

{QName}         {
        // 属性名の省略 (SGML) は, パーサで確認.
        // 属性名の正規化はしない. が, case-insensitive manner で重複はエラーに
        // する.
        yylval->sval = strdup(yytext);
        return tokentype::ATTNAME;
    }

{wsp}?"="{wsp}?  { return tokentype::EQ; }

"?>"           { 
        // XMLDecl. PI ではない.
        yy_pop_state();
        return tokentype::PIC; }

"/>"           { yy_pop_state(); return tokentype::NETC; }

">"            {
        yy_pop_state(); yy_push_state(S_CONTENT);
        buffer.clear();
        return tokentype('>');
    }

{wsp}    {  // skip
    }
}


    /**********
     * content 内
     * [43] content ::=CharData? ((element | Reference | CDSect | PI | Comment)
     *                             CharData?)*
     */
  
<S_CONTENT>{
{etago}{QName}  {
        yylval->sval = strdup(yytext + 2);
        BEGIN S_ETAG;   // push しない.
        return tokentype::ETAG;
    }

{cdatao}    {
        buffer.clear();
        yy_push_state(S_CDSECT);
    }

    /* &amp; など */
{EntityRef}  {
    // エンティティ宣言を確認する必要あり.
    string* s = expand_ref(yytext, yyleng, EXPAND_ENTITY_REF );
    if ( !s ) {
      yyerror(*yylloc, "entity-ref error in content");
      return tokentype::YYUNDEF; //LEX_ERROR;
    }
    if (*s == yytext) {
        // 見つからなかった.
        delete s;
        yylval->stdstr = new string(yytext + 1, yyleng - 2);
        return tokentype::ENTITY_REF;
    }

    replace_buf(yyleng, *s);
    delete s;
    //unput('X'); unput('<'); unput('Z');
    //yyless(0);
    //return tokentype::CHAR_DATA;
  }

{CharRef} {
    // content への追加は, パーサで行う.
    yylval->stdstr = expand_ref(yytext, yyleng, EXPAND_CHAR_REF);
    assert(yylval->stdstr);
    return tokentype::CHAR_DATA;
  }

[^<&]+|\n    {
        // chardata の連結は, パーサのほうで行う.
        yylval->stdstr = new string(yytext);
        return tokentype::CHAR_DATA;
    }
}

 
    /**********
     * 終了タグ内
     */
 
<S_ETAG>{
{wsp}?">"            {
        yy_pop_state(); // S_CONTENT を pop する
    }
}
 

    /**********
     * CDATA Sections
     * [18] CDSect ::= CDStart CData CDEnd
     */

<S_CDSECT>{
{endcdata}     {
        yy_pop_state();
        // 展開しない.         
        yylval->stdstr = new string(buffer);
        buffer.clear();
        return tokentype::CDATA;
    }

.|\n     { // CDATAセクションでは空白，改行をそのまま返す。
        buffer.append(yytext); }
}


    /**********
     * Processing Instructions
     * [16] PI  ::=  '<?' PITarget (S (Char* - (Char* '?>' Char*)))? '?>'
     */

<S_PI>{
"?>"   { yy_pop_state();
        // 先頭だけカット.
        yylval->sval = strdup(skip_spc(buffer.c_str()));
        buffer.clear();
        return tokentype::PI_BODY;
 }
 
.|\n   { buffer.append(yytext); }
}


    /**********
     * Document Type Definition
     * [28] doctypedecl ::= '<!DOCTYPE' S Name (S ExternalID)? S? 
     *                      ('[' intSubset ']' S?)? '>'
     */

<S_DOCTYPE>{
\"[^\"]*\"|\'[^']*\'  {
        // public_id, system_uri は, 展開しない.
        yylval->stdstr = new string(yytext + 1, strlen(yytext + 1) - 1);
        return tokentype::ATTVALUE;
    }

{NCName}       {
    if (!sub_state) {
        yylval->sval = strdup(yytext);
        sub_state = 1;
        return tokentype::ATTNAME;
    }
    else {
        // 文脈依存
        sub_state = 0;
        if (string(yytext) == "SYSTEM")
            return tokentype::SYSTEM;
        else if (string(yytext) == "PUBLIC")
            return tokentype::PUBLIC;
        else {
          yyerror(*yylloc, string("unknown keyword '") + yytext);
          return tokentype::YYUNDEF; //LEX_ERROR;
        }
    }
  }

"["|"]"  {  return tokentype(yytext[0]); }

{mdo}{NCName}    {
        yy_push_state(S_MD); sub_state = 0;
        buffer.clear();
        if (string(yytext + 2) == "ENTITY")
            return tokentype::ENTITY_DECL;
        else if (string(yytext + 2) == "NOTATION")
            return tokentype::NOTATION_DECL;
        else {
            yylval->sval = strdup(yytext + 2);
            return tokentype::DECL_NAME;  // 手抜き.
        }
    }
 
">"   {
        yy_pop_state();
        return tokentype::DTD_END;
    }

"%"{NCName}";"   {
    string name = string(yytext + 1, yyleng - 2);
    auto itr = param_entities.find(name);
    if (itr == param_entities.end()) {
        // 見つからなかった. lexer 側ではエラーにせず, トークンを投げる.
        yylval->stdstr = new string(yytext + 1, yyleng - 2);
        return tokentype::PE_REF;
        //throw XmlParser::syntax_error(*yylloc,
        //                    string("unknown parameter entity '") + name);
    }

    replace_buf(yyleng, itr->second);
    //return tokentype::PE_REF;
  }

{wsp}   { // empty
    }
}


    /**********
     * Entity Declaration
     * [70] EntityDecl ::= GEDecl | PEDecl
     * [71] GEDecl     ::= '<!ENTITY' S Name S EntityDef S? '>'
     * [72] PEDecl     ::= '<!ENTITY' S '%' S Name S PEDef S? '>'
     * [73] EntityDef  ::= EntityValue | (ExternalID NDataDecl?)
     * [74] PEDef      ::= EntityValue | ExternalID
     *
     *     <!ENTITY % zz '&#60;!ENTITY tricky "error-prone" >' >
     */

<S_MD>{
    /* [9] EntityValue */
\"[^\"]*\"|\'[^']*\'  {
    // See 4.4 XML Processor Treatment of Entities and References
    // Reference in EntityValue: CharRef *のみ* 即時展開.
    yylval->stdstr = expand_ref(yytext + 1, strlen(yytext + 1) - 1,
                                EXPAND_CHAR_REF );
    assert(yylval->stdstr);
    return tokentype::ATTVALUE;
  }

"%"    { return tokentype('%'); }

{NCName}   {
        if (!sub_state) {
            yylval->sval = strdup(yytext);
            sub_state = 1;
            return tokentype::ATTNAME;
        }
        else {
            //sub_state = 0;
            if (string(yytext) == "SYSTEM")
                return tokentype::SYSTEM;
            else if (string(yytext) == "PUBLIC")
                return tokentype::PUBLIC;
            else if (string(yytext) == "NDATA") {
                sub_state = 0;  // Name が続く.
                return tokentype::NDATA;
            }
            else {
                // <!ATTLIST doc a1 CDATA #IMPLIED>
                yylval->sval = strdup(yytext);
                return tokentype::ATTNAME;
            }
        }            
    }

{wsp}   { // DTD内では S に意味がある。 -> でも構文が冗長になる。skipする.
        //return tokentype::WSP;
    }

  /* 最長一致なので, こうしないと, {wsp} に何もマッチしなくなる. */
[^ \f\v\t\r\n>\"']+    {
        yylval->sval = strdup(skip_spc(yytext));
        return tokentype::DECL_BODY;
    }

">"   {
        yy_pop_state();
        return tokentype::DECL_END;
    }
}


    /**********
     * Comments
     * XML では, "--" のネストは考慮不要.
     * [15] Comment  ::=  '<!--' ((Char - '-') | ('-' (Char - '-')))* '-->'
     */

<S_COMMENT>{
"-->"   {
        // 展開しない.
        yylval->stdstr = new string(buffer); 
        buffer.clear();  
        yy_pop_state();
        return tokentype::T_COMMENT;
    }

"--" {
        yyerror(*yylloc, "`--` must not occur within comments." );
        return tokentype::YYUNDEF; //LEX_ERROR;
    }

.|\n   { buffer.append(yytext); }
}


    /* fallback : error */

<*>.|\n      {
    fprintf(stderr, "Lexical Error (state %d, character %s 0x%x)\n",
                YY_START, yytext, (unsigned char) yytext[0]);
    yyerror(*yylloc, string("unknown character '") + yytext);
    return tokentype::YYUNDEF; //LEX_ERROR;
  }

%%

/* don't use lexer.l for code, organize it logically elsewhere */
