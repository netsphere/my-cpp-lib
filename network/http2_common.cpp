﻿// -*- coding:utf-8-with-signature -*-

#include "http2_common.h"
#include <string>

#ifdef _WIN32
  #define STRICT
  #define WIN32_LEAN_AND_MEAN
  #include <winsock2.h>
  #include <windows.h>
#else
  #include <sys/socket.h> // shutdown(), recv()
  #include <unistd.h>  // close()
  #include <arpa/inet.h>
#endif
#include <string.h>  // memmove()
#include <assert.h>
#include <nghttp2/nghttp2.h>  // 要 libnghttp2-devel パッケージ
  
using namespace std;


int get_error()
{
#ifdef _WIN32
    return WSAGetLastError();
#else
    return errno;
#endif
}


/**
 * 3バイトのネットワークオーダーを ホストの整数へ変換する.
 * @return 3バイト進めたポインタ.
 */
const char* ntoh3( const char* p, int* n )
{
    assert( n );
    
    char buf[4] = { 0 };
    memcpy(buf + 1, p, 3);
    memcpy(n, buf, 4);
    *n = ntohl(*n);
    
    return p + 3;
}


////////////////////////////////////////////////////////////////
// Http2Reader

nghttp2_hd_inflater* m_inflater = nullptr;

void Http2Reader::attach( streambuf* conn )
{
    assert( conn );
    assert( !m_conn );
    
    m_conn = conn;

    m_bufsize = 4096;
    m_framebuf = (char*) malloc(m_bufsize);

    int rv = nghttp2_hd_inflate_new(&m_inflater);
    if (rv) {
        fprintf(stderr, "nghttp2_hd_inflate_new() failed: %s\n",
                nghttp2_strerror(rv));
        throw "foo"; // TODO:
    }
}


// 1フレーム分を読む.
// @return eofの場合 NULL.
char* Http2Reader::read_frame()
{
    assert(m_conn);
    assert(m_bufsize >= FRAME_HDR_LENGTH);

    // ヘッダ (9バイト)
    int r = m_conn->sgetn(m_framebuf, FRAME_HDR_LENGTH);
    if (!r)
        return nullptr;  // TODO: eofビットの設定.

    int payload_length = 0;
    ntoh3(m_framebuf, &payload_length);

    if (FRAME_HDR_LENGTH + payload_length > m_bufsize) {
        m_bufsize = FRAME_HDR_LENGTH + payload_length;
        m_framebuf = (char*) realloc(m_framebuf, m_bufsize);
    }

    // payload
    r = m_conn->sgetn(m_framebuf + FRAME_HDR_LENGTH, payload_length);
    if (!r)
        return nullptr;  // TODO: eofビットの設定.

    return m_framebuf;
}


// @return 改行文字 (CRLF or LF) を含めない, 文字列.
string Http2Reader::getline()
{
    assert(m_conn);
    
    string ret;

    // sgetc() は位置を進めない. 罠か.
    int ch;
    while ( ch = m_conn->sbumpc(), (ch != EOF && ch != '\r' && ch != '\n') )
        ret.append(1, ch);

    if (ch == EOF)
        return ret;
    
    if (ch == '\r') {
        ch = m_conn->sgetc();
        if (ch == '\n')
            m_conn->sbumpc();
    }

    return ret;
}


/**
 * HEADERSフレーム (0x1) の送信.
 *
    // このフレームに必要なヘッダがすべて含まれていてこれでストリームを終わらせることを示すために、
    // END_STREAM(0x1)とEND_HEADERS(0x4)を有効にします。
    // 具体的には5バイト目のフラグに「0x05」を立てます。
    //
    // ここまででヘッダフレームは「ペイロードの長さ(3バイト), 0x01, 0x05, 0x00, 0x00, 0x00, 0x01」になります.
    //
    // ●HTTP/2 でのセマンティクス
    //      :method GET
    //      :path /
    //      :scheme http
    //      :authority nghttp2.org
    //
    // 本来HTTP2はHPACKという方法で圧縮します.
    // 今回は上記のHTTP2のセマンティクスを圧縮なしで記述します.

     0   1   2   3   4   5   6   7
   +---+---+---+---+---+---+---+---+
   | 0 | 0 | 0 | 0 |       0       | このオクテットの上位ビットで、後続が変わる。
   +---+---+-----------------------+
   | H |     Name Length (7+)      |
   +---+---------------------------+
   |  Name String (Length octets)  |
   +---+---------------------------+
   | H |     Value Length (7+)     |
   +---+---------------------------+
   | Value String (Length octets)  |
   +-------------------------------+
        Figure 9: Literal Header Field without Indexing -- New Name

    // 一つのヘッダフィールドの記述例:
    // |0|0|0|0|      0|   // 最初の4ビットは圧縮に関する情報、次の4ビットはヘッダテーブルのインデクス.(今回は圧縮しないのですべて0)
    // |0|            7|   // 最初の1bitは圧縮に関する情報(今回は0)、次の7bitはフィールドの長さ
    // |:method|           // フィールドをそのままASCIIのオクテットで書く。
    // |0|            3|   // 最初の1bitは圧縮に関する情報(今回は0)、次の7bitはフィールドの長さ
    // |GET|               // 値をそのままASCIIのオクテットで書く。
    //
    // 上記が一つのヘッダフィールドの記述例で、ヘッダーフィールドの数だけこれを繰り返す.
    //
 * @return エラーの場合, 負値. 正常な場合, 送信したバイト数.
 */
int send_HEADERS_frame( streambuf* conn, int stream_id,
                               const map<string, string>& m )
{
    assert(conn);
    
    char buf[1000];
    char* p = buf;

    // ヘッダ (9バイト)
    *p++ = 0; *p++ = 0; *p++ = 0;
    *p++ = 0x01; // HEADERS
    *p++ = 0x04;
    uint32_t nid = htonl(stream_id);
    memcpy(p, &nid, 4); p+= 4;

    // payload
    for ( auto& req : m ) {
        *p++ = 0x00;   // 圧縮情報
        int len = req.first.size();
        assert(len <= 255);
        *p++ = len;
        memcpy(p, req.first.c_str(), len); p += len;
        len = req.second.size();
        assert(len <= 255);
        *p++ = len;
        memcpy(p, req.second.c_str(), len); p += len;
    }

    uint32_t n3 = htonl(p - buf - FRAME_HDR_LENGTH); // ヘッダの長さは含まない
    memcpy(buf, ((char*) &n3) + 1, 3);

    dump_frame_header('S', buf);
    dump_HEADERS_payload( buf + FRAME_HDR_LENGTH, p - buf - FRAME_HDR_LENGTH );
    ssize_t r = conn->sputn( buf, p - buf );
    if ( r < 0 ) {
        //int error = get_error();
        //conn->close();
        return r;
    }

    return r;
}


static map<int, string> setup_frame_types()
{
    map<int, string> types;
    
    // リクエストボディや、レスポンスボディを転送する
    types.insert( make_pair(0x0, "DATA") );
    // 圧縮済みのHTTPヘッダを転送する
    types.insert( make_pair(0x1, "HEADERS") );
    // ストリームの優先度を変更する
    types.insert( make_pair(0x2, "PRIORITY") );
    // ストリームの終了を通知する
    types.insert( make_pair(0x3, "RST_STREAM") );
    // 接続に関する設定を変更する
    types.insert( make_pair(0x4, "SETTINGS") );
    // サーバからのリソースのプッシュを通知する
    types.insert( make_pair(0x5, "PUSH_PROMISE") );
    // 接続状況を確認する
    types.insert( make_pair(0x6, "PING") );
    // 接続の終了を通知する
    types.insert( make_pair(0x7, "GOAWAY") );
    // フロー制御ウィンドウを更新する
    types.insert( make_pair(0x8, "WINDOW_UPDATE") );
    // HEADERSフレームやPUSH_PROMISEフレームの続きのデータを転送する
    types.insert( make_pair(0x9, "CONTINUATION") );

    return types;
}

static map<int, string> frame_types = setup_frame_types();


/*
   All frames begin with a fixed 9-octet header followed by a variable-
   length payload.
    +-----------------------------------------------+
    |                 Length (24)                   |
    +---------------+---------------+---------------+
    |   Type (8)    |   Flags (8)   |
    +-+-------------+---------------+-------------------------------+
    |R|                 Stream Identifier (31)                      |
    +=+=============================================================+
    |                   Frame Payload (0...)                      ...
    +---------------------------------------------------------------+
                          Figure 1: Frame Layout

    1-3バイト目  payloadの長さ。長さにヘッダの9バイトは含まない。.
    4バイト目    フレームのタイプ.
    5バイト目    フラグ.
    6-9バイト目  ストリームID.(最初の1bitは予約で必ず0)
                それぞれのリクエストやレスポンスにはストリームIDが付与される.
                クライアントから発行されるストリームIDは奇数.
                サーバーから発行されるストリームIDは偶数.
                ストリームには優先順位が付けられています.
 */
void dump_frame_header_( const char* func, int lineno,
                         char send_or_recv, const char* p )
{
    printf("%s:%d %c ", func, lineno, send_or_recv );
    
    if ( !p ) {
        printf("(null)\n");
        return;
    }
    
    int len = 0;
    ntoh3(p, &len);
    printf("payload length:%d bytes, ", len);
    unsigned int type = *(p + 3);
    if (type > 0x9)
        printf("type:UNKNOWN!!(%d), ", type);
    else
        printf("type:%s, ", frame_types[type].c_str() );
    printf("flags:%02x, ", *(p + 4) );
    printf("stream_id:%x\n", ntohl( *((uint32_t*) (p + 5))) );
}


/* 
 * RFC 7541 HPACK: Header Compression for HTTP/2
 */
int dump_HEADERS_payload( const char* payload, int payload_length )
{
    int isfinal = 1; // TODO:
    int rv;

    const char* in;
    for ( in = payload; in < payload + payload_length; ) {
        nghttp2_nv nv;
        int inflate_flags = 0; // ret
        rv = nghttp2_hd_inflate_hd2(m_inflater, &nv, &inflate_flags,
                        (unsigned char*) in, payload_length - (in - payload),
                        isfinal);
        if (rv < 0) {
            fprintf(stderr, "inflate failed: %d\n", rv);
            return -1;
        }
        if ( (inflate_flags & NGHTTP2_HD_INFLATE_EMIT) != 0 ) {
            string name, value;
            name.assign((char*) nv.name, nv.namelen);
            value.assign((char*) nv.value, nv.valuelen);
            printf("%s: %s\n", name.c_str(), value.c_str());
        }
        
        in += rv;
    }
    
    return 0;
}


#define SETTINGS_ACK_FLAG 1u

const char settingframeAck[FRAME_HDR_LENGTH] = {
        0x00, 0x00, 0x00,
        0x04, // SETTINGS
        SETTINGS_ACK_FLAG,
        0x00, 0x00, 0x00, 0x00 };

// 複数のストリームが、多重化されて送られてくる.
// TODO: フレームをストリームに割り振る.
void recv_streams(Http2Reader& reader)
{
    int frame_type = 0;
    char* frame = nullptr;
    
    // HEADERS フレームを受信して, リソースの長さを取得する。
    while (true) {
        frame = reader.read_frame();
        if (!frame) {
            printf("recv: eof\n");
            return; // eof
        }
        dump_frame_header('R', frame);
            
        // payloadの長さ, frame typeを取得する。
        int payload_length = 0;
        ntoh3(frame, &payload_length);
        memcpy(&frame_type, frame + 3, 1);

        // ACKが返ってくる場合があるのでACKなら無視して次を読む。
        // TODO: listener を呼び出すようにする.
        if (memcmp(frame, settingframeAck, FRAME_HDR_LENGTH) == 0) {
            // 読みとばす
            continue;
        }

        if (frame_type == 0x3) { // RST_STREAM
            printf("RST_STREAM: error code = %x\n", *((int*) (frame + 9)));
            break;
        }
            
        if (frame_type == 0x1) { // HEADERS
            // TODO: リソースの長さ
            int payload_length = 0;
            ntoh3(frame, &payload_length);
            dump_HEADERS_payload(frame + 9, payload_length);
            continue;
        }

        if (frame_type == 0x5) { // PUSH_PROMISE
            int payload_length = 0;
            ntoh3(frame, &payload_length);
            int stream_id = ntohl( *(int*) (frame + 9) );
            printf("promised stream-id: %d\n", stream_id);
            dump_HEADERS_payload(frame + 13, payload_length - 4);
            continue;
        }
        if (frame_type == 0x7) { // GOAWAY
            printf("last stream-id = %d; ", ntohl(*(int*) (frame + 9)));
            printf("error code = 0x%x\n", ntohl(*(int*) (frame + 13)));
            return;
        }
        if (frame_type == 0 ) // DATA
            break;
    }
    
    //------------------------------------------------------------
    // DATAフレームの受信.
    //------------------------------------------------------------
    int payload_length = 0;
    ntoh3(frame, &payload_length);

    frame[FRAME_HDR_LENGTH + min(100, payload_length)] = '\0'; // 手抜き
    printf("====DATA====\n");
    printf("%s", frame + FRAME_HDR_LENGTH);
    printf("====/DATA====\n");
}




// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
