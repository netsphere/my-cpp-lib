﻿// -*- mode:c++; coding:utf-8-with-signature -*-

// Q's C++ Library
// Copyright (c) 1996-2001 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

#ifndef QSLIB_HTTP
#define QSLIB_HTTP

#include "../URI.h"
#include "qsocket.h"
#include "../observer.h"
#include "socket_stream.h"

#include <set>
#include <string>


////////////////////////////////////////////////////////////////////////////
// HttpHeader

class HttpHeader
{
    // types /////////////////////////////////////////////////////
    struct StatusMap {
        int code;
        const char* msg;
    };

public:
    struct Field: public std::pair<std::string, std::string> {
        typedef std::pair<std::string, std::string> super;

        Field();
        Field(const std::string& name, const std::string& value);

        // ':' で区切る.
        Field(const char* line);
    };

private:
    struct FieldComp {
        bool operator () (const Field& a, const Field& b) const {
            return a.first < b.first;
    }};
public:
    typedef std::set<Field, FieldComp> Fields;

    // properties ////////////////////////////////////////////////
private:
    static const StatusMap statusMsgs[];
    Fields fields;

public:
    std::string version;     // "HTTP/1.1"
    int code;           // status code: '200'など

    // methods ///////////////////////////////////////////////////
    HttpHeader(): code(-1) { }
    HttpHeader(const HttpHeader& );
    virtual ~HttpHeader() { clear(); }

    void insertField(const char* n, const char* v) {
        Field f(n, v); fields.insert(f);
    }
    void insertField(Field& f) { fields.insert(f); }
    void clear();

    const Fields& getFields() const { return fields; }
    const char* getReason() const;
    const Field* findField(const char* n) const;

private:
    HttpHeader& operator = (const HttpHeader& );    // not implement
};

////////////////////////////////////////////////////////////////////////////

/*
#if _MSC_VER == 1100
    // Visual C++ 5.0はnamespaceの扱いにバグがある。
namespace std
{
inline ostream& operator << (ostream& os, const HttpHeader::Field& x)
    { return os << (const char*) x.first << ':' << (const char*) x.second; }
}
#else
inline ostream& operator << (ostream& os, const HttpHeader::Field& x)
    { return os << x.first << ':' << x.second; }
#endif
*/

////////////////////////////////////////////////////////////////////////
// HttpListenerArg

struct HttpListenerArg
{
    enum Type {
        HTTP_RESOLVE_NAME,
        HTTP_SEND_REQUEST,
        HTTP_READ_BODY,
        HTTP_WAIT_BODY // header, entityとも
    } type;
    int elapsed;
    HttpListenerArg(Type t, int time): type(t), elapsed(time) { }
};

////////////////////////////////////////////////////////////////////////
// HttpConnection

// ソケットを再利用するHTTPアクセスクラス。
class HttpConnection: public Observable
{
    SocketStream ss;

    std::string cur_host;
    int cur_port;

    // User-Agent
    std::string m_app_name;

    int timeout;
    std::string proxy_;
    int elapsed;
    volatile bool to_stop;
public:

    HttpConnection(const char* app_name);
    virtual ~HttpConnection();

    /**
     * リソースをダウンロードする.
     * @param localfile   nullptr のとき，ヘッダのみ取得. NULL でないときは
     *                    リソース本体のみを保存する.
     */
    bool download(const URI& uri, const char* referer, HttpHeader* hh,
                  const char* localfile);

    void stop(); // listenerから呼び出す

    // SocketStream& getStream();

    void getCurrentHost(std::string& host, int& port) const;
    void setTimeout(int ms);

    void setProxy(const char* proxy);
    std::string getProxy() const;

private:
    std::string createRequest(const char* method, const URI& uri, const char* referer);
    ssize_t read_sub(void* buf, size_t len);
    std::string getline_sub();

    HttpConnection(const HttpConnection& );                 // not implement
    HttpConnection& operator = (const HttpConnection& );    // not implement
};


#endif

// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
