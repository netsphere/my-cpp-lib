// -*- mode:c++ -*-

#ifndef Q_SOCKET_STREAM_H
#define Q_SOCKET_STREAM_H

#include <string>
#include <algorithm>
//#include <ios>
#include <iostream>
#include <assert.h>
#include "qsocket.h"
#include "socket_streambuf.h"
#include "../noncopyable.h"

#ifdef _WIN32
  #define STRICT
  #define WIN32_LEAN_AND_MEAN
  #include <winsock2.h>
  #include <windows.h>
#else
  #include <sys/types.h>
  #include <sys/socket.h>
  #include <unistd.h> // close()
  typedef int SOCKET;
  //typedef unsigned char BYTE;
#endif


////////////////////////////////////////////////////////////////////////
// SocketStream

/**
  C++ では, 菱形継承で、次の継承関係になっている;
    basic_fstream < basic_iostream < (basic_istream, basic_ostream)
                        basic_istream < basic_ios
                        basic_ostream < basic_ios
                                        basic_ios < ios_base
  ストリームバッファは, basic_ios<> が宣言している。型が
  basic_streambuf<_CharT, _Traits>* で固定。
    protected: basic_streambuf<_CharT, _Traits>* _M_streambuf;

  - このポインタを所有しているわけではない。
  - basic_ios<> 派生クラスでは basic_streambuf<> 派生クラスのインスタンスを持
    ち, ポインタだけセットしている。
  stream 側からバッファにアクセスするときは, 必ず rdbuf() を通すことで差替えに
  対応.
 */

/**
  ★設計について:
  ファイル, ソケット, パイプのような資源については, 資源と stream クラスを分け
  るようにデザインすべき。

  例えばソケットは, クラス Socket, クラス SocketStream を用意し, クラス Socket
  がストリームクラスの Factory として動作するようにする。
  こうすると，ファイル，パイプ，ソケットを統一的なインターフェイスで使える。

  MFCの CGopherFile クラスは，メンバWrite()，WriteString()を呼ぶと
  CNotSupportedException 例外を投げる。みっともない。
  資源とストリームを一つのクラスの派生とするから生じる問題であって，
  GopherFile::getOutputStream() が NULL を返すようにすれば良い。

class Socket: HogeHogeBase
    // java.net.Socket
{
public:
    virtual InputStream* getInputStream();
    virtual OutputStream* getOutputStream();
};
*/


/**
 * クライアント側 socket.
 * バイナリとしても読めるし、テキストとして1行読むこともできる。
 *
 * std::ios から派生するのは実用性低い.
 * <s>istream, ostream として渡せるようにするため, std::iostream から派生させる.</s>
 * istream, ostream だけでよいことも多い。このクラスは std::ios から派生するよ
 * うに戻し, 別に, istream, ostream, iostream の派生クラスを作る.
 */
class SocketIOS: virtual public std::ios, private noncopyable
{
protected:
    // SSL/TLS を使うときは, rdbuf() に TlsSocketStreamBuf インスタンスを与え
    // よ.
    // 菱形継承にするため、このメンバを直接使ってはならない. rdbuf() 経由で使う
    // こと.
    SocketStreamBuf m_sbuf;

    //bool eof_;
    int timeout;


    // メソッド //////////////////////////////////
public:
    SocketIOS(): std::ios(&m_sbuf), timeout(20 * 1000) /*, count_(0)*/ {
        setstate(eofbit);
    }

    virtual ~SocketIOS() {
        // close(); streambuf を外部から与えた場合, 先に解放されていることがある.
    }

    /**
     * TCP でサーバに接続する。
     * @throws SocketException ホストが見つからない.
     */
    void open(const char* host, int port) {
        assert(host && *host);
        assert(port != -1);
        assert( !is_open() );

        // 名前解決し, IPアドレスを得る。
        struct addrinfo* hp = ngethostbyname(host, port, SOCK_STREAM);
        if (!hp) {
            setstate(failbit);
            throw SocketException(h_errno, "No such host is known");
        }

        // ソケットを生成。
        SOCKET sock = socket(hp->ai_family, hp->ai_socktype, hp->ai_protocol);
        if (sock < 0) {
            freeaddrinfo(hp);
            setstate(failbit);
            throw SocketException(h_errno, "Fail to create a socket");
        }

        // 接続
        int r = connect(sock, hp->ai_addr, hp->ai_addrlen);
        freeaddrinfo(hp); // もういらん.
        if ( r < 0 ) {
            close();
            setstate(failbit);
            throw SocketException(h_errno, "Fail to connect");
        }

        rdbuf()->connected(sock, host);
        clear(); // eof_ = false;
    }


    SocketStreamBuf* rdbuf() const {
        // m_sbuf メンバを直接使ってはならない.
        return static_cast<SocketStreamBuf*>(std::ios::rdbuf());
    }

    bool is_open() const {
        assert( rdbuf() );
        return rdbuf()->is_open();
    }


    /** ホストから切断する */
    void close() {
        rdbuf()->close();
        setstate(eofbit); // eof_ = true;
    }

#if 0  // basic_ios<_C>::eof() でよい. 状態管理は basic_ios<_C> に任せる.
    /** @return 終端のときtrue */
    bool eof() const {
        return eof_;
    }
#endif


    // @param ms ミリ秒単位
    void setSoTimeout(int ms) {
        timeout = ms;
    }

    int getSoTimeout() const {
        return timeout;
    }

//private:
//    SocketIOS(const SocketIOS& ) = delete;              // not implement
//    SocketIOS& operator = (const SocketIOS& ) = delete; // not implement
};


// SocketIOS を菱形継承にする.
class SocketInputStream: virtual public SocketIOS, public std::istream
{
    // <s>istream::_M_gcount を使え.</s> -> VC++ では private になっている。そりゃそうだ
    //ssize_t count_; // 読んだバイト数。一部読んでから例外を投げることがある。

public:
    SocketInputStream(): SocketIOS(), std::istream(&m_sbuf) { }

    /**
     * バイナリとして length バイト受信する. sgetn() と異なり, length に満たな
     * いことがある.
     * Block する.
     */
    ssize_t read(void* buffer, ssize_t length) {
        if ( eof() ) {
            //count_ = 0;
            return 0;
        }

        int avail = rdbuf()->in_avail();
        if (length <= avail) {
            return /*count_ =*/ rdbuf()->sgetn((char*) buffer, length);
        }

        // 高々1回だけ, 読み込ませる.
        // sgetn() は指定のバイト数に達するまで block するので、次のように 1 バ
        // イトのみ指定しなければならない。罠.
        int ret;
        ret = rdbuf()->sgetn((char*) buffer, avail + 1);
        if (ret < avail + 1) {
            setstate(eofbit); // eof_ = true;  // sysread() が 0 を返した場合.
            return /*count_ =*/ ret;
        }

        ret += rdbuf()->sgetn( ((char*)buffer) + ret,
                             std::min(length - ret, rdbuf()->in_avail()) );

        return /*count_ =*/ ret;
    }


#if 0 // istream::gcount() を覆い隠すような実装は不可!
    // 最後に実行した read() での入力文字数.
    std::streamsize gcount() const {
        return count_;
    }
#endif


    /**
     * テキストとして1行読む。基本的に std::getline() と同じ.
     *   - 改行文字（LF, CRLF）は削除する. std::getline() と異なり, LF, CR,
     *     CRLF のいずれも対応する.
     *   - ストリームの終端 (EOF) に到達した場合, ios_base::eofbit を立てる.
     *   - バッファ長 string::max_size() に達した場合, ios_base::failbit を立て
     *     る.
     * Note. istream::getline() と次の点が異なる (のも std::getline() と同じ).
     *   - istream::getline() は char_type* の固定長バッファに入力するので使い
     *     にくい.
     *   - gcount() には影響させない.
     *
     * 実装上の注意:
     *   countが変化しては不味いので，内部で read() を呼んではならない。
     */
    std::string getline()
    {
        if ( eof() )
            return "";

        std::string ret;
        std::string::size_type len = 0;
        const std::string::size_type max_size = ret.max_size();

        // sgetc() は位置を進めない. 罠か.
        int ch;
        while ( len < max_size ) {
            ch = rdbuf()->sbumpc();
            if (ch == EOF || ch == '\r' || ch == '\n')
                break;
            ret.append(1, ch);
            ++len;
        }
        if (ch == EOF) {
            setstate(eofbit); // setstate() は状態を "追加" する. 罠か!
            return ret;
        }
        else if ( len == max_size ) {
            setstate(failbit);
            return ret;
        }

        if (ch == '\r') {
            ch = rdbuf()->sgetc();
            if (ch == '\n')
                rdbuf()->sbumpc(); // CRLF
        }

        return ret;
    }

};


// SocketIOS を菱形継承にする.
class SocketOutputStream: virtual public SocketIOS, public std::ostream
{
public:
    SocketOutputStream(): SocketIOS(), std::ostream(&m_sbuf) { }

#if 0 // ostream::write() がある. 失敗した時は ios_base::badbit が立つ.
      // 書き込んだバイト数を知りたいときは, rdbuf()->sputn(buf, len)
    /** バイナリとしてnバイト送信する。 */
    ssize_t write(const void* buffer, size_t length){
        return rdbuf()->sputn((char*) buffer, length);
    }
#endif // 0

};


class SocketStream: public SocketInputStream, public SocketOutputStream
{
};


////////////////////////////////////////////////////////////////////////////


#endif // !Q_SOCKET_STREAM_H



// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
