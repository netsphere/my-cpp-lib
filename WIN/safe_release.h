
#ifndef RELEASE_NULL_H__
#define RELEASE_NULL_H__ 1

template <typename _T>
void safe_release(_T** unk)
{
    static_assert(std::is_base_of<IUnknown, _T>::value,
                  "_T must be derived of IUnknown");
    if (!unk)
        return;

    // 二重に Release() されることを防ぐため、先に nullptr にする.
    _T* temp = static_cast<_T*>(
                    InterlockedExchangePointer((volatile PVOID*) unk, nullptr));
    if (temp) {
        //_T* temp = *unk;
        //*unk = nullptr;
        temp->Release();
    }
}

#endif
