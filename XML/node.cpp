
#include "node.h"
#include <string.h>
using namespace std;


Attribute* Element::get_attribute(const string& name) const
{
    assert(_attributes);
    auto r = _attributes->find(name);
    if (r == _attributes->end())
        return nullptr;
    return (*r).second;
}


/*
std::pair<std::string, std::string> Element::ns_split() const
{
  auto split_name = Helpers::split(_name, ':');
  if (split_name.size() < 2)
  {
    return { "", split_name[0] };
  }
  return { split_name[0], split_name[1] };
}
*/


string Element::str() const
{
    assert(_attributes);
  
    string ret = string("<") + _name;
    for (auto attr : *_attributes)
        ret += " " + attr.second->str();

    if (_emptytag)
        ret += "/>";
    else {
        ret += ">";
        for (auto child : *_children)
            ret += child->str();
        ret += string("</") + _name + ">";
    }
    
    return ret;
}


// TODO: escape
string Attribute::str() const
{
    if (!_name || strcmp(_name, "") == 0)
        return *_value;
    else
        return string(_name) + "=\"" + *_value + "\"";
}


// TODO: escape
string CDATASect::str() const
{
    return string("<![CDATA[") + *_data + "]]>";
}


string EntityRef::str() const {
    return string("&") + *_str + ";";
}


// 展開されたエンティティ宣言
string Entity::str() const
{
    string ret = "<!ENTITY " + _name;
    if (_body)
        ret += " \"" + *_body + "\"";
    ret += ">";
    
    return ret;
}


string PI::str() const
{
    string ret = string("<?") + _target + " " + _data + "?>";
    return ret;
}


string Comment::str() const
{
    return string("<!--") + *_data + "-->";
}


string Document::str() const
{
    string ret;

    if (_version != "") {
        // XML宣言あり
        string s = "<?xml version=\"" + _version + "\"";
        if (_encoding != "")
            s += " encoding=\"" + _encoding + "\"";
        if (_standalone != "")
            s += " standalone=\"" + _standalone + "\"";
        ret += s + "?>\n";;
    }

    for (auto node : *_children)
        ret += node->str();
    
    return ret;
}


string DocType::str() const
{
    string ret = string("<!DOCTYPE ") + _name;

    // ExternalID
    if ( _public_id )
      ret += string(" PUBLIC \"") + *_public_id + "\" \"" + *_system_uri + "\"";
    else if (_system_uri)
      ret += string(" SYSTEM \"") + *_system_uri + "\"";
      
    if (_decls) {
        ret += " [\n";
        for (auto node : *_decls)
            ret += node->str() + "\n";
        ret += "]";
    }
    return ret + ">";
}


string NotationDecl::str() const
{
    string ret = string("<!NOTATION ") + _name;
    if (_body)
        ret += " " + *_body;
    ret += ">";

    return ret;
}
