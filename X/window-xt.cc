﻿// -*- coding:utf-8-with-signature -*-

// Q's C++ Library
// Copyright (c) 1998-1999 Hisashi HORIKAWA. All rights reserved.

#include <assert.h>
#include <X11/Xlib.h>
#include <X11/StringDefs.h>
#include <X11/Intrinsic.h>

#include "x-misc.h"
#include "../window.h"
#include "../qsdebug.h"


//////////////////////////////////////////////////////////////////////
// CWindow

CWindow::CWindow(): window(0)
{
}

CWindow::~CWindow()
{
    // destroy();
        // 1999.05.12 デストラクタはメインループの外で呼ばれることがある。
}

int CWindow::setVisible(bool visible)
{
    assert(0);  // TODO: impl
    return 0;
}

bool CWindow::isVisible() const
{
    return window && XtIsRealized(window);
        // TODO: 正しく実装し直す
    return false;
}

void CWindow::setTitle(const string& title)
{
    // TODO: impl
}

void CWindow::add(Window child)
{
    assert(0);  // TODO: impl
}

void CWindow::destroy()
{
    if (window) {
        XtDestroyWidget(window);
        window = 0;
    }
}

CPoint CWindow::getLocation() const
    // トップレベルウィンドウに対してしか使ってはならない
    //      XQueryTree()でトップレベルか調べれるが，手を抜いている
{
    assert(window);
    Display* disp = XtDisplay(window);
    Window w = XtWindow(window);
    if (!w) {
        QsTRACE(1, "window == 0\n");
        return CPoint(0, 0);
    }

    Window root;
    int gx, gy;
    unsigned int width, height, border, depth;
    XGetGeometry(disp, w, &root, &gx, &gy, &width, &height, &border, &depth);
    return CPoint(gx, gy);
}

void CWindow::setLocation(const CPoint& pt)
{
    if (!window)
        return;

    XtWidgetGeometry req;
    memset(&req, 0, sizeof(req));
    req.request_mode = CWX | CWY;
    req.x = pt.x;
    req.y = pt.y;
    XtMakeGeometryRequest(window, &req, NULL);
}

void CWindow::setSize(int width, int height)
{
    if (!window)
        return;

    XtWidgetGeometry req;
    memset(&req, 0, sizeof(req));
    req.request_mode = CWWidth | CWHeight;
    req.width = width;
    req.height = height;
    XtMakeGeometryRequest(window, &req, NULL);
}

void CWindow::setBounds(const CRect& r)
{
    XtWidgetGeometry req;
    memset(&req, 0, sizeof(req));
    req.request_mode = CWX | CWY | CWWidth | CWHeight;
    req.x = r.x;
    req.y = r.y;
    req.width = r.width;
    req.height = r.height;
    XtMakeGeometryRequest(window, &req, NULL);
}

//////////////////////////////////////////////////////////////////////
// PopupWindow

PopupWindow::PopupWindow()
{
}

PopupWindow::~PopupWindow()
{
}

int PopupWindow::setVisible(bool v)
{
    if (!window)
        return 0;
    if (v)
        XtPopup(window, XtGrabNone); // モードレス
    else
        XtPopdown(window);
    return 0;
}
