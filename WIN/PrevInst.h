﻿// -*- coding:utf-8-with-signature; mode:c++ -*-

// Q's C++ Library
// Copyright (c) 1996-1998 Hisashi Horikawa. All rights reserved.

#ifndef QSLIB_PREVINST
#define QSLIB_PREVINST

#include <assert.h>

/**
 * 二重起動されていないか調べる.
 *
 * 使い方:
 *     loaderあたりで, getExistInstance() を呼び出す.
 *     ウィンドウハンドルを使って, メッセージを投げたりする.
 *
 *     ウィンドウが開いたところで, setMainWindow() で、自分のウィンドウハンド
 *     ルを登録する.
 */
class CPrevInstance
{
private:
	// HANDLE mutex;
    HANDLE mapFile;
    bool alreadyExists;

public:
    CPrevInstance(): 
	  // mutex(NULL), 
	  mapFile(NULL),
	  alreadyExists(false) 
    {
    }

    virtual ~CPrevInstance()
    {
/*
        if (mutex != NULL) {
            ::ReleaseMutex(mutex);
            mutex = NULL;
        }
*/
        if (mapFile) {
            ::CloseHandle(mapFile);
            mapFile = NULL;
        }
    }


    // @return すでにインスタンスが実行されていれば, true.
    //         そうでなければ false.
    // @param other_window 別ウィンドウのハンドル (登録されていれば). NULL 可.
    bool isExistInstance( LPCTSTR uniqueName, HWND* other_window)
    {
        assert(uniqueName);
/*
		if (!mutex) {
			mutex = ::CreateMutex(NULL, FALSE, uniqueName);
			alreadyExists = mutex && ::GetLastError() == ERROR_ALREADY_EXISTS;
		}
*/
        if (!mapFile) {
            mapFile = ::CreateFileMapping(INVALID_HANDLE_VALUE, NULL,
                                          PAGE_READWRITE, 
                                          0, sizeof(HWND), uniqueName);
            if (!mapFile) {
                assert(0); // TODO: raise error
                return false;
            }
            alreadyExists = ::GetLastError() == ERROR_ALREADY_EXISTS;
        }

        if ( !alreadyExists )
            return false;

        if ( other_window ) {
            *other_window = NULL;

            const HWND* const p = (HWND*) ::MapViewOfFile(mapFile,
                                                          FILE_MAP_READ, 0, 0, 0);
            if (!p) {
                ::CloseHandle(mapFile);
                mapFile = NULL;
                assert(0); // TODO: raise error
                return false;
            }

            *other_window = *p; // タイミングによっては、まだ入っていないことがある.
            ::UnmapViewOfFile(p);
        }

        return true;
    }


    // インスタンスのウィンドウハンドルをセットする
    void setMainWindow(HWND window)
    {
        assert(window);
        if (!mapFile || alreadyExists)
            return;

        HWND* const p = (HWND*) ::MapViewOfFile(mapFile, FILE_MAP_WRITE, 0, 0, 0);
        if (!p) {
            ::CloseHandle(mapFile);
            mapFile = NULL;
            assert(0); // TODO: raise error.
            return;
        }
        memcpy(p, &window, sizeof(HWND));
        ::UnmapViewOfFile(p);
    }

private:
    CPrevInstance(const CPrevInstance&);                // not impl
    CPrevInstance& operator = (const CPrevInstance&);   // not impl
};

#endif // QSLIB_PREVINST
