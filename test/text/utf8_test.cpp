﻿// -*- coding:utf-8-with-signature -*-

#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "text/utf8.h"
#include <string>
using namespace std;

void dump8(const char* s, int len) {
    for (int i = 0; i < len; i++)
      printf("%02x ", (unsigned char) s[i]);
    printf("\n");
}

void dump16(const char16_t* s, int len) {
    for (int i = 0; i < len; i++)
        printf("%04x ", s[i]);
    printf("\n");
}

int OUTPUT_BUFSIZ = 1000;

// 入力ストリームが中途半端な箇所で切れることがある
// 中途半端なバイト列は読み残す。OK
void test_partial_utf8()
{
    printf("func = %s\n", __func__);

    // 41 E6 97 A5
    static char utf8[] = u8"A日";
    char16_t buf[OUTPUT_BUFSIZ];

    utf8[3] = '\0';
    const char* p = utf8;
    int ret = utf8_to_utf16(buf, &p, OUTPUT_BUFSIZ, NULL);
    printf("ret = %d, ptr = %p, p - src = %ld\n", ret, p, p - utf8);
    //=> ret = 1, ptr = 略, p - src = 1

    utf8[3] = 0xA5;
    //p = utf8 + 3;   2バイト目からやり直し
    ret = utf8_to_utf16(buf + 1, &p, OUTPUT_BUFSIZ - 1, NULL);
    printf("ret = %d, ptr = %p, p - src = %ld\n", ret, p, p - utf8);
    //=> ret = 1, ptr = (nil), p - src = 略
}


void test_utf8_to_utf16()
{
    // U+1F600 GRINNING FACE
    static const char utf8[] = u8"ABC日本語♥☎\U0001f600";
    char16_t buf[1000];

    //=> 41 42 43 e6 97 a5 e6 9c ac e8 aa 9e e2 99 a5 e2 98 8e f0 9f 98 80 00
    dump8(utf8, strlen(utf8) + 1);

    // dest_len が無視されるか
    const char* p = utf8;
    int r = utf8_to_utf16(NULL, &p, 2, NULL);
    printf("utf8_to_utf16(NULL, ...): ret = %d, p - src = %ld\n", r, p - utf8);

    // 変換する
    p = utf8;
    r = utf8_to_utf16(buf, &p, sizeof(buf) / sizeof(char16_t), NULL);
    dump16(buf, r + 1); // ナル値まで.

    // Round-trip するか.
    char utf8_3[1000];
    const char16_t* q = buf;
    r = utf16_to_utf8(utf8_3, &q, sizeof(utf8_3), NULL);
    dump8(utf8_3, r + 1);
    assert( strcmp(utf8, utf8_3) == 0 );

    //////////////////////////////////
    // BOM がある場合

    static const char utf8_2[] = u8"\ufeff日本語\ufeffｶﾀｶﾅ";
    p = utf8_2;
    r = utf8_to_utf16(buf, &p, sizeof(buf) / sizeof(char16_t), NULL);
    dump16(buf, r + 1);
}

void test_utf16_to_utf8()
{
    printf("%s start\n", __func__);

    static const char16_t utf16[] = u"ABC日本語♥☎\U0001f600";
    char buf[1000];

    // dest_len が無視されるか
    const char16_t* p = utf16;
    int r = utf16_to_utf8(NULL, &p, 2, NULL);
    printf("utf16_to_utf8(NULL, ...): ret = %d, p - src = %ld\n", r, p - utf16);

    // 変換する
    p = utf16;
    r = utf16_to_utf8(buf, &p, sizeof(buf), NULL);
    dump8(buf, r + 1);

    // Round-trip するか.
    char16_t utf16_3[1000];
    const char* q = buf;
    utf8_to_utf16(utf16_3, &q, sizeof(utf16_3) / sizeof(char16_t), NULL);
    assert( u16string(utf16) == u16string(utf16_3) );

    // BOM がある場合
    static const char16_t utf16_2[] = u"\ufeff日本語\ufeffｶﾀｶﾅ";
    p = utf16_2;
    r = utf16_to_utf8(buf, &p, sizeof(buf), NULL);
    dump8(buf, r + 1);
}

int main()
{
    test_utf8_to_utf16();
    test_utf16_to_utf8();
    test_partial_utf8();

    return 0;
}
