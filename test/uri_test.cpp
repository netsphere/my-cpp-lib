
#include "../URI.h"
#include <memory>
using namespace std;

struct TestPattern {
    const char* str;
    bool matched;
};

static TestPattern pattern[] = {
    // scheme
    // https://www.iana.org/assignments/uri-schemes/uri-schemes.xhtml
    {"chrome-extension://x123/y123.html", true},
    {"coap+tcp://x/y", true},
    {"soap.beep://x/y", true},

    // authority
    {"http://foo@host:123/", true},
    {"http://bar:1@host:123", true},  // authority のみ.
    {"http://[1:2:3]:567", true},      // [...] は IPv6のみ
    {"http://[::ffff:1.2.3.4]/path", true}, // 下位にIPv4アドレス
    {"http://[f:2:/file", false},       // error

    // RFC 8089 The "file" URI Scheme (February 2017)
    {"file:///path1/path2", true},     // authorityを省略.
    {"file:/path/to/file", true},
    {"file:c:/path/to/file", true},    // drive letter
    {"file:///c|/path/to/file", true}, // historical drive letter. replace with ':'
    {"file:/c|/path/to/file", true},
    {"file:c|/path/to/file", true},
    {"file://host.example.com/path/to/file", true},
    {"file:////host.example.com/path/to/file", true}, // empty authority, UNC
    {"file://///host.example.com/path/to/file", true}, // ?? どう解釈?
        
    // scheme はあるが, "//" authority が続かない. mailtoの本体は pathに格納.
    {"mailto:hisashi.horikawa+web@gmail.com", true},

    // authority 有無
    {"foo://example.com:8042/over/there?name=ferret#nose", true},
    {"urn:example:animal:ferret:nose", true},
    
    //[RFC 3986] の例
    {"ftp://ftp.is.co.za/rfc/rfc1808.txt", true},
    {"http://www.ietf.org/rfc/rfc2396.txt", true},
    {"ldap://[2001:db8::7]/c=GB?objectClass?one", true},
    {"mailto:John.Doe@example.com", true},
    {"news:comp.infosystems.www.servers.unix", true},
    {"tel:+1-816-555-1212", true},
    // RFC 4248 The telnet URI Scheme. 最後の '/' は付けても付けなくてもよい.
    {"telnet://192.0.2.16:80/", true},
    {"urn:oasis:names:specification:docbook:dtd:xml:4.1.2", true},

    {nullptr, false},
};


void default_port_test()
{
    URI u1("http://bar:1@host:123");
    printf("%s\t%s\t%s\t%s\t%s\n",
                       u1.getScheme().c_str(),
                       u1.getAuthority().c_str(),
                       u1.getPath().c_str(),
                       u1.getQuery().c_str(),
                       u1.getFragment().c_str());
    
    URI u2("http://bar:1@host");  // default = 80
    printf("%s\t%s\t%s\t%s\t%s\n",
                       u2.getScheme().c_str(),
                       u2.getAuthority().c_str(),
                       u2.getPath().c_str(),
                       u2.getQuery().c_str(),
                       u2.getFragment().c_str());
    printf("default port = %d\n", u2.getPort());
}

int main()
{
    // parse() のテスト
    for (int i = 0; pattern[i].str; i++) {
        URI* uri = URI::parse(pattern[i].str, nullptr);
        if (pattern[i].matched) {
            if (!uri) {
                printf("%s\t", pattern[i].str);
                printf("%s\t", pattern[i].matched ? "must match" : "must not");
        
                printf("failed\n");
            }
            else {
                /*
                printf("%s\t%s\t%s\t%s\t%s\n",
                       uri->getScheme().c_str(),
                       uri->getAuthority().c_str(),
                       uri->getPath().c_str(),
                       uri->getQuery().c_str(),
                       uri->getFragment().c_str());
                */
            }
        }
        else {
            if (uri) {
                printf("%s\t", pattern[i].str);
                printf("%s\t", pattern[i].matched ? "must match" : "must not");
        
                printf("failed: ");
                printf("%s\t%s\t%s\t%s\t%s\n",
                       uri->getScheme().c_str(),
                       uri->getAuthority().c_str(),
                       uri->getPath().c_str(),
                       uri->getQuery().c_str(),
                       uri->getFragment().c_str());
            }
            else {
                //printf("OK. not matched\n");
            }
        }
        if (uri)
            delete uri;
    }

    // 相対URI relative-ref の解決.
    URI base("https://hoge/fuga");
    URI rel("//foo/bar");
    URI resolved = base.resolve(rel);
    if ( resolved != URI("https://foo/bar") ) {
        printf("failed: rel = %s, resolved = %s\n",
               rel.toString().c_str(), resolved.toString().c_str());
    }

    default_port_test();
    
    return 0;
}

// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
