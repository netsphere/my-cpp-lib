﻿// Q's C++ Library
// Copyright (c) 1998-2001 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

#include <cstdio>
#include <cerrno>
#include <cstdarg>
#include <cassert>
#include <stdlib.h>
#include <sys/stat.h>   // mkdir()
#include <pwd.h>        // getpwuid()
#include <unistd.h>     // geteuid()
#include <strings.h>    // strcasecmp()
#include <X11/Xlib.h>

#include "misc.h"
using namespace std;


//////////////////////////////////////////////////////////////////////

string getHomeDir()
{
/*
    // あまり環境変数は使わない
    const char* home = getenv("HOME");
    if (home)
        return home;
*/
    struct passwd* p = getpwuid(geteuid());
    if(p)
        return p->pw_dir;
    else
        return "";
}

void error(const char* format, ...)
{
    va_list args;
    va_start(args, format);
    // g_logv(G_LOG_DOMAIN, G_LOG_LEVEL_MESSAGE, format, args);
            // g_log()は末尾に改行が入る
    vfprintf(stderr, format, args);
    va_end(args);
}

int char_length(const char* b, const char* e)
    // [b, e)の文字数を返す
{
    int count = 0;
    while (*b && b < e) {
        b += mblen(b, MB_CUR_MAX);
        count++;
    }
    return count;
}

int byte_length(const char* s, int clen)
{
    const char* p = s;
    while (*p && --clen >= 0)
        p += mblen(p, MB_CUR_MAX);
    return p - s;
}

bool createFolder(const char* path)
    // 機能
    //      フォルダを（階層が深くても）作成する。
    // 入力
    //      pathの末尾に'\'を付けても付けなくても良い
    // 戻り値
    //      TRUE: フォルダを作成したか既に存在する。
    //      FALSE: 失敗
{
    assert(path);

    string cur;
    const char* p = path;
    if (path[0] == '/' || path[1] == '\\')
        p++;

    while (*p) {
        cur += '/';
        while (*p && *p != '/' && *p != '\\') {
#ifdef _WIN32
            if (::IsDBCSLeadByte(*p))
                cur += *p++;
#endif
            cur += *p++;
        }

        struct stat st;
        if (stat(cur.c_str(), &st) < 0) {
            if (errno == ENOENT) {
                if (mkdir(cur.c_str(), 0755) != 0 && errno != EEXIST) {
                    perror("mkdir");
                    return false;
                }
            }
            else {
                perror("stat");
                return false;
            }
        }

        if (*p)
            p++;
    }
    return true;
}

bool isYesString(const char* s)
    // "yes" or "true"のときtrue
{
    return !strcasecmp(s, "yes") || !strcasecmp(s, "true");
}

