﻿// Q's C++ Library
// Copyright (c) 1996-1999 Hisashi HORIKAWA. All rights reserved.

//#include "stdafx.h"
#include "AppHelp.h"

#include <string>
using namespace std;

#ifdef _DEBUG
  #undef THIS_FILE
  static char THIS_FILE[]=__FILE__;
  #define new DEBUG_NEW
#endif

////////////////////////////////////////////////////////////////////////////
// CAppWithHelp

BEGIN_MESSAGE_MAP(CAppWithHelp, CWinApp)
    //{{AFX_MSG_MAP(CAppWithHelp)
    //}}AFX_MSG_MAP

    // ルーティング先をサブクラスにする.
    ON_COMMAND(ID_HELP, OnHelp)
END_MESSAGE_MAP()

CAppWithHelp::CAppWithHelp()
{
}

CAppWithHelp::~CAppWithHelp()
{
}


// F1 (uses current context)
// ON_COMMAND(ID_HELP, OnHelp) で, ルーティングされてくる.
//
// ここ (https://docs.microsoft.com/en-us/cpp/mfc/reference/cwnd-class#onhelpinfo) や
// そのほかWeb上には OnHelpInfo() をオーバーライドするような記事があるが、
// virtual でないので、呼び出されるわけない.
void CAppWithHelp::OnHelp()
{
    // See https://docs.microsoft.com/en-us/cpp/mfc/reference/cwinapp-class#onhelp
    WinHelp(0, 0);
}


void CAppWithHelp::WinHelp(DWORD_PTR /* dwData */, UINT /* nCmd */)
{
    char procfile[MAX_PATH + 1];
    string path;

    GetModuleFileName(NULL, procfile, sizeof(procfile));
    path.assign(procfile, 0, strrchr(procfile, '\\') - procfile + 1);

	WIN32_FIND_DATA wfd;
	HANDLE handle = ::FindFirstFile((path + "index.htm").c_str(), &wfd);
	if (handle == INVALID_HANDLE_VALUE) {
		AfxMessageBox("ヘルプ（HTML）ファイルが見つかりません。実行ファイルと同じフォルダに置いてください。");
		return;
	}
	::FindClose(handle);

    path = string("\"") + path + wfd.cFileName + "\"";

	SHELLEXECUTEINFO sei;
	memset(&sei, 0, sizeof(sei));
    sei.cbSize = sizeof(sei);
    sei.lpFile = path.c_str();
    sei.lpVerb = "Open";
	if (!::ShellExecuteEx(&sei)) {
		AfxMessageBox("ヘルプ (HTML) ファイルを開けません");
		return;
	}
}


/*
05862/05862 CXI00013  morry            RE:Win98でのShellExecute()
( 5)   98/09/01 12:37  05861へのコメント


  morry といいます。

  shell\ キーの「標準の値」に、例えば "play,open" という様に、カンマ
区切りで複数の verb をつなげた場合は、先頭の verb がデフォルト動作と
みなされます。wav でも bmp でもよいですが、shell\ キーの標準の値を
「play,open」や「view,open」の様にしてデフォルト動作を変えると、エクス
プローラ上でのダブルクリック動作が変わります。

  この様に設定した状態で、ShellExecute() と ShellExecuteEx() を、いずれも
verb=NULL で試してみてください。
  morry が試したところ、以下の様になりました。

[NT4]
    ShellExecute()  : open が実行された。
    ShellExecureEx(): こちらが指定したデフォルト動作が実行された。

[Win95]
    ShellExecute()  : open が実行された。
    ShellExecureEx(): こちらが指定したデフォルト動作が実行された。

[Win98]
    ShellExecute()  : エラー 31 (SE_ERR_NOASSOC)
                      (shell\ キーの verb リストを消せば open 実行)
    ShellExecuteEx(): こちらが指定したデフォルト動作が実行された。

-----
  CXI00013  morry    http://www.bekkoame.ne.jp/~tmorita
      "Windows Underground Programming (Un'Gramming) Page"

*/
