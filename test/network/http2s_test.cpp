﻿// -*- coding:utf-8-with-signature -*-

// HTTP2最速実装をmain()関数だけで簡単に説明する(SSL編)
// https://qiita.com/0xfffffff7/items/0e19d255abb84372bb9a

//*****************************************************
// OpenSSL 1.0.2以上を使用.
//*****************************************************

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string>
#include "network/http2_common.h"
#include "network/qsocket.h"
#include "URI.h"

#ifdef _WIN32
  #include <winsock2.h>
  #include <windows.h>
  #pragma warning(disable:4996)
  #define SHUT_RDWR SD_BOTH
#else
  #include <sys/types.h>
  #include <sys/socket.h>
  #include <netinet/in.h>
  #include <netinet/tcp.h>
  #include <netdb.h>
  #include <fcntl.h>
  #include <signal.h>
  #include <unistd.h>
  typedef int SOCKET;
  //typedef unsigned char BYTE;
#endif
#include <string.h> // memset()
#include <memory> // unique_ptr
#include <map>
#include <assert.h>

#include <openssl/ssl.h>
#include <openssl/err.h>
#include "network/tls_socket_streambuf.h"

#ifdef _WIN32
  #define CA_BUNDLE_FILE "c:/git-sdk-32/mingw32/ssl/certs/ca-bundle.crt"
#else
  #define CA_BUNDLE_FILE "/etc/pki/tls/certs/ca-bundle.crt"
#endif

using namespace std;


// ALPN識別子. "h2"
static constexpr int protos_len = 2;
static const unsigned char protos[] = { protos_len, 0x68, 0x32 };


#define SETTINGS_ACK_FLAG 1u

const char settingframeAck[FRAME_HDR_LENGTH] = {
        0x00, 0x00, 0x00,
        0x04, // SETTINGS
        SETTINGS_ACK_FLAG,
        0x00, 0x00, 0x00, 0x00 };


int main(int argc, char* argv[])
{
    //------------------------------------------------------------
    // 接続先ホスト名.
    // HTTP2に対応したホストを指定します.
    //------------------------------------------------------------
    //unique_ptr<URI> uri( URI::parse("https://nghttp2.org/", nullptr) );
    // ヘッダが, 無圧縮で, 見やすい
    unique_ptr<URI> uri( URI::parse("https://www.yahoo.co.jp/", nullptr) );

    // Not HTTP/2
    //unique_ptr<URI> uri( URI::parse("https://tools.ietf.org/html/rfc7230", nullptr) );

    //------------------------------------------------------------
    // TCPの準備.
    //------------------------------------------------------------
#ifdef _WIN32
    WSADATA wsaData;
    if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
        return 1;
#endif

    //------------------------------------------------------------
    // SSLの準備.
    //------------------------------------------------------------

    // SSLライブラリの初期化.
    SSL_library_init();

    // エラーを文字列化するための準備.
    SSL_load_error_strings();

// v1.0.2n 0x100020efL
#if OPENSSL_VERSION_NUMBER >= 0x10100000L
    // グローバルコンテキスト初期化.
    SSL_CTX* _ctx = SSL_CTX_new( TLS_client_method() );
    // HTTP/2 は, TLS 1.2以降が必須.
    // HTTP/1.1 への fallback のために, TLS 1.1 も有効にする.
    SSL_CTX_set_min_proto_version(_ctx, TLS1_1_VERSION);
#else
    // v1.0.2: SSLv2 .. TLSv1.2
    SSL_CTX* _ctx = SSL_CTX_new( SSLv23_client_method() );
    SSL_CTX_set_options(_ctx,
                SSL_OP_NO_SSLv2 | SSL_OP_NO_SSLv3 | SSL_OP_NO_TLSv1);
#endif

    // 必須. ルートCA証明書を読み込む.
    // Fedora: 'ca-certificates' package.
    int r = SSL_CTX_load_verify_locations(_ctx, CA_BUNDLE_FILE, nullptr);
    if (!r) {
        ERR_print_errors_fp(stderr);
        fprintf(stderr, "loading certificate failed.\n");
        return 1;
    }

    ////////////////////////////////////////////
    // 接続

    addrinfo* res = ngethostbyname(uri->getHost().c_str(), uri->getPort(),
                                   SOCK_STREAM);
    if (!res) {
        fprintf(stderr, "host not found: %s\n", uri->getHost().c_str());
        return 1;
    }

    int error = 0;

    SOCKET _socket = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    if (_socket < 0) {
        fprintf(stderr, "Fail to create a socket.\n");
        freeaddrinfo(res);
        return 1;
    }

    // 必須. See https://http2.github.io/faq/
    // man 7 TCP
    int flag = 1;
    r = setsockopt(_socket, IPPROTO_TCP, TCP_NODELAY, (char*) &flag, sizeof(flag));
    if ( r < 0 ) {
        error = get_error();
        //::shutdown(_socket, SHUT_RDWR);
        //close_socket(_socket);
        return 1;
    }

    if (connect(_socket, res->ai_addr, res->ai_addrlen) < 0 ) {
        fprintf(stderr, "connect() error.\n");
        freeaddrinfo(res);
        return 1;
    }

    // もういらん.
    freeaddrinfo(res);
    res = nullptr;

    TlsSocketStreamBuf conn(_ctx);
/*
    // sslセッションオブジェクトを作成する.
    SSL* _ssl = SSL_new(_ctx);

    // ソケットと関連づける.
    r = SSL_set_fd(_ssl, _socket);
    if (!r) {
        ERR_print_errors_fp(stderr);
        return 1;
    }
*/
    conn.connected(_socket, uri->getHost().c_str() );
    SSL* _ssl = conn.ssl();
    //conn.attach(_socket, _ssl);
    Http2Reader reader;
    reader.attach(&conn);

    //------------------------------------------------------------
    // HTTP2の準備.
    //
    // プロトコルのネゴシエーションにALPNという方法を使います。
    // 具体的にはTLSのClientHelloのALPN拡張領域ににこれから使うプロトコル名を記
    // 述します.
    //
    // protosには文字列ではなくバイナリで、「0x02, 'h','2'」と指定する。
    // 最初の0x02は「h2」の長さを表している.
    //------------------------------------------------------------
    SSL_set_alpn_protos(_ssl, protos, protos_len + 1);

    // ハンドシェイク.
    if (SSL_connect(_ssl) <= 0){
        error = get_error();
        conn.close();
        return 1;
    }

    // 証明書の検証結果を得る. 必須.
    // SSL_get_verify_result() は, SSL_get_peer_certificate() との併用時のみ
    // 使える.
    X509* x509 = SSL_get_peer_certificate(_ssl);
    if ( !x509 ) {
        fprintf(stderr, "certificate missing.\n");
        conn.close();
        return 1;
    }
    //X509_print_fp(stdout, x509);  // DEBUG

    // [BUGS]
    // If no peer certificate was presented, the returned result code is
    // X509_V_OK.
    r = SSL_get_verify_result(_ssl);
    if (r != X509_V_OK) {
        fprintf(stderr, "verification failed.\n");
        conn.close();
        return 1;
    }
    printf("Connected to %s\n", uri->getHost().c_str());

    X509_free(x509); // 忘れやすい. 忘れると memory leak する.

    // 採用された ALPN を確認する.
    const unsigned char* ret_alpn = nullptr;
    unsigned int alpn_len = 0;
    SSL_get0_alpn_selected(_ssl, &ret_alpn, &alpn_len);
    if (!ret_alpn || !alpn_len) {
        printf("Not HTTP/2\n");
        return 1;
    }
    if (alpn_len != protos_len ||
                memcmp(ret_alpn, protos + 1, alpn_len) != 0) {
        error = get_error();
        conn.close();
        printf("ALPN unexpected\n");
        return 1;
    }

    //------------------------------------------------------------
    // これからHTTP/2 通信を開始する合図.
    //
    // 24オクテットのバイナリを送信します
    // PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n
    //------------------------------------------------------------
    string pri = CLIENT_CONNECTION_PREFACE;
    r = conn.sputn(pri.c_str(), pri.length() );
    if (r < 0) {
        error = get_error();
        conn.close();
        return 1;
    }

    //------------------------------------------------------------
    // HTTP/2 通信のフロー
    //
    // まず最初に SETTINGS フレームを必ず交換します.
    // SETTINGS フレームを交換したら、設定を適用したことを伝えるために必ずACKを送ります.
    //
    // Client -> Server  SettingFrame
    // Client <- Server  SettingFrame
    // Client -> Server  ACK
    // Client <- Server  ACK
    //
    // Client -> Server  HEADERS_FRAME (GETなど)
    // Client <- Server  HEADERS_FRAME (ステータスコードなど)
    // Client <- Server  DATA_FRAME (Body)
    //
    // Client -> Server  GOAWAY_FRAME (送信終了)
    //------------------------------------------------------------

    //------------------------------------------------------------
    // Settings フレームの送信.
    // フレームタイプは「0x04」
    // 全てデフォルト値を採用するためpayloadは空です。
    // SettingフレームのストリームIDは0です.
    const char settingframe[FRAME_HDR_LENGTH] = {
        0x00, 0x00, 0x00,
        0x04, // SETTINGS
        0x00, 0x00, 0x00, 0x00, 0x00};
    dump_frame_header('S', settingframe);
    r = conn.sputn(settingframe, FRAME_HDR_LENGTH);
    if ( r < 0 ) {
        error = get_error();
        conn.close();
        return 1;
    }

    //------------------------------------------------------------
    // Settingsフレームの受信.
    //------------------------------------------------------------
    char* frame = reader.read_frame();
    dump_frame_header('R', frame);
    assert(*(frame + 3) == 0x4); // SETTINGS

    //------------------------------------------------------------
    // ACKの送信.
    // ACKはSettingsフレームを受け取った側が送る必要がある.
    // ACKはSettingsフレームのフラグに0x01を立てて payloadを空にしたもの.
    //
    // フレームタイプは「0x04」
    // 5バイト目にフラグ0x01を立てます。
    //------------------------------------------------------------
    dump_frame_header('S', settingframeAck);
    r = conn.sputn(settingframeAck, FRAME_HDR_LENGTH);
    if ( r < 0 ) {
        error = get_error();
        conn.close();
        return 1;
    }

    // サーバーからのACKの受信は下でやります..

    //------------------------------------------------------------
    // HEADERSフレームの送信.
    //
    // フレームタイプは「0x01」
    // このフレームに必要なヘッダがすべて含まれていてこれでストリームを終わらせることを示すために、
    // END_STREAM(0x1)とEND_HEADERS(0x4)を有効にします。
    // 具体的には5バイト目のフラグに「0x05」を立てます。
    // ストリームIDは「0x01」を使います.
    //
    // ●HTTP2でのセマンティクス
    //      :method GET
    //      :path /
    //      :scheme https
    //      :authority nghttp2.org
    //
    map<string, string> m;
    m.insert(make_pair(":method", "GET"));
    m.insert(make_pair(":path", uri->getPath()) );
    m.insert(make_pair(":scheme", "https"));
    m.insert(make_pair(":authority", uri->getHost()));
    r = send_HEADERS_frame(&conn, 1, m);
    if ( r < 0 ) {
        int error = get_error();
        conn.close();
        return 1;
    }

    // ストリームを受信する.
    recv_streams(reader);

    recv_streams(reader);

    //------------------------------------------------------------
    // GOAWAYの送信.
    //
    // これ以上データを送受信しない場合はGOAWAYフレームを送信します.
    // フレームタイプは「0x07」
    // ストリームIDは「0x00」(コネクション全体に適用するため)
    //------------------------------------------------------------
    static const char goawayframe[17] = {
        0x00, 0x00, 0x00,
        0x07, // GOAWAY
        0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00 };
    dump_frame_header('S', goawayframe);
    r = conn.sputn( goawayframe, 17 );
    if (r < 0 ) {
        error = get_error();
        conn.close();
        return 1;
    }

    //------------------------------------------------------------
    // 後始末.
    //------------------------------------------------------------
    conn.close();

    SSL_CTX_free(_ctx);
    ERR_free_strings();

    return 0;
}


// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
