﻿// -*- coding:utf-8-with-signature -*-

// Q's C++ Library
// Copyright (c) 1996-2001 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

#include <vector>
#include <list>
#include <deque>
#include <cassert>
#include <cstdio>
#include <cctype>
#include <stdlib.h>
#include <string.h>
#include <map>
#include <algorithm> // transform()
#include "URI.h"
#include "file_name.h"

using namespace std;

#ifdef _WIN32
  #define STRICT
  #define WIN32_LEAN_AND_MEAN
  #include <winnls.h>
#else
inline const char* CharNext(const char* p)
{
    return p + mblen(p, MB_CUR_MAX);
}
inline char* CharNext(char* p)
{
    return p + mblen(p, MB_CUR_MAX);
}
#endif // _WIN32

#ifdef _MSC_VER
//not #if defined(_WIN32) || defined(_WIN64) because we have strncasecmp in mingw
  #define strncasecmp _strnicmp
  #define strcasecmp _stricmp
#endif


////////////////////////////////////////////////////////////////////////
// class URI

/*
 *   URI-reference = URI / relative-ref
 *   URI           = scheme ":" hier-part [ "?" query ] [ "#" fragment ]
 *
 *   hier-part     = "//" authority path-abempty
 *                 / path-absolute
 *                 / path-rootless  # mailtoはここへ.
 *                 / %empty  # <mailto:?to=addr@an.example> を認める
 *
 *   relative-ref  = relative-part [ "?" query ] [ "#" fragment ]
 *
 *   relative-part = "//" authority path-abempty
 *                 / path-absolute
 *                 / path-noscheme   # hier-part とはここが異なる.
 *                 / %empty  # こちらは認める. See RFC 3986 5.4.1 examples
 *
 *   authority     = [ userinfo "@" ] host [ ":" port ]
 */

URI::URI(): m_port(-1)
{
}

URI::URI(const URI& a): scheme(a.scheme),
                m_userinfo(a.m_userinfo), m_host(a.m_host), m_port(a.m_port),
                path(a.path), query(a.query), fragment(a.fragment)
{
}


URI::URI(const string& a)
{
    URI* u = parse(a.c_str(), nullptr);
    if ( u ) {
        scheme = u->scheme;
        m_userinfo = u->m_userinfo;
        m_host = u->m_host;
        m_port = u->m_port;
        //authority = u->authority;
        path = u->path;
        query = u->query;
        fragment = u->fragment;
        delete u;
    }
}


URI::URI(const char* a)
{
    assert(a);
    URI* u = parse(a, nullptr);
    if ( u ) {
        scheme = u->scheme;
        m_userinfo = u->m_userinfo;
        m_host = u->m_host;
        m_port = u->m_port;
        //authority = u->authority;
        path = u->path;
        query = u->query;
        fragment = u->fragment;
        delete u;
    }
}


URI::~URI()
{
}

URI& URI::operator = (const URI& a)
{
    if (this != &a) {
        scheme = a.scheme;
        m_userinfo = a.m_userinfo;
        m_host = a.m_host;
        m_port = a.m_port;
        //authority = a.authority;
        path = a.path;
        query = a.query;
        fragment = a.fragment;
    }
    return *this;
}

// @return スキーム名. 小文字.
string URI::getScheme() const noexcept
{
    return scheme;
}

string URI::getUserInfo() const noexcept
{
    return m_userinfo;
}


// authority = [ userinfo "@" ] host [ ":" port ]
// @return 'host' 部分. 正規化しない.
string URI::getHost() const noexcept
{
    return m_host;
/*
    if (authority == "")
        return "";

    const char* p = authority.c_str();
    const char* h = strchr(p, '@');
    h = h != nullptr ? h + 1 : p;

    const char* t = strchr(h, ':');
    if (t)
        return string(h, t - h);
    else
        return string(h);
 */
}


static const map<string, int> default_port = {
    {"ftp", 21},
    {"file", -1},  // 逆に, あったらエラー
    {"http", 80},
    {"https", 443},
    {"ws", 80},
    {"wss", 443},
};

// authority = [ userinfo "@" ] host [ ":" port ]
// @return `port` 部を返す. `port` 部がないとき, デフォルトポート番号を返す.
//         デフォルトもないとき -1
int URI::getPort() const noexcept
{
    if (m_port != -1)
        return m_port;

    map<string, int>::const_iterator i = default_port.find(scheme);
    if (i == default_port.end())
        return -1;

    return i->second;
}


string URI::getPath() const noexcept
{
    return path;
}

string URI::getQuery() const noexcept
{
    return query;
}

string URI::getFragment() const noexcept
{
    return fragment;
}


// *thisを基点にしてrefへの絶対URIを返す
// Windows 95でのInternetCombineUrl() APIは使い物にならない
// RFC 2396 C. Examples of Resolving Relative URI References
URI URI::resolve(const URI& ref) const
{
    URI retval(ref);

    if (ref.scheme != "")
        return ref;

    retval.scheme = this->scheme;

    if (ref.getAuthority() != "")
        return retval;
    retval.m_userinfo = m_userinfo; retval.m_host = m_host;
    retval.m_port = m_port;

    if (ref.path != "") {
        FileName p = path == "" ? FileName("/") : FileName(path);
        retval.path = p.resolve(FileName(ref.path)).toString();
        return retval;
    }

    if (ref.query != "") {
        // ref = "?..."のとき，現在の文書ではなく，
        // ディレクトリに'?'を付ける
        retval.path = FileName(path).getPath();
        return retval;
    }

    retval.path = path;
    retval.query = query;

    if (ref.fragment != "") {
        // 現在の文書に"#..."を付ける。
        retval.fragment = ref.fragment;
        return retval;
    }

    // refが空の時，現在の文書を返す. TODO: 例外を投げるか, "" を返すか?
    return *this;
}


// baseを基点にした相対URI文字列を返す
// relative-ref  = relative-part [ "?" query ] [ "#" fragment ]
string URI::getRelative(const URI& base) const
{
    if (base.scheme != scheme)
        return toString();  // 絶対URIで返す

    string retval;

    string my_authority = getAuthority();
    if ( my_authority != "" && base.getAuthority() != my_authority ) {
        retval = "//" + my_authority;
        retval += path;
        if (query != "")
            retval += "?" + query;
        if (fragment != "")
            retval += fragment;  // fragment は先頭の "#" を含む.

        return retval;
    }

    retval = FileName(path).getRelative(FileName(base.path));
    if (query != "")
        retval += "?" + query;
    if (fragment != "")
        retval += fragment;   // fragment は先頭の "#" を含む.

    return retval;
}


bool URI::isDescendantOf(const URI& parent) const
{
    if (scheme != parent.scheme || getAuthority() != parent.getAuthority() )
        return false;
    string check_path = path[0] == '/' ? path : parent.resolve(*this).getPath();

    string p = FileName(parent.path).getPath();
    string c = FileName(check_path).getPath();
    if (p.size() > c.size())
        return false;
    bool f = true;
    for (unsigned i = 0; i < p.size(); i++) {
        if (c[i] != p[i]) {
            f = false;
            break;
        }
    }
    return f;
}


////////////////////////////////////////////////////////////////////////
// parse

// @return 有効なバイト数.
// scheme  = ALPHA *( ALPHA / DIGIT / "+" / "-" / "." )
static int parse_scheme(const char* str_uri)
{
    assert(str_uri);

    const char* p = str_uri;

    while ( (*p >= 'A' && *p <= 'Z') || (*p >= 'a' && *p <= 'z') )
        p++;
    while ( (*p >= 'A' && *p <= 'Z') || (*p >= 'a' && *p <= 'z') ||
            (*p >= '0' && *p <= '9') || *p == '+' || *p == '-' || *p == '.') {
        p++;
    }

    return p - str_uri;
}

#define SUB_DELIMS "!$&'()*+,;="

static bool isUnreservedOrSubDelims(const char* p)
{
    if (!*p)
        return false; // strchr() トラップ.

    if ( (*p >= 'A' && *p <= 'Z') || (*p >= 'a' && *p <= 'z') ||
         (*p >= '0' && *p <= '9') )
        return true;
    return strchr("-._~" SUB_DELIMS, *p) != nullptr;
}


static bool isHexDig(const char* p)
{
    return (*p >= '0' && *p <= '9') || (*p >= 'a' && *p <= 'f') ||
           (*p >= 'A' && *p <= 'F');
}


// authority     = [ userinfo "@" ] host [ ":" port ]
// @return 不正な文字列の場合, -1
static int parse_authority(const char* str_uri,
                           string* userinfo, string* host, int* port )
{
    assert(str_uri);
    assert(userinfo); assert(host); assert(port);

    const char* p = str_uri;

    // userinfo "@"
    // userinfo      = *( unreserved / pct-encoded / sub-delims / ":" )
    do {
        if (isUnreservedOrSubDelims(p)) {
            p++; continue;
        }
        else if (*p == '%' && isHexDig(p + 1) && isHexDig(p + 2)) {
            p += 3; continue;
        }
        else if (*p == ':') {
            p++; continue;
        }
        else
            break;
    } while (true);

    if (*p == '@') {
        userinfo->assign(str_uri, p - str_uri);
        p++;
    }
    else
        p = str_uri; // userinfoがない。巻き戻す.

    // host ////////////////////////////////////////////////////////////
    // host          = IP-literal / IPv4address / reg-name
    // reg-name      = *( unreserved / pct-encoded / sub-delims )
    // 'IPv4address' は, reg-name と区別できない.
    const char* host_begin = p;
    if (*p == '[') {
        // IP-literal    = "[" ( IPv6address / IPvFuture  ) "]"
        p++;
        while (isHexDig(p) || *p == ':' || *p == '.')
            p++;
        if (*p != ']')
            return -1; // error
        p++;
    }
    else {
        // reg-name      = *( unreserved / pct-encoded / sub-delims )
        do {
            if (isUnreservedOrSubDelims(p)) {
                p++; continue;
            }
            else if (*p == '%' && isHexDig(p + 1) && isHexDig(p + 2)) {
                p += 3; continue;
            }
            else
                break;
        } while (true);
    }
    //if (p == host_begin)
    //    return -1;          host も省略できる!
    host->assign(host_begin, p - host_begin);
    // 手抜き. Percent-encoded byte は大文字にしないといけない.
    transform(host->begin(), host->end(), host->begin(), ::tolower);

    // port
    *port = -1;
    if (*p == ':') {
        p++;
        const char* port_begin = p;
        while (*p >= '0' && *p <= '9')
            p++;
        *port = strtol(port_begin, NULL, 10);
    }

    return p - str_uri;
}


static int parse_path(const char* str_uri, string* path)
{
    assert(str_uri);
    assert(path);

    const char* p = str_uri;

    // '/' で終わっていい.
    // pchar  = unreserved / pct-encoded / sub-delims / ":" / "@"
    do {
        if (isUnreservedOrSubDelims(p)) {
            p++; continue;
        }
        else if (*p == '%' && isHexDig(p + 1) && isHexDig(p + 2)) {
            p += 3; continue;
        }
        else if (*p == ':' || *p == '@' || *p == '/' || *p == '|') {
            // '|' は backward compatibility (drive letter). 手抜き.
            p++; continue;
        }
        else
            break;
    } while (true);

    path->assign(str_uri, p - str_uri);
    return p - str_uri;
}


/**
 *   relative-part = "//" authority path-abempty
 *                 / path-absolute
 *                 / path-noscheme   # hier-part とはここが異なる.
 *                 / %empty  # こちらは認める. See RFC 3986 5.4.1 examples
 *
 * @return 読み進めたバイト数. 不正な場合 -1.
 */
static int parse_relative_part(const char* str_uri,
                               string* userinfo, string* host, int* port,
                               //string* authority,
                               string* path)
{
    assert(str_uri);
    assert(userinfo); assert(host); assert(port);
    assert(path);

    int r;
    const char* p = str_uri;

    if (*p == '/' && *(p + 1) == '/') {
        // "//" authority path-abempty
        p += 2;
        r = parse_authority(p, userinfo, host, port);
        if (r < 0) {
            // ホスト名が不正, など.
            return -1;
        }
        //authority->assign(p, r);
        p += r;

        // path-abempty
        if (*p != '/')
            return p - str_uri; // path 部なし.
    }

    // path 部
    r = parse_path(p, path);

    return (p + r) - str_uri;
}


/*   hier-part     = "//" authority path-abempty
 *                 / path-absolute
 *                 / path-rootless
 *                 / %empty    # 妥当: <mailto:?to=addr1@an.example>
 *
 * @return 不正な場合 -1.
 *         1) ホスト名が不正
 */
static int parse_hier_part(const char* str_uri,
                           string* userinfo, string* host, int* port,
                           //string* authority,
                           string* path)
{
    assert(str_uri);
    assert(userinfo); assert(host); assert(port);
    assert(path);

    int r;
    const char* p = str_uri;

    if (*p == '/' && *(p + 1) == '/') {
        // "//" authority path-abempty
        p += 2;
        r = parse_authority(p, userinfo, host, port);
        if (r < 0) {
            // ホスト名が不正, など.
            return -1;
        }
        //authority->assign(p, r);
        p += r;

        // path-abempty  = *( "/" segment )
        if (*p != '/')
            return p - str_uri; // path 部なし
    }
    // 次の場合, authority は空: 例) mailto:foo@example.com

    // path 部
    r = parse_path(p, path);

    return (p + r) - str_uri;
}


// query, fragment 共通
static int parse_query(const char* str_uri)
{
    const char* p = str_uri;
    do {
        if (isUnreservedOrSubDelims(p)) {
            p++; continue;
        }
        else if (*p == '%' && isHexDig(p + 1) && isHexDig(p + 2)) {
            p += 3; continue;
        }
        else if (*p == ':' || *p == '@' || *p == '/' || *p == '?') {
            p++; continue;
        }
        else
            break;
    } while (true);

    return p - str_uri;
}


/**
 * 文字列をparseし, 新しい URI オブジェクトを生成する.
 * RFC 3986
 *   URI-reference = URI / relative-ref
 *
 * @param urllen   URIとして有効なバイト数. NULL 可.
 */
URI* URI::parse(const char* str_uri, int* urllen )
{
    string scheme, userinfo, host;
    int port = -1;
    string path, query, fragment;

    if ( urllen )
        *urllen = 0;
    bool abs_uri = false;

    const char* p = str_uri;

    // scheme
    int scheme_len = parse_scheme(p);
    if (scheme_len > 0 && *(str_uri + scheme_len) == ':') {
        // URI = scheme ":" hier-part [ "?" query ] [ "#" fragment ]
        scheme.assign(p, scheme_len);
        transform(scheme.begin(), scheme.end(), scheme.begin(), ::tolower);
        p += scheme_len + 1;

        int r = parse_hier_part(p, &userinfo, &host, &port, &path);
        if (r < 0)
            return nullptr;
        if (scheme == "file" && port != -1)  // special scheme
            return nullptr;
        p += r;
        abs_uri = true;
        //printf("h%d", r);
    }
    else {
        // relative-ref = relative-part [ "?" query ] [ "#" fragment ]
        // 最初からやり直し.
        int r = parse_relative_part(p, &userinfo, &host, &port, &path);
        if (r < 0)
            return nullptr;
        p += r;
        //printf("r%d", r);
    }

    // query
    if (*p == '?') {
        p++;
        int r = parse_query(p);
        query.assign(p, r);
        p += r;
    }

    // fragment
    if (*p == '#') {
        p++;
        int r = parse_query(p);
        fragment.assign(p - 1, r + 1); // 先頭の "#" を含める
        p += r;
    }

    if (urllen)
        *urllen = p - str_uri;

    URI* uri = new URI();
    uri->scheme = scheme;
    uri->m_userinfo = userinfo;
    uri->m_host = host;
    uri->m_port = port;
    //uri->authority = authority;
    uri->path = path;
    uri->query = query;
    uri->fragment = fragment;

    return uri;
}


string URI::toString() const
{
    string retval;
    if (scheme != "")
        retval = scheme + ":";
    if ( getAuthority() != "")
        retval += "//" + getAuthority();
    retval += path;
    if (query != "")
        retval += "?" + query;
    if (fragment != "")
        retval += fragment;

    return retval;
}


static inline int sign(int x)
{
    return x > 0 ? +1 : (x < 0 ? -1 : 0);
}

static inline unsigned char hex2char(const unsigned char* h)
{
    return (*h >= '0' && *h <= '9' ? *h - '0' :
            (*h >= 'A' && *h <= 'F' ? *h - 'A' + 10 : *h - 'a' + 10)) * 16
        + (h[1] >= '0' && h[1] <= '9' ? h[1] - '0' :
           (h[1] >= 'A' && h[1] <= 'F' ? h[1] - 'A' + 10 : h[1] - 'a' + 10));
}


/**
 * URLによる比較
 * 判断がつかないときはm_sUrlを単に文字列として比較する
 */
int URI::compare(const URI& uri1, const URI& uri2)
    // TODO: rfc2068によれば，次の3つのURIを等しいと判定できないといけない。
    //      http://abc.com:80/~smith/home.html
    //      http://ABC.com/%7Esmith/home.html
    //      http://ABC.com:/%7esmith/home.html
{
    // ホスト -> スキーム -> パス -> クエリー

    deque<string> domain[2];
    string h[2];
    h[0] = uri1.getHost();
    h[1] = uri2.getHost();

    int i;
    for (i = 0; i < 2; i++) {
        const char* p = h[i].c_str();
        while (*p) {
            string a;
            while (*p && *p != '.')
                a += *p++;
            domain[i].push_front(a);
            if (*p == '.')
                p++;
        }
    }

    for (i = 0; i < domain[0].size() && i < domain[1].size(); i++) {
        int r = strcasecmp(domain[0].operator [](i).c_str(),
                        domain[1].operator [](i).c_str());
        if (r)
            return r;
    }

    if (uri1.scheme != uri2.scheme)
        return uri1.scheme.compare(uri2.scheme);

    int r = strcmp(uri1.path.c_str(), uri2.path.c_str());
    if (r)
        return r;

    r = strcmp(uri1.query.c_str(), uri2.query.c_str());
    if (r)
        return r;

    r = strcmp(uri1.fragment.c_str(), uri2.fragment.c_str());
    if (r)
        return r;

    return uri1.toString().compare(uri2.toString());
}

string URI::escape(const char* uri)
    // URIとして使えない文字を%xxに変換する。すでに%xxに変換されている文字には触らない。
    //      変換対象はcontrol, space, delims, unwise, 0x80以上の文字
    //      '#'は変換しない。
    //      　多バイト文字は考慮しない。特にシフトJISだと酷いことになるが，
    //      現時点でURIにUS-ASCII以外指定するな，と。
{
    const char* e = "<>%\"{}|\\^[]`";
    char* dest = new char[strlen(uri) * 3 + 1];
    char* d = dest;
    const unsigned char* p = (unsigned char*) uri;

    while (*p) {
        if (*p < 0x21 || *p > 0x7e || strchr(e, *p)) {
            if (*p == '%' && isxdigit(*(p + 1)) && isxdigit(*(p + 2)))
                *(d++) = *p;
            else
                d += sprintf(d, "%%%02x", *p);
        }
        else
            *(d++) = *p;
        p++;
    }
    *d = 0;

    string retval = dest;
    delete [] dest;
    return retval;
}


/**
 * %xxを復元する。ただし次の文字と0x80以上の文字は復元しない。
 *     reserved = ";" | "/" | "?" | ":" | "@" | "&" | "=" | "+" |
 *                "$" | ","
 *     control  = <US-ASCII coded characters 00-1F and 7F hexadecimal>
 *     space    = <US-ASCII coded character 20 hexadecimal>
 *     delims   = "<" | ">" | "#" | "%" | <">
 *     unwise   = "{" | "}" | "|" | "\" | "^" | "[" | "]" | "`"
 * 解説
 *     http://www.ics.uci.edu/pub/ietf/uri/draft-fielding-uri-syntax-03.txt:
 */
string URI::unescape(const char* escaped_uri)
{
    const char* ununescape = ";/?:@&=+$,<>#%\"{}|\\^[]`";
    string r;
    const char* p = escaped_uri;
    unsigned char c;

    while (*p) {
        if (*p == '%' && isxdigit(*(p + 1)) && isxdigit(*(p + 2))) {
            c = hex2char((unsigned char*) p + 1);
#ifdef _WIN32
            if (c >= 0x20 && c <= 0x7e && !strchr(ununescape, c))
                // fileスキームで空白文字を許容する。
#else
            if (c >= 0x21 && c <= 0x7e && !strchr(ununescape, c))
#endif  // _WIN32
                r += c;
            else
                r.append(p, 3);
            p += 3;
        }
        else
            r += *(p++);
    }

    return r;
}

bool URI::operator == (const URI& a) const
{
    return compare(*this, a) == 0;
}

bool URI::operator != (const URI& a) const
{
    return compare(*this, a) != 0;
}

bool URI::operator < (const URI& a) const
{
    return compare(*this, a) < 0;
}


// URI->filename
string URI::toLocal() const
{
    static const char* unuse = "\"$%*/:<>?\\|";
        // VFATで使用不可な文字，schemeのエスケープ用'$'，通常のエスケープ用'%'

    string retval = "";
    char buf[4];

    retval = scheme + "$\\" + getHost();
    if (getPort() != -1) {
        char dmy[100];
        sprintf(dmy, "$3a%d", getPort());
        retval += dmy;
    }

    const char* p = path == "" ? "/" : path.c_str();
    while (*p) {
#ifdef _WIN32
        if (::IsDBCSLeadByte(*p)) {
            retval.append(p, 2);
            p += 2;
            continue;
        }
#else
        int clen = mblen(p, MB_CUR_MAX);
        if (clen > 1) {
            retval.append(p, clen);
            p += clen;
            continue;
        }
#endif
        else if (*p == '/' || *p == '\\') {
            retval += '\\';
            p++;
            continue;
        }
        else if (((unsigned char) *p) < 0x20 || *p == 0x7f
                    || strchr(unuse, *p) != NULL) {
            sprintf(buf, "%%%02x", *p);
            retval += buf;
            p++;
            continue;
        }
        else
            retval += *(p++);
    }

    if (query != "") {
        retval += "%3f";
        p = query.c_str();
        while (*p) {
#ifdef _WIN32
            if (::IsDBCSLeadByte(*p)) {
                retval.append(p, 2);
                p += 2;
                continue;
            }
#else
            int clen = mblen(p, MB_CUR_MAX);
            if (clen > 1) {
                retval.append(p, clen);
                p += clen;
                continue;
            }
#endif
            else if (((unsigned char) *p) < 0x20 || strchr(unuse, *p) != NULL) {
                sprintf(buf, "%%%02x", *p);
                retval += buf;
                p++;
                continue;
            }
            else
                retval += *(p++);
        }
    }
    if (retval.length() > 0 && retval[retval.length() - 1] == '\\')
        retval += "index.html";

    return retval;
}


// authority = [ userinfo "@" ] host [ ":" port ]
string URI::getAuthority() const noexcept
{
    string ret;

    if (m_userinfo != "")
        ret = m_userinfo + "@";
    ret += m_host;
    if ( m_port != -1 )
        ret += ":" + to_string(m_port);

    return ret;
}

// 過去との互換性のため, "#" で始まらない場合は補う
void URI::setFragment(const char* str)
{
    this->fragment = (!str[0] || str[0] == '#') ? str : string("#") + str;
}


// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
