// Q's C++ Library
// Copyright (c) 1996-1999 Hisashi HORIKAWA. All rights reserved.

#include "stdafx.h"
#include <mfc/QsThread.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

////////////////////////////////////////////////////////////////////////////
// Thread

Thread::Thread(): _pt(NULL), exitflag(false), waitObject(NULL)
{
}

Thread::~Thread()
{
    if (isRunning()) {
        exitflag = true;
        resume();
        ::WaitForSingleObject(_pt->m_hThread, INFINITE);
    }
    delete _pt;
    _pt = NULL;

    if (waitObject) {
        ::CloseHandle(waitObject);
        waitObject = NULL;
    }
}

bool Thread::start(void* arg)
    // この関数は，起動するスレッドで呼ぶ
{
    if (_pt) {
        if (isRunning())
            return false;
        delete _pt;
    }

    _p = arg;
    exitflag = false;
    _pt = AfxBeginThread(_e, this, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
    if (!_pt)
        return 0;
    _pt->m_bAutoDelete = FALSE;
    _pt->ResumeThread();

    return true;
}

void Thread::exit()
{
    if (isRunning()) {
        exitflag = true;
        resume();
    }
}

void Thread::suspend(int time)
    // resume()されるかtimeミリ秒過ぎるまで停止
{
    if (!waitObject)
        waitObject = ::CreateEvent(NULL, FALSE, FALSE, NULL);
    ASSERT(waitObject);
    ::WaitForSingleObject(waitObject, time);
}

void Thread::resume()
    // 自分自身を起こす。
{
    if (waitObject)
        ::PulseEvent(waitObject);
}

/* static */ UINT Thread::_e(void* p)
    // この関数は，新しいスレッドで呼ばれる
{
    Thread* pt = (Thread*) p;
    return pt->run(pt->_p);
}

bool Thread::isRunning() const
{
    if (!_pt || !_pt->m_hThread)
        return 0;

    DWORD dwExitCode;
    ::GetExitCodeThread(_pt->m_hThread, &dwExitCode);
    return dwExitCode == STILL_ACTIVE;
}
