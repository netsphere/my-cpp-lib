// -*- mode:c++ -*-

#ifndef SOCKET_STREAMBUF_H
#define SOCKET_STREAMBUF_H 1

// <fstream> の basic_filebuf<> を真似して, ソケット対応する.

#include <ios>
#include <assert.h>
#include <algorithm> // min()
#include <string.h> // memmove()
#ifdef _WIN32
  #define STRICT
  #define WIN32_LEAN_AND_MEAN
  #include <winsock2.h>
  #include <windows.h>
  #define SHUT_RDWR SD_BOTH
#else
  #include <sys/socket.h> // recv(), send(), shutdown()
  #include <unistd.h> // close()
  typedef int SOCKET;
  typedef unsigned char BYTE;
  #define INVALID_SOCKET -1
#endif


// TCP socket.
// 非SSL (TLS). TLS 版は TlsSocketStreamBuf クラス.
// std::streambuf から派生させるが, メソッドは basic_filebuf に近い.
// -> close(), is_open() の実装.
class SocketStreamBuf: public std::streambuf
{
    // sgetn() は読めた分だけ返すのではなく, 指定のバイト数に達するまで,
    // ブロックする。
    // そのため, どれだけ読むべきか事前に不明な場合は sgetn() は使えず,
    // 1バイトずつ読まなければならない。
    // 実装としては, バッファが (ほぼ) 必須.
    char* m_buf;

#define max_headroom ((ssize_t) 16)
    static constexpr long bufsiz = 4096 + max_headroom;

protected:
    // 所有.
    SOCKET m_sock;

public:
    SocketStreamBuf(): m_buf(nullptr), m_sock(INVALID_SOCKET) { }

    virtual ~SocketStreamBuf() {
        close();
        if (m_buf) {
            free(m_buf);
            m_buf = nullptr;
        }
    }


    // SocketStream::open() から呼び出される.
    virtual void connected( SOCKET sock, const char* host ) {
        assert( sock != INVALID_SOCKET );
        assert( host && *host );
        assert( m_sock == INVALID_SOCKET ); // TODO: 例外を投げる?

        m_sock = sock;
        if (!m_buf)
            m_buf = (char*) malloc(bufsiz);
        setg(m_buf, m_buf, m_buf);
    }

    // std::streambuf<> には close() がない. => オブジェクトを破壊する必要.
    // @return If failed, -1.
    virtual int close() {
        if (m_sock == INVALID_SOCKET )
            return 0;

        // なくてもいいはずだが、OSなどの不具合などによりデータ消失があるた
        // め (?), 入れた方がいいという意見もある.
        // See TCPのはなし
        //     http://www.silex.jp/blog/wireless/2015/11/tcp.html
        // See close vs shutdown socket?
        //     https://code.i-harness.com/en/q/3f7b5b
        ::shutdown(m_sock, SHUT_RDWR);

        int r;
#ifdef _WIN32
        r = ::closesocket(m_sock);
#else
        r = ::close(m_sock);
#endif
        m_sock = INVALID_SOCKET;
        return r;
    }


    bool is_open() const throw() {
        return m_sock != INVALID_SOCKET;
    }


protected:
    // バッファを無視して, 読み込む.
    // length に満たない場合がある.
    // @return エラーの場合, -1
    virtual ssize_t sysread(void* buffer, size_t length) {
        ssize_t ret;
        do {
            ret = ::recv(m_sock, (char*) buffer, length, 0);
        } while (ret == -1 && errno == EINTR);
        return ret;
    }

    // @override
    // 読み込み用で最低限 override しなければならないのは, underflow() だけ.
    // @return 読み込んだ 1文字. EOF だった場合, traits_type::eof().
    virtual int_type underflow() {
        // eback() 先頭, gptr() 現在の位置, egptr() 終端. 名前が??
        if ( gptr() && gptr() < egptr() )
            return *gptr();  // return buffered.

        // バッファを読み切った.
        if ( !is_open() )
            return traits_type::eof();

        // sungetc() できるように, 多少, バッファを先頭に詰める.
        long rest = !gptr() ? 0 : std::min(egptr() - eback(), max_headroom);
        if (rest > 0)
            memmove(m_buf, gptr() - rest, rest);
        ssize_t r = this->sysread(m_buf + rest, bufsiz - rest);
        if (r == 0) // EOF
            return traits_type::eof();
        else if (r < 0) {
            throw std::runtime_error(std::string("recv() error: errno=") +
                                     std::to_string(errno) );
        }
        // set input pointers: new_eback, new_gptr, new_egptr
        setg(m_buf, m_buf + rest, m_buf + rest + r);

        return *gptr();  // ポインタ進めない.
    }


    // @override
    // 同様に, 書き込み側については, 最低限 overflow() を override すればよい.
    // 1バイトを書き込む.
    // @param __c 書き込む値.
    // @return 成功したら EOF 以外の何か. 失敗したら EOF.
    //         ここの解説では, __c が EOF の場合, 失敗 (EOF) を返す.
    //         http://www.cplusplus.com/reference/streambuf/basic_streambuf/overflow/
    //         basic_filebuf<>::overflow() の実装では, __c が EOF の場合も
    //         「成功」を返している.
    virtual int_type overflow(int_type __c = traits_type::eof() )
    {
        if ( traits_type::eq_int_type(__c, traits_type::eof()) )
            return __c; // error とする.
        char cc = __c;
        if ( xsputn(&cc, 1) <= 0 )
            return traits_type::eof();

        return 1; // 成功
    }

    // 効率悪いので, xsputn() も override.
    // @return 書き込めたバイト数.
    //         basic_streambuf<>::xsputn() の実装を見ると, overflow() が EOF
    //         を返すと, そこまでのバイト数を返す.
    //         エラーで1バイトも書き込めなかったときも, 0 を返す. マジか!!
    virtual std::streamsize xsputn( const char_type* s,
                                    std::streamsize count )
    {
        if ( !is_open() )
            return 0;
        ssize_t r = ::send(m_sock, s, count, 0 );
        if (r >= 0)
            return r;
        else
            throw errno;
    }
};

#endif // !SOCKET_STREAMBUF_H



// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
