﻿// -*- coding:utf-8-with-signature; mode:c++; tab-width:4; -*-


#ifndef PTR_LIST_H__
#define PTR_LIST_H__ 1

#include <list>
#ifndef NDEBUG
  #include <stdio.h>
#endif
#if __cplusplus >= 201103L
  #include <initializer_list>
#endif


/**
 * 使い方:
 *     ptr_list<Foo*> var;
 *     var.push_back(new Foo);
 * 実装ノート: 派生にするのは誤り. 値に対する操作が list<> と異なる.
 */
template <typename T>
class ptr_list
{
    static_assert(std::is_pointer<T>::value,
                  "template parameter T must be pointer type");

#if __cplusplus >= 201703L
    typedef std::list<T, std::pmr::polymorphic_allocator<T> > Container;
#else
    typedef std::list<T > Container;
#endif
    Container container;

public:
    typedef typename Container::iterator        iterator;
    typedef typename Container::const_iterator  const_iterator;
    typedef typename Container::size_type       size_type;
    typedef typename Container::reference       reference;
    typedef typename Container::const_reference const_reference;
    typedef typename Container::allocator_type  allocator_type;

    // コンストラクタ
#if __cplusplus >= 201103L
    ptr_list() = default;
#else
    ptr_list() { }
#endif

    explicit ptr_list(const allocator_type& __a) : container(__a) { }

#if __cplusplus >= 201103L
    ptr_list(std::initializer_list<T> __l,
             const allocator_type& __a = allocator_type() ) : container(__a) {
        for (const T& v: __l)
            push_back(v);
    }
#endif

    virtual ~ptr_list() { clear(); }

    iterator begin() noexcept { return container.begin(); }
    const_iterator begin() const noexcept { return container.begin(); }

    iterator end() noexcept { return container.end(); }
    const_iterator end() const noexcept { return container.end(); }

#if __cplusplus >= 201103L
    const_iterator cbegin() const noexcept { return container.cbegin(); }
    const_iterator cend() const noexcept { return container.cend(); }
#endif

    size_type size() const noexcept {
        return container.size();
    }

    reference front() {
        assert( size() > 0 );
        return *container.begin();
    }

    const_reference front() const {
        assert( size() > 0 );
        return *container.begin();
    }

    reference back() {
        assert( size() > 0 );
        return container.back();
    }

    const_reference back() const {
        assert( size() > 0 );
        return container.back();
    }


    void push_front(const T& v)
    {
#ifndef NDEBUG
        if ( !v )
            printf("push_front() warning: add NULL.\n");
#endif
        container.push_front(v);
    }

    // 要素を delete する
    void pop_front() {
        erase(begin());
    }

    void push_back(const T& v)
    {
#ifndef NDEBUG
        if ( !v )
            printf("warning: add NULL.\n");
#endif // !NDEBUG
        container.push_back(v);
    }


    iterator insert( const_iterator it, const T& v ) {
        return container.insert(it, v);
    }

    // 解放する
    iterator
#if __cplusplus >= 201103L
    erase(const_iterator it) noexcept
#else
    erase(iterator it)
#endif
    {
        delete (*it);
        return container.erase(it);
    }


    iterator
#if __cplusplus >= 201103L
    erase(const_iterator __first, const_iterator __last) noexcept
#else
    erase(iterator __first, iterator __last)
#endif
    {
        while (__first != __last)
            __first = erase(__first);
        return __last;
    }

    void clear() {
        iterator it;
        for (it = begin(); it != end(); it++)
            delete *it;
        container.clear();
    }

    /** deleteせずにエントリを削除 */
    T detach_front() {
        T r = front();
        container.erase(begin());
        return r;
    }
};



#endif


// Local variables:
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: nil
// End:
